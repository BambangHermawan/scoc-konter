<?php $this->load->view('_layouts/_template/header') ?>

<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div class="card card-outline card-primary">
				<div class="card-header">
					<h3 class="card-title">Outlet <b><?php echo $outlet[0]->name ?></b></h3>
					<div class="float-right">
						<button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
								data-target="#modal_rekapan_uang_laci">
							<i class="fas fa-plus"></i> Tambah
						</button>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-2">
							<div class="form-group">
								<label for="month">Bulan</label>
								<select class="form-control form-control-sm" id="month">
									<option value="01">Januari</option>
									<option value="02">Februari</option>
									<option value="03">Maret</option>
									<option value="04">April</option>
									<option value="05">Mei</option>
									<option value="06">Juni</option>
									<option value="07">Juli</option>
									<option value="08">Agustus</option>
									<option value="09">September</option>
									<option value="10">Oktober</option>
									<option value="11">November</option>
									<option value="12">Desember</option>
								</select>
							</div>
						</div>

						<div class="col-lg-1">
							<div class="form-group">
								<label for="month">Tahun</label>
								<input type="number" class="form-control form-control-sm" id="year">
							</div>
						</div>

						<div class="col-lg-5">
							<label>Pilih Shift</label>
							<div class="form-group">
								<button class="btn btn-success btn-sm btn_search" value="pagi">Pagi</button>
								<button class="btn btn-warning btn-sm btn_search" value="sore">Sore</button>
								<button class="btn btn-info btn-sm btn_search" value="sehari">Sehari</button>
							</div>
						</div>
					</div>

					<div class="table-responsive">
						<table class="table table-bordered table-hover" id="table_rekap_total_uang">
							<thead>
							<tr>
								<th>Tanggal</th>
								<th>Total Uang di Laci</th>
								<th>Total Uang yang Seharusnya</th>
							</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('rekapan/modal/rekapan_pusat'); ?>

<script>

	//init tabel rekap total uang
	$(document).ready(function () {
		get_total_uang_di_laci()

		$("#table_rekap_total_uang").DataTable({
			"searching": false,
			"paging": false
		})
		get_time_now()
	})

	//filter tabel rekap total uang
	$(".btn_search").click(function () {
		month = $('#month').val()
		year = $('#year').val()
		shift = $(this).val()

		table_rekap_total_uang = $("#table_rekap_total_uang").DataTable({
			"searching": false,
			"paging": false,
			"destroy": true,
			width: "0.8%",
			"ajax": "<?= base_url(); ?>rekapan/get_rekap_total_uang/<?=$this->uri->segment(3);?>/" + month + "/" + year + "/" + shift,
			"columns": [
				{
					data: "tanggal"
				},
				{
					data: "total"
				},
				{
					data: "total_seharusnya"
				}
			]
		})
	})

	//validasi edit total uang di laci
	$("#rekapan_uang_laci_form").validate({
		rules: {
			total_uang_di_laci: {
				required: true,
				digits: true,
				maxlength: 100
			},
			tanggal_uang_di_laci: {
				required: true
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//simpan data total uang di laci
	$("#btn_total_uang_di_laci_submit").click(function () {

		if ($('#rekapan_uang_laci_form').valid()) {
			$.ajax({
				url: "<?= '/';?>rekapan/edit_total_uang_di_laci",
				type: "post",
				data: {
					outlet_id: "<?=$this->uri->segment(3);?>",
					total: $("#total_uang_di_laci").val(),
					tanggal: $("#tanggal_uang_di_laci").val(),
					shift: $("[name='shift_uang_di_laci']:checked").val()
				},
				success(result) {

					if (result.success) {
						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

					} else {
						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});
					}
				}
			})
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});
		}
	})

	//tanggal jam ini
	function get_time_now() {
		var today = new Date();
		$('#year').val(today.getFullYear())

		month_now = ('0' + (today.getMonth() + 1)).slice(-2)
		$('#month option[' + month_now + ']').attr('selected')
	}

	//tanggal tanggal ini
	function get_date() {
		var today = new Date();
		document.getElementById("tanggal_uang_di_laci").value = today.getFullYear() + '-' + ('0' + (today.getMonth() +
				1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2);
	}

	//tanggal waktu shift
	function get_shift() {
		var today = new Date();
		now = ('0' + (today.getHours())).slice(-2) + '-' + ('0' + (today.getMinutes())).slice(-2) + '-' + ('0' + today
				.getSeconds()).slice(-2);

		if (now >= "05-00-00" && now < "16-00-00") $("#shift_pagi").attr('checked', true)
		else $("#shift_sore").attr('checked', true)
	}

	//init tabel rekap total uang
	function get_total_uang_di_laci() {
		get_date()
		get_shift()
		$.ajax({
			url: "<?= base_url(); ?>rekapan/get_total_uang_di_laci",
			type: "post",
			data: {
				outlet_id: "<?=$this->uri->segment(3);?>",
				tanggal: $("#tanggal_uang_di_laci").val(),
				shift: $("[name='shift_uang_di_laci']:checked").val()
			},
			success(result) {
				$("#total_uang_di_laci").val(result['total'])
			}
		})
	}
</script>

<?php $this->load->view('_layouts/_template/js') ?>
