<?php $this->load->view('_layouts/_template/header') ?>

<div class="container-fluid">

	<div class="row">
		<div class="col-12">
			<div class="card card-primary">
				<div class="card-header">
					<h3 class="card-title"><b><?php echo $outlet ?></b></h3>
					<div class="float-right">
						<a href="<?= base_url(); ?>_gudang/outlet/bulk_transfer_outlet/<?= $this->uri->segment(4) ?>"
						   class="btn btn-info btn-sm"><i
									class="fas fa-exchange-alt"></i>Transfer Antar Outlet</a>
					</div>
				</div>
				<?php $this->load->view('gudang/modal/gudang_outlet'); ?>
				<div class="card-body">
					<ul class="nav nav-tabs" id="outlet_header_tab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="semua-tab" data-toggle="pill"
							   href="#semua" role="tab" aria-controls="semua"
							   aria-selected="true">Semua</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="aksesoris-tab" data-toggle="pill"
							   href="#aksesoris" role="tab" aria-controls="aksesoris"
							   aria-selected="false">Aksesoris</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="perdana-tab" data-toggle="pill"
							   href="#perdana" role="tab"
							   aria-controls="perdana" aria-selected="false">Perdana</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="voucher-tab" data-toggle="pill"
							   href="#voucher" role="tab"
							   aria-controls="voucher" aria-selected="false">Voucher</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="pulsa-tab" data-toggle="pill"
							   href="#pulsa" role="tab"
							   aria-controls="pulsa" aria-selected="false">Pulsa</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="peringatan-tab" data-toggle="pill"
							   href="#peringatan" role="tab"
							   aria-controls="peringatan" aria-selected="false">Peringatan</a>
						</li>
					</ul>
				</div>

				<div class="card-body">
					<div class="tab-content" id="outlet_tab">
						<div class="tab-pane fade show active" id="semua" role="tabpanel"
							 aria-labelledby="semua-tab">
							<?php $this->load->view('gudang/outlet/semua') ?>
						</div>
						<div class="tab-pane fade" id="aksesoris" role="tabpanel"
							 aria-labelledby="aksesoris-tab">
							<?php $this->load->view('gudang/outlet/aksesoris') ?>
						</div>
						<div class="tab-pane fade" id="perdana" role="tabpanel"
							 aria-labelledby="perdana-tab">
							<?php $this->load->view('gudang/outlet/perdana') ?>
						</div>
						<div class="tab-pane fade" id="voucher" role="tabpanel"
							 aria-labelledby="voucher-tab">
							<?php $this->load->view('gudang/outlet/voucher') ?>
						</div>
						<div class="tab-pane fade" id="pulsa" role="tabpanel"
							 aria-labelledby="pulsa-tab">
							<?php $this->load->view('gudang/outlet/pulsa') ?>
						</div>
						<div class="tab-pane fade" id="peringatan" role="tabpanel"
							 aria-labelledby="peringatan-tab">
							<?php $this->load->view('gudang/outlet/peringatan') ?>
						</div>
					</div>
				</div>
				<!-- /.card -->
			</div>
		</div>
	</div>
	<!-- /.row -->
</div>

<?php $this->load->view('_layouts/_template/js') ?>

<script>

	user_id = "<?=$_SESSION['id'];?>"

	//init halaman index gudang
	$(document).ready(function () {
		get_outlet()
	})

	//validasi form impor data bluk
	$("#modal_outlet_import_product_bluk").validate({
		rules: {
			input_outlet_import_tujuan_bluk: {
				required: true
			},
			input_outlet_import_sheet_bluk: {
				required: true,
				maxlength: 100
			},
			input_outlet_import_file_bluk: {
				required: true
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//button modal impor data bluk
	$("#btn_add_import_outlet_bluk").click(function () {
		$("#modal_outlet_import_product_bluk").modal({backdrop: 'static', keyboard: false})
	})

	//validasi form impor data bluk
	$("#modal_outlet_export_product_bluk").validate({
		rules: {
			tujuan_export: {
				required: true
			},
			format_excel_export: {
				required: true,
				maxlength: 100
			},
			input_outlet_export_sheet_bluk: {
				required: true
			},
			input_export_berkas: {
				required: true
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//button modal export data bluk
	$("#btn_add_export_outlet_bluk").click(function () {
		$("#modal_outlet_export_product_bluk").modal({backdrop: 'static', keyboard: false})
	})

	//validasi form tambah modal bluk
	$("#modal_outlet_add_product_bluk_form").validate({
		rules: {
			input_outlet_product_category_bluk: {
				required: true,
				maxlength: 11
			},
			input_outlet_product_name_bluk: {
				required: true,
				maxlength: 100
			},
			input_outlet_product_modal_bluk: {
				required: true,
				digits: true,
				maxlength: 11
			},
			input_outlet_product_cost_bluk: {
				required: true,
				digits: true,
				maxlength: 7
			},
			input_outlet_product_unit_bluk: {
				required: true,
				maxlength: 100
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//button modal tambah produk bluk
	$("#btn_add_product_outlet_bluk").click(function () {
		$("#modal_outlet_add_product_bluk").modal({backdrop: 'static', keyboard: false})
	})

	//simpan modal tambah produk bluk
	$("#btn_outlet_add_product_bluk_submit").click(function () {

		type = $("#input_outlet_product_category_bluk").val();

		if ($('#modal_outlet_add_product_bluk_form').valid()) {

			$.ajax({
				url: "<?= base_url(); ?>_gudang/bluk/add_product_bluk",
				type: "POST",
				data: {
					outlet_id: "<?= $this->uri->segment(4); ?>",
					category_id: $('#input_outlet_product_category_bluk').val(),
					name: $('#input_outlet_product_name_bluk').val(),
					modal: $('#input_outlet_product_modal_bluk').val(),
					cost: $('#input_outlet_product_cost_bluk').val(),
					unit: $('#input_outlet_product_unit_bluk').val()
				},
				success: function (result) {

					if (result.success) {
						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						init_table_outlet_bluk()

					} else {
						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});
					}
				}
			})
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});
		}
	});

	//validasi form transfer barang
	$("#transfer_barang_outlet_form").validate({
		rules: {
			input_transfer_outlet_outlet_id: {
				required: true,
				maxlength: 11
			},
			input_transfer_outlet_many: {
				required: true,
				maxlength: 3
			},
			input_transfer_outlet_note: {
				maxlength: 50
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//transfer dari outlet ke outlet
	$("#btn_transfer_outlet_submit").click(function () {

		type = $("#input_transfer_outlet_type_id").val();

		if ($('#transfer_barang_outlet_form').valid()) {

			$.ajax({
				url: "<?= base_url(); ?>_gudang/outlet/transfer_from_outlet",
				type: "POST",
				data: {
					product_id: $('#input_transfer_outlet_product_id').val(),
					from_outlet_id: "<?= $this->uri->segment(4); ?>",
					to_outlet_id: $('#input_transfer_outlet_outlet_id').val(),
					many: $('#input_transfer_outlet_many').val(),
					note: $('#input_transfer_outlet_note').val()
				},
				success: function (result) {

					if (result.success) {
						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						if (type === 'Aksesoris') {
							init_table_outlet_aksesoris()
						} else if (type === 'Perdana') {
							init_table_outlet_perdana()
						} else if (type === 'Voucher') {
							init_table_outlet_voucher()
						} else if (type === 'Pulsa') {
							init_table_outlet_bluk()
						}

						init_table_outlet_semua()

					} else {
						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});
					}
				}
			})
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});
		}
	});

	//validasi form ubah harga barang
	$("#cost_barang_outlet_form").validate({
		rules: {
			input_cost_outlet_product_name: {
				required: true,
				maxlength: 11
			},
			input_cost_outlet_cost: {
				required: true,
				maxlength: 5
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//simpan pengaturan harga barang outlet
	$("#btn_cost_outlet_submit").click(function () {

		type = $("#input_cost_outlet_type_id").val();

		if ($('#cost_barang_outlet_form').valid()) {

			$.ajax({
				url: "<?= base_url(); ?>_gudang/outlet/set_product_cost",
				type: "POST",
				data: {
					outlet_id: "<?= $this->uri->segment(4); ?>",
					product_id: $('#input_cost_outlet_product_id').val(),
					cost: $('#input_cost_outlet_cost').val()
				},
				success: function (result) {

					if (result.success) {
						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						if (type === 'Aksesoris') {
							init_table_outlet_aksesoris()
						} else if (type === 'Perdana') {
							init_table_outlet_perdana()
						} else if (type === 'Voucher') {
							init_table_outlet_voucher()
						} else if (type === 'Pulsa') {
							init_table_outlet_bluk()
						}

						init_table_outlet_semua()

					} else {
						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});
					}
				}
			})
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});
		}
	});

	//validasi form ubah diskon barang
	$("#discount_barang_outlet_form").validate({
		rules: {
			input_outlet_discount_product_name: {
				required: true,
				maxlength: 11
			},
			input_outlet_discount_cost: {
				required: true,
				digits: true,
				maxlength: 10
			},
			input_outlet_discount_many: {
				required: true,
				digits: true,
				maxlength: 2
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//pengaturan diskon barang outlet
	$("#btn_discount_outlet_submit").click(function () {

		type = $("#input_outlet_discount_type_id").val();

		if ($('#cost_barang_outlet_form').valid()) {

			$.ajax({
				url: "<?= base_url(); ?>_gudang/outlet/set_product_discount",
				type: "POST",
				data: {
					outlet_id: "<?= $this->uri->segment(4); ?>",
					product_id: $('#input_discount_outlet_product_id').val(),
					discount: $('#input_outlet_discount_many').val(),
					discount_until: $('#input_outlet_discount_until').val()
				},
				success: function (result) {

					if (result.success) {
						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						if (type === 'Aksesoris') {
							init_table_outlet_aksesoris()
						} else if (type === 'Perdana') {
							init_table_outlet_perdana()
						} else if (type === 'Voucher') {
							init_table_outlet_voucher()
						} else if (type === 'Pulsa') {
							init_table_outlet_bluk()
						}

						init_table_outlet_semua()

					} else {
						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});
					}
				}
			})
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});
		}
	});

	//dropdown pilih outlet
	function get_outlet() {
		$.ajax({
			url: "<?= base_url(); ?>outlet/get_all_outlet",
			success: function (result) {

				if (result.success) {

					var outlet_list = '<option value="">Pilih...</option>'
					result.data.forEach(function (outlet) {

						if ("<?= $this->uri->segment(4); ?>" !== outlet['id']) {
							outlet_list += "<option value='" + outlet['id'] + "'>" + outlet['name'] + "</option>";
						}

					})

					$('#input_transfer_outlet_outlet_id').html(outlet_list)

				}

			}
		})
	}

	//dropdown unit produk
	$("#input_product_unit").change(function () {
		if ($(this).val() == 'Unit') {
			$("#input_product_many").removeAttr('disabled')
		} else {
			$("#input_product_many").val('')
			$("#input_product_many").attr('disabled', true)
		}
	})

	//reset kolom setiap modal ditutup
	$('[data-dismiss=modal]').on('click', function (e) {

		$('.form-control').removeClass("is-valid");
		$('.form-control').removeClass("is-invalid");

		var $t = $(this),
				target = $t[0].href || $t.data("target") || $t.parents('.modal') || [];

		$(target)
				.find("input,textarea,select")
				.val('')
				.end()
				.find("input[type=checkbox], input[type=radio]")
				.prop("checked", "")
				.end();
	})

</script>
