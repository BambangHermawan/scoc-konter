<div class="table-responsive">
	<table id="table_aksesoris" class="table table-striped table-hover dt-responsive display nowrap" style="width:100%">
		<thead>
		<tr>
			<th data-orderable="false">No</th>
			<th>Kategori</th>
			<th>Nama</th>
			<th>Modal</th>
			<th>Banyak</th>
			<th>Tanggal Masuk</th>
			<th>Qrcode</th>
			<th data-orderable="false">Aksi</th>
		</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>

<script>
	$(document).ready(function () {
		init_table_aksesoris()
	})

	//button modal transfer aksesoris
	$("#table_aksesoris").on('click', 'tr #btn_transfer_click', function () {

		tr = $(this).closest('tr')
		data = table_aksesoris.row(tr).data()

		$("#input_transfer_product_id").val(data.id)
		$("#input_transfer_product_name").val(data.product_name)
		$("#input_transfer_product_stock").val(data.many)
		$("#input_transfer_type_id").val('Aksesoris')

		$('#modal_transfer').modal({backdrop: 'static', keyboard: false})
	})

	//button modal tambah stok aksesoris
	$("#table_aksesoris").on('click', 'tr #btn_add_stock_click', function () {

		tr = $(this).closest('tr')
		data = table_aksesoris.row(tr).data()

		$("#input_add_stock_name").val(data.product_name)
		$("#input_add_stock_many").val(data.many.replace(',', ''))
		$("#input_add_stock_id").val(data.id)
		$("#input_add_stock_type_id").val('Aksesoris')

		$('#modal_stock').modal({backdrop: 'static', keyboard: false})
	})

	//button modal ubah data aksesoris
	$("#table_aksesoris").on('click', 'tr #btn_edit_click', function () {

		tr = $(this).closest('tr')
		data = table_aksesoris.row(tr).data()

		$("#input_edit_name").val(data.product_name)
		$("#input_edit_many").val(data.many.replace(',', ''))
		$("#input_edit_modal").val(data.modal.replace(',', ''))
		$("#input_edit_id").val(data.id)
		$("#input_edit_type_id").val('Aksesoris')

		$('#modal_edit').modal({backdrop: 'static', keyboard: false})
	})

	//hapus data produk aksesoris
	$("#table_aksesoris").on('click', 'tr #btn_delete_click', function () {
		tr = $(this).closest('tr')
		data = table_aksesoris.row(tr).data()
		id = data.id

		Swal.fire({
			title: 'Peringatan!',
			text: 'Apakah Anda yakin akan menghapus produk ' + data.product_name + ' ?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya, hapus data ini!'
		}).then((result) => {
			if (result.value) {
				$.ajax({
					url: "<?= base_url(); ?>produk/delete_product",
					type: "POST",
					data: {id: id},
					success(result) {

						if (result.success) {
							Swal.fire({
								title: 'Berhasil',
								text: result.message,
								icon: result.type,
								showConfirmButton: false,
								timer: 1500
							});

							init_table_aksesoris()

						} else {
							Swal.fire({
								title: 'gagal',
								text: result.message,
								icon: result.type,
								showConfirmButton: false,
								timer: 1500
							});

							init_table_aksesoris()
						}
					}
				})
			}
		})
	})

	//modal preview qrcode
	$("#table_aksesoris").on('click', 'tr #qrcode_image', function () {
		$('#qrcode_preview').attr('src', $(this).attr('src'));
		$('#modal_qrcode').modal({backdrop: 'static', keyboard: false});
	});

	//init datatables table aksesoris
	function init_table_aksesoris() {
		button = "<button class='btn btn-info btn-sm' id='btn_transfer_click'>\
            <i class='fas fa-exchange-alt'></i>&nbsp;Transfer</button>"
		button += "&nbsp;<button class='btn btn-success btn-sm' id='btn_add_stock_click'>\
            <i class='fas fa-plus'></i>&nbsp;Stok</button>"
		button += "&nbsp;<button class='btn btn-warning btn-sm' id='btn_edit_click'>\
            <i class='fas fa-edit'></i>&nbsp;Ubah</button>"
		button += "&nbsp;<button class='btn btn-danger btn-sm' id='btn_delete_click'>\
            <i class='fas fa-trash'></i>&nbsp;Hapus</button>"

		table_aksesoris = $("#table_aksesoris").DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			autoWidth: true,
			ajax: "<?= base_url(); ?>produk/get_all_product/1",
			columns: [
				{
					data: "id",
					width: "0.8%",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: "category_name"
				},
				{
					data: "product_name"
				},
				{
					data: "modal"
				},
				{
					data: "many"
				},
				{
					data: "entry_date"
				},
				{
					data: "qrcode",
					targets: 6,
					render: function (url, type, full) {
						return '<img onerror="this.onerror=null; this.remove();" class="center image_' + full['id'] + '"id="qrcode_image" height="100px" width="100px" src="' + "<?= base_url(); ?>uploads/qrcode/" + full['qrcode'] + '" />';
					}
				},
				{
					defaultContent: button
				},
			]
		})
	}
</script>
