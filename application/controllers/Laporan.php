<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_laporan', '', true);
		$this->load->model('M_pengguna', '', true);
		$this->load->model('M_outlet', '', true);

		if (!isset($_SESSION)) {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'ADMIN') {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'OWNER') {
			redirect('auth/login');
		} elseif ($_SESSION['status_user'] == 'KASIR') {
			redirect('auth/login');
		}
	}

	function index()
	{
		$data['title'] = 'Laporan Transaksi';
		$data['site_navigation'] = 'Laporan';

		return $this->load->view('laporan/index', $data);
	}

	function outlet($outlet_id)
	{
		$data['title'] = 'Laporan Transaksi Outlet';
		$data['site_navigation'] = 'Laporan Outlet';

		$data['outlet'] = $this->M_outlet->get_outlet_by_id($outlet_id);

		$this->load->view('laporan/outlet/index', $data);
	}

	function get_transaction_outlet($user_id, $outlet_id, $start_date, $end_date)
	{
		$filter = $this->generate_date_filter($start_date, $end_date);

		$transaction = $this->get_transaction($user_id, $outlet_id, $filter);

		$this->print_to_pdf($user_id, $outlet_id, $filter, $transaction);
	}

	function get_total_all_by_outlet_id($user_id, $outlet_id, $start_date, $end_date)
	{
		$data = $this->M_laporan->get_total_all_by_outlet_id($user_id, $outlet_id, $start_date, $end_date);

		if ($data->advantage != null) {

			$result['lk'] = $data->lk;
			$result['omset'] = $data->omset;
			$result['advantage'] = $data->advantage;
			$result['asset'] = $data->asset;

			if (count($result) > 0) {
				$response = [
					'success' => true,
					'data' => $result,
					'total' => count($result)
				];

				return to_json($response);
			}
		}

		$result['lk'] = 0;
		$result['omset'] = 0;
		$result['advantage'] = 0;
		$result['rugi'] = 0;
		$result['asset'] = 0;
		$result['persentase'] = 0;

		$response = [
			'success' => false,
			'data' => $result,
			'total' => 0
		];

		return to_json($response);
	}

	function get_transaction($user_id, $outlet_id, $date)
	{
		$data['aksesoris'] = $this->M_laporan->get_laporan_transaksi_fisik($user_id, 'Aksesoris', $outlet_id, $date);
		$data['perdana'] = $this->M_laporan->get_laporan_transaksi_fisik($user_id, 'Perdana', $outlet_id, $date);
		$data['voucher'] = $this->M_laporan->get_laporan_transaksi_fisik($user_id, 'Voucher', $outlet_id, $date);

		return $data;
	}

	function print_to_pdf($user_id, $outlet_id, $date, $transaction)
	{
		$outlet = '';
		if ($outlet_id != 0) {
			$item = $this->M_outlet->get_outlet_by_id($outlet_id);
			$outlet = $item[0]->name;
		}

		$shift = '';
		if ($user_id != 0) {
			$shift = $this->M_pengguna->get_pengguna_by_id($user_id);
			$shift = $shift->name;
		}

		$date_start = date('d-m-Y', strtotime($date['start']));
		$date_end = date('d-m-Y', strtotime($date['end']));

		if (empty($outlet)) {
			$outlet = 'Semua Outlet';
		} else {
			$outlet = 'Outlet ' . $outlet;
		}

		$this->load->library('Pdf');

		$pdf = new FPDF('p', 'mm', 'A4');

		$pdf->AddPage();
		$pdf->SetFont('Arial', 'B', 16);
		$pdf->Cell(190, 7, "LAPORAN TRANSAKSI {$shift}", 0, 1, 'C');
		$pdf->SetFont('Arial', 'B', 12);
		$pdf->Cell(190, 7, "{$outlet} Tanggal {$date_start} sampai {$date_end}", 0, 1, 'C');

		#AKSESORIS
		$pdf->Cell(190, 5, '', 0, 1);
		$pdf->SetFont('Arial', 'B', 12);
		$pdf->Cell(190, 10, 'Aksesoris', 0, 1);
		$pdf->SetFont('Arial', 'B', 7);
		$pdf->Cell(30, 6, 'Kategori', 1, 0);
		$pdf->Cell(30, 6, 'Nama Barang', 1, 0);
		$pdf->Cell(25, 6, 'Harga', 1, 0);
		$pdf->Cell(25, 6, 'Stok', 1, 0);
		$pdf->Cell(25, 6, '+/-', 1, 0);
		$pdf->Cell(25, 6, 'Laku', 1, 0);
		$pdf->Cell(25, 6, 'Sisa', 1, 1);
		$pdf->SetFont('Arial', '', 7);

		foreach ($transaction['aksesoris'] as $data) {
			$pdf->Cell(30, 6, $data->category, 1, 0);
			$pdf->Cell(30, 6, $data->product, 1, 0);
			$pdf->Cell(25, 6, $data->cost, 1, 0);
			$pdf->Cell(25, 6, $data->awl, 1, 0);
			$pdf->Cell(25, 6, $data->plus, 1, 0);
			$pdf->Cell(25, 6, $data->lk, 1, 0);
			$pdf->Cell(25, 6, $data->ak, 1, 1);
		}

		#PERDANA
		$pdf->Cell(190, 5, '', 0, 1);
		$pdf->SetFont('Arial', 'B', 12);
		$pdf->Cell(190, 10, 'Perdana', 0, 1);
		$pdf->SetFont('Arial', 'B', 7);
		$pdf->Cell(30, 6, 'Kategori', 1, 0);
		$pdf->Cell(30, 6, 'Nama Barang', 1, 0);
		$pdf->Cell(25, 6, 'Harga', 1, 0);
		$pdf->Cell(25, 6, 'Stok', 1, 0);
		$pdf->Cell(25, 6, '+/-', 1, 0);
		$pdf->Cell(25, 6, 'Laku', 1, 0);
		$pdf->Cell(25, 6, 'Sisa', 1, 1);
		$pdf->SetFont('Arial', '', 7);

		foreach ($transaction['perdana'] as $data) {
			$pdf->Cell(30, 6, $data->category, 1, 0);
			$pdf->Cell(30, 6, $data->product, 1, 0);
			$pdf->Cell(25, 6, $data->cost, 1, 0);
			$pdf->Cell(25, 6, $data->awl, 1, 0);
			$pdf->Cell(25, 6, $data->plus, 1, 0);
			$pdf->Cell(25, 6, $data->lk, 1, 0);
			$pdf->Cell(25, 6, $data->ak, 1, 1);
		}

		#VOUCHER
		$pdf->Cell(190, 5, '', 0, 1);
		$pdf->SetFont('Arial', 'B', 12);
		$pdf->Cell(190, 10, 'Voucher', 0, 1);
		$pdf->SetFont('Arial', 'B', 7);
		$pdf->Cell(30, 6, 'Kategori', 1, 0);
		$pdf->Cell(30, 6, 'Nama Barang', 1, 0);
		$pdf->Cell(25, 6, 'Harga', 1, 0);
		$pdf->Cell(25, 6, 'Stok', 1, 0);
		$pdf->Cell(25, 6, '+/-', 1, 0);
		$pdf->Cell(25, 6, 'Laku', 1, 0);
		$pdf->Cell(25, 6, 'Sisa', 1, 1);
		$pdf->SetFont('Arial', '', 7);

		foreach ($transaction['voucher'] as $data) {
			$pdf->Cell(30, 6, $data->category, 1, 0);
			$pdf->Cell(30, 6, $data->product, 1, 0);
			$pdf->Cell(25, 6, $data->cost, 1, 0);
			$pdf->Cell(25, 6, $data->awl, 1, 0);
			$pdf->Cell(25, 6, $data->plus, 1, 0);
			$pdf->Cell(25, 6, $data->lk, 1, 0);
			$pdf->Cell(25, 6, $data->ak, 1, 1);
		}

		$pdf->Output();
	}

	function get_statistic_by_date($user_id, $outlet_id, $start_date, $end_date)
	{
		$filter = $this->generate_date_filter($start_date, $end_date);

		if ($outlet_id != 0) {

			$result = $this->M_outlet->get_outlet_by_id($outlet_id);

			$data['outlet'][] = $result[0]->name;
			$data['aksesoris'][] = $this->M_laporan->get_statistic_by_date($user_id, 'Aksesoris', $outlet_id, $filter);
			$data['perdana'][] = $this->M_laporan->get_statistic_by_date($user_id, 'Perdana', $outlet_id, $filter);
			$data['voucher'][] = $this->M_laporan->get_statistic_by_date($user_id, 'Voucher', $outlet_id, $filter);
			$data['pulsa'][] = $this->M_laporan->get_statistic_by_date($user_id, 'Pulsa', $outlet_id, $filter);

			$response = [
				'success' => true,
				'data' => $data,
				'total' => count($data)
			];

			return to_json($response);

		} else {

			$result = $this->M_outlet->get_all_outlet();

			$i = 0;
			$outlet_name = [];
			if (count($result) > 0) {
				foreach ($result as $item) {
					$outlet_name[] = $item->name;
					$data['aksesoris'][$i] = $this->M_laporan->get_statistic_by_date($user_id, 'Aksesoris', $item->id, $filter);
					$data['perdana'][$i] = $this->M_laporan->get_statistic_by_date($user_id, 'Perdana', $item->id, $filter);
					$data['voucher'][$i] = $this->M_laporan->get_statistic_by_date($user_id, 'Voucher', $item->id, $filter);
					$data['pulsa'][$i] = $this->M_laporan->get_statistic_by_date($user_id, 'Pulsa', $item->id, $filter);
					$i++;
				}

				$data['outlet'] = $outlet_name;

				$response = [
					'success' => true,
					'data' => $data,
					'total' => count($data)
				];

				return to_json($response);
			}
		}

		$response = [
			'success' => false,
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function get_statistic_by_outlet_id($outlet_id, $start_date, $end_date)
	{
		$start = substr($start_date, 9, 1);
		$end = substr($end_date, 8, 2);

		for ($i = intval($start); $i <= $end; $i++) {
			$hari[] = $i;
		}

		$filter = $this->generate_date_filter($start_date, $end_date);

		$result = $this->M_outlet->get_outlet_by_id($outlet_id);

		$i = 0;
		$outlet_name = [];
		if (count($result) > 0) {
			foreach ($result as $item) {
				$outlet_name[] = $item->name;
				$data['label'][] = $hari;
				$data['aksesoris'][$i] = $this->M_laporan->get_statistic_by_outlet_id('Aksesoris', $item->id, $filter);
				$data['perdana'][$i] = $this->M_laporan->get_statistic_by_outlet_id('Perdana', $item->id, $filter);
				$data['voucher'][$i] = $this->M_laporan->get_statistic_by_outlet_id('Voucher', $item->id, $filter);
				$data['pulsa'][$i] = $this->M_laporan->get_statistic_by_outlet_id('Pulsa', $item->id, $filter);
				$i++;
			}

			$data['outlet'] = $outlet_name;

			$response = [
				'success' => true,
				'data' => $data,
				'total' => count($data)
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function get_best_seller_product_by_outlet_id($user_id, $type, $outlet_id, $start_date, $end_date)
	{
		$filter = $this->generate_date_filter($start_date, $end_date);

		$result = $this->M_laporan->get_best_seller_product_by_outlet_id($user_id, $type, $outlet_id, $filter);

		if ($result) {

			$response = [
				'success' => true,
				'data' => $result,
				'total' => count($result)
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function generate_date_filter($start_date, $end_date)
	{
		if (empty($start_date) && empty($end_date)) {
			$filter = [
				'start' => date('Y-m-d', strtotime("-1 days")),
				'end' => date('Y-m-d')
			];
		} else if (date('Y-m-d', strtotime($start_date)) == date('Y-m-d', strtotime($end_date))) {
			$filter = [
				'start' => date('Y-m-d', strtotime('-1 day', strtotime($start_date))),
				'end' => date('Y-m-d', strtotime($end_date))
			];
		} else {
			$filter = [
				'start' => date('Y-m-d', strtotime($start_date)),
				'end' => date('Y-m-d', strtotime($end_date))
			];
		}

		return $filter;
	}

}
