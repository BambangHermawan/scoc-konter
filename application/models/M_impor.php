<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_impor extends CI_Model
{
	function kategori_fisik()
	{
		return $this->db->query("select * from item_type where deleted_date is null")->result();
	}

	function produk_fisik()
	{
		return $this->db->query("select item_category.*, item_type.name as type_name from item_category, item_type where item_category.item_type_id=item_type.id and item_category.deleted_date is null and item_type.deleted_date is null")->result();
	}

	function pulsa_unit()
	{
		return $this->db->query("select item_outlet_category.*, outlet.name as outlet_name from item_outlet_category, outlet where item_outlet_category.outlet_id=outlet.id and item_outlet_category.deleted_date is null and outlet.deleted_date is null")->result();
	}

	function pulsa_non_unit()
	{
		return $this->db->query("select item_outlet_category.*, outlet.name as outlet_name from item_outlet_category, outlet where item_outlet_category.outlet_id=outlet.id and item_outlet_category.deleted_date is null and outlet.deleted_date is null")->result();
	}

	function insert_excel_add_new_product($data)
	{
		$created_by = created_by();
		$created_date = created_date();

		$this->db->trans_begin();

		$cek_gudang = $this->db->query("  select item_type.id as item_type_id, item_type.name as item_type_name, item_category.id as item_category_id, item_category.name as item_category_name, item_product.id as item_product_id, item_product.name as item_product_name, modal, many, entry_date
				from item_product, item_category, item_type
                where item_product.item_category_id = item_category.id 
                and item_category.item_type_id = item_type.id 
                and item_type.name= LOWER('{$data['ID_ITEM_TIPE']}')
                and item_category.name=LOWER('{$data['ID_ITEM_KATEGORI']}')
                and item_product.name=LOWER('{$data['NAMA_PRODUK']}')
				and item_product.deleted_date is null
                and item_category.deleted_date is null
                and item_type.deleted_date is null
                ")->result();

		if (count($cek_gudang) > 0) {
			$many = $cek_gudang[0]->many + $data['BANYAK'];

			$this->db->query("update item_product set many='$many', entry_date='$created_date', entry_date='$created_date', modified_by='$created_by', modified_date='$created_date' where id='{$cek_gudang[0]->item_product_id}' and deleted_date is null");

			$CI =& get_instance();
			$CI->load->model('M_log_produk');
			$detail = $CI->M_log_produk->get_detail_product($cek_gudang[0]->item_product_id);

			$activity = "Gudang pusat menambahkan produk \"$detail->type_name $detail->category_name $detail->product_name sebanyak {$data['BANYAK']}\"";
			$CI->M_log_produk->insert_log_product($_SESSION['id'], 16, 'item_product', $cek_gudang[0]->item_product_id, $cek_gudang[0]->modal, NULL, $cek_gudang[0]->many, $data['BANYAK'], $many, NULL, $activity, '-', 'add_new_product_gudang');

			$this->db->trans_complete();

			return true;
		} else {

			$this->load->library('ciqrcode');

			$config['cacheable'] = true;
			$config['cachedir'] = 'uploads/';
			$params['errorlog'] = FCPATH . 'application/logs/';
			$config['imagedir'] = 'uploads/qrcode/';
			$config['quality'] = true;
			$config['size'] = '1024';
			$config['black'] = array(224, 255, 255);
			$config['white'] = array(70, 130, 180);
			$this->ciqrcode->initialize($config);

			$product_image = md5(time() . '_' . uniqid()) . '.png';

			$params['data'] = $product_image;
			$params['level'] = 'H';
			$params['size'] = 10;
			$params['savename'] = FCPATH . $config['imagedir'] . $product_image;
			$this->ciqrcode->generate($params);

			$item_category = $this->db->query("select item_type.id as item_type_id, item_category.id as item_category_id
				from item_category, item_type
                where item_category.item_type_id = item_type.id 
                and item_type.name='{$data['ID_ITEM_TIPE']}'
                and item_category.name='{$data['ID_ITEM_KATEGORI']}'
                and item_category.deleted_date is null
				and item_type.deleted_date is null
                ")->result();

			$this->db->query("insert into item_product (item_category_id, name, modal, many, qrcode, entry_date, created_by, created_date)
			values ('{$item_category[0]->item_category_id}','{$data['NAMA_PRODUK']}','{$data['MODAL']}','{$data['BANYAK']}','$product_image','$created_date','$created_by','$created_date')");

			$transaction_id = $this->db->insert_id();

			$CI =& get_instance();
			$CI->load->model('M_log_produk');
			$detail = $CI->M_log_produk->get_detail_product($transaction_id);

			$activity = "Gudang pusat menambahkan produk baru \"$detail->type_name $detail->category_name $detail->product_name sebanyak {$data['BANYAK']} pcs\"";
			$CI->M_log_produk->insert_log_product($_SESSION['id'], 16, 'item_product', $transaction_id, $data['MODAL'], NULL, $data['BANYAK'], NULL, $data['BANYAK'], NULL, $activity, '-', 'add_new_product_gudang');

			$this->db->trans_complete();

			return true;
		}
	}

	function insert_excel_add_product_stock($data)
	{
		$created_by = created_by();
		$created_date = created_date();

		$this->db->trans_begin();

		$cek_gudang = $this->db->query("  select item_type.id as item_type_id, item_type.name as item_type_name, item_category.id as item_category_id, item_category.name as item_category_name, item_product.id as item_product_id, item_product.name as item_product_name, modal, many, entry_date
				from item_product, item_category, item_type
                where item_product.item_category_id = item_category.id 
                and item_category.item_type_id = item_type.id 
                and item_type.name= LOWER('{$data['ID_ITEM_TIPE']}')
                and item_category.name=LOWER('{$data['ID_ITEM_KATEGORI']}')
				and item_product.name=LOWER('{$data['NAMA_PRODUK']}')
				and item_product.deleted_date is null
                and item_category.deleted_date is null
                and item_type.deleted_date is null
                ")->result();

		if ($data['PENAMBAHAN_PRODUK'] == null || $data['PENAMBAHAN_PRODUK'] == 0) {
			return false;
		}

		if (count($cek_gudang) > 0) {

			$many = $cek_gudang[0]->many + $data['PENAMBAHAN_PRODUK'];

			$this->db->query("update item_product set many='$many', modified_by='$created_by', modified_date='$created_date' where id='{$cek_gudang[0]->item_product_id}' and deleted_date is null");

			$CI =& get_instance();
			$CI->load->model('M_log_produk');
			$detail = $CI->M_log_produk->get_detail_product($cek_gudang[0]->item_product_id);

			$activity = "Gudang pusat menambahkan stok \"$detail->type_name $detail->category_name $detail->product_name sebanyak {$data['PENAMBAHAN_PRODUK']} pcs\"";
			$CI->M_log_produk->insert_log_product($_SESSION['id'], 16, 'item_product', $cek_gudang[0]->item_product_id, NULL, NULL, $cek_gudang[0]->many, $data['PENAMBAHAN_PRODUK'], $many, NULL, $activity, '-', 'add_stock_product_gudang');

			$this->db->trans_complete();

			return true;
		} else {
			return false;
		}
	}

	function insert_excel_add_product_outlet_modal($data, $outlet_id)
	{
		$created_by = created_by();
		$created_date = created_date();

		$this->db->trans_begin();

		$item = $this->db->query("
		select item_outlet_category.id as item_category_id, item_outlet_category.name as category_name, item_outlet_category.outlet_id as outlet_id, item_outlet_category.modal as modal, outlet.name as outlet_name, item_outlet.id as item_outlet_id
        from outlet, item_outlet_category, item_outlet
        where item_outlet_category.outlet_id = outlet.id 
        and item_outlet_category.id = item_outlet.item_outlet_category_id 
        and item_outlet_category.type_id = 4 
        and item_outlet_category.name=LOWER('{$data['ID_ITEM_KATEGORI']}')
        and outlet.name = LOWER('{$data['OUTLET_NAMA']}')
        and item_outlet_category.outlet_id = '$outlet_id'
		and outlet.deleted_date is null
		and item_outlet_category.deleted_date is null
		and item_outlet.deleted_date is null
        ")->result();

		if (count($item) > 0) {

			$modal = $item[0]->modal + $data['PENAMBAHAN_MODAL'];

			$tambah_modal = $data['PENAMBAHAN_MODAL'] != null ? $data['PENAMBAHAN_MODAL'] : 0;

			$this->db->query("
			insert into transaction (user_id, outlet_id, item_outlet_id, type, category, product, awl, plus, ak, status, unit, from_outlet, created_by, created_date)
        	values('{$_SESSION['id']}','{$item[0]->outlet_id}','{$item[0]->item_category_id}','Pulsa','{$item[0]->category_name}','Rp','{$item[0]->modal}','$tambah_modal','$modal','tambah_saldo','Bluk','{$item[0]->outlet_id}','$created_by', '$created_date')");

			$transaction_id = $this->db->insert_id();

			$this->db->query("update item_outlet_category set modal='$modal' where id='{$item[0]->item_category_id}' and deleted_date is null");

			$CI =& get_instance();
			$CI->load->model('M_log');
			$product = $this->M_log->get_item_outlet_category($item[0]->item_category_id);
			$outlet = $this->M_log->get_outlet($outlet_id);

			$activity = "Outlet $outlet->name menambahkan saldo \"$product->name sebesar {$data['PENAMBAHAN_MODAL']}\"";
			$CI->M_log->insert_log($_SESSION['id'], $outlet_id, 'transaction', $transaction_id, $activity, '-', 'tambah_saldo_pulsa_outlet');

			$this->db->trans_complete();

			return true;

		} else {
			return false;
		}
	}

	function insert_excel_add_product_outlet_non_unit($data, $outlet_id)
	{
		$created_by = created_by();
		$created_date = created_date();

		$this->db->trans_begin();

		$item = $this->db->query("select item_outlet_category.id as id from item_outlet, item_outlet_category
		where item_outlet.item_outlet_category_id = item_outlet_category.id
		and item_outlet_category.name = '{$data['ID_ITEM_KATEGORI']}'
		and item_outlet_category.outlet_id = '$outlet_id'
		and item_outlet.deleted_date is null 
		and item_outlet_category.deleted_date is null 
		")->result();

		if (count($item) > 0) {
			$this->load->library('ciqrcode');
			$config['cacheable'] = true;
			$config['cachedir'] = 'uploads/';
			$config['errorlog'] = 'uploads/';
			$config['imagedir'] = 'uploads/qrcode/';
			$config['quality'] = true;
			$config['size'] = '1024';
			$config['black'] = array(224, 255, 255);
			$config['white'] = array(70, 130, 180);
			$this->ciqrcode->initialize($config);

			$product_image = md5(time() . '_' . uniqid()) . '.png';

			$params['data'] = $product_image;
			$params['level'] = 'H';
			$params['size'] = 10;
			$params['savename'] = FCPATH . $config['imagedir'] . $product_image;
			$this->ciqrcode->generate($params);

			$this->db->query("
			insert into item_outlet (item_outlet_category_id, name, modal, cost, unit, qrcode, created_by, created_date)
			values('{$item[0]->id}','{$data['NAMA_ITEM']}','{$data['MODAL']}','{$data['HARGA']}','{$data['UNIT_ITEM']}','$product_image','$created_by','$created_date')");

			$transaction_id = $this->db->insert_id();

			$CI =& get_instance();
			$CI->load->model('M_log_produk');
			$product = $this->M_log_produk->get_detail_product_bluk_outlet($item[0]->id);
			$outlet = $this->M_log_produk->get_outlet($outlet_id);

			$activity = "Outlet $outlet->name menambahkan produk baru non unit \"$product->product_name dengan modal {$data['MODAL']} dan harga {$data['HARGA']}\"";
			$CI->M_log_produk->insert_log_product($_SESSION['id'], $outlet_id, 'item_outlet', $transaction_id, $data['MODAL'], $data['HARGA'], NULL, NULL, NULL, NULL, $activity, '-', 'add_product_non_unit_outlet');

			$this->db->trans_complete();

			return true;

		} else {
			return false;
		}
	}
}
