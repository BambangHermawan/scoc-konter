<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transfer extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_transfer', '', true);
		$this->load->model('M_cart', '', true);
		$this->load->model('M_outlet', '', true);
		$this->load->model('M_log', '', true);

		if (!isset($_SESSION)) {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'ADMIN') {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'OWNER') {
			redirect('auth/login');
		} elseif ($_SESSION['status_user'] == 'KASIR') {
			redirect('auth/login');
		}
	}

	function index()
	{
		$data['title'] = 'Bulk Transfer';
		$data['site_navigation'] = 'Gudang';

		return $this->load->view('gudang/pusat/transfer', $data);
	}

	function get_all_product_transfer($type_id, $category_id)
	{
		$result = $this->M_transfer->get_datatables($type_id, $category_id);

		$response = [
			'success' => true,
			'draw' => $_REQUEST['draw'],
			'recordsTotal' => $this->M_transfer->count_all($type_id, $category_id),
			'recordsFiltered' => $this->M_transfer->count_filtered($type_id, $category_id),
			'data' => $result
		];

		return to_json($response);
	}

	function get_all_cart_transfer($outlet_id)
	{
		$result = $this->M_cart->get_datatables($outlet_id);

		$response = [
			'success' => true,
			'draw' => $_REQUEST['draw'],
			'recordsTotal' => $this->M_cart->count_all($outlet_id),
			'recordsFiltered' => $this->M_cart->count_filtered($outlet_id),
			'data' => $result
		];

		return to_json($response);
	}

	function add_cart_transfer()
	{
		$product_id = $this->input->post('product_id');
		$name = $this->input->post('name');
		$modal = $this->input->post('modal');
		$cost = $this->input->post('cost');
		$quantity = $this->input->post('quantity');

		$result = $this->M_cart->add_cart_transfer($product_id, $name, $modal, $cost, $quantity);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Transfer produk ' . $name . ' ke keranjang berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Transfer produk ' . $name . ' ke keranjang gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function edit_cart_transfer()
	{
		$product_id = $this->input->post('product_id');
		$cost = $this->input->post('cost');
		$quantity = $this->input->post('quantity');

		$result = $this->M_cart->edit_cart_transfer($product_id, $cost, $quantity);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Ubah data keranjang berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Ubah data keranjang gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function delete_cart_transfer()
	{
		$cart_id = $this->input->post('id');

		$result = $this->M_cart->delete_cart_transfer($cart_id);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Hapus produk dari keranjang transfer berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Hapus produk dari keranjang transfer gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function bulk_transfer_to_outlet()
	{
		$outlet_id = $this->input->post('outlet_id');
		$status_pusat = 'transfer_from_pusat';
		$sender_outlet = 0;

		$cart = $this->M_cart->get_all_cart_transfer($sender_outlet, $status_pusat);

		$this->load->library('ciqrcode');

		$config['cacheable'] = true;
		$config['cachedir'] = 'uploads/';
		$config['errorlog'] = 'uploads/';
		$config['imagedir'] = 'uploads/qrcode/';
		$config['quality'] = true;
		$config['size'] = '1024';
		$config['black'] = array(224, 255, 255);
		$config['white'] = array(70, 130, 180);
		$this->ciqrcode->initialize($config);

		$status = '';
		if (count($cart) > 0) {
			foreach ($cart as $item) {

				$product_image = md5(time() . '_' . uniqid()) . '.png';

				$result = $this->M_cart->bulk_transfer_to_outlet($outlet_id, $item->item_product_id, $item->cost, $item->quantity, $product_image);

				if ($result) {

					$params['data'] = $product_image;
					$params['level'] = 'H';
					$params['size'] = 10;
					$params['savename'] = FCPATH . $config['imagedir'] . $product_image;
					$this->ciqrcode->generate($params);

					$product = $this->M_log->get_detail_product($item->item_product_id);
					$outlet = $this->M_log->get_outlet($outlet_id);

					$activity = "Gudang pusat transfer \"$product->type_name $product->category_name $product->product_name sebanyak $item->quantity pcs\" ke outlet $outlet->name";
					$this->M_log->insert_log($_SESSION['id'], $outlet_id, 'transaction', $result, $activity, '-', 'transfer_from_gudang');

					$status .= 'Berhasil transfer ' . $item->quantity . ' produk ' . $item->name . '<hr>';

				} else {

					$status .= 'Gagal transfer produk ' . $item->name . '<hr>';
				}
			}

			if ($status) {

				$response = [
					'success' => true,
					'type' => 'success',
					'message' => $status,
					'data' => $status,
					'total' => $status
				];

				return to_json($response);
			}
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Terjadi kesalahan...',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function add_cart_transfer_from_outlet()
	{
		$outlet_id = $this->input->post('outlet_id');
		$outlet_product_id = $this->input->post('outlet_product_id');
		$item_product_id = $this->input->post('item_product_id');
		$name = $this->input->post('name');
		$modal = $this->input->post('modal');
		$cost = $this->input->post('cost');
		$quantity = $this->input->post('quantity');

		$result = $this->M_cart->add_cart_transfer_from_outlet($outlet_id, $outlet_product_id, $item_product_id, $name, $modal, $cost, $quantity);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Transfer produk ' . $name . ' ke keranjang berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Transfer produk ' . $name . ' ke keranjang gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function edit_cart_transfer_from_outlet()
	{
		$outlet_id = $this->input->post('outlet_id');
		$cart_id = $this->input->post('cart_id');
		$item_product_id = $this->input->post('item_product_id');
		$cost = $this->input->post('cost');
		$quantity = $this->input->post('quantity');

		$result = $this->M_cart->edit_cart_transfer_from_outlet($outlet_id, $cart_id, $item_product_id, $cost, $quantity);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Ubah data keranjang berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Ubah data keranjang gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function get_product_transfer_status($outlet_id, $item_product_id)
	{
		$result = $this->M_transfer->get_product_transfer_status($outlet_id, $item_product_id);

		if (count($result) > 0) {

			$response = [
				'success' => true,
				'data' => $result,
				'total' => count($result)
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function bulk_transfer_to_outlet_from_outlet()
	{
		$sender_outlet_id = $this->input->post('sender_outlet_id');
		$destination_outlet_id = $this->input->post('destination_outlet_id');

		$status_cart = 'transfer_from_outlet';

		$cart = $this->M_cart->get_all_cart_transfer($sender_outlet_id, $status_cart);

		$this->load->library('ciqrcode');

		$config['cacheable'] = true;
		$config['cachedir'] = 'uploads/';
		$config['errorlog'] = 'uploads/';
		$config['imagedir'] = 'uploads/qrcode/';
		$config['quality'] = true;
		$config['size'] = '1024';
		$config['black'] = array(224, 255, 255);
		$config['white'] = array(70, 130, 180);
		$this->ciqrcode->initialize($config);

		$status = '';
		if (count($cart) > 0) {
			foreach ($cart as $item) {

				$product_image = md5(time() . '_' . uniqid()) . '.png';

				$result = $this->M_cart->bulk_transfer_to_outlet_from_outlet($sender_outlet_id, $destination_outlet_id, $item->item_product_id, $item->cost, $item->quantity, $product_image, '-');

				if ($result) {

					$params['data'] = $product_image;
					$params['level'] = 'H';
					$params['size'] = 10;
					$params['savename'] = FCPATH . $config['imagedir'] . $product_image;
					$this->ciqrcode->generate($params);

					$product = $this->M_log->get_detail_product($item->item_product_id);
					$sender = $this->M_log->get_outlet($sender_outlet_id);
					$destination = $this->M_log->get_outlet($destination_outlet_id);

					$activity = "Outlet $sender->name transfer \"$product->type_name $product->category_name $product->product_name sebanyak $item->quantity pcs\" ke outlet $destination->name";
					$this->M_log->insert_log($_SESSION['id'], $sender_outlet_id, 'transaction', $result, $activity, '-', 'transfer_from_outlet_to_outlet');

					$status .= 'Berhasil transfer ' . $item->quantity . ' produk ' . $item->name . '<hr>';
				} else {
					$status .= 'Gagal transfer produk ' . $item->name . '<hr>';
				}
			}

			if ($status) {

				$response = [
					'success' => true,
					'type' => 'success',
					'message' => $status,
					'data' => $status,
					'total' => $status
				];

				return to_json($response);
			}
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Terjadi kesalahan...',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

}
