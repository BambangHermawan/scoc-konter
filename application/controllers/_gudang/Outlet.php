<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Outlet extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('_gudang/M_outlet', 'M_outlet', true);
		$this->load->model('M_log', '', true);

		if (!isset($_SESSION)) {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'ADMIN') {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'OWNER') {
			redirect('auth/login');
		} elseif ($_SESSION['status_user'] == 'KASIR') {
			redirect('auth/login');
		}
	}

	function index($outlet_id)
	{
		$data['title'] = 'Gudang Outlet';
		$data['site_navigation'] = 'Outlet';

		$outlet = $this->M_log->get_outlet($outlet_id);

		$data['outlet'] = $outlet->name;

		$this->load->view('gudang/outlet/index', $data);
	}

	function bulk_transfer_outlet($outlet_id)
	{
		$data['title'] = 'Bulk Transfer Outlet';
		$data['site_navigation'] = 'Gudang';

		$outlet = $this->M_log->get_outlet($outlet_id);

		$data['outlet'] = $outlet->name;

		return $this->load->view('gudang/outlet/transfer', $data);
	}

	function get_all_product_by_outlet_id($type, $outlet_id)
	{
		$result = $this->M_outlet->get_datatables($type, $outlet_id);

		$response = [
			'success' => true,
			'draw' => $_REQUEST['draw'],
			'recordsTotal' => $this->M_outlet->count_all($type, $outlet_id),
			'recordsFiltered' => $this->M_outlet->count_filtered($type, $outlet_id),
			'data' => $result
		];

		return to_json($response);
	}

	function transfer_from_outlet()
	{
		$product_id = $this->input->post('product_id');
		$sender_outlet_id = $this->input->post('from_outlet_id');
		$destination_outlet_id = $this->input->post('to_outlet_id');
		$many = $this->input->post('many');
		$note = $this->input->post('note');

		$this->load->library('ciqrcode');

		$config['cacheable'] = true;
		$config['cachedir'] = 'uploads/';
		$config['errorlog'] = 'uploads/';
		$config['imagedir'] = 'uploads/qrcode/';
		$config['quality'] = true;
		$config['size'] = '1024';
		$config['black'] = array(224, 255, 255);
		$config['white'] = array(70, 130, 180);
		$this->ciqrcode->initialize($config);

		$product_image = md5(time() . '_' . uniqid()) . '.png';

		$result = $this->M_outlet->transfer_from_outlet($product_id, $sender_outlet_id, $destination_outlet_id, $many, $note, $product_image);

		if ($result) {

			$params['data'] = $product_image;
			$params['level'] = 'H';
			$params['size'] = 10;
			$params['savename'] = FCPATH . $config['imagedir'] . $product_image;
			$this->ciqrcode->generate($params);

			$product = $this->M_log->get_detail_product($product_id);
			$sender = $this->M_log->get_outlet($sender_outlet_id);
			$destination = $this->M_log->get_outlet($destination_outlet_id);

			$activity = "Outlet $sender->name transfer \"$product->type_name $product->category_name $product->product_name sebanyak $many pcs\" ke outlet $destination->name";
			$this->M_log->insert_log($_SESSION['id'], $sender_outlet_id, 'transaction', $result, $activity, '-', 'transfer_from_outlet_to_outlet');

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Transfer produk berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Jumlah barang tidak mencukupi.',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function set_product_cost()
	{
		$outlet_id = $this->input->post('outlet_id');
		$product_id = $this->input->post('product_id');
		$cost = $this->input->post('cost');

		$result = $this->M_outlet->set_product_cost($outlet_id, $product_id, $cost);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Ubah harga produk berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Ubah harga produk gagal.',
			'data' => [],
			'total' => 0
		];

		return to_json($response);

	}

	function set_product_discount()
	{
		$outlet_id = $this->input->post('outlet_id');
		$product_id = $this->input->post('product_id');
		$discount = $this->input->post('discount');
		$discount_until = $this->input->post('discount_until');

		$result = $this->M_outlet->set_product_discount($outlet_id, $product_id, $discount, $discount_until);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Ubah harga produk berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Ubah harga produk gagal.',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function delete_product()
	{
		$outlet_id = $this->input->post('outlet_id');
		$product_id = $this->input->post('product_id');

		$result = $this->M_outlet->delete_product($outlet_id, $product_id);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Hapus produk berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Hapus produk gagal.',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function get_stok_peringatan($outlet_id)
	{
		$result = $this->M_outlet->get_stok_peringatan($outlet_id);

		if (count($result) > 0) {

			$response = [
				'success' => true,
				'data' => $result,
				'total' => count($result)
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

}
