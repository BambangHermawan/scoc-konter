<?php $this->load->view('_layouts/_template/header') ?>

<div class="container-fluid">

	<div class="row">
		<div class="col-12">
			<div class="card card-outline card-primary">
				<div class="card-header">
					<h3 class="card-title"><b>Wewenang</b></h3>
					<div class="float-right">
						<button class="btn btn-primary btn-sm" id="btn_add_wewenang"><i class="fa fa-plus"></i> Wewenang
						</button>
					</div>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table id="table_wewenang" class="table table-striped table-hover dt-responsive display nowrap"
							   style="width:100%">
							<thead>
							<tr>
								<th class="nomor" data-orderable="false">No</th>
								<th>Wewenang</th>
								<th data-orderable="false">Aksi</th>
							</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--modal tambah wewenang-->
<div class="modal fade" id="modal_add_wewenang" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Tambah Wewenang</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="add_wewenang_form">
					<div class="form-group">
						<div class="col-lg-4">Wewenang</div>
						<div class="col-lg-12">
							<input type="text" class="form-control form-control-sm" id="add_wewenang_name"
								   name="add_wewenang_name">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"
						id="save_add_wewenang">Simpan
				</button>
			</div>
		</div>
	</div>
</div>

<!--modal ubah wewenang-->
<div class="modal fade" id="modal_edit_wewenang" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Tambah Wewenang</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="edit_wewenang_form">
					<div class="form-group">
						<div class="col-lg-4">Wewenang</div>
						<div class="col-lg-12">
							<input type="text" class="form-control form-control-sm" id="edit_wewenang_name"
								   name="edit_wewenang_name">
						</div>
					</div>
					<div class="form-group" hidden>
						<div class="col-lg-4">Type</div>
						<div class="col-lg-12"><input type="text" class="form-control form-control-sm"
													  id="edit_wewenang_id" name="edit_wewenang_id"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"
						id="save_edit_wewenang">Simpan
				</button>
			</div>
		</div>
	</div>
</div>

<script>
	//init table wewenang
	$(document).ready(function () {
		init_table_wewenang()
	})

	//validasi form tambah wewenang
	$("#add_wewenang_form").validate({
		rules: {
			add_wewenang_name: {
				required: true,
				maxlength: 50
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//modal tambah wewenang
	$("#btn_add_wewenang").on('click', function () {
		$("#modal_add_wewenang").modal({backdrop: 'static', keyboard: false})
	})

	//simpan tambah data wewenang
	$("#save_add_wewenang").on('click', function () {

		if ($('#add_wewenang_form').valid()) {
			$.ajax({
				url: "<?= base_url(); ?>wewenang/add_wewenang",
				type: "POST",
				data: {
					'wewenang': $("#add_wewenang_name").val()
				},
				success: function (result) {

					if (result.success) {
						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						init_table_wewenang()

					} else {
						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						init_table_wewenang()
					}
				}
			})
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});

			init_table_wewenang()
		}

	})

	//validasi form ubah wewenang
	$("#edit_wewenang_form").validate({
		rules: {
			add_wewenang_name: {
				required: true,
				maxlength: 50
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//modal ubah data wewenang
	$("#table_wewenang").on('click', 'tr #btn_edit_click', function () {

		tr = $(this).closest('tr')
		data = table_wewenang.row(tr).data()
		Batal = table_wewenang.row(tr).index()
		$("#edit_wewenang_id").val(data.id)
		$("#edit_wewenang_name").val(data.wewenang)

		$('#modal_edit_wewenang').modal({backdrop: 'static', keyboard: false})
	})

	//simpan ubah data wewenang
	$("#save_edit_wewenang").on('click', function () {

		if ($('#edit_wewenang_form').valid()) {
			$.ajax({
				url: "<?= base_url(); ?>wewenang/edit_wewenang",
				type: "POST",
				data: {
					'id': $("#edit_wewenang_id").val(),
					'wewenang': $("#edit_wewenang_name").val()
				},
				success: function (result) {

					if (result.success) {
						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});
						init_table_wewenang()

					} else {
						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						init_table_wewenang()
					}
				}
			})
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});

			init_table_wewenang()
		}

	})

	//hapus data wewenang
	$("#table_wewenang").on('click', 'tr #btn_delete_click', function () {
		tr = $(this).closest('tr')
		data = table_wewenang.row(tr).data()
		id = data.id

		Swal.fire({
			title: 'Peringatan!',
			text: 'Apakah Anda yakin akan menghapus wewenang ' + data.name + ' ?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya, hapus data ini!'
		}).then((result) => {
			if (result.value) {

				$.ajax({
					url: "<?= base_url(); ?>wewenang/delete_wewenang",
					type: "POST",
					data: {id: id},
					success(result) {

						if (result.success) {
							Swal.fire({
								title: 'Berhasil',
								text: result.message,
								icon: result.type,
								showConfirmButton: false,
								timer: 1500
							});

							init_table_wewenang()
						} else {
							Swal.fire({
								title: 'gagal',
								text: result.message,
								icon: result.type,
								showConfirmButton: false,
								timer: 1500
							});

							init_table_wewenang()
						}

					}
				})
			}
		})
	})

	//init datatables table wewenang
	function init_table_wewenang() {
		button = "&nbsp;<button class='btn btn-warning btn-sm' id='btn_edit_click'>\
            <i class='fas fa-edit'></i>&nbsp;Ubah</button>"
		button += "&nbsp;<button class='btn btn-danger btn-sm' id='btn_delete_click'>\
            <i class='fas fa-trash'></i>&nbsp;Hapus</button>"

		table_wewenang = $("#table_wewenang").DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			autoWidth: true,
			ajax: "<?= base_url(); ?>wewenang/get_all_wewenang",
			columns: [
				{
					data: "id",
					width: "0.8%",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: 'wewenang'
				},
				{
					defaultContent: button
				},
			]
		})
	}
</script>


<?php $this->load->view('_layouts/_template/js') ?>
