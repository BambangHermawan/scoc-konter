<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Log extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('kasir/M_laporan', 'M_laporan', true);
		$this->load->model('M_log', 'M_log', true);

		if (!isset($_SESSION)) {
			redirect('auth/login');
		} elseif ($_SESSION['status_user'] != 'KASIR') {
			redirect('auth/login');
		}
	}

	function index($outlet_id)
	{
		$data['title'] = 'Log Aktifitas';
		$data['site_navigation'] = 'Log Aktifitas';

		if ($outlet_id != 0) {
			$outlet = $this->M_log->get_outlet($outlet_id);
			$data['outlet'] = $outlet->name;
		} else {
			$data['outlet'] = 'Gudang';
		}

		return $this->load->view('kasir/log/index', $data);
	}

	function get_all_log($user_id, $outlet_id, $start_date, $end_date)
	{
		$filter = $this->generate_date_filter($start_date, $end_date);

		$result = $this->M_log->get_datatables($user_id, $outlet_id, $filter);

		$response = [
			'success' => true,
			'draw' => $_REQUEST['draw'],
			'recordsTotal' => $this->M_log->count_all($user_id, $outlet_id, $filter),
			'recordsFiltered' => $this->M_log->count_filtered($user_id, $outlet_id, $filter),
			'data' => $result
		];

		return to_json($response);
	}

	function get_detail_log($transaction_id)
	{
		$result = $this->M_log->get_detail_log($transaction_id);

		if (count($result) > 0) {
			$response = [
				'success' => 'true',
				'data' => $result,
				'count' => count($result)
			];

			return to_json($response);
		}

		$response = [
			'success' => 'false',
			'data' => [],
			'count' => 0
		];

		return to_json($response);
	}

	function generate_date_filter($start_date, $end_date)
	{
		if (empty($start_date) && empty($end_date)) {
			$filter = [
				'start' => date('Y-m-d', strtotime("-1 days")),
				'end' => date('Y-m-d')
			];
		} else if (date('Y-m-d', strtotime($start_date)) == date('Y-m-d', strtotime($end_date))) {
			$filter = [
				'start' => date('Y-m-d', strtotime($start_date)),
				'end' => date('Y-m-d', strtotime($end_date))
			];
		} else {
			$filter = [
				'start' => date('Y-m-d', strtotime($start_date)),
				'end' => date('Y-m-d', strtotime($end_date))
			];
		}

		return $filter;
	}
}
