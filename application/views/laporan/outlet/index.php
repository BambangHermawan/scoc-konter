<?php $this->load->view('_layouts/_template/header') ?>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-3">
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Filter <b><?= $outlet[0]->name; ?></b></h3>
				</div>
				<div class="card-body p-2">
					<div class="form-group">
						<div class="col-lg-4"><label for="date_begin">Mulai</label></div>
						<div class="col-lg-12">
							<input type="date" class="form-control form-control-sm"
								   name="date_begin"
								   id="date_begin" required>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4"><label for="date_end">Sampai</label></div>
						<div class="col-lg-12">
							<input type="date" class="form-control form-control-sm"
								   name="date_end"
								   id="date_end" required>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4"><label for="outlet_id">Outlet</label></div>
						<div class="col-lg-12">
							<select type="date" class="form-control form-control-sm"
									name="outlet_id"
									id="outlet_id">
							</select>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4"><label for="outlet_id">Shift</label></div>
						<div class="col-lg-12">
							<select type="date" class="form-control form-control-sm"
									name="outlet_employee_id"
									id="outlet_employee_id">
							</select>
						</div>
					</div>

				</div>
				<div class="card-footer">
					<div class="float-right">
						<a class="btn btn-sm btn-warning" id="print_laporan"><i
									class="fa fa-print"></i> Cetak Laporan</a>
						<button type="submit" class="btn btn-primary btn-sm" id="btn_search_all">Filter
							Data
						</button>
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Grafik Penjualan Bulan Ini</h3>
				</div>
				<div class="card-body">
					<ul class="nav nav-pills flex-column">
						<?php foreach ($_SESSION['menu'] as $menu) { ?>
							<?php if ($menu->id == $this->uri->segment(3)) { ?>
								<li class="nav-item">
									<a class="nav-link btn_show_outlet" id="outlet_<?= $menu->id; ?>"><i
												class="far fa-circle"></i> <?= $menu->name; ?></a>
								</li>
							<?php } ?>
						<?php } ?>
					</ul>
				</div>
				<div class="card-footer">
					<div class="float-right">
						<a href="<?= base_url(); ?>rekapan/rekap_total_uang/<?= $this->uri->segment(3); ?>"
						   class="btn btn-primary btn-sm">Rekap total uang</a>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-9">
			<div class="row">
				<div class="col-12 col-sm-6 col-md-6">
					<div class="info-box">
						<span class="info-box-icon bg-info elevation-1"><i class="fas fa-shopping-cart"></i></span>
						<div class="info-box-content">
							<span class="info-box-text">Laku</span>
							<span class="info-box-number" id="rekapan_laku">0</span>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-6 col-md-6">
					<div class="info-box mb-3">
						<span class="info-box-icon bg-info elevation-1"><i class="fas fa-wallet"></i></span>
						<div class="info-box-content">
							<span class="info-box-text">Omset</span>
							<span class="info-box-number" id="rekapan_omset">0</span>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-6 col-md-6">
					<div class="info-box mb-3">
						<span class="info-box-icon bg-info elevation-1"><i class="fas fa-plus"></i></span>
						<div class="info-box-content">
							<span class="info-box-text">Untung</span>
							<span class="info-box-number" id="rekapan_untung">0</span>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-6 col-md-6">
					<div class="info-box mb-3">
						<span class="info-box-icon bg-info elevation-1"><i class="fas fa-money-check"></i></span>
						<div class="info-box-content">
							<span class="info-box-text">Total Aset</span>
							<span class="info-box-number" id="rekapan_aset">0</span>
						</div>
					</div>
				</div>
			</div>

			<div class="card card-primary card-outline">
				<div class="card-header">
					<h3 class="card-title">Grafik <b><?= $outlet[0]->name; ?></b></h3>
				</div>
				<div class="card-body">
					<div id="laporan_chart_container">
						<canvas id="laporan_chart"></canvas>
					</div>
				</div>
			</div>

			<div class="card card-primary card-outline">
				<div class="card-header">
					<h3 class="card-title">Penjualan Terbaik</h3>
					<div class="float-right">
						<div class="form-group">
							<select type="date" class="form-control form-control-sm"
									name="item_type_id"
									id="item_type_id">
								<option value="Semua">Semua</option>
							</select>
						</div>
					</div>

				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table id="table_penjualan_terbaik"
							   class="table table-striped table-hover dt-responsive display nowrap"
							   style="width:100%">
							<thead>
							<tr>
								<th class="nomor" data-orderable="false">No</th>
								<th>Outlet</th>
								<th>Kategori</th>
								<th>Produk</th>
								<th>Jumlah Terjual</th>
								<th>Tanggal Penjualan</th>
							</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--modal grafik outlet-->
<div class="modal fade" id="modal_show_outlet">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header"></div>
			<div class="modal-body">
				<div id="laporan_outlet_chart_container">
					<canvas id="laporan_outlet_chart"></canvas>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Selesai</button>
			</div>

		</div>
	</div>
</div>

<script>
	var data_hari
	var data_outlet
	var data_aksesoris
	var data_perdana
	var data_voucher
	var data_pulsa

	//init grafik laporan
	$(document).ready(function () {
		var today = new Date();
		date = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2)

		$("#date_begin").val(date)
		$("#date_end").val(date)
		item = $('#item_type_id option:selected').text();

		get_outlet()
		get_item_type()
		init_laporan_chart()
		init_table_penjualan_terbaik()
		get_outlet_employee()
	})

	//print laporan
	$('#print_laporan').click(function () {
		link = "<?= base_url(); ?>laporan/get_transaction_outlet/" + $('#outlet_employee_id option:selected').val() + "/" + $('#outlet_id option:selected').val() + "/" + $('#date_begin').val() + "/" + $('#date_end').val()
		window.open(link, '_blank');
	})

	//filter grafik laporan
	$('#btn_search_all').click(function () {

		outlet_id = '<?= $this->uri->segment(3) ?>';

		$.ajax({
			url: "<?= base_url(); ?>laporan/get_total_all_by_outlet_id/" + $('#outlet_employee_id option:selected').val() + "/" + outlet_id + "/" + $('#date_begin').val() + "/" + $("#date_end").val(),
			success(result) {
				$('#rekapan_laku').text(result.data['lk'])
				$('#rekapan_omset').text(result.data['omset'])
				$('#rekapan_untung').text(result.data['advantage'])
				$('#rekapan_rugi').text(result.data['rugi'])
				$('#rekapan_aset').text(result.data['asset'])
				$('#rekapan_persentase').text(result.data['persentase'])
			}
		})

		$.ajax({
			url: "<?= base_url(); ?>laporan/get_statistic_by_date/" + $('#outlet_employee_id option:selected').val() + "/" + outlet_id + "/" + $('#date_begin').val() + "/" + $("#date_end").val(),
			success(result) {
				if (result.success) {
					data_outlet = result.data.outlet
					data_aksesoris = result.data.aksesoris
					data_perdana = result.data.perdana
					data_voucher = result.data.voucher
					data_pulsa = result.data.pulsa
				}
			}
		})

		init_table_penjualan_terbaik()

		setTimeout(() => {

			document.getElementById("laporan_chart_container").innerHTML = '&nbsp;';
			document.getElementById("laporan_chart_container").innerHTML = '<canvas id="laporan_chart"></canvas>';
			var ctx = document.getElementById("laporan_chart").getContext("2d");

			var laporan_chart = new Chart(ctx, {
				type: 'bar',
				data: {
					labels: data_outlet,
					datasets: [{
						label: 'Aksesoris',
						data: data_aksesoris,
						backgroundColor: [
							'rgba(255, 99, 132, 0.5)',
							'rgba(255, 99, 132, 0.5)',
							'rgba(255, 99, 132, 0.5)',
							'rgba(255, 99, 132, 0.5)'
						],
					}, {
						label: 'Perdana',
						data: data_perdana,
						backgroundColor: [
							'rgba(54, 162, 235, 0.5)',
							'rgba(54, 162, 235, 0.5)',
							'rgba(54, 162, 235, 0.5)',
							'rgba(54, 162, 235, 0.5)'
						]
					}, {
						label: 'Voucher',
						data: data_voucher,
						backgroundColor: [
							'rgba(255, 206, 86, 0.5)',
							'rgba(255, 206, 86, 0.5)',
							'rgba(255, 206, 86, 0.5)',
							'rgba(255, 206, 86, 0.5)'
						],
					}, {
						label: 'Pulsa',
						data: data_pulsa,
						backgroundColor: [
							'rgba(75, 192, 192, 0.5)',
							'rgba(75, 192, 192, 0.5)',
							'rgba(75, 192, 192, 0.5)',
							'rgba(75, 192, 192, 0.5)'
						]
					}]
				},
				options: {
					scales: {
						yAxes: [{
							ticks: {
								beginAtZero: true
							}
						}]
					}
				}
			})
		}, 500);
	})

	//modal grafik laporan per outlet
	$('.btn_show_outlet').click(function () {

		var today = new Date();
		var firstDay = new Date(today.getFullYear(), today.getMonth(), 1);
		var lastDay = new Date(today.getFullYear(), today.getMonth() + 1, 0);

		var firstDate = firstDay.getFullYear() + '-' + ('0' + (firstDay.getMonth() + 1)).slice(-2) + '-' + ('0' + firstDay.getDate()).slice(-2);
		var lastDate = lastDay.getFullYear() + '-' + ('0' + (lastDay.getMonth() + 1)).slice(-2) + '-' + ('0' + lastDay.getDate()).slice(-2);

		var outlet_id = $(this).attr('id').substring(7);

		$.ajax({
			url: "<?= base_url(); ?>laporan/get_statistic_by_outlet_id/" + outlet_id + "/" + firstDate + "/" + lastDate,
			success(result) {
				if (result.success) {
					data_hari = result.data.label[0]
					data_outlet = result.data.outlet[0]
					data_aksesoris = result.data.aksesoris[0]
					data_perdana = result.data.perdana[0]
					data_voucher = result.data.voucher[0]
					data_pulsa = result.data.pulsa[0]
				}
			}
		})

		setTimeout(() => {
			document.getElementById("laporan_outlet_chart_container").innerHTML = '&nbsp;';
			document.getElementById("laporan_outlet_chart_container").innerHTML = '<canvas id="laporan_outlet_chart"></canvas>';
			var ctx = document.getElementById("laporan_outlet_chart").getContext("2d");

			new Chart(ctx, {
				type: 'line',
				data: {
					labels: data_hari,
					datasets: [{
						data: data_aksesoris,
						label: "Aksesoris",
						borderColor: "rgba(255, 99, 132, 0.5)",
						backgroundColor: [
							'rgba(255, 99, 132, 0.5)',
							'rgba(255, 99, 132, 0.5)',
							'rgba(255, 99, 132, 0.5)',
							'rgba(255, 99, 132, 0.5)'
						],
						fill: false
					}, {
						data: data_perdana,
						label: "Perdana",
						borderColor: "rgba(54, 162, 235, 0.5)",
						backgroundColor: [
							'rgba(54, 162, 235, 0.5)',
							'rgba(54, 162, 235, 0.5)',
							'rgba(54, 162, 235, 0.5)',
							'rgba(54, 162, 235, 0.5)'
						],
						fill: false
					}, {
						data: data_voucher,
						label: "Voucher",
						borderColor: "rgba(255, 206, 86, 0.5)",
						backgroundColor: [
							'rgba(255, 206, 86, 0.5)',
							'rgba(255, 206, 86, 0.5)',
							'rgba(255, 206, 86, 0.5)',
							'rgba(255, 206, 86, 0.5)'
						],
						fill: false
					}, {
						data: data_pulsa,
						label: "Pulsa",
						borderColor: "rgba(75, 192, 192, 0.5)",
						backgroundColor: [
							'rgba(75, 192, 192, 0.5)',
							'rgba(75, 192, 192, 0.5)',
							'rgba(75, 192, 192, 0.5)',
							'rgba(75, 192, 192, 0.5)'
						],
						fill: false
					}]
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Month'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Value'
						}
					}]
				},
				options: {
					title: {
						display: true,
						text: 'Data dari ' + firstDate + ' sampai ' + lastDate
					}
				}
			})
		}, 500);

		$('#modal_show_outlet').find('.modal-header').html('<h5>Catatan transaksi ' + $(this).text() + '</h5>');

		$('#modal_show_outlet').modal({backdrop: 'static', keyboard: false})
	})

	//dropdown outlet
	$('#outlet_id').change(function () {

		outlet_id = $('#outlet_id option:selected').val();

		//dropdown pilih pengguna
		$.ajax({
			url: "<?= base_url(); ?>karyawan/get_karyawan/" + outlet_id,
			success: function (result) {
				if (result.success) {

					user_list = '<option value="">Pilih...</option>'
					user_list += '<option value="0">Semua Shift</option>'

					result.data.forEach(function (outlet) {
						if (outlet['wewenang'] !== 'ADMIN' && outlet['wewenang'] !== 'OWNER' && outlet['outlet_id'] === outlet_id) {
							user_list += "<option value='" + outlet['user_id'] + "'>" + outlet['name'] + "</option>";
						}
					})
					$('#outlet_employee_id').html(user_list)
				}
			}
		})
	});

	//filter item penjualan
	$('#item_type_id').change(function () {

		table_penjualan_terbaik = $("#table_penjualan_terbaik").DataTable({
			destroy: true,
			autoWidth: true,
			width: "0.8%",
			ajax: "<?= base_url(); ?>laporan/get_best_seller_product_by_outlet_id/" + $('#outlet_employee_id option:selected').val() + "/" + $('#item_type_id option:selected').text() + "/" + $('#outlet_id option:selected').val() + "/" + $('#date_begin').val() + "/" + $("#date_end").val(),
			columns: [
				{
					data: "id",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: 'outlet_name'
				},
				{
					data: 'category'
				},
				{
					data: 'product'
				},
				{
					data: 'many'
				},
				{
					data: 'created_date'
				}
			]
		})
	})

	//chart grafik laporan hari ini
	function init_laporan_chart() {
		outlet_id = '<?= $this->uri->segment(3) ?>';

		$.ajax({
			url: "<?= base_url(); ?>laporan/get_statistic_by_date/" + $('#outlet_employee_id option:selected').val() + "/" + outlet_id + "/" + $('#date_begin').val() + "/" + $("#date_end").val(),
			success(result) {
				if (result.success) {
					data_outlet = result.data.outlet
					data_aksesoris = result.data.aksesoris
					data_perdana = result.data.perdana
					data_voucher = result.data.voucher
					data_pulsa = result.data.pulsa
				}
			}
		})

		setTimeout(() => {

			document.getElementById("laporan_chart_container").innerHTML = '&nbsp;';
			document.getElementById("laporan_chart_container").innerHTML = '<canvas id="laporan_chart"></canvas>';
			var ctx = document.getElementById("laporan_chart").getContext("2d");

			var laporan_chart = new Chart(ctx, {
				type: 'bar',
				data: {
					labels: data_outlet,
					datasets: [{
						label: 'Aksesoris',
						data: data_aksesoris,
						backgroundColor: [
							'rgba(255, 99, 132, 0.5)',
							'rgba(255, 99, 132, 0.5)',
							'rgba(255, 99, 132, 0.5)',
							'rgba(255, 99, 132, 0.5)'
						],
					}, {
						label: 'Perdana',
						data: data_perdana,
						backgroundColor: [
							'rgba(54, 162, 235, 0.5)',
							'rgba(54, 162, 235, 0.5)',
							'rgba(54, 162, 235, 0.5)',
							'rgba(54, 162, 235, 0.5)'
						]
					}, {
						label: 'Voucher',
						data: data_voucher,
						backgroundColor: [
							'rgba(255, 206, 86, 0.5)',
							'rgba(255, 206, 86, 0.5)',
							'rgba(255, 206, 86, 0.5)',
							'rgba(255, 206, 86, 0.5)'
						],
					}, {
						label: 'Pulsa',
						data: data_pulsa,
						backgroundColor: [
							'rgba(75, 192, 192, 0.5)',
							'rgba(75, 192, 192, 0.5)',
							'rgba(75, 192, 192, 0.5)',
							'rgba(75, 192, 192, 0.5)'
						]
					}]
				},
				options: {
					scales: {
						yAxes: [{
							ticks: {
								beginAtZero: true
							}
						}]
					}
				}
			})
		}, 500);
	}

	//init datatables table penjualan
	function init_table_penjualan_terbaik() {
		table_penjualan_terbaik = $("#table_penjualan_terbaik").DataTable({
			destroy: true,
			autoWidth: true,
			width: "0.8%",
			ajax: "<?= base_url(); ?>laporan/get_best_seller_product_by_outlet_id/" + $('#outlet_employee_id option:selected').val() + "/" + $('#item_type_id option:selected').text() + "/" + $('#outlet_id option:selected').val() + "/" + $('#date_begin').val() + "/" + $("#date_end").val(),
			columns: [
				{
					data: "id",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: 'outlet_name'
				},
				{
					data: 'category'
				},
				{
					data: 'product'
				},
				{
					data: 'many'
				},
				{
					data: 'created_date'
				}
			]
		})
	}

	//dropdown pilih outlet
	function get_outlet() {
		$.ajax({
			url: "<?= base_url(); ?>outlet/get_all_outlet",
			success: function (result) {

				outlet_id = '<?= $this->uri->segment(3) ?>';

				if (result.success) {
					var outlet_list = ''
					result.data.forEach(function (outlet) {
						if (outlet['id'] === outlet_id) {
							outlet_list += "<option value='" + outlet['id'] + "' selected>" + outlet['name'] + "</option>";
						}
					})

					$('#outlet_id').html(outlet_list)
				}

			}
		})
	}

	//dropdown tipe item
	function get_item_type() {
		$.ajax({
			url: "<?= base_url(); ?>item/get_item",
			type: 'GET',
			success: function (result) {

				if (result.success) {

					item = '<option value="Semua">Semua</option>'

					result.data.forEach(function (data) {
						if (data.id !== 4) {
							item += "<option value='" + data.id + "'>" + data.name + "</option>";
						}
					})

					$('#item_type_id').html(item)
				}
			}
		})
	}

	//dropdown pilih pengguna
	function get_outlet_employee() {
		outlet_id = $('#outlet_id option:selected').val()

		$.ajax({
			url: "<?= base_url(); ?>karyawan/get_karyawan/" + outlet_id,
			success: function (result) {
				if (result.success) {

					var outlet_id = $('#outlet_id option:selected').val()

					var user_list = '<option value="0">Semua Shift</option>'

					result.data.forEach(function (outlet) {
						if (outlet['wewenang'] !== 'ADMIN' && outlet['wewenang'] !== 'OWNER' && outlet['outlet_id'] === outlet_id) {
							user_list += "<option value='" + outlet['user_id'] + "'>" + outlet['name'] + "</option>";
						}
					})
					$('#outlet_employee_id').html(user_list)
				}
			}
		})
	}
</script>

<?php $this->load->view('_layouts/_template/js') ?>
