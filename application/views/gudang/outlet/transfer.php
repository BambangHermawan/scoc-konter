<?php $this->load->view('_layouts/_template/header') ?>

<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div class="card card-outline card-primary">
				<div class="card-header">
					<h3 class="card-title"><b>Transfer Produk Outlet <?= $outlet ?></b></h3>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-2">
							<div class="form-group">
								<label for="month">Tipe Item</label>
								<select class="form-control form-control-sm" id="input_transfer_type_id"
										name="input_transfer_type_id">
								</select>
							</div>
						</div>

						<div class="col-lg-5">
							<label>Filter</label>
							<div class="form-group">
								<button class="btn btn-success btn-sm btn_transfer_filter_submit">Submit</button>
							</div>
						</div>
					</div>
                    <hr>
                    <div class="table-responsive">
                        <table id="table_bulk_transfer"
                               class="table table-striped table-hover dt-responsive display nowrap" style="width:100%">
                            <thead>
                            <tr>
                                <th data-orderable="false">No</th>
                                <th>Kategori</th>
                                <th>Nama</th>
                                <th>Modal</th>
                                <th>Harga</th>
                                <th>Stok Tersedia</th>
                                <th data-orderable="false">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<div class="card card-outline card-primary">
				<div class="card-header">
					<h3 class="card-title"><b>Keranjang Transfer Outlet</b></h3>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-2">
							<div class="form-group">
								<label for="month">Outlet Tujuan</label>
								<select class="form-control form-control-sm" id="input_bulk_cart_transfer_outlet_id"
										name="input_bulk_cart_transfer_outlet_id">
								</select>
							</div>
						</div>

						<div class="col-lg-5">
							<label>Transfer</label>
							<div class="form-group">
								<button class="btn btn-success btn-sm" id="btn_bulk_cart_transfer_outlet_submit">Submit
								</button>
							</div>
						</div>
					</div>
					<hr>
					<div class="table-responsive">
						<table id="table_bulk_cart_transfer"
							   class="table table-striped table-hover dt-responsive display nowrap" style="width:100%">
							<thead>
							<tr>
								<th data-orderable="false">No</th>
								<th>Kategori</th>
								<th>Nama</th>
								<th>Modal</th>
								<th>Harga Jual</th>
								<th>Stok Tersedia</th>
								<th data-orderable="false">Kuantitas</th>
								<th data-orderable="false">Pilih</th>
							</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--Modal transfer barang-->
<div class="modal fade" id="modal_bulk_transfer" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Transfer Barang</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="bulk_transfer_form">
					<input type="text" id="input_bulk_transfer_outlet_product_id" hidden>
					<input type="text" id="input_bulk_transfer_item_product_id" hidden>

					<div class="form-group">
						<div class="col-lg-4">Barang</div>
						<div class="col-lg-12"><input type="text" class="form-control form-control-sm"
													  id="input_bulk_transfer_product_name"
													  name="input_bulk_transfer_product_name" readonly></div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Modal</div>
						<div class="col-lg-12"><input type="text" class="form-control form-control-sm"
													  id="input_bulk_transfer_modal" name="input_bulk_transfer_modal"
													  readonly>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Harga</div>
						<div class="col-lg-12"><input type="number" class="form-control form-control-sm"
													  id="input_bulk_transfer_cost" name="input_bulk_transfer_cost">
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-6">Stok Tersedia</div>
						<div class="col-lg-12"><input type="text" class="form-control form-control-sm"
													  id="input_bulk_transfer_many" name="input_bulk_transfer_many"
													  readonly>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-6">Jumlah Transfer</div>
						<div class="col-lg-12"><input type="number" class="form-control form-control-sm"
													  id="input_bulk_transfer_quantity"
													  name="input_bulk_transfer_quantity"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"
						id="btn_bulk_transfer_submit">Simpan
				</button>
			</div>
		</div>
	</div>
</div>

<!--Modal ubah data keranjang-->
<div class="modal fade" id="modal_edit_bulk_transfer" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Ubah Barang Keranjang</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="edit_bulk_transfer_form">
					<input type="text" id="edit_bulk_transfer_outlet_product_id" hidden>
					<input type="text" id="edit_bulk_transfer_item_product_id" hidden>
					<div class="form-group">
						<div class="col-lg-4">Barang</div>
						<div class="col-lg-12"><input type="text" class="form-control form-control-sm"
													  id="edit_bulk_transfer_product_name"
													  name="edit_bulk_transfer_product_name" readonly></div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Modal</div>
						<div class="col-lg-12"><input type="text" class="form-control form-control-sm"
													  id="edit_bulk_transfer_modal" name="edit_bulk_transfer_modal"
													  readonly>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Harga</div>
						<div class="col-lg-12"><input type="number" class="form-control form-control-sm"
													  id="edit_bulk_transfer_cost" name="edit_bulk_transfer_cost">
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-6">Stok Tersedia</div>
						<div class="col-lg-12"><input type="text" class="form-control form-control-sm"
													  id="edit_bulk_transfer_many" name="edit_bulk_transfer_many"
													  readonly>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-6">Jumlah Transfer</div>
						<div class="col-lg-12"><input type="number" class="form-control form-control-sm"
													  id="edit_bulk_transfer_quantity"
													  name="edit_bulk_transfer_quantity"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"
						id="btn_edit_bulk_transfer_submit">Simpan
				</button>
			</div>
		</div>
	</div>
</div>

<script>
	// init bulk transfer
	$(document).ready(function () {
		init_table_bulk_transfer_outlet()
		init_table_bulk_cart_transfer()
		get_item_type()
		get_outlet()
	});

	//Filter data produk bulk transfer
	$('.btn_transfer_filter_submit').click(function () {

		button = "<button class='btn btn-warning btn-sm' id='btn_add_bulk_transfer_click'>\
            <i class='fas fa-plus'></i>&nbsp;Tambah</button>"

		table_bulk_transfer = $("#table_bulk_transfer").DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			autoWidth: true,
			ajax: "<?= base_url(); ?>_gudang/outlet/get_all_product_by_outlet_id/" + $('#input_transfer_type_id').val() + "/" + "<?= $this->uri->segment(4); ?>",
			columns: [
				{
					data: "id",
					width: "0.8%",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: "category_name"
				},
				{
					data: "name"
				},
				{
					data: "modal"
				},
				{
					data: "many"
				},
				{
					width: "0.8%",
					defaultContent: button
				},
			],
		})

		init_table_bulk_cart_transfer()
	})

	//validasi form tambah keranjang bulk transfer
	$("#bulk_transfer_form").validate({
		rules: {
			input_bulk_transfer_cost: {
				required: true,
				digits: true,
				maxlength: 7
			},
			input_bulk_transfer_quantity: {
				required: true,
				digits: true,
				maxlength: 3
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
        unhighlight: function (element, errorClass, validClass) {
            $(element).addClass("is-valid").removeClass("is-invalid");
        }
    });

    //button modal tambah data keranjang bulk transfer
    $("#table_bulk_transfer").on('click', 'tr #btn_add_bulk_transfer_click', function () {

		tr = $(this).closest('tr')
		data = table_bulk_transfer.row(tr).data()

		$("#input_bulk_transfer_outlet_product_id").val(data.id)
		$("#input_bulk_transfer_item_product_id").val(data.item_product_id)
		$("#input_bulk_transfer_product_name").val(data.name)
		$("#input_bulk_transfer_cost").val(data.cost.replace(',', ''))
		$("#input_bulk_transfer_many").val(data.many)
		$("#input_bulk_transfer_modal").val(data.modal)
		$("#input_bulk_transfer_type_id").val(data.type_name)
		$('#modal_bulk_transfer').modal({backdrop: 'static', keyboard: false})

	})

	//tambah data keranjang bulk transfer
	$("#btn_bulk_transfer_submit").click(function () {

		if ($('#bulk_transfer_form').valid()) {
			$.ajax({
				url: "<?= base_url(); ?>transfer/add_cart_transfer_from_outlet",
				type: "POST",
				data: {
					outlet_id: "<?= $this->uri->segment(4); ?>",
					outlet_product_id: $('#input_bulk_transfer_outlet_product_id').val(),
					item_product_id: $('#input_bulk_transfer_item_product_id').val(),
					name: $('#input_bulk_transfer_product_name').val(),
					modal: $('#input_bulk_transfer_modal').val(),
					cost: $('#input_bulk_transfer_cost').val(),
					quantity: $('#input_bulk_transfer_quantity').val()
				},
				success: function (result) {
					if (result.success) {

						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						init_table_bulk_transfer_outlet()
						init_table_bulk_cart_transfer()

					} else {

						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});
					}
				}
			})
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});
		}

	})

	//validasi form tambah keranjang bulk transfer
	$("#edit_bulk_transfer_form").validate({
		rules: {
			edit_bulk_transfer_cost: {
				required: true,
				digits: true,
				maxlength: 7
			},
			edit_bulk_transfer_quantity: {
				required: true,
				digits: true,
				maxlength: 3
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//button modal edit data keranjang bulk transfer
	$("#table_bulk_cart_transfer").on('click', 'tr #btn_edit_bulk_cart_transfer_click', function () {

		tr = $(this).closest('tr')
		data = table_bulk_cart_transfer.row(tr).data()

		$("#edit_bulk_transfer_outlet_product_id").val(data.cart_id)
		$("#edit_bulk_transfer_item_product_id").val(data.item_product_id)
		$("#edit_bulk_transfer_product_name").val(data.product_name)
		$("#edit_bulk_transfer_many").val(data.stock)
		$("#edit_bulk_transfer_modal").val(data.modal)
		$("#edit_bulk_transfer_cost").val(data.cost.replace(',', ''))
		$("#edit_bulk_transfer_quantity").val(data.quantity.replace(',', ''))
		$('#modal_edit_bulk_transfer').modal({backdrop: 'static', keyboard: false})

	})

	//ubah data keranjang bulk transfer
	$("#btn_edit_bulk_transfer_submit").click(function () {

		if ($('#edit_bulk_transfer_form').valid()) {
			$.ajax({
				url: "<?= base_url(); ?>transfer/edit_cart_transfer_from_outlet",
				type: "POST",
				data: {
					outlet_id: "<?= $this->uri->segment(4); ?>",
					cart_id: $('#edit_bulk_transfer_outlet_product_id').val(),
					item_product_id: $('#edit_bulk_transfer_item_product_id').val(),
					cost: $('#edit_bulk_transfer_cost').val(),
					quantity: $('#edit_bulk_transfer_quantity').val()
				},
				success: function (result) {
					if (result.success) {

						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						init_table_bulk_transfer_outlet()
						init_table_bulk_cart_transfer()

					} else {

						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});
					}
				}
			})
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});
		}

	})

	//tambah data transfer ke outlet
	$("#btn_bulk_cart_transfer_outlet_submit").click(function () {

		outlet_id = $("#input_bulk_cart_transfer_outlet_id").val();
		name = $("#input_bulk_cart_transfer_outlet_id option:selected").text();

		if (outlet_id == 0) {
			Swal.fire({
				title: 'Peringatan!',
				text: "Pilih outlet terlebih dahulu",
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});
		} else {
			Swal.fire({
				title: 'Peringatan!',
				text: 'Anda yakin akan mentransfer semua produk ke outlet ' + name + ' ?',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Ya, transfer produk ini!'
			}).then((result) => {
				if (result.value) {

					$.ajax({
						url: "<?= base_url(); ?>transfer/bulk_transfer_to_outlet_from_outlet",
						type: "POST",
						data: {
							sender_outlet_id: "<?= $this->uri->segment(4) ?>",
							destination_outlet_id: $("#input_bulk_cart_transfer_outlet_id").val(),
							name: $("#input_bulk_cart_transfer_outlet_id option:selected").text()
						},
						success(result) {

							if (result.success) {
								Swal.fire({
									title: 'Status Transfer',
									html: result.message,
									icon: result.type,
									showConfirmButton: true
								});

								init_table_bulk_transfer_outlet()
								init_table_bulk_cart_transfer()

							} else {
								Swal.fire({
									title: 'gagal',
									html: result.message,
									icon: result.type,
									showConfirmButton: true
								});
							}
						}
					})
				}
			})
		}
	})

	//button modal hapus data bulk cart transfer
	$("#table_bulk_cart_transfer").on('click', 'tr #btn_delete_bulk_cart_transfer_click', function () {
		tr = $(this).closest('tr')
		data = table_bulk_cart_transfer.row(tr).data()
		id = data.cart_id

		Swal.fire({
			title: 'Peringatan!',
			text: 'Apakah Anda yakin akan menghapus outlet ' + data.product_name + ' ?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya, hapus data ini!'
		}).then((result) => {
			if (result.value) {

				$.ajax({
					url: "<?= base_url(); ?>transfer/delete_cart_transfer",
					type: "POST",
					data: {id: id},
					success(result) {
						if (result.success) {
							Swal.fire({
								title: 'Berhasil',
								text: result.message,
								icon: result.type,
								showConfirmButton: false,
								timer: 1500
							});

							init_table_bulk_transfer_outlet()
							init_table_bulk_cart_transfer()

						} else {
							Swal.fire({
								title: 'gagal',
								text: result.message,
								icon: result.type,
								showConfirmButton: false,
								timer: 1500
							});
						}
					}
				})
			}
		})
	})

	//dropdown tipe item
	function get_item_type() {
		$.ajax({
			url: "<?= base_url(); ?>item/get_item",
			type: 'GET',
			success: function (result) {
				if (result.success) {

					type = '<option value="0">Pilih...</option>';
					category = '<option value="0">Pilih...</option>';

					result.data.forEach(function (data) {
						if (data.id != 4) {
							type += "<option value='" + data.id + "'>" + data.name + "</option>";
							category += "<option value='" + data.id + "'>" + data.name + "</option>";
						}
					})

					$('#input_transfer_type_id').html(type)
				}
			}
		})
	}

	//dropdown pilih outlet
	function get_outlet() {
		$.ajax({
			url: "<?= base_url(); ?>outlet/get_all_outlet",
			success: function (result) {

				if (result.success) {

					outlet_id = "<?= $this->uri->segment(4) ?>"
					var outlet_list = '<option value="">Pilih...</option>'
					result.data.forEach(function (outlet) {
						if (outlet['id'] !== outlet_id) {
							outlet_list += "<option value='" + outlet['id'] + "'>" + outlet['name'] + "</option>";
						}
					})

					$('#input_bulk_cart_transfer_outlet_id').html(outlet_list)
				}
			}
		})
	}

	//init datatables table bulk transfer
	function init_table_bulk_transfer_outlet() {
		button = "<button class='btn btn-warning btn-sm' id='btn_add_bulk_transfer_click'>\
            <i class='fas fa-plus'></i>&nbsp;Tambah</button>"

		table_bulk_transfer = $("#table_bulk_transfer").DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			autoWidth: true,
			ajax: "<?= base_url(); ?>_gudang/outlet/get_all_product_by_outlet_id/0/<?= $this->uri->segment(4); ?>",
			columns: [
				{
					data: "id",
					width: "0.8%",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: "category_name"
				},
				{
					data: "name"
				},
				{
					data: "modal"
				},
				{
					data: "cost"
				},
                {
                    data: "many"
                },
                {
                    width: "0.8%",
                    defaultContent: button
                },
            ],
        })
    }

	//init datatables table bulk cart transfer
	function init_table_bulk_cart_transfer() {
		button = "<button class='btn btn-warning btn-sm' id='btn_edit_bulk_cart_transfer_click'>\
            <i class='fas fa-edit'></i>&nbsp;Ubah</button>&nbsp;"
		button += "<button class='btn btn-danger btn-sm' id='btn_delete_bulk_cart_transfer_click'>\
            <i class='fas fa-trash'></i>&nbsp;Hapus</button>"

		table_bulk_cart_transfer = $("#table_bulk_cart_transfer").DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			autoWidth: true,
			ajax: "<?= base_url(); ?>transfer/get_all_cart_transfer/" + <?= $this->uri->segment(4) ?>,
			columns: [
				{
					data: "id",
					width: "0.8%",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: "category_name"
				},
				{
					data: "product_name"
				},
				{
					data: "modal"
				},
				{
					data: "cost"
				},
				{
					data: "stock"
				},
				{
					data: "quantity"
				},
				{
					width: "2%",
					defaultContent: button
				},
			],
		})
	}

	//reset kolom setiap modal ditutup
	$('[data-dismiss=modal]').on('click', function (e) {

		$('.form-control').removeClass("is-valid");
		$('.form-control').removeClass("is-invalid");

		var $t = $(this),
				target = $t[0].href || $t.data("target") || $t.parents('.modal') || [];

		$(target)
				.find("input,textarea,select")
				.val('')
				.end()
				.find("input[type=checkbox], input[type=radio]")
				.prop("checked", "")
				.end();
	});
</script>

<?php $this->load->view('_layouts/_template/js') ?>
