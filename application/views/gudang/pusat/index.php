<?php $this->load->view('_layouts/_template/header') ?>

<div class="container-fluid">
	<?php $this->load->view('gudang/modal/gudang_pusat'); ?>
	<div class="row">
		<div class="col-12">
			<div class="card card-primary">
				<div class="card-header">
					<h3 class="card-title"><b>Gudang Pusat</b></h3>
					<div class="float-right">
						<a href="<?= base_url('transfer'); ?>" class="btn btn-info btn-sm"><i
									class="fas fa-exchange-alt"></i>Transfer</a>
						<button class="btn btn-danger btn-sm" id="btn_add_export"><i class="fas fa-download"></i> Export
						</button>
						<button class="btn btn-success btn-sm" id="btn_add_impor"><i class="fas fa-upload"></i> Import
						</button>
						<button class="btn btn-warning btn-sm" id="btn_add_product"><i class="fas fa-plus"></i> Produk
						</button>
					</div>
				</div>
				<div class="card-body">
					<ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="semua-tab" data-toggle="pill"
							   href="#semua" role="tab" aria-controls="semua"
							   aria-selected="true">Semua</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="aksesoris-tab" data-toggle="pill"
							   href="#aksesoris" role="tab" aria-controls="aksesoris"
							   aria-selected="false">Aksesoris</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="perdana-tab" data-toggle="pill"
							   href="#perdana" role="tab"
							   aria-controls="perdana" aria-selected="false">Perdana</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="voucher-tab" data-toggle="pill"
							   href="#voucher" role="tab"
							   aria-controls="voucher" aria-selected="false">Voucher</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="pulsa-tab" data-toggle="pill"
							   href="#pulsa" role="tab"
							   aria-controls="pulsa" aria-selected="false">Pulsa</a>
						</li>
					</ul>
				</div>
				<div class="card-body">
					<div class="tab-content" id="custom-tabs-three-tabContent">
						<div class="tab-pane fade show active" id="semua" role="tabpanel"
							 aria-labelledby="semua-tab">
							<?php $this->load->view('gudang/pusat/semua') ?>
						</div>
						<div class="tab-pane fade" id="aksesoris" role="tabpanel"
							 aria-labelledby="aksesoris-tab">
							<?php $this->load->view('gudang/pusat/aksesoris') ?>
						</div>
						<div class="tab-pane fade" id="perdana" role="tabpanel"
							 aria-labelledby="perdana-tab">
							<?php $this->load->view('gudang/pusat/perdana') ?>
						</div>
						<div class="tab-pane fade" id="voucher" role="tabpanel"
							 aria-labelledby="voucher-tab">
							<?php $this->load->view('gudang/pusat/voucher') ?>
						</div>
						<div class="tab-pane fade" id="pulsa" role="tabpanel"
							 aria-labelledby="pulsa-tab">
							<?php $this->load->view('gudang/pusat/pulsa') ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('_layouts/_template/js') ?>

<script>

	//init halaman index gudang
	$(document).ready(function () {
		var today = new Date();
		date = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2)

		$("#input_product_entry_date").val(date)

		get_item_type()
		get_outlet()
	})

	//validasi form produk baru
	$("#add_product_form").validate({
		rules: {
			input_product_type_id: {
				required: true,
				maxlength: 2
			},
			input_product_category_id: {
				required: true,
				maxlength: 2
			},
			input_product_name: {
				required: true,
				maxlength: 50
			},
			input_product_modal: {
				required: true,
				digits: true,
				maxlength: 8
			},
			input_product_many: {
				required: true,
				digits: true,
				maxlength: 3
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//modal tambah produk baru
	$("#btn_add_product").click(function () {
		$("#modal_add_product").modal({backdrop: 'static', keyboard: false})
	})

	//simpan produk baru
	$("#btn_add_new_product").click(function () {

		type = $("#input_product_type_id option:selected").text();

		if ($('#add_product_form').valid()) {

			$.ajax({
				url: "<?= base_url(); ?>produk/add_product",
				type: "POST",
				data: {
					category_id: $('#input_product_category_id').val(),
					name: $('#input_product_name').val(),
					modal: $('#input_product_modal').val(),
					many: $('#input_product_many').val(),
					entry_date: $('#input_product_entry_date').val()
				},
				success: function (result) {

					if (result.success) {
						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						if (type === 'Aksesoris') {
							init_table_aksesoris()
						} else if (type === 'Perdana') {
							init_table_perdana()
						} else if (type === 'Voucher') {
							init_table_voucher()
						} else if (type === 'Pulsa') {
							init_table_pulsa()
						}

						init_table_semua()

					} else {
						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});
					}
				}
			})
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});
		}
	});

	//validasi form edit barang
	$("#edit_product_form").validate({
		rules: {
			input_edit_name: {
				required: true,
				maxlength: 50
			},
			input_edit_many: {
				required: true,
				digits: true,
				maxlength: 3
			},
			input_edit_modal: {
				required: true,
				digits: true,
				maxlength: 8
			},
			input_edit_note: {
				required: true
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//ubah data produk gudang
	$("#btn_save_edit").click(function () {

		type = $("#input_edit_type_id").val();

		if ($('#edit_product_form').valid()) {
			$.ajax({
				url: "<?= base_url(); ?>produk/edit_product",
				type: "POST",
				data: {
					id: $("#input_edit_id").val(),
					name: $("#input_edit_name").val(),
					modal: $("#input_edit_modal").val(),
					many: $("#input_edit_many").val(),
					note: $("#input_edit_note").val(),
				},
				success(result) {

					if (result.success) {
						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						if (type === 'Aksesoris') {
							init_table_aksesoris()
						} else if (type === 'Perdana') {
							init_table_perdana()
						} else if (type === 'Voucher') {
							init_table_voucher()
						} else if (type === 'Pulsa') {
							init_table_pulsa()
						}

						init_table_semua()

					} else {
						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});
					}
				}
			})
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});
		}
	})

	//button modal impor data
	$("#btn_add_impor").click(function () {
		$("#modal_impor").modal({backdrop: 'static', keyboard: false})
	})

	//button modal export data
	$("#btn_add_export").click(function () {
		$("#modal_export").modal({backdrop: 'static', keyboard: false})
	})

	//validasi form transfer barang
	$("#transfer_barang_form").validate({
		rules: {
			input_transfer_outlet_id: {
				required: true,
				maxlength: 11
			},
			input_transfer_many: {
				required: true,
				maxlength: 3
			},
			input_transfer_cost: {
				required: true,
				maxlength: 7
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	$("#input_transfer_outlet_id").change(function () {
		outlet_id = $(this).val()
		item_product_id = $("#input_transfer_product_id").val()

		$.ajax({
			url: "<?= base_url(); ?>transfer/get_product_transfer_status/" + outlet_id + "/" + item_product_id,
			success: function (result) {
				if (result.success) {
					$("#input_transfer_cost").val(result.data)
				}
			}
		})
	})

	//transfer dari gudang ke outlet
	$("#btn_transfer").click(function () {

		type = $("#input_transfer_type_id").val();

		if ($('#transfer_barang_form').valid()) {

			$.ajax({
				url: "<?= base_url(); ?>gudang/transfer_from_pusat",
				type: "POST",
				data: {
					product_id: $('#input_transfer_product_id').val(),
					outlet_id: $('#input_transfer_outlet_id').val(),
					many: $('#input_transfer_many').val(),
					cost: $('#input_transfer_cost').val()
				},
				success: function (result) {

					if (result.success) {
						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						if (type == 'Aksesoris') {
							init_table_aksesoris()
						} else if (type == 'Perdana') {
							init_table_perdana()
						} else if (type == 'Voucher') {
							init_table_voucher()
						} else if (type == 'Pulsa') {
							init_table_pulsa()
						}

						init_table_semua()

					} else {
						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});
					}
				}
			})
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});
		}
	})

	//validasi form tambah stok barang
	$("#add_stock_form").validate({
		rules: {
			input_add_stock: {
				required: true,
				digits: true,
				maxlength: 3
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//simpan tambah stok barang
	$("#btn_add_stock_submit").click(function () {

		type = $('#input_add_stock_type_id').val()

		if ($('#add_stock_form').valid()) {

			$.ajax({
				url: "<?= base_url(); ?>produk/add_stock_product",
				type: "POST",
				data: {
					product_id: $('#input_add_stock_id').val(),
					name: $('#input_add_stock_name').val(),
					many: $('#input_add_stock').val()
				},
				success: function (result) {

					if (result.success) {
						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						if (type == 'Aksesoris') {
							init_table_aksesoris()
						} else if (type == 'Perdana') {
							init_table_perdana()
						} else if (type == 'Voucher') {
							init_table_voucher()
						} else if (type == 'Pulsa') {
							init_table_pulsa()
						}

						init_table_semua()

					} else {
						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});
					}
				}
			})
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});
		}
	})

	//salah transfer dari gudang ke pusat
	$("#btn_salah_transfer").click(function () {

		type = $("#input_transfer_type_id").val();

		product_id = $('#input_transfer_product_id').val()
		outlet_id = $('#input_transfer_outlet_id').val()
		many = $('#input_transfer_many').val()

		Swal.fire({
			title: 'Apakah anda yakin akan menarik barang dari outlet?',
			text: "Pastikan jumlah penarikan barang sudah benar!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya, saya salah transfer!'
		}).then((result) => {
			if (result.value) {

				$.ajax({
					url: "<?= base_url(); ?>gudang/salah_transfer_from_pusat",
					type: "POST",
					data: {
						product_id: product_id,
						outlet_id: outlet_id,
						many: many
					},
					success(result) {

						if (result.success) {
							Swal.fire({
								title: 'Berhasil',
								text: result.message,
								icon: result.type,
								showConfirmButton: false,
								timer: 1500
							});

							if (type === 'Aksesoris') {
								init_table_aksesoris()
							} else if (type === 'Perdana') {
								init_table_perdana()
							} else if (type === 'Voucher') {
								init_table_voucher()
							} else if (type === 'Pulsa') {
								init_table_pulsa()
							}

							init_table_semua()

						} else {
							Swal.fire({
								title: 'gagal',
								text: result.message,
								icon: result.type,
								showConfirmButton: false,
								timer: 1500
							});
						}
					}
				})
			}
		})
	})

	//dropdown tipe produk
	$("#input_product_type_id").change(function () {

		type_id = $('#input_product_type_id').val()

		$.ajax({
			url: "<?= base_url(); ?>kategori/get_category_by_id/" + type_id,
			type: 'GET',
			success: function (result) {

				if (result.success) {

					var product_category = '<option value="">Pilih...</option>'
					result.data.forEach(function (category) {
						product_category += "<option value='" + category['id'] + "'>" + category['name'] + "</option>";
					})

					$("#input_product_category_id").html(product_category)

				}
			}
		})
	})

	//dropdown unit produk
	$("#input_product_unit").change(function () {
		if ($(this).val() === 'Unit') {
			$("#input_product_many").removeAttr('disabled')
		} else {
			$("#input_product_many").val('')
			$("#input_product_many").attr('disabled', true)
		}
	})

	//reset kolom setiap modal ditutup
	$('[data-dismiss=modal]').on('click', function (e) {

		$('.form-control').removeClass("is-valid");
		$('.form-control').removeClass("is-invalid");

		var $t = $(this),
				target = $t[0].href || $t.data("target") || $t.parents('.modal') || [];

		$(target)
				.find("input,textarea,select")
				.val('')
				.end()
				.find("input[type=checkbox], input[type=radio]")
				.prop("checked", "")
				.end();
	})

	//dropdown pilih outlet
	function get_outlet() {
		$.ajax({
			url: "<?= base_url(); ?>outlet/get_all_outlet",
			success: function (result) {

				if (result.success) {

					var outlet_list = '<option value="">Pilih...</option>'
					result.data.forEach(function (outlet) {
						outlet_list += "<option value='" + outlet['id'] + "'>" + outlet['name'] + "</option>";
					})

					$('#input_transfer_outlet_id').html(outlet_list)

				}

			}
		})
	}

	//dropdown tipe item
	function get_item_type() {
		$.ajax({
			url: "<?= base_url(); ?>item/get_item",
			type: 'GET',
			success: function (result) {

				if (result.success) {

					result_new_category = '<option value="">Pilih...</option>'
					result_new_product = '<option value="">Pilih...</option>'

					result.data.forEach(function (data) {
						if (data.id != 4) {
							result_new_category += "<option value='" + data.id + "'>" + data.name + "</option>";
							result_new_product += "<option value='" + data.id + "'>" + data.name + "</option>";
						}
					})

					$('#input_product_type_id').html(result_new_product)
					$('#input_category_type_id').html(result_new_product)
				}
			}
		})
	}

</script>
