-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: scoc_konter
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.10-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `item_category`
--

DROP TABLE IF EXISTS `item_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_category` (
                                 `id`            int(11)     NOT NULL AUTO_INCREMENT,
                                 `item_type_id`  int(11)     NOT NULL,
                                 `name`          varchar(50) NOT NULL,
                                 `created_by`    varchar(50) DEFAULT NULL,
                                 `created_date`  datetime    DEFAULT current_timestamp(),
                                 `modified_by`   varchar(50) DEFAULT NULL,
                                 `modified_date` datetime    DEFAULT NULL,
                                 `deleted_by`    varchar(50) DEFAULT NULL,
                                 `deleted_date`  datetime    DEFAULT NULL,
                                 PRIMARY KEY (`id`),
                                 KEY `item_type_id` (`item_type_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 23
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_category`
--

LOCK TABLES `item_category` WRITE;
/*!40000 ALTER TABLE `item_category`
    DISABLE KEYS */;
INSERT INTO `item_category`
VALUES (1, 2, 'Telkomsel', 'bambang hermawan', '2020-09-10 10:23:12', NULL, NULL, NULL, NULL),
       (2, 2, 'XL', 'bambang hermawan', '2020-09-10 10:23:28', NULL, NULL, NULL, NULL),
       (3, 2, 'Indosat', 'bambang hermawan', '2020-09-10 10:23:40', NULL, NULL, NULL, NULL),
       (4, 2, 'Axis', 'bambang hermawan', '2020-09-10 10:23:48', NULL, NULL, NULL, NULL),
       (5, 2, 'Smartfren', 'bambang hermawan', '2020-09-10 10:23:58', NULL, NULL, NULL, NULL),
       (6, 2, 'Tri', 'bambang hermawan', '2020-09-10 10:24:07', NULL, NULL, NULL, NULL),
       (7, 3, 'Telkomsel', 'bambang hermawan', '2020-09-10 10:24:38', NULL, NULL, NULL, NULL),
       (8, 3, 'XL', 'bambang hermawan', '2020-09-10 10:24:44', NULL, NULL, NULL, NULL),
       (9, 3, 'Indosat', 'bambang hermawan', '2020-09-10 10:24:52', NULL, NULL, NULL, NULL),
       (10, 3, 'Axis', 'bambang hermawan', '2020-09-10 10:24:59', NULL, NULL, NULL, NULL),
       (11, 3, 'Smartfren', 'bambang hermawan', '2020-09-10 10:25:06', NULL, NULL, NULL, NULL),
       (12, 3, 'Tri', 'bambang hermawan', '2020-09-10 10:25:12', NULL, NULL, NULL, NULL),
       (13, 1, 'Power Bank', 'bambang hermawan', '2020-09-10 10:26:01', NULL, NULL, NULL, NULL),
       (14, 1, 'Headset (Earphone)', 'bambang hermawan', '2020-09-10 10:26:21', NULL, NULL, NULL, NULL),
       (15, 1, 'Memory Card', 'bambang hermawan', '2020-09-10 10:26:36', NULL, NULL, NULL, NULL),
       (16, 1, 'Baterai', 'bambang hermawan', '2020-09-10 10:26:52', NULL, NULL, NULL, NULL),
       (17, 1, 'Charger', 'bambang hermawan', '2020-09-10 10:26:59', NULL, NULL, NULL, NULL),
       (18, 1, 'Kabel Data', 'bambang hermawan', '2020-09-10 10:27:07', NULL, NULL, NULL, NULL),
       (19, 1, 'Screen Guard', 'bambang hermawan', '2020-09-10 10:27:16', NULL, NULL, NULL, NULL),
       (20, 1, 'Casing & Cover', 'bambang hermawan', '2020-09-10 10:27:30', NULL, NULL, NULL, NULL),
       (21, 1, 'Speaker', 'bambang hermawan', '2020-09-10 10:31:07', NULL, NULL, NULL, NULL),
       (22, 1, 'Adaptor', 'bambang hermawan', '2020-09-10 10:31:27', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `item_category`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_outlet`
--

DROP TABLE IF EXISTS `item_outlet`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_outlet` (
                               `id`                      int(11)  NOT NULL AUTO_INCREMENT,
                               `item_outlet_category_id` int(11)  NOT NULL,
                               `name`                    text     NOT NULL,
                               `unit`                    text     NOT NULL,
                               `modal`                   int(11)  NOT NULL,
                               `cost`                    int(7)   NOT NULL,
                               `discount`                int(2)   NOT NULL,
                               `many`                    int(3)   NOT NULL,
                               `qrcode`                  varchar(100)      DEFAULT NULL,
                               `created_by`              varchar(50)       DEFAULT NULL,
                               `created_date`            datetime NOT NULL DEFAULT current_timestamp(),
                               `modified_by`             varchar(50)       DEFAULT NULL,
                               `modified_date`           datetime          DEFAULT NULL,
                               `deleted_by`              varchar(50)       DEFAULT NULL,
                               `deleted_date`            datetime          DEFAULT NULL,
                               PRIMARY KEY (`id`),
                               KEY `item_outlet_category_id` (`item_outlet_category_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_outlet`
--

LOCK TABLES `item_outlet` WRITE;
/*!40000 ALTER TABLE `item_outlet`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `item_outlet`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_outlet_category`
--

DROP TABLE IF EXISTS `item_outlet_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_outlet_category` (
                                        `id`            int(11)     NOT NULL AUTO_INCREMENT,
                                        `name`          text        NOT NULL,
                                        `outlet_id`     int(11)     NOT NULL,
                                        `modal`         varchar(10) NOT NULL DEFAULT '0',
                                        `type_id`       int(11)     NOT NULL DEFAULT 4,
                                        `created_by`    varchar(50)          DEFAULT NULL,
                                        `created_date`  datetime    NOT NULL DEFAULT current_timestamp(),
                                        `modified_by`   varchar(50)          DEFAULT NULL,
                                        `modified_date` datetime             DEFAULT NULL,
                                        `deleted_by`    varchar(50)          DEFAULT NULL,
                                        `deleted_date`  datetime             DEFAULT NULL,
                                        PRIMARY KEY (`id`),
                                        KEY `item_outlet_id` (`outlet_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_outlet_category`
--

LOCK TABLES `item_outlet_category` WRITE;
/*!40000 ALTER TABLE `item_outlet_category`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `item_outlet_category`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_product`
--

DROP TABLE IF EXISTS `item_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_product` (
                                `id`               int(11) NOT NULL AUTO_INCREMENT,
                                `item_category_id` int(11) NOT NULL,
                                `name`             varchar(50) NOT NULL,
                                `modal`            varchar(8) NOT NULL,
                                `many`             int(3) NOT NULL,
                                `qrcode`           varchar(100)      DEFAULT NULL,
                                `entry_date`       datetime          DEFAULT NULL,
                                `created_by`       varchar(50)       DEFAULT NULL,
                                `created_date`     datetime NOT NULL DEFAULT current_timestamp(),
                                `modified_by`      varchar(50)       DEFAULT NULL,
                                `modified_date`    datetime          DEFAULT NULL,
                                `deleted_by`       varchar(50)       DEFAULT NULL,
                                `deleted_date`     datetime          DEFAULT NULL,
                                PRIMARY KEY (`id`),
                                KEY `category_id` (`item_category_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_product`
--

LOCK TABLES `item_product` WRITE;
/*!40000 ALTER TABLE `item_product`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `item_product`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_product_outlet`
--

DROP TABLE IF EXISTS `item_product_outlet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_product_outlet` (
                                       `id`              int(11) NOT NULL AUTO_INCREMENT,
                                       `item_product_id` int(11) NOT NULL,
                                       `outlet_id`       int(11) NOT NULL,
                                       `cost`            int(7) NOT NULL,
                                       `discount`        int(2) NOT NULL,
                                       `many`            int(3) NOT NULL,
                                       `discount_until`  date              DEFAULT NULL,
                                       `qrcode`          varchar(100)      DEFAULT NULL,
                                       `created_by`      varchar(50)       DEFAULT NULL,
                                       `created_date`    datetime NOT NULL DEFAULT current_timestamp(),
                                       `modified_by`     varchar(50)       DEFAULT NULL,
                                       `modified_date`   datetime          DEFAULT NULL,
                                       `deleted_by`      varchar(50)       DEFAULT NULL,
                                       `deleted_date`    datetime          DEFAULT NULL,
                                       PRIMARY KEY (`id`),
                                       KEY `item_outlet_category_id` (`item_product_id`),
                                       KEY `outlet_id` (`outlet_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_product_outlet`
--

LOCK TABLES `item_product_outlet` WRITE;
/*!40000 ALTER TABLE `item_product_outlet`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `item_product_outlet`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_transfer_cart`
--

DROP TABLE IF EXISTS `item_transfer_cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_transfer_cart` (
                                      `id`              int(11)     NOT NULL AUTO_INCREMENT,
                                      `outlet_id`       int(11) DEFAULT NULL,
                                      `item_product_id` int(11)     NOT NULL,
                                      `name`            varchar(50) NOT NULL,
                                      `modal`           varchar(8)  NOT NULL,
                                      `cost`            int(7)      NOT NULL,
                                      `quantity`        int(3)      NOT NULL,
                                      `status`          varchar(50) DEFAULT NULL,
                                      `created_by`      varchar(50) DEFAULT NULL,
                                      `created_date`    datetime    NOT NULL DEFAULT current_timestamp(),
                                      `modified_by`     varchar(50) DEFAULT NULL,
                                      `modified_date`   datetime DEFAULT NULL,
                                      `deleted_by`      varchar(50) DEFAULT NULL,
                                      `deleted_date`    datetime DEFAULT NULL,
                                      PRIMARY KEY (`id`)
) ENGINE = MyISAM
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_transfer_cart`
--

LOCK TABLES `item_transfer_cart` WRITE;
/*!40000 ALTER TABLE `item_transfer_cart`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `item_transfer_cart`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_type`
--

DROP TABLE IF EXISTS `item_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_type`
(
    `id`            int(11)     NOT NULL AUTO_INCREMENT,
    `name`          varchar(20) NOT NULL,
    `created_by`    varchar(50)          DEFAULT NULL,
    `created_date`  datetime    NOT NULL DEFAULT current_timestamp(),
    `modified_by`   varchar(50)          DEFAULT NULL,
    `modified_date` datetime             DEFAULT NULL,
    `deleted_by`    varchar(50)          DEFAULT NULL,
    `deleted_date`  datetime             DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 5
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_type`
--

LOCK TABLES `item_type` WRITE;
/*!40000 ALTER TABLE `item_type`
    DISABLE KEYS */;
INSERT INTO `item_type`
VALUES (1, 'Aksesoris', 'bambang hermawan', '2020-07-06 01:07:17', NULL, NULL, NULL, NULL),
       (2, 'Perdana', 'bambang hermawan', '2020-07-06 01:07:17', NULL, NULL, NULL, NULL),
       (3, 'Voucher', 'bambang hermawan', '2020-07-06 01:07:17', NULL, NULL, NULL, NULL),
       (4, 'Pulsa', 'bambang hermawan', '2020-07-06 01:07:17', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `item_type`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `log` (
                       `id`            int(11) NOT NULL AUTO_INCREMENT,
                       `user_id`       int(11) NOT NULL,
                       `outlet_id`     varchar(10)       DEFAULT NULL,
                       `target`        varchar(30)       DEFAULT NULL,
                       `object_id`     int(11)           DEFAULT NULL,
                       `activity`      text NOT NULL,
                       `note`          text              DEFAULT NULL,
                       `status_id`     varchar(100)      DEFAULT NULL,
                       `created_by`    varchar(50)       DEFAULT NULL,
                       `created_date`  datetime NOT NULL DEFAULT current_timestamp(),
                       `modified_by`   varchar(50)       DEFAULT NULL,
                       `modified_date` datetime          DEFAULT NULL,
                       `deleted_by`    varchar(50)       DEFAULT NULL,
                       `deleted_date`  datetime          DEFAULT NULL,
                       PRIMARY KEY (`id`),
                       KEY `user_id` (`user_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log`
--

LOCK TABLES `log` WRITE;
/*!40000 ALTER TABLE `log`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `log`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_product`
--

DROP TABLE IF EXISTS `log_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `log_product` (
                               `id`            int(11) NOT NULL AUTO_INCREMENT,
                               `user_id`       int(11)           DEFAULT NULL,
                               `outlet_id`     int(11)           DEFAULT NULL,
                               `target`        varchar(50)       DEFAULT NULL,
                               `object_id`     int(11)           DEFAULT NULL,
                               `modal`         varchar(10)       DEFAULT NULL,
                               `cost`          int(7)            DEFAULT NULL,
                               `awl`           varchar(10)       DEFAULT NULL,
                               `plus`          varchar(10)       DEFAULT NULL,
                               `ak`            varchar(10)       DEFAULT NULL,
                               `discount`      int(2)            DEFAULT NULL,
                               `activity`      text              DEFAULT NULL,
                               `note`          varchar(100)      DEFAULT NULL,
                               `status_id`     text              DEFAULT NULL,
                               `created_by`    varchar(50)       DEFAULT NULL,
                               `created_date`  datetime NOT NULL DEFAULT current_timestamp(),
                               `modified_by`   varchar(50)       DEFAULT NULL,
                               `modified_date` datetime          DEFAULT NULL,
                               `deleted_by`    varchar(50)       DEFAULT NULL,
                               `deleted_date`  datetime          DEFAULT NULL,
                               PRIMARY KEY (`id`)
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_product`
--

LOCK TABLES `log_product` WRITE;
/*!40000 ALTER TABLE `log_product`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `log_product`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `outlet`
--

DROP TABLE IF EXISTS `outlet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `outlet` (
                          `id`            int(11)     NOT NULL AUTO_INCREMENT,
                          `name`          varchar(20) NOT NULL,
                          `address`       text                 DEFAULT NULL,
                          `created_by`    varchar(50)          DEFAULT NULL,
                          `created_date`  datetime    NOT NULL DEFAULT current_timestamp(),
                          `modified_by`   varchar(50)          DEFAULT NULL,
                          `modified_date` datetime             DEFAULT NULL,
                          `deleted_by`    varchar(50)          DEFAULT NULL,
                          `deleted_date`  datetime             DEFAULT NULL,
                          PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `outlet`
--

LOCK TABLES `outlet` WRITE;
/*!40000 ALTER TABLE `outlet`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `outlet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `outlet_employee`
--

DROP TABLE IF EXISTS `outlet_employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `outlet_employee` (
                                   `id`            int(11)  NOT NULL AUTO_INCREMENT,
                                   `outlet_id`     int(11)  NOT NULL,
                                   `user_id`       int(11)  NOT NULL,
                                   `password`      varchar(100)      DEFAULT NULL,
                                   `created_by`    varchar(50)       DEFAULT NULL,
                                   `created_date`  datetime NOT NULL DEFAULT current_timestamp(),
                                   `modified_by`   varchar(50)       DEFAULT NULL,
                                   `modified_date` datetime          DEFAULT NULL,
                                   `deleted_by`    varchar(50)       DEFAULT NULL,
                                   `deleted_date`  datetime          DEFAULT NULL,
                                   PRIMARY KEY (`id`)
) ENGINE = MyISAM
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `outlet_employee`
--

LOCK TABLES `outlet_employee` WRITE;
/*!40000 ALTER TABLE `outlet_employee`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `outlet_employee`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `saldo_awal`
--

DROP TABLE IF EXISTS `saldo_awal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `saldo_awal` (
                              `id`            int(11)  NOT NULL AUTO_INCREMENT,
                              `outlet_id`     int(11)  NOT NULL,
                              `category`      text     NOT NULL,
                              `saldo`         text     NOT NULL,
                              `shift`         text     NOT NULL,
                              `tanggal`       date     NOT NULL,
                              `created_by`    varchar(50)       DEFAULT NULL,
                              `created_date`  datetime NOT NULL DEFAULT current_timestamp(),
                              `modified_by`   varchar(50)       DEFAULT NULL,
                              `modified_date` datetime          DEFAULT NULL,
                              `deleted_by`    varchar(50)       DEFAULT NULL,
                              `deleted_date`  datetime          DEFAULT NULL,
                              PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `saldo_awal`
--

LOCK TABLES `saldo_awal` WRITE;
/*!40000 ALTER TABLE `saldo_awal`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `saldo_awal`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `total_uang_di_laci`
--

DROP TABLE IF EXISTS `total_uang_di_laci`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `total_uang_di_laci` (
                                      `id`            int(11)  NOT NULL AUTO_INCREMENT,
                                      `outlet_id`     int(11)  NOT NULL,
                                      `total`         text     NOT NULL,
                                      `tanggal`       date     NOT NULL,
                                      `shift`         text     NOT NULL,
                                      `created_by`    varchar(50)       DEFAULT NULL,
                                      `created_date`  datetime NOT NULL DEFAULT current_timestamp(),
                                      `modified_by`   varchar(50)       DEFAULT NULL,
                                      `modified_date` datetime          DEFAULT NULL,
                                      `deleted_by`    varchar(50)       DEFAULT NULL,
                                      `deleted_date`  datetime          DEFAULT NULL,
                                      PRIMARY KEY (`id`),
                                      KEY `total_uang_di_laci_ibfk_1` (`outlet_id`),
                                      CONSTRAINT `total_uang_di_laci_ibfk_1` FOREIGN KEY (`outlet_id`) REFERENCES `outlet` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `total_uang_di_laci`
--

LOCK TABLES `total_uang_di_laci` WRITE;
/*!40000 ALTER TABLE `total_uang_di_laci`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `total_uang_di_laci`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaction` (
                               `id`            int(11)     NOT NULL AUTO_INCREMENT,
                               `transaction_code` varchar(100) DEFAULT NULL,
                               `user_id` int(11) NOT NULL,
                               `outlet_id` int(11) NOT NULL,
                               `item_outlet_id` int(11) NOT NULL,
                               `type` varchar(20) NOT NULL,
                               `category` varchar(50) NOT NULL,
                               `product` varchar(50) NOT NULL,
                               `modal` varchar(10) NOT NULL,
                               `cost` varchar(10) NOT NULL,
                               `discount` int(2) NOT NULL,
                               `many` varchar(10) NOT NULL,
                               `awl` varchar(10) NOT NULL,
                               `lk` varchar(10) NOT NULL,
                               `plus` varchar(10) NOT NULL,
                               `transfer` text NOT NULL,
                               `retur_br` varchar(10) NOT NULL,
                               `retur_gbb`     varchar(10) NOT NULL,
                               `retur_bu`      varchar(10) NOT NULL,
                               `retur_c`       text        NOT NULL,
                               `retur_u`       varchar(10) NOT NULL,
                               `ak`            varchar(10) NOT NULL,
                               `note`          text        NOT NULL,
                               `status`        text        NOT NULL,
                               `unit`          varchar(10) NOT NULL DEFAULT '',
                               `from_outlet`   int(11)     NOT NULL,
                               `created_by`    varchar(50)          DEFAULT NULL,
                               `created_date`  datetime    NOT NULL DEFAULT current_timestamp(),
                               `modified_by`   varchar(50)          DEFAULT NULL,
                               `modified_date` datetime             DEFAULT NULL,
                               `deleted_by`    varchar(50)          DEFAULT NULL,
                               `deleted_date`  datetime             DEFAULT NULL,
                               PRIMARY KEY (`id`),
                               KEY `outlet_id` (`outlet_id`),
                               KEY `user_id` (`user_id`),
                               KEY `from_outlet` (`from_outlet`),
                               CONSTRAINT `transaction_ibfk_1` FOREIGN KEY (`from_outlet`) REFERENCES `outlet` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction`
--

LOCK TABLES `transaction` WRITE;
/*!40000 ALTER TABLE `transaction`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `transaction`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction_cart`
--

DROP TABLE IF EXISTS `transaction_cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaction_cart` (
                                    `id`             int(11)     NOT NULL AUTO_INCREMENT,
                                    `user_id`        int(11)     NOT NULL,
                                    `outlet_id`      int(11)     NOT NULL,
                                    `item_outlet_id` int(11)     NOT NULL,
                                    `cost`           varchar(10) NOT NULL,
                                    `discount`       int(2)      NOT NULL,
                                    `many`           varchar(10) NOT NULL,
                                    `created_by`     varchar(50)          DEFAULT NULL,
                                    `created_date`   datetime    NOT NULL DEFAULT current_timestamp(),
                                    `modified_by`    varchar(50)          DEFAULT NULL,
                                    `modified_date`  datetime             DEFAULT NULL,
                                    `deleted_by`     varchar(50)          DEFAULT NULL,
                                    `deleted_date`   datetime             DEFAULT NULL,
                                    PRIMARY KEY (`id`)
) ENGINE = MyISAM
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_cart`
--

LOCK TABLES `transaction_cart` WRITE;
/*!40000 ALTER TABLE `transaction_cart`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `transaction_cart`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction_summary`
--

DROP TABLE IF EXISTS `transaction_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaction_summary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_code` varchar(100) NOT NULL,
  `total_item` varchar(15) NOT NULL,
  `sub_total` varchar(25) NOT NULL,
  `discount_total` varchar(10) NOT NULL,
  `payment` varchar(25) NOT NULL,
  `change_payment` varchar(25) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `modified_by` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_by` varchar(50) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_summary`
--

LOCK TABLES `transaction_summary` WRITE;
/*!40000 ALTER TABLE `transaction_summary` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaction_summary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
                        `id`            int(11)      NOT NULL AUTO_INCREMENT,
                        `user_roles_id` int(10)      NOT NULL,
                        `username`      varchar(5)   NOT NULL,
                        `password`      varchar(100) NOT NULL,
                        `name`          varchar(30)  NOT NULL,
                        `address`       text         NOT NULL,
                        `hp`            varchar(13)  NOT NULL,
                        `created_by`    varchar(50)           DEFAULT NULL,
                        `created_date`  datetime     NOT NULL DEFAULT current_timestamp(),
                        `modified_by`   varchar(50)           DEFAULT NULL,
                        `modified_date` datetime              DEFAULT NULL,
                        `deleted_by`    varchar(50)           DEFAULT NULL,
                        `deleted_date`  datetime              DEFAULT NULL,
                        PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 8
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user`
    DISABLE KEYS */;
INSERT INTO `user`
VALUES (1, 1, 'w4A6', '202cb962ac59075b964b07152d234b70', 'ADMINISTRATOR',
        'Jl. Wates KM. 5,5 Ambarketarang Gamping, Kab. Sleman, Yogyakarta', '087878242423', 'bambang hermawan',
        '2020-07-12 00:54:12', NULL, NULL, NULL, NULL),
       (2, 2, 'mpAoO', '07fef02fd3f877868ae6682dd486770a', 'OWNER',
        'Jl. Wates KM. 5,5 Ambarketarang Gamping, Kab. Sleman, Yogyakarta', '087878242423', 'bambang hermawan',
        '2020-08-21 08:26:20', NULL, NULL, NULL, NULL),
       (3, 3, 'lOjIG', 'a455617ac576ebdacde308799fa9e45b', 'SHIFT_1',
        'Jl. Wates KM. 5,5 Ambarketarang Gamping, Kab. Sleman, Yogyakarta', '087878242423', 'bambang hermawan',
        '2020-09-11 05:51:10', NULL, NULL, NULL, NULL),
       (4, 3, 'mbFBY', '82fc884dbfb3c4f77ded6e349091c9e3', 'SHIFT_2',
        'Jl. Wates KM. 5,5 Ambarketarang Gamping, Kab. Sleman, Yogyakarta', '087878242423', 'bambang hermawan',
        '2020-09-11 05:51:16', NULL, NULL, NULL, NULL),
       (5, 3, 'yUV05', 'b80b185e978671d4f630f693804679e5', 'SHIFT_3',
        'Jl. Wates KM. 5,5 Ambarketarang Gamping, Kab. Sleman, Yogyakarta', '087878242423', 'bambang hermawan',
        '2020-09-11 05:51:22', NULL, NULL, NULL, NULL),
       (6, 3, 'VoPuT', '1ef886f09b911af60878c4e3eb975280', 'SHIFT_4',
        'Jl. Wates KM. 5,5 Ambarketarang Gamping, Kab. Sleman, Yogyakarta', '087878242423', 'bambang hermawan',
        '2020-09-11 05:51:28', NULL, NULL, NULL, NULL),
       (7, 3, '2dis9', '8022bda33c40c484010bbc2df7e97c38', 'SHIFT_5',
        'Jl. Wates KM. 5,5 Ambarketarang Gamping, Kab. Sleman, Yogyakarta', '087878242423', 'bambang hermawan',
        '2020-09-11 05:51:33', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `user`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_roles`
(
    `id`            int(10)  NOT NULL AUTO_INCREMENT,
    `wewenang`      varchar(50)       DEFAULT NULL,
    `created_by`    varchar(50)       DEFAULT NULL,
    `created_date`  datetime NOT NULL DEFAULT current_timestamp(),
    `modified_by`   varchar(50)       DEFAULT NULL,
    `modified_date` datetime          DEFAULT NULL,
    `deleted_by`    varchar(50)       DEFAULT NULL,
    `deleted_date`  datetime          DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = MyISAM
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles`
    DISABLE KEYS */;
INSERT INTO `user_roles`
VALUES (1, 'ADMIN', 'bambang hermawan', '2020-07-13 21:35:17', NULL, NULL, NULL, NULL),
       (2, 'OWNER', 'bambang hermawan', '2020-07-13 21:36:11', NULL, NULL, NULL, NULL),
       (3, 'KASIR', 'bambang hermawan', '2020-07-13 21:36:19', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `user_roles`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'scoc_konter'
--
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-19 23:14:04
