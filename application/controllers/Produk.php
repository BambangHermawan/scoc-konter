<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Produk extends MY_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('M_produk', '', true);
		$this->load->model('M_log_produk', '', true);

		if (!isset($_SESSION)) {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'ADMIN') {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'OWNER') {
			redirect('auth/login');
		} elseif ($_SESSION['status_user'] == 'KASIR') {
			redirect('auth/login');
		}
	}

	function index()
	{
		$data['title'] = 'Gudang';
		$data['site_navigation'] = 'Gudang';

		$this->load->view('gudang/index', $data);
	}

	function get_all_product($type_id)
	{
		$result = $this->M_produk->get_datatables($type_id);

		$response = [
			'success' => true,
			'draw' => $_REQUEST['draw'],
			'recordsTotal' => $this->M_produk->count_all($type_id),
			'recordsFiltered' => $this->M_produk->count_filtered($type_id),
			'data' => $result
		];

		return to_json($response);
	}

	function add_stock_product()
	{
		$product_id = $this->input->post('product_id');
		$name = $this->input->post('name');
		$many = $this->input->post('many');

		$result = $this->M_produk->add_stock_product($product_id, $many);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Tambah stok produk ' . $name . ' berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Tambah stok produk ' . $name . ' gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function add_product()
	{
		$category_id = $this->input->post('category_id');
		$name = $this->input->post('name');
		$modal = $this->input->post('modal');
		$many = $this->input->post('many');
		$entry_date = $this->input->post('entry_date') != null ? $this->input->post('entry_date') : created_date();

		$this->load->library('ciqrcode');

		$config['cacheable'] = true;
		$config['cachedir'] = 'uploads/';
		$config['errorlog'] = 'uploads/';
		$config['imagedir'] = 'uploads/qrcode/';
		$config['quality'] = true;
		$config['size'] = '1024';
		$config['black'] = array(224, 255, 255);
		$config['white'] = array(70, 130, 180);
		$this->ciqrcode->initialize($config);

		$product_image = md5(time() . '_' . uniqid()) . '.png';

		$result = $this->M_produk->add_product($category_id, $name, $modal, $many, $product_image, $entry_date);

		if ($result) {

			$params['data'] = $product_image;
			$params['level'] = 'H';
			$params['size'] = 10;
			$params['savename'] = FCPATH . $config['imagedir'] . $product_image;
			$this->ciqrcode->generate($params);

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Tambah produk ' . $name . ' berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Tambah produk ' . $name . ' gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function edit_product()
	{
		$id = $this->input->post('id');
		$name = $this->input->post('name');
		$many = $this->input->post('many');
		$modal = $this->input->post('modal');
		$note = $this->input->post('note');

		$result = $this->M_produk->edit_product($name, $many, $modal, $id, $note);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Ubah produk ' . $name . ' berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Ubah produk ' . $name . ' gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function delete_product()
	{
		$id = $this->input->post('id');

		$result = $this->M_produk->delete_product($id);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Hapus produk berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Hapus produk gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}
}
