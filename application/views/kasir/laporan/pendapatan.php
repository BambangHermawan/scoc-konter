<?php $this->load->view('_layouts/_template/header') ?>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-3">
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Filter Laporan <b><?= $outlet[0]->name ?></b></h3>
				</div>
				<div class="card-body p-2">
					<div class="form-group">
						<div class="col-lg-4"><label for="date_begin">Mulai</label></div>
						<div class="col-lg-12">
							<input type="date" class="form-control form-control-sm"
								   name="date_begin"
								   id="date_begin" required>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4"><label for="date_end">Sampai</label></div>
						<div class="col-lg-12">
							<input type="date" class="form-control form-control-sm"
								   name="date_end"
								   id="date_end" required>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4"><label for="outlet_employee_id">Pengguna</label></div>
						<div class="col-lg-12">
							<select type="date" class="form-control form-control-sm"
									name="outlet_employee_id"
									id="outlet_employee_id">
							</select>
						</div>
					</div>

				</div>
				<div class="card-footer">
					<div class="float-right">
						<button type="submit" class="btn btn-primary btn-sm" id="btn_search_all">Filter
							Data
						</button>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-9">
			<div class="card card-primary card-outline">
				<div class="card-header">
					<h3 class="card-title">Laporan Pendapatan</b></h3>
					<div class="float-right">
						<button class="btn btn-primary btn-sm" id="btn_add_laporan_pendapatan"><i
									class="fa fa-plus"></i> Laporan Pendapatan
						</button>
					</div>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table id="table_laporan_pendapatan"
							   class="table table-striped table-hover dt-responsive display nowrap"
							   style="width:100%">
							<thead>
							<tr>
								<th class="nomor" data-orderable="false">No</th>
								<th>Pengguna</th>
								<th>Outlet</th>
								<th>Total Uang Penjualan</th>
								<th>Total Uang Cash</th>
								<th>Selisih</th>
								<th>Catatan</th>
								<th>Tanggal</th>
							</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--modal laporan pendapatan-->
<div class="modal fade" id="modal_show_laporan_pendapatan">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><h5>Tambah Laporan Pendapatan</h5></div>
			<div class="modal-body">
				<form id="laporan_pendapatan_form">
					<input type="number" id="input_laporan_pendapatan_total_data"
						   name="input_laporan_pendapatan_total_data" hidden>
					<input type="number" id="input_laporan_pendapatan_selisih_data"
						   name="input_laporan_pendapatan_selisih_data" hidden>
					<div class="form-group">
						<div class="col-lg-4">Total Uang Penjualan</div>
						<div class="col-lg-12"><input type="text" class="form-control form-control-sm"
													  id="input_laporan_pendapatan_total"
													  name="input_laporan_pendapatan_total" readonly></div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Total Uang Cash</div>
						<div class="col-lg-12"><input type="text" class="form-control form-control-sm"
													  id="input_laporan_pendapatan_current_total"
													  name="input_laporan_pendapatan_current_total"
													  onkeypress="return check_int(event)"></div>
					</div>

					<div class="form-group">
						<div class="col-lg-6">Selisih</div>
						<div class="col-lg-12"><input type="text" class="form-control form-control-sm"
													  id="input_laporan_pendapatan_selisih"
													  name="input_laporan_pendapatan_selisih" readonly></div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Catatan</div>
						<div class="col-lg-12">
							<textarea class="form-control form-control-sm" id="input_laporan_pendapatan_catatan"
									  name="input_laporan_pendapatan_catatan"></textarea>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"
						id="btn_laporan_pendapatan_submit">Simpan
				</button>
			</div>
		</div>
	</div>
</div>

<script>
	//init laporan
	$(document).ready(function () {
		var today = new Date();
		date = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2)

		$("#date_begin").val(date)
		$("#date_end").val(date)

		init_table_laporan_pendapatan()
		get_outlet_employee()
		hitung_uang_selisih()
	})

	$('#btn_add_laporan_pendapatan').click(function () {
		$.ajax({
			url: "<?= base_url(); ?>kasir/laporan/get_total_pendapatan_by_date",
			success: function (result) {
				if (result.data.total_uang === 0) {
					$('#input_laporan_pendapatan_total').val('Rp. 0')
					$('#input_laporan_pendapatan_total_data').val(0)
				}
				$('#input_laporan_pendapatan_total').val('Rp. ' + result.data.total_uang)
				$('#input_laporan_pendapatan_total_data').val(result.data.total_uang.replace(',', ''))
			}
		})

		$('#modal_show_laporan_pendapatan').modal({backdrop: 'static', keyboard: false})
	})

	//validasi form laporan pendapatan
	$("#laporan_pendapatan_form").validate({
		rules: {
			input_laporan_pendapatan_total: {
				required: true,
				maxlength: 50
			},
			input_laporan_pendapatan_current_total: {
				required: true,
				maxlength: 50
			},
			input_laporan_pendapatan_selisih: {
				required: true
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//simpan data laporan pendapatan
	$("#btn_laporan_pendapatan_submit").click(function () {

		if ($('#laporan_pendapatan_form').valid()) {
			$.ajax({
				url: "<?= base_url(); ?>kasir/laporan/add_laporan_pendapatan",
				type: "POST",
				data: {
					user_id: "<?= $_SESSION['id'] ?>",
					outlet_id: "<?= $this->uri->segment(4); ?>",
					total: $('#input_laporan_pendapatan_total_data').val(),
					uang_cash: $('#input_laporan_pendapatan_current_total').val(),
					selisih: $('#input_laporan_pendapatan_selisih_data').val(),
					catatan: $('#input_laporan_pendapatan_catatan').val()
				},
				success: function (result) {

					if (result.success) {
						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						init_table_laporan_pendapatan()

					} else {
						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});
					}
				}
			})
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});
		}
	})

	//filter laporan
	$('#btn_search_all').click(function () {

		var user_id = $('#outlet_employee_id option:selected').val();

		table_laporan_pendapatan = $("#table_laporan_pendapatan").DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			autoWidth: true,
			ajax: "<?= base_url(); ?>kasir/laporan/get_all_pendapatan/" + user_id + "/" + "<?= $this->uri->segment(4); ?>" + "/" + $('#date_begin').val() + "/" + $('#date_end').val(),
			columns: [
				{
					data: "id",
					width: "0.8%",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: 'user_name'
				},
				{
					data: 'outlet_name'
				},
				{
					data: 'total_uang_penjualan'
				},
				{
					data: 'total_uang_cash'
				},
				{
					data: 'total_uang_selisih'
				},
				{
					data: 'catatan'
				},
				{
					data: 'created_date'
				}
			]
		})
	})

	//init datatables table laporan
	function init_table_laporan_pendapatan() {

		var user_id = "<?= $_SESSION['id'] ?>";

		table_laporan_pendapatan = $("#table_laporan_pendapatan").DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			autoWidth: true,
			ajax: "<?= base_url(); ?>kasir/laporan/get_all_pendapatan/" + user_id + "/" + "<?= $this->uri->segment(4); ?>" + "/" + $('#date_begin').val() + "/" + $('#date_end').val(),
			columns: [
				{
					data: "id",
					width: "0.8%",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: 'user_name'
				},
				{
					data: 'outlet_name'
				},
				{
					data: 'total_uang_penjualan'
				},
				{
					data: 'total_uang_cash'
				},
				{
					data: 'total_uang_selisih'
				},
				{
					data: 'catatan'
				},
				{
					data: 'created_date'
				}
			]
		})
	}

	//hitung uang kembalian
	$('#input_laporan_pendapatan_current_total').keyup(function () {
		hitung_uang_selisih();
	});

	//hitung uang total bayar
	function hitung_uang_selisih() {
		var total_uang = $('#input_laporan_pendapatan_total_data').val()
		var total_uang_cash = $('#input_laporan_pendapatan_current_total').val()

		var selisih;
		if (total_uang_cash.length > 0) {
			selisih = parseInt(total_uang) - parseInt(total_uang_cash);

			$('#input_laporan_pendapatan_selisih').val(to_rupiah(selisih));
			$('#input_laporan_pendapatan_selisih_data').val(selisih);

		} else {
			$('#input_laporan_pendapatan_selisih').val(to_rupiah(0));
			$('#input_laporan_pendapatan_selisih_data').val(0);
		}
	}

	//konversi angka ke format rupiah
	function to_rupiah(angka) {
		var rev = parseInt(angka, 10).toString().split('').reverse().join('');

		var rev2 = '';
		for (var i = 0; i < rev.length; i++) {
			rev2 += rev[i];
			if ((i + 1) % 3 === 0 && i !== (rev.length - 1)) {
				rev2 += ',';
			}
		}

		return 'Rp. ' + rev2.split('').reverse().join('');
	}

	//cek inputan angka atau bukan
	function check_int(event) {
		var charCode = (event.which) ? event.which : event.keyCode;
		return (charCode >= 48 && charCode <= 57 || charCode === 8);
	}

	//dropdown pilih pengguna outlet
	function get_outlet_employee() {
		user_id = "<?= $_SESSION['id'] ?>"
		outlet_id = "<?= $this->uri->segment(4) ?>"

		$.ajax({
			url: "<?= base_url(); ?>kasir/karyawan/get_karyawan/" + outlet_id,
			success: function (result) {
				if (result.success) {

					var user_list = ''

					result.data.forEach(function (outlet) {
						if (outlet['wewenang'] !== 'ADMIN' && outlet['wewenang'] !== 'OWNER' && outlet['outlet_id'] === outlet_id && outlet['user_id'] === user_id) {
							user_list += "<option value='" + outlet['user_id'] + "'>" + outlet['name'] + "</option>";
						}
					})
					$('#outlet_employee_id').html(user_list)
				}
			}
		})
	}

</script>

<?php $this->load->view('_layouts/_template/js') ?>
