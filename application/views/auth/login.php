<?php $this->load->view('_layouts/_welcome/header') ?>

<div class="login-box" style="margin: 0 auto; padding-top: 5%; padding-bottom: 5%">
	<div class="login-logo">
		<a href="#"><b>SC.OC</b> Cellular</a>
	</div>
	<div class="card">
		<div class="card-body login-card-body">
			<p class="login-box-msg">Masuk untuk memulai sesi anda</p>

			<form id="login_form">

				<div class="input-group mb-3">
					<select class="form-control" name="login_role" id="login_role">
					</select>
					<div class="input-group-append">
						<div class="input-group-text">
							<span class="fas fa-user-shield"></span>
						</div>
					</div>
				</div>

				<div class="input-group mb-3">
					<input type="text" class="form-control"
						   name="login_username"
						   id="login_username" autocomplete="username" placeholder="Username" required>
					<div class="input-group-append">
						<div class="input-group-text">
							<span class="fas fa-envelope"></span>
						</div>
					</div>
				</div>

				<div class="input-group mb-3">
					<input type="password" class="form-control"
						   name="login_password"
						   id="login_password" autocomplete="current-password" placeholder="Password">
					<div class="input-group-append">
						<div class="input-group-text">
							<span class="fas fa-lock"></span>
						</div>
					</div>
				</div>

				<div class="row float-right">
					<div class="col-12">
						<button type="submit" class="btn btn-primary btn-block" id="submit" name="submit">Masuk
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<?php $this->load->view('_layouts/_welcome/footer') ?>

<script>
	$(function () {
		get_user_roles()
	});

	//validasi login form
	$("#login_form").validate({
		rules: {
			login_role: {
				required: true,
				maxlength: 1
			},
			login_username: {
				required: true,
				maxlength: 5
			},
			login_password: {
				required: true,
				maxlength: 250
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				element.next().after(error);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//check login
	$("#login_form").submit(function (e) {

		e.preventDefault();

		if ($('#login_form').valid()) {
			$.ajax({
				url: "<?= base_url(); ?>auth/login/check_login",
				type: 'POST',
				data: {
					'user_roles_id': $("#login_role").val(),
					'username': $("#login_username").val(),
					'password': $("#login_password").val(),
				},
				success: function (result) {
					if (result.success) {

						if (result.data !== 'KASIR') {
							Swal.fire({
								title: 'Berhasil',
								text: result.message,
								icon: result.type,
								showConfirmButton: true,
							}).then(function () {
								window.location.href = "<?= base_url(); ?>beranda";
							});
						} else {
							Swal.fire({
								title: 'Berhasil',
								text: result.message,
								icon: result.type,
								showConfirmButton: true,
							}).then(function () {
								window.location.href = "<?= base_url(); ?>kasir/beranda";
							});
						}
					} else {

						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: true,
						}).then(function () {
							window.location.href = "<?= base_url(); ?>auth/login";
						});

					}

				}
			});
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});
		}

	});

	//dropdown user roles
	function get_user_roles() {
		$.ajax({
			url: "<?= base_url(); ?>auth/login/get_all_wewenang",
			type: 'GET',
			success: function (result) {

				if (result.success) {
					var user_roles = '<option value="">Masuk sebagai...</option>'

					result.data.forEach(function (data) {
						user_roles += "<option value='" + data.id + "'>" + data.wewenang + "</option>";
					})

					$('#login_role').html(user_roles)
				}
			}
		})
	}
</script>
