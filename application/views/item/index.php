<?php $this->load->view('_layouts/_template/header') ?>

<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <div class="card card-outline card-primary">
                <div class="card-header">
                    <h3 class="card-title"><b>Item Barang</b></h3>
                    <div class="float-right">
                        <button class="btn btn-primary btn-sm" id="btn_add_item"><i class="fa fa-plus"> </i> Item
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="table_item" class="table table-striped table-hover dt-responsive display nowrap"
                               style="width:100%">
                            <thead>
                            <tr>
                                <th class="nomor" data-orderable="false">No</th>
                                <th>Item</th>
                                <th data-orderable="false">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>

<!--Modal tambah kategori baru-->
<div class="modal fade" id="modal_add_item">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Tambah Item</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="add_item_form">
					<div class="form-group">
						<div class="col-lg-4">Nama Item</div>
						<div class="col-lg-12">
							<input type="text" class="form-control form-control-sm" id="add_item_name"
                                   name="add_item_name">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"
                        id="btn_new_item">Simpan
                </button>
			</div>
		</div>
	</div>
</div>

<!--Modal ubah kategori baru-->
<div class="modal fade" id="modal_edit_item">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Ubah Item</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="edit_item_form">
					<div class="form-group">
						<div class="col-lg-4">Nama Item</div>
						<div class="col-lg-12">
							<input type="text" class="form-control form-control-sm" id="edit_item_name"
                                   name="edit_item_name">
                        </div>
                    </div>
                    <div class="form-group" hidden>
                        <div class="col-lg-12">
                            <input type="text" class="form-control form-control-sm" id="edit_item_id"
                                   name="edit_item_id">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"
                        id="save_edit_item">Simpan
                </button>
            </div>
        </div>
    </div>
</div>

<script>
    //init table item
    $(document).ready(function () {
        init_table_item()
    })

    //validasi form tambah item
    $("#add_item_form").validate({
        rules: {
            add_item_name: {
                required: true,
                maxlength: 20
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `invalid-feedback` class to the error element
            error.addClass("invalid-feedback");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.next("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid").removeClass("is-valid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).addClass("is-valid").removeClass("is-invalid");
        }
    });

    //validasi form ubah item
    $("#edit_item_form").validate({
        rules: {
            edit_item_name: {
                required: true,
                maxlength: 20
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `invalid-feedback` class to the error element
            error.addClass("invalid-feedback");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.next("label"));
            } else {
                error.insertAfter(element);
            }
        },
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//modal tambah item
	$("#btn_add_item").on('click', function () {
		$("#modal_add_item").modal({backdrop: 'static', keyboard: false})
	})

	//tambah item baru
	$("#btn_new_item").click(function () {

		if ($('#add_item_form').valid()) {
			$.ajax({
				url: "<?= base_url(); ?>item/add_item",
				type: "POST",
				data: {
					name: $('#add_item_name').val()
				},
				success: function (result) {

					if (result.success) {

						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						init_table_item();

					} else {

                        Swal.fire({
                            title: 'gagal',
                            text: result.message,
                            icon: result.type,
                            showConfirmButton: false,
                            timer: 1500
                        });

                        init_table_item();

                    }
                }
            })
        } else {
            Swal.fire({
                title: 'Peringatan',
                text: 'Data yang di isi belum sesuai.',
                icon: 'warning',
                showConfirmButton: false,
                timer: 1500
            });

            init_table_item();
        }

    })

	//modal ubah data item
	$("#table_item").on('click', 'tr #btn_edit_click', function () {

		tr = $(this).closest('tr')
		data = table_item.row(tr).data()

		$("#edit_item_id").val(data.id)
		$("#edit_item_name").val(data.name)

		$('#modal_edit_item').modal({backdrop: 'static', keyboard: false})
	})

	//ubah data kategori
	$("#save_edit_item").on('click', function () {

		if ($('#edit_item_form').valid()) {
			$.ajax({
				url: "<?= base_url(); ?>item/edit_item",
				type: "POST",
				data: {
					'id': $("#edit_item_id").val(),
					'name': $("#edit_item_name").val()
				},
				success: function (result) {

					if (result.success) {
						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});
						init_table_item()

					} else {
						Swal.fire({
							title: 'gagal',
                            text: result.message,
                            icon: result.type,
                            showConfirmButton: false,
                            timer: 1500
                        });

                        init_table_item()
                    }
                }
            })
        } else {
            Swal.fire({
                title: 'Peringatan',
                text: 'Data yang di isi belum sesuai.',
                icon: 'warning',
                showConfirmButton: false,
                timer: 1500
            });

            init_table_item()
        }

	})

	//hapus data kategori
	$("#table_item").on('click', 'tr #btn_delete_click', function () {
		tr = $(this).closest('tr')
		data = table_item.row(tr).data()
		id = data.id

		Swal.fire({
			title: 'Peringatan!',
			text: 'Apakah Anda yakin akan menghapus item ' + data.name + ' ?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya, hapus data ini!'
		}).then((result) => {
			if (result.value) {

				$.ajax({
					url: "<?= base_url(); ?>item/delete_item",
					type: "POST",
					data: {id: id},
					success(result) {

						if (result.success) {
							Swal.fire({
								title: 'Berhasil',
								text: result.message,
								icon: result.type,
								showConfirmButton: false,
								timer: 1500
							});

							init_table_item()
						} else {
							Swal.fire({
								title: 'gagal',
								text: result.message,
								icon: result.type,
								showConfirmButton: false,
								timer: 1500
							});

							init_table_item()
						}

					}
				})
			}
		})
	})

	//init datatables table wewenang
	function init_table_item() {
		button = "&nbsp;<button class='btn btn-warning btn-sm' id='btn_edit_click'>\
            <i class='fas fa-edit'></i>&nbsp;Ubah</button>"
		button += "&nbsp;<button class='btn btn-danger btn-sm' id='btn_delete_click'>\
            <i class='fas fa-trash'></i>&nbsp;Hapus</button>"

		table_item = $("#table_item").DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			autoWidth: true,
			ajax: "<?= base_url(); ?>item/get_all_item",
			columns: [
				{
					data: "id",
					width: "0.8%",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: 'name'
				},
				{
					defaultContent: button
				},
			]
		})
	}
</script>


<?php $this->load->view('_layouts/_template/js') ?>
