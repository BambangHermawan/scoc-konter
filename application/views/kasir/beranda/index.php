<?php $this->load->view('_layouts/_template/header') ?>

<div class="container-fluid">
	<div class="row">
		<div class="col-12 col-sm-6 col-md-4">
			<div class="info-box">
				<span class="info-box-icon bg-info elevation-1"><i class="fas fa-shopping-cart"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Total Transaksi Hari Ini</span>
					<span class="info-box-number" id="rekapan_outlet_laku"><?php echo $transaction; ?></span>
				</div>
			</div>
		</div>
		<div class="clearfix hidden-md-up"></div>
		<div class="col-12 col-sm-6 col-md-4">
			<div class="info-box mb-3">
				<span class="info-box-icon bg-info elevation-1"><i class="fas fa-cash-register"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Total Penjualan Hari Ini</span>
					<span class="info-box-number"
						  id="rekapan_outlet_untung"><b><?php echo $transaction_unit; ?></b></span>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-6 col-md-4">
			<div class="info-box mb-3">
				<span class="info-box-icon bg-info elevation-1"><i class="fas fa-money-bill"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Total Pendapatan Hari Ini</span>
					<span class="info-box-number"
						  id="rekapan_outlet_rugi"><b>Rp. <?php echo $transaction_earning->total_uang ?></b></span>
				</div>
			</div>
		</div>

		<div class="col-6">
			<div class="card card-outline card-primary">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-chart-bar mr-1"></i> <b>Penjualan Hari Ini</b></h3>
				</div>
				<div class="card-body">
					<div id="laporan_chart_by_day_container">
						<canvas id="laporan_chart_by_day"></canvas>
					</div>
				</div>
			</div>
		</div>

		<div class="col-6">
			<div class="card card-outline card-primary">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-chart-bar mr-1"></i> <b>Penjualan Bulan ini</b></h3>
				</div>
				<div class="card-body">
					<div id="laporan_chart_by_month_container">
						<canvas id="laporan_chart_by_month"></canvas>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function () {
		init_cart_by_hour()
		init_cart_by_month()
	})

	function init_cart_by_hour() {
		var today = new Date();

		var day = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2)

		var data_hari
		var data_outlet
		var data_aksesoris
		var data_perdana
		var data_voucher
		var data_pulsa
		var outlet_id = "<?= $_SESSION['outlet_id'] ?>";

		$.ajax({
			url: "<?= base_url(); ?>kasir/laporan/get_statistic_by_day/" + outlet_id + "/" + day,
			success(result) {
				if (result.success) {
					data_outlet = result.data.outlet
					data_aksesoris = result.data.aksesoris
					data_perdana = result.data.perdana
					data_voucher = result.data.voucher
					data_pulsa = result.data.pulsa
				}
			}
		})

		setTimeout(() => {
			document.getElementById("laporan_chart_by_day_container").innerHTML = '&nbsp;';
			document.getElementById("laporan_chart_by_day_container").innerHTML = '<canvas id="laporan_chart_by_day"></canvas>';
			var ctx = document.getElementById("laporan_chart_by_day").getContext("2d");

			new Chart(ctx, {
				type: 'bar',
				data: {
					labels: data_outlet,
					datasets: [{
						data: data_aksesoris,
						label: "Aksesoris",
						borderColor: "rgba(255, 99, 132, 0.5)",
						backgroundColor: [
							'rgba(255, 99, 132, 0.5)',
							'rgba(255, 99, 132, 0.5)',
							'rgba(255, 99, 132, 0.5)',
							'rgba(255, 99, 132, 0.5)'
						],
						fill: false
					}, {
						data: data_perdana,
						label: "Perdana",
						borderColor: "rgba(54, 162, 235, 0.5)",
						backgroundColor: [
							'rgba(54, 162, 235, 0.5)',
							'rgba(54, 162, 235, 0.5)',
							'rgba(54, 162, 235, 0.5)',
							'rgba(54, 162, 235, 0.5)'
						],
						fill: false
					}, {
						data: data_voucher,
						label: "Voucher",
						borderColor: "rgba(255, 206, 86, 0.5)",
						backgroundColor: [
							'rgba(255, 206, 86, 0.5)',
							'rgba(255, 206, 86, 0.5)',
							'rgba(255, 206, 86, 0.5)',
							'rgba(255, 206, 86, 0.5)'
						],
						fill: false
					}, {
						data: data_pulsa,
						label: "Pulsa",
						borderColor: "rgba(75, 192, 192, 0.5)",
						backgroundColor: [
							'rgba(75, 192, 192, 0.5)',
							'rgba(75, 192, 192, 0.5)',
							'rgba(75, 192, 192, 0.5)',
							'rgba(75, 192, 192, 0.5)'
						],
						fill: false
					}]
				},
				options: {
					title: {
						display: true,
						text: 'Data tanggal ' + day
					}
				}
			})
		}, 500);
	}

	function init_cart_by_month() {
		var today = new Date();
		var firstDay = new Date(today.getFullYear(), today.getMonth(), 1);
		var lastDay = new Date(today.getFullYear(), today.getMonth() + 1, 0);

		var firstDate = firstDay.getFullYear() + '-' + ('0' + (firstDay.getMonth() + 1)).slice(-2) + '-' + ('0' + firstDay.getDate()).slice(-2);
		var lastDate = lastDay.getFullYear() + '-' + ('0' + (lastDay.getMonth() + 1)).slice(-2) + '-' + ('0' + lastDay.getDate()).slice(-2);

		var data_hari
		var data_outlet
		var data_aksesoris
		var data_perdana
		var data_voucher
		var data_pulsa
		var outlet_id = "<?= $_SESSION['outlet_id'] ?>";

		$.ajax({
			url: "<?= base_url(); ?>kasir/laporan/get_statistic_by_month/" + outlet_id + "/" + firstDate + "/" + lastDate,
			success(result) {
				if (result.success) {
					data_outlet = result.data.outlet
					data_aksesoris = result.data.aksesoris[0]
					data_perdana = result.data.perdana[0]
					data_voucher = result.data.voucher[0]
					data_pulsa = result.data.pulsa[0]
				}
			}
		})

		setTimeout(() => {
			document.getElementById("laporan_chart_by_month_container").innerHTML = '&nbsp;';
			document.getElementById("laporan_chart_by_month_container").innerHTML = '<canvas id="laporan_chart_by_month"></canvas>';
			var ctx = document.getElementById("laporan_chart_by_month").getContext("2d");

			new Chart(ctx, {
				type: 'bar',
				data: {
					labels: data_outlet,
					datasets: [{
						data: data_aksesoris,
						label: "Aksesoris",
						borderColor: "rgba(255, 99, 132, 0.5)",
						backgroundColor: [
							'rgba(255, 99, 132, 0.5)',
							'rgba(255, 99, 132, 0.5)',
							'rgba(255, 99, 132, 0.5)',
							'rgba(255, 99, 132, 0.5)'
						],
						fill: false
					}, {
						data: data_perdana,
						label: "Perdana",
						borderColor: "rgba(54, 162, 235, 0.5)",
						backgroundColor: [
							'rgba(54, 162, 235, 0.5)',
							'rgba(54, 162, 235, 0.5)',
							'rgba(54, 162, 235, 0.5)',
							'rgba(54, 162, 235, 0.5)'
						],
						fill: false
					}, {
						data: data_voucher,
						label: "Voucher",
						borderColor: "rgba(255, 206, 86, 0.5)",
						backgroundColor: [
							'rgba(255, 206, 86, 0.5)',
							'rgba(255, 206, 86, 0.5)',
							'rgba(255, 206, 86, 0.5)',
							'rgba(255, 206, 86, 0.5)'
						],
						fill: false
					}, {
						data: data_pulsa,
						label: "Pulsa",
						borderColor: "rgba(75, 192, 192, 0.5)",
						backgroundColor: [
							'rgba(75, 192, 192, 0.5)',
							'rgba(75, 192, 192, 0.5)',
							'rgba(75, 192, 192, 0.5)',
							'rgba(75, 192, 192, 0.5)'
						],
						fill: false
					}]
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Month'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Value'
						}
					}]
				},
				options: {
					title: {
						display: true,
						text: 'Data dari ' + firstDate + ' sampai ' + lastDate
					}
				}
			})
		}, 500);
	}
</script>

<?php $this->load->view('_layouts/_template/js') ?>
