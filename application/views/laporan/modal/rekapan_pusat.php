<!--Modal detail rekapan-->
<div class="modal fade" id="modal_detail_rekapan" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Detail Transaksi</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<div class="col-lg-6">HPP</div>
					<div class="col-lg-12" id="detail_rekapan_modal"></div>
				</div>

				<div class="form-group">
					<div class="col-lg-6">Harga</div>
					<div class="col-lg-12" id="detail_rekapan_harga"></div>
				</div>

				<div class="form-group">
					<div class="col-lg-6">Omset</div>
					<div class="col-lg-12" id="detail_rekapan_omset"></div>
				</div>

				<div class="form-group">
					<div class="col-lg-6">Untung</div>
					<div class="col-lg-12" id="detail_rekapan_untung"></div>
				</div>

				<div class="form-group">
					<div class="col-lg-6">Rugi</div>
					<div class="col-lg-12" id="detail_rekapan_rugi"></div>
				</div>

				<div class="form-group">
					<div class="col-lg-6">Aset</div>
					<div class="col-lg-12" id="detail_rekapan_aset"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
			</div>
		</div>
	</div>
</div>

<!--Modal edit transaksi-->
<div class="modal fade" id="modal_edit_transaction" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Ubah Transaksi</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="edit_transaksi_form">
					<input type="number" id="edit_transaction_id" name="edit_transaction_id" hidden>
					<div class="form-group">
						<div class="col-lg-6">Plus</div>
						<div class="col-lg-12">
							<input type="number" id="edit_transaction_plus" name="edit_transaction_plus"
								   class="form-control form-control-sm">
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-6">retur_br</div>
						<div class="col-lg-12">
							<input type="number" id="edit_transaction_retur_br" name="edit_transaction_retur_br"
								   class="form-control form-control-sm">
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-6">Transfer</div>
						<div class="col-lg-12">
							<input type="number" id="edit_transaction_transfer" name="edit_transaction_transfer"
								   class="form-control form-control-sm">
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-6">Retur +</div>
						<div class="col-lg-12">
							<input type="number" id="edit_transaction_retur" name="edit_transaction_retur"
								   class="form-control form-control-sm">
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-6">Retur -</div>
						<div class="col-lg-12">
							<input type="number" id="edit_transaction_retur_bu" name="edit_transaction_retur_bu"
								   class="form-control form-control-sm">
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-6">Laku</div>
						<div class="col-lg-12">
							<input type="number" id="edit_transaction_laku" name="edit_transaction_laku"
								   class="form-control form-control-sm">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" id="edit_transaction_submit">
					Simpan
				</button>
			</div>
		</div>
	</div>
</div>

<!--Modal edit rekapan-->
<div class="modal fade" id="modal_edit_rekapan" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Ubah Rekapan</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="edit_rekapan_form">
					<div class="form-group">
						<div class="col-lg-6">Plus</div>
						<div class="col-lg-12">
							<input type="number" id="edit_rekapan_plus" class="form-control form-control-sm">
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-6">Transfer</div>
						<div class="col-lg-12">
							<input type="number" id="edit_rekapan_transfer" class="form-control form-control-sm">
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-6">Retur GBB</div>
						<div class="col-lg-12">
							<input type="number" id="edit_rekapan_retur" class="form-control form-control-sm">
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-6">Retur BR</div>
						<div class="col-lg-12">
							<input type="number" id="edit_rekapan_retur_br" class="form-control form-control-sm">
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-6">Retur BU</div>
						<div class="col-lg-12">
							<input type="number" id="edit_rekapan_retur_bu" class="form-control form-control-sm">
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-6">Retur C</div>
						<div class="col-lg-12">
							<input type="number" id="edit_rekapan_retur_c" class="form-control form-control-sm">
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-6">Laku</div>
						<div class="col-lg-12">
							<input type="number" id="edit_rekapan_laku" class="form-control form-control-sm">
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-12">Catatan</div>
						<div class="col-lg-12">
							<textarea id="edit_rekapan_catatan" class="form-control form-control-sm"></textarea>
						</div>
					</div>

					<div class="form-group" hidden>
						<div class="col-lg-6">Tanggal</div>
						<div class="col-lg-12">
							<input type="date" id="edit_rekapan_tanggal" class="form-control form-control-sm">
						</div>
					</div>

					<div class="form-group" hidden>
						<div class="col-lg-12">Shift</div>
						<div class="col-lg-12">
							<input id="edit_rekapan_shift" class="form-control form-control-sm">
						</div>
					</div>

					<div class="form-group" hidden>
						<div class="col-lg-12">Outlet Id</div>
						<div class="col-lg-12">
							<input id="edit_rekapan_outlet_id" class="form-control form-control-sm">
						</div>
					</div>

					<div class="form-group" hidden>
						<div class="col-lg-6">Tipe</div>
						<div class="col-lg-12">
							<input type="text" id="edit_rekapan_type" class="form-control form-control-sm">
						</div>
					</div>

					<div class="form-group" hidden>
						<div class="col-lg-6">Kategori</div>
						<div class="col-lg-12">
							<input type="text" id="edit_rekapan_category" class="form-control form-control-sm">
						</div>
					</div>

					<div class="form-group" hidden>
						<div class="col-lg-6">Produk</div>
						<div class="col-lg-12">
							<input type="text" id="edit_rekapan_product" class="form-control form-control-sm">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" id="edit_rekapan_submit">
					Simpan
				</button>
			</div>
		</div>
	</div>
</div>

<!--Modal edit rekapan detail-->
<div class="modal fade" id="modal_edit_rekapan_detail" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Ubah Rekapan +</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="edit_rekapan_detail_form">
					<div class="form-group">
						<div class="col-lg-6">Modal</div>
						<div class="col-lg-12">
							<input type="number" id="edit_rekapan_detail_modal" class="form-control form-control-sm">
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-6">Harga</div>
						<div class="col-lg-12">
							<input type="number" id="edit_rekapan_detail_cost" class="form-control form-control-sm">
						</div>
					</div>

					<div class="form-group" hidden>
						<div class="col-lg-6">Tanggal</div>
						<div class="col-lg-12">
							<input type="date" id="edit_rekapan_detail_tanggal" class="form-control form-control-sm">
						</div>
					</div>

					<div class="form-group" hidden>
						<div class="col-lg-12">Shift</div>
						<div class="col-lg-12">
							<input id="edit_rekapan_detail_shift" class="form-control form-control-sm">
						</div>
					</div>

					<div class="form-group" hidden>
						<div class="col-lg-12">Outlet Id</div>
						<div class="col-lg-12">
							<input id="edit_rekapan_detail_outlet_id" class="form-control form-control-sm">
						</div>
					</div>

					<div class="form-group" hidden>
						<div class="col-lg-6">Tipe</div>
						<div class="col-lg-12">
							<input type="text" id="edit_rekapan_detail_type" class="form-control form-control-sm">
						</div>
					</div>

					<div class="form-group" hidden>
						<div class="col-lg-6">Kategori</div>
						<div class="col-lg-12">
							<input type="text" id="edit_rekapan_detail_category" class="form-control form-control-sm">
						</div>
					</div>

					<div class="form-group" hidden>
						<div class="col-lg-6">Produk</div>
						<div class="col-lg-12">
							<input type="text" id="edit_rekapan_detail_product" class="form-control form-control-sm">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"
						id="edit_rekapan_detail_submit">
					Simpan
				</button>
			</div>
		</div>
	</div>
</div>

<!--Modal rekapan total uang laci-->
<div class="modal fade" id="modal_rekapan_uang_laci" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Total uang di Laci</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="rekapan_uang_laci_form">
					<div class="form-group">
						<div class="col-lg-6">Uang</div>
						<div class="col-lg-12">
							<input type="number" class="form-control form-control-sm" id="total_uang_di_laci"
								   name="total_uang_di_laci">
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-6">Tanggal</div>
						<div class="col-lg-12">
							<input type="date" class="form-control form-control-sm" id="tanggal_uang_di_laci"
								   name="tanggal_uang_di_laci">
						</div>
					</div>

					<div class="form-check">
						<div class="col-lg-12">
							<input class="form-check-input" type="radio" name="shift_uang_di_laci" id="shift_pagi"
								   value="pagi">
							<label class="form-check-label">Pagi</label>
						</div>
					</div>

					<div class="form-check">
						<div class="col-lg-12">
							<input class="form-check-input" type="radio" name="shift_uang_di_laci" id="shift_sore"
								   value="sore">
							<label class="form-check-label">Sore</label>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"
						id="btn_total_uang_di_laci_submit">Simpan
				</button>
			</div>
		</div>
	</div>
</div>
