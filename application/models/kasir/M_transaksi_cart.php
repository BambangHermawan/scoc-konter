<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_transaksi_cart extends CI_Model
{
	var $column_order = array('category.name', 'product.name', 't_cart.cost', 't_cart.discount', 't_cart.many', 'ipo.many');
	var $column_search = array('category.name', 'product.name', 't_cart.cost', 't_cart.discount', 't_cart.many', 'ipo.many');
	var $order = array('t_cart.created_date' => 'asc');

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_datatables($user_id, $outlet_id)
	{
		$this->_get_datatables_query($user_id, $outlet_id);

		if ($_REQUEST['length'] != -1)
			$this->db->limit($_REQUEST['length'], $_REQUEST['start']);
		$query = $this->db->get();

		return $query->result();
	}

	private function _get_datatables_query($user_id, $outlet_id)
	{
		$this->db->select('t_cart.id as cart_id, user.id as user_id, t_cart.outlet_id, t_cart.item_outlet_id as item_outlet_id, format(t_cart.cost,0) as cost, t_cart.discount as discount, t_cart.many as many, product.name as product_name, ipo.many as stock, category.name as category_name')
			->from('transaction_cart as t_cart, user, item_product as product, item_product_outlet as ipo, item_category as category')
			->where('t_cart.user_id = user.id')
			->where('t_cart.item_outlet_id = ipo.id')
			->where('product.id = ipo.item_product_id')
			->where('product.item_category_id = category.id')
			->where('t_cart.user_id', $user_id)
			->where('t_cart.outlet_id', $outlet_id)
			->where('t_cart.deleted_date is null')
			->where('ipo.deleted_date is null');

		$i = 0;
		foreach ($this->column_search as $item) {
			if ($_REQUEST['search']['value']) {

				if ($i === 0) {
					$this->db->group_start();
					$this->db->like($item, $_REQUEST['search']['value']);
				} else {
					$this->db->or_like($item, $_REQUEST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if (isset($_REQUEST['order'])) {
			$this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function count_filtered($user_id, $outlet_id)
	{
		$this->_get_datatables_query($user_id, $outlet_id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_all($user_id, $outlet_id)
	{
		$this->db->select('t_cart.id as cart_id, user.id as user_id, t_cart.outlet_id, t_cart.item_outlet_id as item_outlet_id, format(t_cart.cost,0) as cost, t_cart.discount as discount, t_cart.many as many, product.name as product_name, ipo.many as stock, category.name as category_name')
			->from('transaction_cart as t_cart, user, item_product as product, item_product_outlet as ipo, item_category as category')
			->where('t_cart.user_id = user.id')
			->where('t_cart.item_outlet_id = ipo.id')
			->where('product.id = ipo.item_product_id')
			->where('product.item_category_id = category.id')
			->where('t_cart.user_id', $user_id)
			->where('t_cart.outlet_id', $outlet_id)
			->where('t_cart.deleted_date is null')
			->where('ipo.deleted_date is null');

		return $this->db->count_all_results();
	}

	function get_product_by_qrcode($outlet_id, $qrcode)
	{
		return $this->db->query("select id, item_product_id, cost, discount, DATE_FORMAT(discount_until, \"%d-%m-%Y\") as discount_until from item_product_outlet where outlet_id='$outlet_id' and qrcode='$qrcode'")->result();
	}

	function get_total_payment_cart($user_id, $outlet_id)
	{
		return $this->db->query("SELECT cost, many, discount, sum(many*cost) as total, (discount/100*cost)*many as discount FROM transaction_cart where user_id='$user_id' and outlet_id='$outlet_id' group by item_outlet_id, user_id, outlet_id")->result();
	}

	function add_transaction_cart($user_id, $outlet_id, $item_outlet_id, $cost)
	{
		$created_by = created_by();
		$created_date = created_date();

		$this->db->trans_begin();

		$item_outlet = $this->db->query("select id, discount, many, DATE_FORMAT(discount_until, \"%d-%m-%Y\") as discount_until from item_product_outlet where id='$item_outlet_id'")->result();

		if (count($item_outlet) > 0) {
			if (strtotime(date('d-m-Y')) > strtotime($item_outlet[0]->discount_until)) {
				$discount = 0;
			} else {
				$discount = $item_outlet[0]->discount;
			}

			$cek_keranjang = $this->db->query("select id, many from transaction_cart where user_id='$user_id' and outlet_id='$outlet_id' and item_outlet_id='$item_outlet_id'")->result();

			if (count($cek_keranjang) > 0) {
				if ($cek_keranjang[0]->many + 1 > $item_outlet[0]->many) {
					return false;
				} else {
					$total = $cek_keranjang[0]->many + 1;
					$this->db->query("update transaction_cart set many='$total', discount='$discount' where id='{$cek_keranjang[0]->id}'");
					return $this->db->trans_complete();
				}
			} else if (1 > $item_outlet[0]->many) {
				return false;
			} else {
				$this->db->query("insert into transaction_cart (user_id, outlet_id, item_outlet_id, cost, discount, many, created_by, created_date) values ('$user_id','$outlet_id', '$item_outlet_id', '" . preg_replace('/[.,]/', '', $cost) . "', '$discount', '1', '$created_by', '$created_date')");
				return $this->db->trans_complete();
			}
		}
		return false;
	}

	function edit_product_from_cart($cart_id, $many)
	{
		$modified_by = modified_by();
		$modified_date = modified_date();

		$cek_keranjang = $this->db->query("select id, item_outlet_id, many from transaction_cart where id='$cart_id'")->result();

		if (count($cek_keranjang) > 0) {
			$item_outlet = $this->db->query("select many, discount, DATE_FORMAT(discount_until, \"%d-%m-%Y\") as discount_until from item_product_outlet where id='{$cek_keranjang[0]->item_outlet_id}'")->result();

			if (count($item_outlet) > 0) {
				if (strtotime(date('d-m-Y')) > strtotime($item_outlet[0]->discount_until)) {
					$discount = 0;
				} else {
					$discount = $item_outlet[0]->discount;
				}
			}

			if ($cek_keranjang[0]->many + $many > $item_outlet[0]->many) {
				return false;
			} else {
				$total = $cek_keranjang[0]->many + $many;

				$this->db->query("update transaction_cart set many='$total', discount='$discount', modified_by='$modified_by', modified_date='$modified_date' where id='{$cek_keranjang[0]->id}'");
				$this->db->trans_complete();

				return true;
			}
		} else {
			return false;
		}
	}

	function delete_product_from_cart($transaction_cart_id)
	{
		return $this->db->query("delete from transaction_cart where id='$transaction_cart_id'");
	}
}
