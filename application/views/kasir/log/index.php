<?php $this->load->view('_layouts/_template/header') ?>

<div class="container-fluid">
	<?php $this->load->view('log/modal/transaksi'); ?>
	<div class="row">
		<div class="col-md-3">
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Filter Log <b><?= $outlet ?></b></h3>
				</div>
				<div class="card-body p-2">
					<div class="form-group">
						<div class="col-lg-4"><label for="date_begin">Mulai</label></div>
						<div class="col-lg-12">
							<input type="date" class="form-control form-control-sm"
								   name="date_begin"
								   id="date_begin" required>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4"><label for="date_end">Sampai</label></div>
						<div class="col-lg-12">
							<input type="date" class="form-control form-control-sm"
								   name="date_end"
								   id="date_end" required>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4"><label for="outlet_employee_id">Pengguna</label></div>
						<div class="col-lg-12">
							<select type="date" class="form-control form-control-sm"
									name="outlet_employee_id"
									id="outlet_employee_id">
							</select>
						</div>
					</div>

				</div>
				<div class="card-footer">
					<div class="float-right">
						<button type="submit" class="btn btn-primary btn-sm" id="btn_search_all">Filter
							Data
						</button>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-9">
			<div class="card card-primary card-outline">
				<div class="card-header">
					<h3 class="card-title">Log Transfer</h3>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table id="table_log" class="table table-striped table-hover dt-responsive display nowrap"
							   style="width:100%">
							<thead>
							<tr>
								<th data-orderable="false">No</th>
								<th>Pengguna</th>
								<th>Aktifitas</th>
								<th>Catatan</th>
								<th>Tanggal</th>
								<th data-orderable="false">Aksi</th>
							</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	//init grafik laporan
	$(document).ready(function () {
		var today = new Date();
		date = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2)

		$("#date_begin").val(date)
		$("#date_end").val(date)

		init_table_log()
		get_outlet_employee()
	})

	//filter grafik laporan
	$('#btn_search_all').click(function () {

		button = "&nbsp;<button class='btn btn-info btn-sm' id='btn_detail_click'>\
            <i class='fas fa-search'></i>&nbsp;Detail</button>"

		table_log = $("#table_log").DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			autoWidth: true,
			ajax: "<?= base_url(); ?>kasir/log/get_all_log/" + $('#outlet_employee_id option:selected').val() + "/" + "<?= $this->uri->segment(4) ?>" + "/" + $('#date_begin').val() + "/" + $('#date_end').val(),
			columns: [
				{
					data: "id",
					width: "0.8%",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: 'user_name'
				},
				{
					data: 'activity'
				},
				{
					data: 'note'
				},
				{
					data: 'created_date'
				},
				{
					defaultContent: button
				}
			]
		})

	})

	//dropdown outlet
	$('#outlet_id').change(function () {

		var outlet_id = "<?= $this->uri->segment(4) ?>"

		//dropdown pilih pengguna
		$.ajax({
			url: "<?= base_url(); ?>kasir/karyawan/get_karyawan/" + outlet_id,
			success: function (result) {
				if (result.success) {

					user_list = '<option value="0">Semua Shift</option>'

					result.data.forEach(function (outlet) {
						if (outlet['wewenang'] !== 'ADMIN' && outlet['wewenang'] !== 'OWNER' && outlet['outlet_id'] === outlet_id) {
							user_list += "<option value='" + outlet['user_id'] + "'>" + outlet['name'] + "</option>";
						}
					})

					$('#outlet_employee_id').html(user_list)
				}
			}
		})
	});

	//button modal ubah data pulsa
	$("#table_log").on('click', 'tr #btn_detail_click', function () {

		tr = $(this).closest('tr')
		data = table_log.row(tr).data()

		$.ajax({
			url: "<?= base_url(); ?>kasir/log/get_detail_log/" + data.object_id,
			success: function (result) {
				if (result.success) {
					if (result.data.status === 'tambah_unit' || result.data.status === 'salah_transfer' || result.data.status === 'transfer_unit') {
						$("#detail_log_sender").text(" : " + data.outlet_name)
						$("#detail_log_destination").text(" : " + result.data.name)
						$("#detail_log_type").text(" : " + result.data.type)
						$("#detail_log_kategori").text(" : " + result.data.category)
						$("#detail_log_produk").text(" : " + result.data.product)
						$("#detail_log_many").text(" : " + result.data.awl)
						$("#detail_log_plus").text(" : " + result.data.plus)
						$("#detail_log_after_many").text(" : " + result.data.ak)

						$('#modal_show_detail_transfer_product_unit').modal({backdrop: 'static', keyboard: false})
					}

					if (result.data.status === 'tambah_saldo') {

						$("#detail_add_saldo_sender").text(" : " + data.outlet_name)
						$("#detail_add_saldo_destination").text(" : " + result.data.name)
						$("#detail_add_saldo_type").text(" : " + result.data.type)
						$("#detail_add_saldo_kategori").text(" : " + result.data.category)
						$("#detail_add_saldo_produk").text(" : " + result.data.product)
						$("#detail_add_saldo_many").text(" : " + result.data.awl)
						$("#detail_add_saldo_plus").text(" : " + result.data.plus)
						$("#detail_add_saldo_after_many").text(" : " + result.data.ak)

						$('#modal_show_detail_add_saldo_pulsa').modal({backdrop: 'static', keyboard: false})
					}
				}
			}
		})
	})

	//init datatables table log
	function init_table_log() {

		button = "&nbsp;<button class='btn btn-info btn-sm' id='btn_detail_click'>\
            <i class='fas fa-search'></i>&nbsp;Detail</button>"

		table_log = $("#table_log").DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			autoWidth: true,
			ajax: "<?= base_url(); ?>kasir/log/get_all_log/" + $('#outlet_employee_id option:selected').val() + "/" + "<?= $this->uri->segment(4); ?>" + "/" + $('#date_begin').val() + "/" + $('#date_end').val(),
			columns: [
				{
					data: "id",
					width: "0.8%",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: 'user_name'
				},
				{
					data: 'activity'
				},
				{
					data: 'note'
				},
				{
					data: 'created_date'
				},
				{
					defaultContent: button
				},
			]
		})
	}

	//dropdown pilih pengguna
	function get_outlet_employee() {
		user_id = "<?= $_SESSION['id'] ?>"
		outlet_id = "<?= $this->uri->segment(4) ?>"

		$.ajax({
			url: "<?= base_url(); ?>kasir/karyawan/get_karyawan/" + outlet_id,
			success: function (result) {
				if (result.success) {

					var user_list = ''

					result.data.forEach(function (outlet) {
						if (outlet['wewenang'] !== 'ADMIN' && outlet['wewenang'] !== 'OWNER' && outlet['outlet_id'] === outlet_id && outlet['user_id'] === user_id) {
							user_list += "<option value='" + outlet['user_id'] + "'>" + outlet['name'] + "</option>";
						}
					})

					$('#outlet_employee_id').html(user_list)
				}
			}
		})
	}
</script>

<?php $this->load->view('_layouts/_template/js') ?>
