<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pengguna extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_pengguna_by_id($user_id)
	{
		return $this->db->query("select user.id as user_id, user_roles.id as user_roles_id, user_roles.wewenang as wewenang, username, user.name from user, user_roles where user.user_roles_id = user_roles.id and user.id = '$user_id' and user.deleted_date is null and user_roles.deleted_date is null")->result()[0];
	}
}
