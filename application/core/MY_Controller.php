<?php
session_start();

class MY_Controller extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->helper('datetime_helper');
		$this->load->helper('json_helper');
		$this->load->helper('log_helper');
	}

	function time_of_shift($shift, $date)
	{
		if ($shift == 'pagi') {
			$datetime = new DateTime($date);
			$datetime->modify('+5 hour');
			$hari['begin'] = $datetime->format('Y-m-d H:i:s');

			$datetime = new DateTime($date);
			$datetime->modify('+15 hour');
			$datetime->modify('+59 minute');
			$datetime->modify('+59 second');
			$hari['end'] = $datetime->format('Y-m-d H:i:s');

		} else if ($shift == 'sore') {
			$datetime = new DateTime($date);
			$datetime->modify('+16 hour');
			$hari['begin'] = $datetime->format('Y-m-d H:i:s');

			$datetime = new DateTime($date);
			$datetime->modify('+1 day');
			$datetime->modify('+4 hour');
			$datetime->modify('+59 minute');
			$datetime->modify('+59 second');
			$hari['end'] = $datetime->format('Y-m-d H:i:s');

		} else {
			$datetime = new DateTime($date);
			$datetime->modify('+5 hour');
			$hari['begin'] = $datetime->format('Y-m-d H:i:s');

			$datetime = new DateTime($date);
			$datetime->modify('+1 day');
			$datetime->modify('+4 hour');
			$datetime->modify('+59 minute');
			$datetime->modify('+59 second');

			$hari['end'] = $datetime->format('Y-m-d H:i:s');
		}

		return $hari;
	}
}
