<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Outlet extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_outlet', '', true);

		if (!isset($_SESSION)) {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'ADMIN') {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'OWNER') {
			redirect('auth/login');
		} elseif ($_SESSION['status_user'] == 'KASIR') {
			redirect('auth/login');
		}
	}

	function index()
	{
		$data['title'] = 'Outlet';
		$data['site_navigation'] = 'Outlet';

		return $this->load->view('outlet/index', $data);
	}

	function get_all_outlet()
	{
		$result = $this->M_outlet->get_all_outlet();

		if ($result) {

			$response = [
				'success' => true,
				'data' => $result,
				'total' => count($result)
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function get_all_outlet_detail()
	{
		$result = $this->M_outlet->get_datatables();

		if (count($result) > 0) {
			foreach ($result as $item) {
				$item->asset = $this->M_outlet->get_all_outlet_detail($item->id);
			}
		}

		$response = [
			'success' => true,
			'draw' => $_REQUEST['draw'],
			'recordsTotal' => $this->M_outlet->count_all(),
			'recordsFiltered' => $this->M_outlet->count_filtered(),
			'data' => $result
		];

		return to_json($response);
	}

	function add_outlet()
	{
		$name = $this->input->post('name');
		$address = $this->input->post('address');

		$result = $this->M_outlet->add_outlet($name, $address);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Tambah outlet ' . $name . ' berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Tambah outlet ' . $name . ' gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function edit_outlet()
	{
		$id = $this->input->post('id');
		$name = $this->input->post('name');
		$address = $this->input->post('address');

		$result = $this->M_outlet->edit_outlet($id, $name, $address);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Ubah outlet ' . $name . ' berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Ubah outlet ' . $name . ' gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function delete_outlet()
	{
		$id = $this->input->post('id');

		$result = $this->M_outlet->delete_outlet($id);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Hapus outlet berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Hapus outlet gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}
}
