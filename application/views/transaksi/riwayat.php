<?php $this->load->view('_layouts/_template/header') ?>

<style>
	#scanner_qrcode {
		max-width: 100%;
		height: auto;
	}
</style>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-3">
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Filter Transaksi <b><?= $outlet[0]->name ?></b></h3>
				</div>
				<div class="card-body p-2">
					<div class="form-group">
						<div class="col-lg-4"><label for="date_begin">Mulai</label></div>
						<div class="col-lg-12">
							<input type="date" class="form-control form-control-sm"
								   name="date_begin"
								   id="date_begin" required>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4"><label for="date_end">Sampai</label></div>
						<div class="col-lg-12">
							<input type="date" class="form-control form-control-sm"
								   name="date_end"
								   id="date_end" required>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4"><label for="outlet_employee_id">Pengguna</label></div>
						<div class="col-lg-12">
							<select type="date" class="form-control form-control-sm"
									name="outlet_employee_id"
									id="outlet_employee_id">
							</select>
						</div>
					</div>

				</div>
				<div class="card-footer">
					<div class="float-right">
						<button type="submit" class="btn btn-primary btn-sm" id="btn_search_all">Filter
							Data
						</button>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-9">
			<div class="card card-primary card-outline">
				<div class="card-header">
					<h3 class="card-title">Riwayat Transaksi</b></h3>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table id="table_transaksi_log"
							   class="table table-striped table-hover dt-responsive display nowrap"
							   style="width:100%">
							<thead>
							<tr>
								<th class="nomor" data-orderable="false">No</th>
								<th>Tanggal</th>
								<th>Nomor Transaksi</th>
								<th>Jumlah Beli</th>
								<th>Total Diskon</th>
								<th>Grand Total</th>
								<th>Kasir</th>
								<th class="nomor" data-orderable="false">Aksi</th>
							</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--Modal detail transaksi-->
<div class="modal fade" id="modal_detail_transaksi" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Detail Transaksi</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="table_transaksi_detail"
						   class="table table-striped table-hover dt-responsive display nowrap"
						   style="width:100%">
						<thead>
						<tr>
							<th>Tipe</th>
							<th>Kategori</th>
							<th>Barang</th>
							<th>Harga</th>
							<th>Jumlah Beli</th>
							<th>Diskon (%)</th>
							<th>Sub Total</th>
						</tr>
						</thead>
						<tbody>
						</tbody>
						<input id="detail_transaction_code" hidden>
						<tr>
							<td colspan="6" style='text-align:right'>Total Diskon</td>
							<td id="detail_diskon_total"></td>
						</tr>
						<tr>
							<td colspan="6" style='text-align:right'><b>Grand Total</b></td>
							<td id="detail_grand_total"></td>
						</tr>
						<tr>
							<td colspan="6" style='text-align:right'>Bayar</td>
							<td id="detail_payment"></td>
						</tr>
						<tr>
							<td colspan="6" style='text-align:right'>Kembali</td>
							<td id="detail_change_payment"></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning btn-sm" id="btn_print_transaction_detail"
						data-dismiss="modal"><i class="fa fa-print"></i> Cetak
				</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Selesai</button>
			</div>
		</div>
	</div>
</div>

<script>
	//init transaksi
	$(document).ready(function () {
		var today = new Date();
		date = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2)

		$("#date_begin").val(date)
		$("#date_end").val(date)

		init_table_transaksi_log()
		get_outlet_employee()
	})

	//filter transaksi
	$('#btn_search_all').click(function () {

		var user_id = $('#outlet_employee_id option:selected').val();


		button = "&nbsp;<button class='btn btn-info btn-sm' id='btn_detail_transaction_click'>\
            <i class='fas fa-search'></i>&nbsp;Detail</button>"

		table_transaksi_log = $("#table_transaksi_log").DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			autoWidth: true,
			ajax: "<?= base_url(); ?>transaksi/get_all_transaction/" + user_id + "/" + "<?= $this->uri->segment(3); ?>" + "/" + $('#date_begin').val() + "/" + $('#date_end').val(),
			columns: [
				{
					data: "id",
					width: "0.8%",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: 'created_date'
				},
				{
					data: 'transaction_code'
				},
				{
					data: 'total_item'
				},
				{
					data: 'discount_total'
				},
				{
					data: 'sub_total'
				},
				{
					data: 'user_name'
				},
				{
					defaultContent: button
				}
			]
		})
	})

	//button print detail transaksi
	$('#btn_print_transaction_detail').click(function () {

		var transaction_code = $('#detail_transaction_code').val();

		link = "<?= base_url(); ?>transaksi/print_transaction_detail/" + transaction_code
		window.open(link, '_blank');
	})

	//button modal detail transaksi
	$("#table_transaksi_log").on('click', 'tr #btn_detail_transaction_click', function () {

		tr = $(this).closest('tr')
		data = table_transaksi_log.row(tr).data()

		$.ajax({
			url: "<?= base_url(); ?>transaksi/get_transaction_detail/" + data.transaction_code,
			success: function (result) {
				if (result.success) {
					table_transaksi_detail = $("#table_transaksi_detail").DataTable({
						paging: false,
						ordering: false,
						info: false,
						searching: false,
						destroy: true,
						autoWidth: true,
						ajax: "<?= base_url(); ?>transaksi/get_transaction_detail/" + data.transaction_code,
						columns: [
							{
								data: 'type'
							},
							{
								data: 'category'
							},
							{
								data: 'product'
							},
							{
								data: 'cost'
							},
							{
								data: 'lk'
							},
							{
								data: 'discount'
							},
							{
								data: 'sub_total'
							},

						]
					});

					$("#detail_transaction_code").val(data.transaction_code)
					$("#detail_grand_total").text(to_rupiah(data.sub_total.replace(',', '')))
					$("#detail_diskon_total").text(to_rupiah(data.discount_total.replace(',', '')))
					$("#detail_payment").text(to_rupiah(data.payment.replace(',', '')))
					$("#detail_change_payment").text(to_rupiah(data.change_payment.replace(',', '')))

					$('#modal_detail_transaksi').modal({backdrop: 'static', keyboard: false})
				}
			}
		})
	})

	//init datatables table transaksi cart
	function init_table_transaksi_log() {

		var user_id = $('#user_id option:selected').val();

		button = "&nbsp;<button class='btn btn-info btn-sm' id='btn_detail_transaction_click'>\
            <i class='fas fa-search'></i>&nbsp;Detail</button>"

		table_transaksi_log = $("#table_transaksi_log").DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			autoWidth: true,
			ajax: "<?= base_url(); ?>transaksi/get_all_transaction/" + user_id + "/" + "<?= $this->uri->segment(3); ?>" + "/" + $('#date_begin').val() + "/" + $('#date_end').val(),
			columns: [
				{
					data: "id",
					width: "0.8%",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: 'created_date'
				},
				{
					data: 'transaction_code'
				},
				{
					data: 'total_item'
				},
				{
					data: 'discount_total'
				},
				{
					data: 'sub_total'
				},
				{
					data: 'user_name'
				},
				{
					defaultContent: button
				}
			]
		})
	}

	//dropdown pilih pengguna outlet
	function get_outlet_employee() {
		outlet_id = "<?= $this->uri->segment(3) ?>"

		$.ajax({
			url: "<?= base_url(); ?>karyawan/get_karyawan/" + outlet_id,
			success: function (result) {
				if (result.success) {

					var user_list = '<option value="0">Semua Shift</option>'

					result.data.forEach(function (outlet) {
						if (outlet['wewenang'] !== 'ADMIN' && outlet['wewenang'] !== 'OWNER' && outlet['outlet_id'] === outlet_id) {
							user_list += "<option value='" + outlet['user_id'] + "'>" + outlet['name'] + "</option>";
						}
					})
					$('#outlet_employee_id').html(user_list)
				}
			}
		})
	}

	//konversi angka ke format rupiah
	function to_rupiah(angka) {
		var rev = parseInt(angka, 10).toString().split('').reverse().join('');

		var rev2 = '';
		for (var i = 0; i < rev.length; i++) {
			rev2 += rev[i];
			if ((i + 1) % 3 === 0 && i !== (rev.length - 1)) {
				rev2 += ',';
			}
		}

		return 'Rp. ' + rev2.split('').reverse().join('');
	}

</script>

<?php $this->load->view('_layouts/_template/js') ?>
