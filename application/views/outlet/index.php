<?php $this->load->view('_layouts/_template/header') ?>

<div class="container-fluid">

	<div class="row">
		<div class="col-12">
			<div class="card card-outline card-primary">
				<div class="card-header">
					<h3 class="card-title"><b>Outlet</b></h3>
					<div class="float-right">
						<button class="btn btn-primary btn-sm" id="btn_add_outlet"><i class="fa fa-plus"></i> Outlet
						</button>
					</div>
				</div>
				<!-- /.card-header -->
				<div class="card-body">
					<div class="table-responsive">
						<table id="table_outlet" class="table table-striped table-hover dt-responsive display nowrap"
							   style="width:100%">
							<thead>
							<tr>
								<th class="nomor" data-orderable="false">No</th>
								<th>Nama</th>
								<th>Alamat</th>
								<th data-orderable="false">Total Aset</th>
								<th data-orderable="false">Aksi</th>
							</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--modal tambah outlet-->
<div class="modal fade" id="modal_add_outlet" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Tambah Outlet</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="add_outlet_form">
					<div class="form-group">
						<div class="col-lg-4">Nama</div>
						<div class="col-lg-12">
							<input type="text" class="form-control" id="add_outlet_name" name="add_outlet_name">
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-4">Alamat</div>
						<div class="col-lg-12">
							<textarea class="form-control" id="add_outlet_address" name="add_outlet_address"></textarea>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"
						id="save_add_outlet">Simpan
				</button>
			</div>
		</div>
	</div>
</div>

<!--modal edit outlet-->
<div class="modal fade" id="modal_edit_outlet" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Ubah Outlet</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="edit_outlet_form">
					<div class="form-group">
						<div class="col-lg-4">Nama</div>
						<div class="col-lg-12">
							<input type="text" class="form-control" id="edit_outlet_name" name="edit_outlet_name">
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-4">Alamat</div>
						<div class="col-lg-12">
							<textarea class="form-control" id="edit_outlet_address"
									  name="edit_outlet_address"></textarea>
						</div>
					</div>
					<div class="form-group" hidden>
						<div class="col-lg-12">
							<input class="form-control" id="edit_outlet_id" name="edit_outlet_id">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"
						id="save_edit_outlet">Simpan
				</button>
			</div>
		</div>
	</div>
</div>

<script>
	//init table outlet
	$(document).ready(function () {
		init_table_outlet()
	})

	//validasi form tambah data outlet
	$("#add_outlet_form").validate({
		rules: {
			add_outlet_name: {
				required: true,
				maxlength: 20
			},
			add_outlet_address: {
				required: true,
				maxlength: 100
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//modal tambah data outlet
	$("#btn_add_outlet").on('click', function () {
		$("#modal_add_outlet").modal({backdrop: 'static', keyboard: false})
	})

	//simpan tambah data outlet
	$("#save_add_outlet").on('click', function () {

		if ($('#add_outlet_form').valid()) {
			$.ajax({
				url: "<?php '/'; ?>outlet/add_outlet",
				type: "POST",
				data: {
					'name': $("#add_outlet_name").val(),
					'address': $("#add_outlet_address").val()
				},
				success: function (result) {

					if (result.success) {
						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});
						init_table_outlet()

					} else {
						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						init_table_outlet()
					}
				}
			})
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});

			init_table_outlet()
		}

	})

	//validasi form ubah outlet
	$("#edit_outlet_form").validate({
		rules: {
			edit_outlet_id: {
				required: true,
				maxlength: 11
			},
			edit_outlet_name: {
				required: true,
				maxlength: 20
			},
			edit_outlet_address: {
				required: true,
				maxlength: 100
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//modal ubah data outlet
	$("#table_outlet").on('click', 'tr #btn_edit_click', function () {
		tr = $(this).closest('tr')
		data = table_outlet.row(tr).data()

		$("#edit_outlet_id").val(data.id)
		$("#edit_outlet_name").val(data.name)
		$("#edit_outlet_address").val(data.address)

		$('#modal_edit_outlet').modal({backdrop: 'static', keyboard: false})
	})

	//simpan ubah data outlet
	$("#save_edit_outlet").on('click', function () {

		if ($('#edit_outlet_form').valid()) {
			$.ajax({
				url: "<?= base_url(); ?>outlet/edit_outlet",
				type: "POST",
				data: {
					'id': $("#edit_outlet_id").val(),
					'name': $("#edit_outlet_name").val(),
					'address': $("#edit_outlet_address").val()
				},
				success: function (result) {

					if (result.success) {
						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						init_table_outlet()

					} else {
						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						init_table_outlet()
					}
				}
			})
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});

			init_table_pengguna()
		}

	})

	//hapus data produk outlet
	$("#table_outlet").on('click', 'tr #btn_delete_click', function () {
		tr = $(this).closest('tr')
		data = table_outlet.row(tr).data()
		id = data.id

		Swal.fire({
			title: 'Peringatan!',
			text: 'Apakah Anda yakin akan menghapus outlet ' + data.name + ' ?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya, hapus data ini!'
		}).then((result) => {
			if (result.value) {

				$.ajax({
					url: "<?= base_url(); ?>outlet/delete_outlet",
					type: "POST",
					data: {id: id},
					success(result) {

						if (result.success) {
							Swal.fire({
								title: 'Berhasil',
								text: result.message,
								icon: result.type,
								showConfirmButton: false,
								timer: 1500
							});

							init_table_outlet()
						} else {
							Swal.fire({
								title: 'gagal',
								text: result.message,
								icon: result.type,
								showConfirmButton: false,
								timer: 1500
							});
						}

					}
				})
			}
		})
	})

	//init datatables table aksesoris
	function init_table_outlet() {
		button = "&nbsp;<button class='btn btn-warning btn-sm' id='btn_edit_click'>\
            <i class='fas fa-edit'></i>&nbsp;Ubah</button>"
		button += "&nbsp;<button class='btn btn-danger btn-sm' id='btn_delete_click'>\
            <i class='fas fa-trash'></i>&nbsp;Hapus</button>"

		table_outlet = $("#table_outlet").DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			autoWidth: true,
			width: "0.8%",
			ajax: "<?= base_url(); ?>outlet/get_all_outlet_detail",
			columns: [
				{
					data: "id",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: 'name'
				},
				{
					data: 'address'
				},
				{
					data: 'asset'
				},
				{
					defaultContent: button
				},
			]
		})
	}
</script>

<?php $this->load->view('_layouts/_template/js') ?>
