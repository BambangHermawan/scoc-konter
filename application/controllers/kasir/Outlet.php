<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Outlet extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('kasir/M_outlet', 'M_outlet', true);

		if (!isset($_SESSION)) {
			redirect('auth/login');
		} elseif ($_SESSION['status_user'] != 'KASIR') {
			redirect('auth/login');
		}
	}

	function get_all_outlet()
	{
		$result = $this->M_outlet->get_all_outlet();

		if ($result) {

			$response = [
				'success' => true,
				'data' => $result,
				'total' => count($result)
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}
}
