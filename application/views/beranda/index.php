<?php $this->load->view('_layouts/_template/header') ?>

<div class="container-fluid">

	<div class="row">
		<div class="col-lg-4 col-6">
			<div class="small-box bg-info">
				<div class="inner">
					<h3><b><?php print $transaction; ?></b></h3>
					<p>Transaksi Bulan Ini</p>
				</div>
				<div class="icon">
					<i class="ion ion-bag"></i>
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-6">
			<div class="small-box bg-danger">
				<div class="inner">
					<h3><b><?php print $transaction_unit; ?></b></h3>
					<p>Barang terjual bulan ini</p>
				</div>
				<div class="icon">
					<i class="ion ion-pie-graph"></i>
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-6">
			<div class="small-box bg-success">
				<div class="inner">
					<h3><b>Rp. <?php print isset($laba_rugi) ? $laba_rugi : 0; ?></b></h3>
					<p>Laba / rugi bulan ini</p>
				</div>
				<div class="icon">
					<i class="ion ion-stats-bars"></i>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-8">
			<div class="card card-outline card-primary">
				<div class="card-header">
					<h3 class="card-title"><i class="fas fa-chart-bar mr-1"></i> <b>Grafik Transaksi</b></h3>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="form-group">
							<div class="col-lg-4"><label for="date_begin">Mulai</label></div>
							<div class="col-lg-12">
								<input type="date" class="form-control form-control-sm"
									   name="date_begin"
									   id="date_begin" required>
							</div>
						</div>

						<div class="form-group">
							<div class="col-lg-4"><label for="date_end">Sampai</label></div>
							<div class="col-lg-12">
								<input type="date" class="form-control form-control-sm"
									   name="date_end"
									   id="date_end" required>
							</div>
						</div>

						<div class="form-group">
							<div class="col-lg-4"><label for="outlet_id">Outlet</label></div>
							<div class="col-lg-12">
								<select type="date" class="form-control form-control-sm"
										name="outlet_id"
										id="outlet_id">
								</select>
							</div>
						</div>

						<div class="form-group">
							<div class="col-lg-4"><label for="outlet_id">Shift</label></div>
							<div class="col-lg-12">
								<select type="date" class="form-control form-control-sm"
										name="user_id"
										id="user_id">
								</select>
							</div>
						</div>

						<div class="form-group">
							<div class="col-lg-4"><label for="outlet_id">Filter</label></div>
							<div class="col-lg-12">
								<button type="submit" class="btn btn-primary btn-sm" id="btn_search_all">Submit
								</button>
							</div>

						</div>
					</div>
					<hr>
					<div id="laporan_chart_container">
						<canvas id="laporan_chart"></canvas>
					</div>
				</div>
			</div>
		</div>
		<div class="col-4">
			<div class="col-12 col-md-12 col-md-4">
				<div class="info-box mb-3">
					<span class="info-box-icon bg-info elevation-1"><i class="fas fa-shopping-cart"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">TOTAL TRANSAKSI</span>
						<span class="info-box-number" id="transaction_by_duration_count">0</span>
					</div>
				</div>
			</div>
			<div class="clearfix hidden-md-up"></div>
			<div class="col-12 col-md-12 col-md-4">
				<div class="info-box mb-3">
					<span class="info-box-icon bg-info elevation-1"><i class="fas fa-stamp"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">BARANG TERJUAL</span>
						<span class="info-box-number" id="transaction_unit_by_duration_count">0</span>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-12 col-md-4">
				<div class="info-box">
					<span class="info-box-icon bg-info elevation-1"><i class="fas fa-plus"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">TOTAL LABA / RUGI</span>
						<span class="info-box-number" id="laba_rugi_by_duration_count">0</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	var data_hari
	var data_outlet
	var data_aksesoris
	var data_perdana
	var data_voucher
	var data_pulsa

	var today = new Date();
	var firstDay = new Date(today.getFullYear(), today.getMonth(), 1);
	var lastDay = new Date(today.getFullYear(), today.getMonth() + 1, 0);

	var firstDate = firstDay.getFullYear() + '-' + ('0' + (firstDay.getMonth() + 1)).slice(-2) + '-' + ('0' + firstDay.getDate()).slice(-2);
	var lastDate = lastDay.getFullYear() + '-' + ('0' + (lastDay.getMonth() + 1)).slice(-2) + '-' + ('0' + lastDay.getDate()).slice(-2);


	$(document).ready(function () {

		var today = new Date();
		date = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2)

		$("#date_begin").val(date)
		$("#date_end").val(date)

		get_outlet()
		get_outlet_employee()

		//default transaksi hari ini
		$.ajax({
			type: "GET",
			contentType: "application/json; charset=utf-8",
			url: "<?= base_url(); ?>laporan/get_statistic_by_date/" + 0 + "/" + 0 + "/" + $('#date_begin').val() + "/" + $("#date_end").val(),
			success(result) {
				if (result.success) {
					data_outlet = result.data.outlet
					data_aksesoris = result.data.aksesoris
					data_perdana = result.data.perdana
					data_voucher = result.data.voucher
					data_pulsa = result.data.pulsa

					setTimeout(() => {

						document.getElementById("laporan_chart_container").innerHTML = '&nbsp;';
						document.getElementById("laporan_chart_container").innerHTML = '<canvas id="laporan_chart"></canvas>';
						var ctx = document.getElementById("laporan_chart").getContext("2d");

						var laporan_chart = new Chart(ctx, {
							type: 'bar',
							data: {
								labels: data_outlet,
								datasets: [{
									label: 'Aksesoris',
									data: data_aksesoris,
									backgroundColor: [
										'rgba(255, 99, 132, 0.5)',
										'rgba(255, 99, 132, 0.5)',
										'rgba(255, 99, 132, 0.5)',
										'rgba(255, 99, 132, 0.5)'
									],
								}, {
									label: 'Perdana',
									data: data_perdana,
									backgroundColor: [
										'rgba(54, 162, 235, 0.5)',
										'rgba(54, 162, 235, 0.5)',
										'rgba(54, 162, 235, 0.5)',
										'rgba(54, 162, 235, 0.5)'
									]
								}, {
									label: 'Voucher',
									data: data_voucher,
									backgroundColor: [
										'rgba(255, 206, 86, 0.5)',
										'rgba(255, 206, 86, 0.5)',
										'rgba(255, 206, 86, 0.5)',
										'rgba(255, 206, 86, 0.5)'
									],
								}, {
									label: 'Pulsa',
									data: data_pulsa,
									backgroundColor: [
										'rgba(75, 192, 192, 0.5)',
										'rgba(75, 192, 192, 0.5)',
										'rgba(75, 192, 192, 0.5)',
										'rgba(75, 192, 192, 0.5)'
									]
								}]
							},
							options: {
								scales: {
									yAxes: [{
										ticks: {
											beginAtZero: true
										}
									}]
								}
							}
						})
					}, 500);
				}
			}
		});

		//default transaksi hari ini
		$.ajax({
			type: "POST",
			contentType: "application/json; charset=utf-8",
			url: "<?= base_url(); ?>beranda/rekapan_chart/" + 0 + "/" + 0 + "/" + $('#date_begin').val() + "/" + $("#date_end").val(),
			success(result) {
				if (result.success) {
					$('#transaction_by_duration_count').html(result.data.transaction_by_duration)
					$('#transaction_unit_by_duration_count').html(result.data.transaction_unit_by_duration)
					if (result.data.laba_rugi_by_duration != null) {
						$('#laba_rugi_by_duration_count').html(result.data.laba_rugi_by_duration)
					} else {
						$('#laba_rugi_by_duration_count').html("0")
					}
				} else {
					$('#transaction_by_duration_count').html("0")
					$('#transaction_unit_by_duration_count').html("0")
					$('#laba_rugi_by_duration_count').html("0")
				}
			}
		});
	});

	//filter grafik laporan
	$('#btn_search_all').click(function () {

		$.ajax({
			type: "POST",
			contentType: "application/json; charset=utf-8",
			url: "<?= base_url(); ?>laporan/get_total_all_by_outlet_id/" + $('#user_id option:selected').val() + "/" + $('#outlet_id').val() + "/" + $('#date_begin').val() + "/" + $("#date_end").val(),
			success(result) {
				$('#rekapan_laku').text(result.data['lk'])
				$('#rekapan_omset').text(result.data['omset'])
				$('#rekapan_untung').text(result.data['advantage'])
				$('#rekapan_rugi').text(result.data['rugi'])
				$('#rekapan_aset').text(result.data['asset'])
				$('#rekapan_persentase').text(result.data['persentase'])
			}
		})

		//default transaksi hari ini
		$.ajax({
			type: "POST",
			contentType: "application/json; charset=utf-8",
			url: "<?= base_url(); ?>beranda/rekapan_chart/" + $('#user_id option:selected').val() + "/" + $('#outlet_id').val() + "/" + $('#date_begin').val() + "/" + $("#date_end").val(),
			success(result) {
				if (result.success) {
					$('#transaction_by_duration_count').html(result.data.transaction_by_duration)
					$('#transaction_unit_by_duration_count').html(result.data.transaction_unit_by_duration)
					if (result.data.laba_rugi_by_duration != null) {
						$('#laba_rugi_by_duration_count').html(result.data.laba_rugi_by_duration)
					} else {
						$('#laba_rugi_by_duration_count').html("0")
					}
				} else {
					$('#transaction_by_duration_count').html("0")
					$('#transaction_unit_by_duration_count').html("0")
					$('#laba_rugi_by_duration_count').html("0")
				}
			}
		});

		$.ajax({
			url: "<?= base_url(); ?>laporan/get_statistic_by_date/" + $('#user_id option:selected').val() + "/" + $('#outlet_id').val() + "/" + $('#date_begin').val() + "/" + $("#date_end").val(),
			success(result) {
				if (result.success) {
					data_outlet = result.data.outlet
					data_aksesoris = result.data.aksesoris
					data_perdana = result.data.perdana
					data_voucher = result.data.voucher
					data_pulsa = result.data.pulsa
				}
			}
		})

		setTimeout(() => {

			document.getElementById("laporan_chart_container").innerHTML = '&nbsp;';
			document.getElementById("laporan_chart_container").innerHTML = '<canvas id="laporan_chart"></canvas>';
			var ctx = document.getElementById("laporan_chart").getContext("2d");

			var laporan_chart = new Chart(ctx, {
				type: 'bar',
				data: {
					labels: data_outlet,
					datasets: [{
						label: 'Aksesoris',
						data: data_aksesoris,
						backgroundColor: [
							'rgba(255, 99, 132, 0.5)',
							'rgba(255, 99, 132, 0.5)',
							'rgba(255, 99, 132, 0.5)',
							'rgba(255, 99, 132, 0.5)'
						],
					}, {
						label: 'Perdana',
						data: data_perdana,
						backgroundColor: [
							'rgba(54, 162, 235, 0.5)',
							'rgba(54, 162, 235, 0.5)',
							'rgba(54, 162, 235, 0.5)',
							'rgba(54, 162, 235, 0.5)'
						]
					}, {
						label: 'Voucher',
						data: data_voucher,
						backgroundColor: [
							'rgba(255, 206, 86, 0.5)',
							'rgba(255, 206, 86, 0.5)',
							'rgba(255, 206, 86, 0.5)',
							'rgba(255, 206, 86, 0.5)'
						],
					}, {
						label: 'Pulsa',
						data: data_pulsa,
						backgroundColor: [
							'rgba(75, 192, 192, 0.5)',
							'rgba(75, 192, 192, 0.5)',
							'rgba(75, 192, 192, 0.5)',
							'rgba(75, 192, 192, 0.5)'
						]
					}]
				},
				options: {
					scales: {
						yAxes: [{
							ticks: {
								beginAtZero: true
							}
						}]
					}
				}
			})
		}, 500);
	})

	//dropdown outlet
	$('#outlet_id').change(function () {

		outlet_id = $('#outlet_id option:selected').val();

		//dropdown pilih pengguna
		$.ajax({
			url: "<?= base_url(); ?>karyawan/get_karyawan/" + outlet_id,
			success: function (result) {
				if (result.success) {

					user_list = '<option value="0">Semua Shift</option>'

					result.data.forEach(function (outlet) {
						if (outlet['wewenang'] !== 'ADMIN' && outlet['wewenang'] !== 'OWNER' && outlet['outlet_id'] === outlet_id) {
							user_list += "<option value='" + outlet['user_id'] + "'>" + outlet['name'] + "</option>";
						}
					})

					$('#user_id').html(user_list)
				}
			}
		})
	});

	//chart grafik laporan hari ini
	function init_laporan_chart() {

		$.ajax({
			url: "<?= base_url(); ?>laporan/get_statistic_by_date/" + $('#user_id option:selected').val() + "/" + $('#outlet_id').val() + "/" + $('#date_begin').val() + "/" + $("#date_end").val(),
			success(result) {
				if (result.success) {
					data_outlet = result.data.outlet
					data_aksesoris = result.data.aksesoris
					data_perdana = result.data.perdana
					data_voucher = result.data.voucher
					data_pulsa = result.data.pulsa
				}
			}
		})

		setTimeout(() => {

			document.getElementById("laporan_chart_container").innerHTML = '&nbsp;';
			document.getElementById("laporan_chart_container").innerHTML = '<canvas id="laporan_chart"></canvas>';
			var ctx = document.getElementById("laporan_chart").getContext("2d");

			var laporan_chart = new Chart(ctx, {
				type: 'bar',
				data: {
					labels: data_outlet,
					datasets: [{
						label: 'Aksesoris',
						data: data_aksesoris,
						backgroundColor: [
							'rgba(255, 99, 132, 0.5)',
							'rgba(255, 99, 132, 0.5)',
							'rgba(255, 99, 132, 0.5)',
							'rgba(255, 99, 132, 0.5)'
						],
					}, {
						label: 'Perdana',
						data: data_perdana,
						backgroundColor: [
							'rgba(54, 162, 235, 0.5)',
							'rgba(54, 162, 235, 0.5)',
							'rgba(54, 162, 235, 0.5)',
							'rgba(54, 162, 235, 0.5)'
						]
					}, {
						label: 'Voucher',
						data: data_voucher,
						backgroundColor: [
							'rgba(255, 206, 86, 0.5)',
							'rgba(255, 206, 86, 0.5)',
							'rgba(255, 206, 86, 0.5)',
							'rgba(255, 206, 86, 0.5)'
						],
					}, {
						label: 'Pulsa',
						data: data_pulsa,
						backgroundColor: [
							'rgba(75, 192, 192, 0.5)',
							'rgba(75, 192, 192, 0.5)',
							'rgba(75, 192, 192, 0.5)',
							'rgba(75, 192, 192, 0.5)'
						]
					}]
				},
				options: {
					scales: {
						yAxes: [{
							ticks: {
								beginAtZero: true
							}
						}]
					}
				}
			})
		}, 500);
	}

	//dropdown pilih outlet
	function get_outlet() {
		$.ajax({
			url: "<?= base_url(); ?>outlet/get_all_outlet",
			success: function (result) {

				if (result.success) {

					var outlet_list = '<option value="0">Semua Outlet</option>'
					result.data.forEach(function (outlet) {
						outlet_list += "<option value='" + outlet['id'] + "'>" + outlet['name'] + "</option>";
					})

					$('#outlet_id').html(outlet_list)
				}

			}
		})
	}

	//dropdown pilih pengguna
	function get_outlet_employee() {

		$.ajax({
			url: "<?= base_url(); ?>karyawan/get_karyawan/" + 0,
			success: function (result) {
				if (result.success) {

					var outlet_id = $('#outlet_id option:selected').val()

					var user_list = '<option value="0">Semua Shift</option>'

					result.data.forEach(function (outlet) {
						if (outlet['wewenang'] !== 'ADMIN' && outlet['wewenang'] !== 'OWNER' && outlet['outlet_id'] === outlet_id) {
							user_list += "<option value='" + outlet['user_id'] + "'>" + outlet['name'] + "</option>";
						}
					})
					$('#user_id').html(user_list)
				}
			}
		})
	}
</script>

<?php $this->load->view('_layouts/_template/js') ?>
