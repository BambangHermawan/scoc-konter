<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_transfer extends CI_Model
{
	var $column_order = array('itm_prdk.id', 'itm_ctgy.id', 'itm_ctgy.name', 'itm_prdk.name', 'itm_prdk.modal', '.itm_prdk.many', 'itm_prdk.qrcode', 'itm_prdk.entry_date');
	var $column_search = array('itm_prdk.id', 'itm_ctgy.id', 'itm_ctgy.name', 'itm_prdk.name', 'itm_prdk.modal', 'itm_prdk.many', 'itm_prdk.qrcode', 'itm_prdk.entry_date');
	var $order = array('itm_prdk.name' => 'asc');

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_datatables($type_id, $category_id)
	{
		$this->_get_datatables_query($type_id, $category_id);

		if ($_REQUEST['length'] != -1)
			$this->db->limit($_REQUEST['length'], $_REQUEST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	private function _get_datatables_query($type_id, $category_id)
	{
		if ($type_id != 0) {

			$this->db->select('format(itm_prdk.modal,0) as modal, itm_prdk.id, itm_prdk.name as product_name, format(itm_prdk.many,0) as many, itm_prdk.qrcode, itm_prdk.entry_date, itm_ctgy.name as category_name')
				->from('item_product as itm_prdk, item_category as itm_ctgy')
				->where('itm_prdk.item_category_id = itm_ctgy.id')
				->where('itm_prdk.deleted_date is null')
				->where('itm_ctgy.deleted_date is null')
				->where('itm_ctgy.item_type_id', $type_id)
				->where('itm_prdk.item_category_id', $category_id);

		} else {

			$this->db->select('format(itm_prdk.modal,0) as modal, itm_prdk.id, itm_prdk.name as product_name, format(itm_prdk.many,0) as many, itm_prdk.qrcode, itm_prdk.entry_date, itm_ctgy.name as category_name')
				->from('item_product as itm_prdk, item_category as itm_ctgy')
				->where('itm_prdk.item_category_id = itm_ctgy.id')
				->where('itm_prdk.deleted_date is null')
				->where('itm_ctgy.deleted_date is null');
		}

		$i = 0;
		foreach ($this->column_search as $item) {
			if ($_REQUEST['search']['value']) {

				if ($i === 0) {
					$this->db->group_start();
					$this->db->like($item, $_REQUEST['search']['value']);
				} else {
					$this->db->or_like($item, $_REQUEST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if (isset($_REQUEST['order'])) {
			$this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function count_filtered($type_id, $category_id)
	{
		$this->_get_datatables_query($type_id, $category_id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_all($type_id, $category_id)
	{
		if ($type_id != 0) {

			$this->db->select('format(itm_prdk.modal,0) as modal, itm_prdk.id, itm_prdk.name as product_name, format(itm_prdk.many,0) as many, itm_prdk.qrcode, itm_prdk.entry_date, itm_ctgy.name as category_name')
				->from('item_product as itm_prdk, item_category as itm_ctgy')
				->where('itm_prdk.item_category_id = itm_ctgy.id')
				->where('itm_prdk.deleted_date is null')
				->where('itm_ctgy.deleted_date is null')
				->where('itm_ctgy.item_type_id', $type_id)
				->where('itm_prdk.item_category_id', $category_id);

		} else {

			$this->db->select('format(itm_prdk.modal,0) as modal, itm_prdk.id, itm_prdk.name as product_name, format(itm_prdk.many,0) as many, itm_prdk.qrcode, itm_prdk.entry_date, itm_ctgy.name as category_name')
				->from('item_product as itm_prdk, item_category as itm_ctgy')
				->where('itm_prdk.item_category_id = itm_ctgy.id')
				->where('itm_prdk.deleted_date is null')
				->where('itm_ctgy.deleted_date is null');
		}

		return $this->db->count_all_results();
	}

	function get_product_transfer_status($outlet_id, $item_product_id)
	{
		$data = $this->db->query("select cost from item_product_outlet where outlet_id='$outlet_id' and item_product_id='$item_product_id'")->result();

		$cost = '';
		if (count($data) > 0) {
			foreach ($data as $item) {
				$cost = $item->cost;
			}
			return $cost;
		}
		return $cost;
	}

}
