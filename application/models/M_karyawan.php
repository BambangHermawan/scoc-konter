<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_karyawan extends CI_Model
{
	var $column_order = array('olte.id', 'olt.id', 'olt.name', 'olt.address', 'usr.address', 'usr.id', 'usr.user_roles_id', 'usr.username', 'usr.name', 'usr.address', 'usr.hp', 'usrr.id', 'usrr.wewenang');
	var $column_search = array('olte.id', 'olt.id', 'olt.name', 'olt.address', 'usr.address', 'usr.id', 'usr.user_roles_id', 'usr.username', 'usr.name', 'usr.address', 'usr.hp', 'usrr.id', 'usrr.wewenang');
	var $order = array('olt.name' => 'asc');

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		$this->db->select('olte.id as employee_outlet_id, olt.id as outlet_id, olt.name as outlet_name, olt.address as outlet_address, usr.id as user_id, usr.user_roles_id, usr.username as user_username, usr.name as user_name, usr.address as user_address, usr.hp as user_hp, usrr.id as user_roles_id, usrr.wewenang as user_roles_name')
			->from('outlet_employee as olte, outlet as olt, user as usr, user_roles as usrr')
			->where('olte.outlet_id = olt.id')
			->where('olte.user_id = usr.id')
			->where('usrr.id = usr.user_roles_id')
			->where('usr.deleted_date is null')
			->where('olte.deleted_date is null')
			->where('usrr.deleted_date is null');

		$i = 0;
		foreach ($this->column_search as $item) {
			if ($_REQUEST['search']['value']) {

				if ($i === 0) {
					$this->db->group_start();
					$this->db->like($item, $_REQUEST['search']['value']);
				} else {
					$this->db->or_like($item, $_REQUEST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if (isset($_REQUEST['order'])) {
			$this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();

		if ($_REQUEST['length'] != -1)
			$this->db->limit($_REQUEST['length'], $_REQUEST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_all()
	{
		$this->db->select('olte.id as employee_outlet_id, olt.id as outlet_id, olt.name as outlet_name, olt.address as outlet_address, usr.id as user_id, usr.user_roles_id, usr.username as user_username, usr.name as user_name, usr.address as user_address, usr.hp as user_hp, usrr.id as user_roles_id, usrr.wewenang as user_roles_name')
			->from('outlet_employee as olte, outlet as olt, user as usr, user_roles as usrr')
			->where('olte.outlet_id = olt.id')
			->where('olte.user_id = usr.id')
			->where('usrr.id = usr.user_roles_id')
			->where('usr.deleted_date is null')
			->where('olte.deleted_date is null')
			->where('usrr.deleted_date is null');

		return $this->db->count_all_results();
	}

	function get_all_karyawan($outlet_id)
	{
		if ($outlet_id != 0) {
			return $this->db->query("select outlet_employee.id as outlet_employee_id, user.id as user_id, user.name as name, user.username as username, outlet.id as outlet_id, outlet.name as outlet_name from outlet_employee, outlet, user where outlet_employee.user_id = user.id and outlet_employee.outlet_id = outlet.id and outlet_employee.outlet_id = '$outlet_id' and user.user_roles_id = 3 and outlet_employee.deleted_date is null and user.deleted_date is null order by user.id asc")->result();
		}

		return $this->db->query("select outlet_employee.id as outlet_employee_id, user.id as user_id, user.name as name, user.username as username, outlet.id as outlet_id, outlet.name as outlet_name from outlet_employee, outlet, user where outlet_employee.user_id = user.id and outlet_employee.outlet_id = outlet.id and user.user_roles_id = 3 and outlet_employee.deleted_date is null and user.deleted_date is null order by user.id asc")->result();
	}

	function add_karyawan($outlet_id, $user_id)
	{
		$created_by = created_by();
		$created_date = created_date();

		$outlet_employee = $this->db->query("select id from outlet_employee where outlet_id='$outlet_id' and user_id='$user_id' and deleted_date is null")->result();

		if (count($outlet_employee) > 0) {
			return false;
		}

		$user = $this->db->query("select id, username, name from user where id='$user_id' and deleted_date is null")->result();

		if (count($user) > 0) {
			$password = md5($user[0]->username);
			return $this->db->query("insert into outlet_employee (outlet_id, user_id, password, created_by, created_date) values('$outlet_id','$user_id','$password','$created_by','$created_date')");
		} else {
			return false;
		}
	}

	function edit_karyawan($outlet_employee_id, $outlet_id, $user_id)
	{
		$modified_by = modified_by();
		$modified_date = modified_date();

		$outlet_employee = $this->db->query("select id from outlet_employee where outlet_id='$outlet_id' and user_id='$user_id' and deleted_date is null")->result();

		if (count($outlet_employee) > 0) {
			return false;
		}

		return $this->db->query("update outlet_employee set outlet_id ='$outlet_id', user_id='$user_id', modified_by='$modified_by', modified_date='$modified_date' where id='$outlet_employee_id' and deleted_date is null");
	}

	function reset_password_karyawan($id)
	{
		$modified_by = modified_by();
		$modified_date = modified_date();

		$user = $this->db->query("select outlet_employee.id as outlet_employee_id, user.id as user_id, username from outlet_employee, user where outlet_employee.user_id = user.id and outlet_employee.id='$id' and outlet_employee.deleted_date is null and user.deleted_date is null")->result();

		if (count($user) > 0) {
			$password = md5($user[0]->username);

			return $this->db->query("update outlet_employee set password='$password', modified_by='$modified_by', modified_date='$modified_date' where id='$id' and deleted_date is null");
		} else {
			return false;
		}
	}

	function delete_karyawan($id)
	{
		$deleted_by = deleted_by();
		$deleted_date = deleted_date();

		return $this->db->query("update outlet_employee set deleted_by='$deleted_by', deleted_date='$deleted_date' where id='$id' and deleted_date is null");
	}
}
