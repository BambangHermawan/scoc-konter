<?php

function time_now()
{
	return Date('Y-m-d h:i:s');
}

function date_now()
{
	return Date('Y-m-d');
}

function generate_date_filter($start_date, $end_date)
{
	if (empty($start_date) && empty($end_date)) {
		$filter = [
			'start' => date('Y-m-d', strtotime("-1 days")),
			'end' => date('Y-m-d')
		];
	} else if (date('Y-m-d', strtotime($start_date)) == date('Y-m-d', strtotime($end_date))) {
		$filter = [
			'start' => date('Y-m-d', strtotime('-1 day', strtotime($start_date))),
			'end' => date('Y-m-d', strtotime($end_date))
		];
	} else {
		$filter = [
			'start' => date('Y-m-d', strtotime($start_date)),
			'end' => date('Y-m-d', strtotime($end_date))
		];
	}

	return $filter;
}
