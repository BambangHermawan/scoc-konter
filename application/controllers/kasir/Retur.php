<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Retur extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('kasir/M_retur', 'M_retur', true);
		$this->load->model('kasir/M_outlet', 'M_outlet', true);
		$this->load->model('M_log', 'M_log', true);

		if (!isset($_SESSION)) {
			redirect('auth/login');
		} elseif ($_SESSION['status_user'] != 'KASIR') {
			redirect('auth/login');
		}
	}

	function index($outlet_id)
	{
		$data['title'] = 'Retur Barang';
		$data['site_navigation'] = 'Retur';

		$data['outlet'] = $this->M_outlet->get_outlet_by_id($outlet_id);

		$this->load->view('kasir/retur/index', $data);
	}

	function get_all_transaction_retur($outlet_id, $start_date, $end_date)
	{
		$filter = $this->generate_date_filter($start_date, $end_date);

		$result = $this->M_retur->get_datatables($outlet_id, $filter);

		$response = [
			'success' => true,
			'draw' => $_REQUEST['draw'],
			'recordsTotal' => $this->M_retur->count_all($outlet_id, $filter),
			'recordsFiltered' => $this->M_retur->count_filtered($outlet_id, $filter),
			'data' => $result
		];

		return to_json($response);
	}

	function generate_date_filter($start_date, $end_date)
	{
		if (empty($start_date) && empty($end_date)) {
			$filter = [
				'start' => date('Y-m-d', strtotime("-1 days")),
				'end' => date('Y-m-d')
			];
		} else if (date('Y-m-d', strtotime($start_date)) == date('Y-m-d', strtotime($end_date))) {
			$filter = [
				'start' => date('Y-m-d', strtotime($start_date)),
				'end' => date('Y-m-d', strtotime($end_date))
			];
		} else {
			$filter = [
				'start' => date('Y-m-d', strtotime($start_date)),
				'end' => date('Y-m-d', strtotime($end_date))
			];
		}

		return $filter;
	}

}
