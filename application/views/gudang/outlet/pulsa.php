<div class="row form-group">
	<div class="col-lg-1 col-md-1" style="height:25px"></div>
	<div class="col-lg-11 col-md-11">
		<div style="position:absolute; bottom:0px; right:15px">
			<button class="btn btn-danger btn-sm" id="btn_add_export_outlet_bluk"><i class="fas fa-download"></i> Export
			</button>
			<button class="btn btn-info btn-sm" id="btn_add_import_outlet_bluk"><i class="fas fa-plus"></i> Impor
			</button>
			<button class="btn btn-success btn-sm" id="btn_add_product_outlet_bluk"><i class="fas fa-plus"></i> Produk
			</button>
		</div>
	</div>
</div>

<div class="row" style="padding-top:20px; padding-bottom: 20px" id="data_category_outlet_bluk"></div>
<div class="table-responsive">
	<table id="table_outlet_bluk" class="table table-striped table-hover dt-responsive display nowrap"
		   style="width:100%">
		<thead>
		<tr>
			<th class="nomor" data-orderable="false">No</th>
			<th>Kategori</th>
			<th>Nama</th>
			<th>Modal</th>
			<th>Harga Jual</th>
			<th>Diskon (%)</th>
			<th>Unit</th>
			<th>Qrcode</th>
			<th data-orderable="false">Aksi</th>
		</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>

<script>
	$(document).ready(function () {
		init_table_outlet_bluk()
		init_table_peringatan()
		get_category_bluk()
		get_item_category()
	})

	//validasi form tambah modal bluk
	$("#add_modal_outlet_bluk_form").validate({
		rules: {
			input_add_outlet_modal_bluk: {
				required: true,
				digits: true,
				maxlength: 7
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//modal tambah modal bluk
	function add_modal(id) {
		$('#input_add_outlet_modal_bluk_product_id').val(id)
		$("#modal_outlet_add_modal_bluk").modal({backdrop: 'static', keyboard: false})
	}

	//simpan tambah modal bluk
	$("#btn_add_outlet_modal_submit").click(function () {

		if ($('#add_modal_outlet_bluk_form').valid()) {

			$.ajax({
				url: "<?= base_url(); ?>_gudang/bluk/add_modal_bluk",
				type: "POST",
				data: {
					outlet_id: "<?= $this->uri->segment(4); ?>",
					product_id: $('#input_add_outlet_modal_bluk_product_id').val(),
					modal: $('#input_add_outlet_modal_bluk').val(),
				},
				success: function (result) {

					if (result.success) {
						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						get_category_bluk()
						init_table_peringatan()

					} else {
						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});
					}
				}
			})
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});
		}
	});

	//validasi form edit modal bluk
	$("#edit_modal_outlet_bluk_form").validate({
		rules: {
			input_edit_outlet_modal_bluk: {
				required: true,
				digits: true,
				maxlength: 7
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//modal edit modal bluk
	function edit_modal(id, modal) {

		$('#input_edit_outlet_modal_bluk_product_id').val(id)
		$('#input_edit_outlet_modal_bluk').val(modal)
		$("#modal_outlet_edit_modal_bluk").modal({backdrop: 'static', keyboard: false})
	}

	//simpan edit modal bluk
	$("#btn_edit_outlet_modal_submit").click(function () {

		if ($('#edit_modal_outlet_bluk_form').valid()) {

			$.ajax({
				url: "<?= base_url(); ?>_gudang/bluk/edit_modal_bluk",
				type: "POST",
				data: {
					outlet_id: "<?= $this->uri->segment(4); ?>",
					product_id: $('#input_edit_outlet_modal_bluk_product_id').val(),
					modal: $('#input_edit_outlet_modal_bluk').val(),
				},
				success: function (result) {

					if (result.success) {
						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						get_category_bluk()
						init_table_peringatan()

					} else {
						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});
					}
				}
			})
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});
		}
	});

	//validasi form atur harga barang
	$("#cost_barang_outlet_bluk_form").validate({
		rules: {
			input_cost_outlet_product_cost_bluk: {
				required: true,
				digits: true,
				maxlength: 7
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//button modal atur harga
	$("#table_outlet_bluk").on('click', 'tr #btn_outlet_cost_bluk_click', function () {

		tr = $(this).closest('tr')
		data = table_outlet_bluk.row(tr).data()

		$("#input_cost_outlet_bluk_product_id").val(data.id)
		$("#input_cost_outlet_product_name_bluk").val(data.name)
		$("#input_cost_outlet_product_category_bluk").val(data.category_name)
		$("#input_cost_outlet_product_cost_bluk").val(data.cost.replace(',', ''))

		$('#modal_outlet_cost_bluk').modal({backdrop: 'static', keyboard: false})
	})

	//validasi form atur harga
	$("#cost_barang_outlet_bluk_form").validate({
		rules: {
			input_cost_outlet_cost: {
				required: true,
				digits: true,
				maxlength: 7
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//simpan atur harga barang
	$("#btn_cost_outlet_bluk_submit").click(function () {

		if ($('#cost_barang_outlet_bluk_form').valid()) {

			$.ajax({
				url: "<?= base_url(); ?>_gudang/bluk/set_cost_bluk",
				type: "POST",
				data: {
					product_id: $('#input_cost_outlet_bluk_product_id').val(),
					cost: $('#input_cost_outlet_product_cost_bluk').val()
				},
				success: function (result) {

					if (result.success) {

						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						init_table_outlet_bluk()
						init_table_peringatan()

					} else {
						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});
					}
				}
			})
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});
		}
	});

	//button modal diskon barang
	$("#table_outlet_bluk").on('click', 'tr #btn_outlet_discount_bluk_click', function () {

		tr = $(this).closest('tr')
		data = table_outlet_bluk.row(tr).data()

		$("#input_discount_outlet_bluk_product_id").val(data.id)
		$("#input_discount_outlet_product_name_bluk").val(data.name)
		$("#input_discount_outlet_product_category_bluk").val(data.category_name)
		$("#input_discount_outlet_product_cost_bluk").val(data.cost.replace(',', ''))
		$("#input_discount_outlet_product_discount_bluk").val(data.discount)
		$('#modal_outlet_discount_bluk').modal({backdrop: 'static', keyboard: false})
	})

	//validasi form diskon barang
	$("#discount_barang_outlet_bluk_form").validate({
		rules: {
			input_discount_outlet_product_discount_bluk: {
				required: true,
				digits: true,
				maxlength: 2
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//simpan diskon barang
	$("#btn_discount_outlet_bluk_submit").click(function () {

		if ($('#discount_barang_outlet_bluk_form').valid()) {

			$.ajax({
				url: "<?= base_url(); ?>_gudang/bluk/set_discount_bluk",
				type: "POST",
				data: {
					product_id: $('#input_discount_outlet_bluk_product_id').val(),
					discount: $('#input_discount_outlet_product_discount_bluk').val(),
				},
				success: function (result) {

					if (result.success) {
						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						init_table_outlet_bluk()
						init_table_peringatan()

					} else {
						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});
					}
				}
			})
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});
		}
	});

	//hapus data produk outlet
	$("#table_outlet_bluk").on('click', 'tr #btn_outlet_delete_bluk_click', function () {

		tr = $(this).closest('tr')
		data = table_outlet_bluk.row(tr).data()
		product_id = data.id

		Swal.fire({
			title: 'Peringatan!',
			text: 'Apakah Anda yakin akan menghapus produk ' + data.name + ' ?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya, hapus data ini!'
		}).then((result) => {
			if (result.value) {
				$.ajax({
					url: "<?= base_url(); ?>_gudang/bluk/delete_product_bluk",
					type: "POST",
					data: {
						product_id: product_id
					},
					success(result) {

						if (result.success) {
							Swal.fire({
								title: 'Berhasil',
								text: result.message,
								icon: result.type,
								showConfirmButton: false,
								timer: 1500
							});

							init_table_outlet_bluk()
							init_table_peringatan()

						} else {
							Swal.fire({
								title: 'gagal',
								text: result.message,
								icon: result.type,
								showConfirmButton: false,
								timer: 1500
							});

						}
					}
				})
			}
		})
	})

	//modal preview qrcode
	$("#table_outlet_bluk").on('click', 'tr #qrcode_image', function () {
		$('#qrcode_preview').attr('src', $(this).attr('src'));
		$('#modal_qrcode').modal({backdrop: 'static', keyboard: false});
	});

	//konversi angka ke format rupiah
	function to_rupiah(angka) {
		var rev = parseInt(angka, 10).toString().split('').reverse().join('');

		var rev2 = '';
		for (var i = 0; i < rev.length; i++) {
			rev2 += rev[i];
			if ((i + 1) % 3 === 0 && i !== (rev.length - 1)) {
				rev2 += ',';
			}
		}

		return rev2.split('').reverse().join('');
	}

	//dropdown tipe item category
	function get_item_category() {
		$.ajax({
			url: "<?= base_url(); ?>kategori/get_category_by_outlet_id/<?= $this->uri->segment(4) ?>",
			type: 'GET',
			success: function (result) {

				if (result.success) {

					category = '<option value="">Pilih...</option>'

					result.data.forEach(function (data) {
						if (data.id !== 4) {
							category += "<option value='" + data.id + "'>" + data.name + "</option>";
						}
					})

					$('#input_outlet_product_category_bluk').html(category)
				}
			}
		})
	}

	//get kategori bluk
	function get_category_bluk() {

		$.ajax({
			url: "<?= base_url(); ?>kategori/get_category_by_outlet_id/<?= $this->uri->segment(4) ?>",
			type: 'GET',
			success: function (result) {
				if (result.success) {
					category = ''
					result.data.forEach(function (data) {
						category += '<div class="col-lg-4" style="margin-top: 5px">\
                            <div class="row">\
                                <div class="col-lg-4">' + data['name'] + '</div>\
                                <div class="col-lg-4">\
                                    <input type="text" class="form-control form-control-sm" value="' + to_rupiah(data['modal']) + '" readonly>\
                                </div>\
                                <div class="col-lg-4">\
                                    <button class="btn btn-info btn-sm" onclick="add_modal(' + data['id'] + ')">\
                                        <i class="fas fa-plus"></i>\
                                    </button>\
                                    <button class="btn btn-warning btn-sm" onclick="edit_modal(' + data['id'] + ',' + data['modal'] + ')">\
                                        <i class="fas fa-edit"></i>\
                                    </button>\
                                </div>\
                            </div>\
                        </div>'
					})

					$('#data_category_outlet_bluk').html(category)
				}
			}
		})
	}

	//init table outlet bluk
	function init_table_outlet_bluk() {
		button = "&nbsp;<button class='btn btn-success btn-sm' id='btn_outlet_cost_bluk_click'>\
            <i class='fas fa-edit'></i>&nbsp;Harga</button>"
		button += "&nbsp;<button class='btn btn-warning btn-sm' id='btn_outlet_discount_bluk_click'>\
            <i class='fas fa-edit'></i>&nbsp;Diskon</button>"
		button += "&nbsp;<button class='btn btn-danger btn-sm' id='btn_outlet_delete_bluk_click'>\
            <i class='fas fa-trash'></i>&nbsp;Hapus</button>"

		table_outlet_bluk = $("#table_outlet_bluk").DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			autoWidth: true,
			width: "0.8%",
			ajax: "<?= base_url(); ?>_gudang/bluk/get_all_bluk/<?= $this->uri->segment(4); ?>",
			columns: [
				{
					data: "id",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: "category_name"
				},
				{
					data: "name"
				},
				{
					data: "modal"
				},
				{
					data: "cost"
				},
				{
					data: "discount"
				},
				{
					data: "unit"
				},
				{
					data: "qrcode",
					"targets": 6,
					"render": function (url, type, full) {
						return '<img onerror="this.onerror=null; this.remove();" class="center image_' + full['id'] + '"id="qrcode_image" height="100px" width="100px" src="' + "<?= base_url(); ?>uploads/qrcode/" + full['qrcode'] + '" />';
					}
				},
				{
					defaultContent: button
				},
			]
		})
	}
</script>
