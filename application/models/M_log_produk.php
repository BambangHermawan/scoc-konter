<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_log_produk extends CI_Model
{
	var $column_order = array('log_product.id', 'log_product.outlet_id', 'log_product.target', 'log_product.object_id', 'log_product.activity', 'log_product.note');
	var $column_search = array('log_product.id', 'log_product.outlet_id', 'log_product.target', 'log_product.object_id', 'log_product.activity', 'log_product.note');
	var $order = array('log_product.id' => 'desc');

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_datatables($user_id, $outlet_id, $hari)
	{
		$this->_get_datatables_query($user_id, $outlet_id, $hari);

		if ($_REQUEST['length'] != -1)
			$this->db->limit($_REQUEST['length'], $_REQUEST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	private function _get_datatables_query($user_id, $outlet_id, $date)
	{
		if ($user_id != 0 && $outlet_id != 0) {
			$this->db->select('log_product.id, log_product.outlet_id, log_product.target, log_product.modal, log_product.cost, log_product.awl, log_product.plus, log_product.ak, log_product.discount, log_product.object_id, log_product.activity, log_product.note, log_product.status_id as status, user.name as user_name, outlet.name as outlet_name, DATE_FORMAT(log_product.created_date, "%d-%m-%Y - %h:%i") as created_date')
				->from('log_product, user, outlet')
				->where('log_product.user_id = user.id')
				->where('log_product.outlet_id = outlet.id')
				->where('log_product.deleted_date is null')
				->where('user.deleted_date is null')
				->where('outlet.deleted_date is null')
				->where('log_product.user_id', $user_id)
				->where('log_product.outlet_id', $outlet_id)
				->where('DATE(log_product.created_date) BETWEEN "' . $date['start'] . '" and "' . $date['end'] . '"');
		}

		if ($user_id == 0 && $outlet_id != 0) {
			$this->db->select('log_product.id, log_product.outlet_id, log_product.target, log_product.modal, log_product.cost, log_product.awl, log_product.plus, log_product.ak, log_product.discount, log_product.object_id, log_product.activity, log_product.note, log_product.status_id as status, user.name as user_name, outlet.name as outlet_name, DATE_FORMAT(log_product.created_date, "%d-%m-%Y - %h:%i") as created_date')
				->from('log_product, user, outlet')
				->where('log_product.user_id = user.id')
				->where('log_product.outlet_id = outlet.id')
				->where('log_product.deleted_date is null')
				->where('user.deleted_date is null')
				->where('outlet.deleted_date is null')
				->where('log_product.outlet_id', $outlet_id)
				->where('DATE(log_product.created_date) BETWEEN "' . $date['start'] . '" and "' . $date['end'] . '"');
		}

		if ($user_id != 0 && $outlet_id == 0) {
			$this->db->select('log_product.id, log_product.outlet_id, log_product.target, log_product.modal, log_product.cost, log_product.awl, log_product.plus, log_product.ak, log_product.discount, log_product.object_id, log_product.activity, log_product.note, log_product.status_id as status, user.name as user_name, outlet.name as outlet_name, DATE_FORMAT(log_product.created_date, "%d-%m-%Y - %h:%i") as created_date')
				->from('log_product, user, outlet')
				->where('log_product.user_id = user.id')
				->where('log_product.outlet_id = outlet.id')
				->where('log_product.deleted_date is null')
				->where('user.deleted_date is null')
				->where('outlet.deleted_date is null')
				->where('log_product.user_id', $user_id)
				->where('DATE(log_product.created_date) BETWEEN "' . $date['start'] . '" and "' . $date['end'] . '"');
		}

		if ($user_id == 0 && $outlet_id == 0) {
			$this->db->select('log_product.id, log_product.outlet_id, log_product.target, log_product.modal, log_product.cost, log_product.awl, log_product.plus, log_product.ak, log_product.discount, log_product.object_id, log_product.activity, log_product.note, log_product.status_id as status, user.name as user_name, outlet.name as outlet_name, DATE_FORMAT(log_product.created_date, "%d-%m-%Y - %h:%i") as created_date')
				->from('log_product, user, outlet')
				->where('log_product.user_id = user.id')
				->where('log_product.outlet_id = outlet.id')
				->where('log_product.deleted_date is null')
				->where('user.deleted_date is null')
				->where('DATE(log_product.created_date) BETWEEN "' . $date['start'] . '" and "' . $date['end'] . '"')
				->where('log_product.outlet_id', 5);
		}

		$i = 0;
		foreach ($this->column_search as $item) {
			if ($_REQUEST['search']['value']) {

				if ($i === 0) {
					$this->db->group_start();
					$this->db->like($item, $_REQUEST['search']['value']);
				} else {
					$this->db->or_like($item, $_REQUEST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if (isset($_REQUEST['order'])) {
			$this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function count_filtered($user_id, $outlet_id, $hari)
	{
		$this->_get_datatables_query($user_id, $outlet_id, $hari);
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_all($user_id, $outlet_id, $date)
	{
		if ($user_id != 0 && $outlet_id != 0) {
			$this->db->select('log_product.id, log_product.outlet_id, log_product.target, log_product.modal, log_product.cost, log_product.awl, log_product.plus, log_product.ak, log_product.discount, log_product.object_id, log_product.activity, log_product.note, log_product.status_id as status, user.name as user_name, outlet.name as outlet_name, DATE_FORMAT(log_product.created_date, "%d-%m-%Y - %h:%i") as created_date')
				->from('log_product, user, outlet')
				->where('log_product.user_id = user.id')
				->where('log_product.outlet_id = outlet.id')
				->where('log_product.deleted_date is null')
				->where('user.deleted_date is null')
				->where('outlet.deleted_date is null')
				->where('log_product.user_id', $user_id)
				->where('log_product.outlet_id', $outlet_id)
				->where('DATE(log_product.created_date) BETWEEN "' . $date['start'] . '" and "' . $date['end'] . '"');
		}

		if ($user_id == 0 && $outlet_id != 0) {
			$this->db->select('log_product.id, log_product.outlet_id, log_product.target, log_product.modal, log_product.cost, log_product.awl, log_product.plus, log_product.ak, log_product.discount, log_product.object_id, log_product.activity, log_product.note, log_product.status_id as status, user.name as user_name, outlet.name as outlet_name, DATE_FORMAT(log_product.created_date, "%d-%m-%Y - %h:%i") as created_date')
				->from('log_product, user, outlet')
				->where('log_product.user_id = user.id')
				->where('log_product.outlet_id = outlet.id')
				->where('log_product.deleted_date is null')
				->where('user.deleted_date is null')
				->where('outlet.deleted_date is null')
				->where('log_product.outlet_id', $outlet_id)
				->where('DATE(log_product.created_date) BETWEEN "' . $date['start'] . '" and "' . $date['end'] . '"');
		}

		if ($user_id != 0 && $outlet_id == 0) {
			$this->db->select('log_product.id, log_product.outlet_id, log_product.target, log_product.modal, log_product.cost, log_product.awl, log_product.plus, log_product.ak, log_product.discount, log_product.object_id, log_product.activity, log_product.note, log_product.status_id as status, user.name as user_name, outlet.name as outlet_name, DATE_FORMAT(log_product.created_date, "%d-%m-%Y - %h:%i") as created_date')
				->from('log_product, user, outlet')
				->where('log_product.user_id = user.id')
				->where('log_product.outlet_id = outlet.id')
				->where('log_product.deleted_date is null')
				->where('user.deleted_date is null')
				->where('outlet.deleted_date is null')
				->where('log_product.user_id', $user_id)
				->where('DATE(log_product.created_date) BETWEEN "' . $date['start'] . '" and "' . $date['end'] . '"');
		}

		if ($user_id == 0 && $outlet_id == 0) {
			$this->db->select('log_product.id, log_product.outlet_id, log_product.target, log_product.modal, log_product.cost, log_product.awl, log_product.plus, log_product.ak, log_product.discount, log_product.object_id, log_product.activity, log_product.note, log_product.status_id as status, user.name as user_name, outlet.name as outlet_name, DATE_FORMAT(log_product.created_date, "%d-%m-%Y - %h:%i") as created_date')
				->from('log_product, user, outlet')
				->where('log_product.user_id = user.id')
				->where('log_product.outlet_id = outlet.id')
				->where('log_product.deleted_date is null')
				->where('user.deleted_date is null')
				->where('DATE(log_product.created_date) BETWEEN "' . $date['start'] . '" and "' . $date['end'] . '"')
				->where('log_product.outlet_id', 5);
		}

		return $this->db->count_all_results();
	}

	function get_detail_log_activity($target, $object_id)
	{
		if ($target == 'item_product' || $target == 'add_stock_product_gudang' || $target = 'edit_stock_product_gudang') {
			return $this->db->query("select item_product.name as product_name, item_category.name as category_name, item_type.name as type_name from item_product, item_category, item_type
       		where item_product.id='$object_id' and item_product.item_category_id=item_category.id and item_category.item_type_id=item_type.id and item_category.deleted_date is null and item_type.deleted_date is null")->result()[0];
		}

		return false;
	}

	function get_detail_product($product_id)
	{
		return $this->db->query("select item_product.name as product_name, item_category.name as category_name, item_type.name as type_name from item_product, item_category, item_type
        where item_product.id='$product_id' and item_product.item_category_id=item_category.id and item_category.item_type_id=item_type.id and item_product.deleted_date is null and item_category.deleted_date is null and item_type.deleted_date is null")->result()[0];
	}

	function get_outlet($outlet_id)
	{
		return $this->db->query("select id, name from outlet where id='$outlet_id' and deleted_date is null")->result()[0];
	}

	function get_detail_product_outlet($product_id)
	{
		return $this->db->query("
                select item_product.name as product_name, item_category.name as category_name, item_type.name as type_name, item_product.modal, item_product_outlet.cost, item_product_outlet.many as awl
                from item_product_outlet, item_product, item_category, item_type
                where item_product_outlet.item_product_id=item_product.id 
                and item_product.item_category_id=item_category.id 
                and item_category.item_type_id=item_type.id 
                and item_product.deleted_date is null
                and item_category.deleted_date is null
                and item_type.deleted_date is null
				and item_product_outlet.id='$product_id'
                ")->result()[0];
	}

	function get_detail_product_bluk_outlet($product_id)
	{
		return $this->db->query("
                select item_outlet.ame as product_name, item_outlet_category.id as get_detail_product_bluk_outlet, item_outlet.name as category_name, item_type.name as type_name, item_outlet.modal, item_outlet.cost, item_outlet.discount, item_outlet_category.outlet_id as item_outlet_id
                from item_outlet, item_outlet_category, outlet, item_type
                where item_outlet.item_outlet_category_id = item_outlet_category.id 
                and item_outlet_category.outlet_id = outlet.id 
                and item_outlet_category.type_id = item_type.id 
                and item_outlet.deleted_date is null
                and item_outlet_category.deleted_date is null
                and outlet.deleted_date is null
                and item_type.deleted_date is null
				and item_outlet_category.id='$product_id'
                ")->result()[0];
	}

	function get_detail_product_pulsa_bluk_outlet($product_id)
	{
		return $this->db->query("
                select item_outlet.name as product_name, item_outlet_category.id as get_detail_product_bluk_outlet, item_outlet.name as category_name, item_type.name as type_name, item_outlet.modal, item_outlet.cost, item_outlet.discount, item_outlet_category.outlet_id as item_outlet_id
                from item_outlet, item_outlet_category, outlet, item_type
                where item_outlet.item_outlet_category_id = item_outlet_category.id 
                and item_outlet_category.outlet_id = outlet.id 
                and item_outlet_category.type_id = item_type.id 
                and item_outlet.deleted_date is null
                and item_outlet_category.deleted_date is null
                and outlet.deleted_date is null
                and item_type.deleted_date is null
				and item_outlet.id='$product_id'
                ")->result()[0];
	}

	public function insert_log_product($user_id, $outlet_id, $target, $object_id, $modal, $cost, $awl, $plus, $ak, $discount, $activity, $note, $status_id)
	{
		$created_by = created_by();
		$created_date = created_date();

		$this->db->query("insert into log_product (user_id, outlet_id, target, object_id, modal, cost, awl, plus, ak, discount, activity, note, status_id, created_by, created_date)
    			 values('$user_id','$outlet_id','$target','$object_id','$modal','$cost','$awl','$plus','$ak','$discount','$activity','$note','$status_id','$created_by','$created_date')");
	}

}
