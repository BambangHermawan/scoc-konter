<?php

function to_json($data)
{
	header('content-type:application/json');
	echo json_encode($data, JSON_PRETTY_PRINT);
}
