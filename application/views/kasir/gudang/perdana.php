<div class="table-responsive">
	<table id="table_outlet_perdana" class="table table-striped table-hover dt-responsive display nowrap"
		   style="width:100%">
		<thead>
		<tr>
			<th class="nomor" data-orderable="false">No</th>
			<th>Kategori</th>
			<th>Nama</th>
			<th>Harga Jual</th>
			<th>Banyak</th>
			<th>Diskon (%)</th>
			<th>Disc. Sampai</th>
			<th>Qrcode</th>
			<th data-orderable="false">Aksi</th>
		</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>

<script>
	$(document).ready(function () {
		init_table_outlet_perdana()
	})

	//button modal transfer
	$("#table_outlet_perdana").on('click', 'tr #btn_outlet_transfer_click', function () {
		tr = $(this).closest('tr')
		data = table_outlet_perdana.row(tr).data()

		$("#input_transfer_outlet_product_id").val(data.item_product_id)
		$("#input_transfer_outlet_product_name").val(data.name)
		$("#input_transfer_outlet_stock").val(data.many)
		$("#input_transfer_outlet_product_qrcode").val(data.qrcode)
		$("#input_transfer_outlet_type_id").val(data.type_name)
		$('#modal_transfer_outlet').modal({backdrop: 'static', keyboard: false})
	})

	//modal preview qrcode
	$("#table_outlet_perdana").on('click', 'tr #qrcode_image', function () {
		$('#qrcode_preview').attr('src', $(this).attr('src'));
		$('#modal_qrcode').modal({backdrop: 'static', keyboard: false});
	});

	//init datatables table perdana
	function init_table_outlet_perdana() {
		button = "<button class='btn btn-info btn-sm' id='btn_outlet_transfer_click'>\
            <i class='fas fa-exchange-alt'></i>&nbsp;Transfer</button>"

		table_outlet_perdana = $("#table_outlet_perdana").DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			autoWidth: true,
			ajax: "<?= base_url(); ?>kasir/_gudang/outlet/get_all_product_by_outlet_id/" + 2 + "/<?= $this->uri->segment(5); ?>",
			columns: [
				{
					data: "id",
					width: "0.8%",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: "category_name"
				},
				{
					data: "name"
				},
				{
					data: "cost"
				},
				{
					data: "many"
				},
				{
					data: "discount"
				},
				{
					data: "discount_until"
				},
				{
					data: "qrcode",
					"targets": 6,
					"render": function (url, type, full) {
						return '<img onerror="this.onerror=null; this.remove();" class="center image_' + full['id'] + '"id="qrcode_image" height="100px" width="100px" src="' + "<?= base_url(); ?>uploads/qrcode/" + full['qrcode'] + '" />';
					}
				},
				{
					defaultContent: button
				}
			]
		})
	}
</script>
