<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('kasir/M_laporan', 'M_laporan', true);
		$this->load->model('kasir/M_pengguna', 'M_pengguna', true);
		$this->load->model('kasir/M_pendapatan', 'M_pendapatan', true);
		$this->load->model('kasir/M_transaksi', 'M_transaksi', true);
		$this->load->model('kasir/M_outlet', 'M_outlet', true);

		if (!isset($_SESSION)) {
			redirect('auth/login');
		} elseif ($_SESSION['status_user'] != 'KASIR') {
			redirect('auth/login');
		}
	}

	function index($outlet_id)
	{
		$data['title'] = 'Laporan Penjualan';
		$data['site_navigation'] = 'Laporan Penjualan';

		$data['outlet'] = $this->M_outlet->get_outlet_by_id($outlet_id);

		$this->load->view('kasir/laporan/penjualan', $data);
	}

	function pendapatan($outlet_id)
	{
		$data['title'] = 'Laporan Pendapatan';
		$data['site_navigation'] = 'Laporan Pendapatan';

		$data['outlet'] = $this->M_outlet->get_outlet_by_id($outlet_id);

		$this->load->view('kasir/laporan/pendapatan', $data);
	}

	function add_laporan_pendapatan()
	{
		$user_id = $this->input->post('user_id');
		$outlet_id = $this->input->post('outlet_id');
		$total = $this->input->post('total');
		$uang_cash = $this->input->post('uang_cash');
		$selisih = $this->input->post('selisih');
		$catatan = $this->input->post('catatan');

		$result = $this->M_pendapatan->add_laporan_pendapatan($user_id, $outlet_id, $total, $uang_cash, $selisih, $catatan);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Tambah Laporan Pendapatan berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Tambah Laporan Pendapatan gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function get_all_pendapatan($user_id, $outlet_id, $start_date, $end_date)
	{
		$filter = $this->generate_date_filter($start_date, $end_date);

		$result = $this->M_pendapatan->get_datatables($user_id, $outlet_id, $filter);

		$response = [
			'success' => true,
			'draw' => $_REQUEST['draw'],
			'recordsTotal' => $this->M_pendapatan->count_all($user_id, $outlet_id, $filter),
			'recordsFiltered' => $this->M_pendapatan->count_filtered($user_id, $outlet_id, $filter),
			'data' => $result
		];

		return to_json($response);
	}

	function get_total_pendapatan_by_date()
	{
		$today = date('Y-m-d');

		$result = $this->M_laporan->get_transaction_earning_by_date($_SESSION['id'], $_SESSION['outlet_id'], $today);

		if ($result) {

			$response = [
				'success' => true,
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function get_statistic_by_date($user_id, $outlet_id, $start_date, $end_date)
	{
		$filter = $this->generate_date_filter($start_date, $end_date);

		if ($outlet_id != 0) {

			$result = $this->M_outlet->get_outlet_by_id($outlet_id);

			$data['outlet'][] = $result[0]->name;
			$data['aksesoris'][] = $this->M_laporan->get_statistic_by_date($user_id, 'Aksesoris', $outlet_id, $filter);
			$data['perdana'][] = $this->M_laporan->get_statistic_by_date($user_id, 'Perdana', $outlet_id, $filter);
			$data['voucher'][] = $this->M_laporan->get_statistic_by_date($user_id, 'Voucher', $outlet_id, $filter);
			$data['pulsa'][] = $this->M_laporan->get_statistic_by_date($user_id, 'Pulsa', $outlet_id, $filter);

			$response = [
				'success' => true,
				'data' => $data,
				'total' => count($data)
			];

			return to_json($response);

		} else {

			$result = $this->M_outlet->get_all_outlet();

			$i = 0;
			$outlet_name = [];
			if (count($result) > 0) {
				foreach ($result as $item) {
					$outlet_name[] = $item->name;
					$data['aksesoris'][$i] = $this->M_laporan->get_statistic_by_date($user_id, 'Aksesoris', $item->id, $filter);
					$data['perdana'][$i] = $this->M_laporan->get_statistic_by_date($user_id, 'Perdana', $item->id, $filter);
					$data['voucher'][$i] = $this->M_laporan->get_statistic_by_date($user_id, 'Voucher', $item->id, $filter);
					$data['pulsa'][$i] = $this->M_laporan->get_statistic_by_date($user_id, 'Pulsa', $item->id, $filter);
					$i++;
				}

				$data['outlet'] = $outlet_name;

				$response = [
					'success' => true,
					'data' => $data,
					'total' => count($data)
				];

				return to_json($response);
			}
		}

		$response = [
			'success' => false,
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function get_statistic_by_day($outlet_id, $today)
	{
		$j = 24;
		for ($i = 1; $i <= $j; $i++) {
			$jam[] = $i;
		}

		$time = [
			'start' => '1',
			'end' => '24',
		];

		$user_id = $_SESSION['id'];

		$result = $this->M_outlet->get_outlet_by_id($outlet_id);

		$i = 0;
		$outlet_name = [];
		if (count($result) > 0) {
			foreach ($result as $item) {
				$outlet_name[] = $item->name;
				$data['aksesoris'][$i] = $this->M_laporan->get_statistic_by_day('Aksesoris', $user_id, $item->id, $today);
				$data['perdana'][$i] = $this->M_laporan->get_statistic_by_day('Perdana', $user_id, $item->id, $today);
				$data['voucher'][$i] = $this->M_laporan->get_statistic_by_day('Voucher', $user_id, $item->id, $today);
				$data['pulsa'][$i] = $this->M_laporan->get_statistic_by_day('Pulsa', $user_id, $item->id, $today);
				$i++;
			}

			$data['outlet'] = $outlet_name;

			$response = [
				'success' => true,
				'data' => $data,
				'total' => count($data)
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function get_statistic_by_month($outlet_id, $start_date, $end_date)
	{
		$start = substr($start_date, 9, 1);
		$end = substr($end_date, 8, 2);

		for ($i = intval($start); $i <= $end; $i++) {
			$hari[] = $i;
		}

		$user_id = $_SESSION['id'];

		$filter = $this->generate_date_filter($start_date, $end_date);

		$result = $this->M_outlet->get_outlet_by_id($outlet_id);

		$i = 0;
		$outlet_name = [];
		if (count($result) > 0) {
			foreach ($result as $item) {
				$outlet_name[] = $item->name;
				$data['aksesoris'][$i] = $this->M_laporan->get_statistic_by_month('Aksesoris', $user_id, $item->id, $filter);
				$data['perdana'][$i] = $this->M_laporan->get_statistic_by_month('Perdana', $user_id, $item->id, $filter);
				$data['voucher'][$i] = $this->M_laporan->get_statistic_by_month('Voucher', $user_id, $item->id, $filter);
				$data['pulsa'][$i] = $this->M_laporan->get_statistic_by_month('Pulsa', $user_id, $item->id, $filter);
				$i++;
			}

			$data['outlet'] = $outlet_name;

			$response = [
				'success' => true,
				'data' => $data,
				'total' => count($data)
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function outlet($user_id, $outlet_id, $start_date, $end_date)
	{
		$filter = $this->generate_date_filter($start_date, $end_date);

		$transaction = $this->get_transaction($user_id, $outlet_id, $filter);

		$this->print_to_pdf($user_id, $outlet_id, $filter, $transaction);
	}

	function get_transaction($user_id, $outlet_id, $date)
	{
		$data['aksesoris'] = $this->M_laporan->get_transaction_detail_by_date($user_id, 'Aksesoris', $outlet_id, $date);
		$data['perdana'] = $this->M_laporan->get_transaction_detail_by_date($user_id, 'Perdana', $outlet_id, $date);
		$data['voucher'] = $this->M_laporan->get_transaction_detail_by_date($user_id, 'Voucher', $outlet_id, $date);

		return $data;
	}

	function print_to_pdf($user_id, $outlet_id, $date, $transaction)
	{
		$outlet = '';
		if ($outlet_id != 0) {
			$item = $this->M_outlet->get_outlet_by_id($outlet_id);
			$outlet = $item[0]->name;
		}

		$shift = '';
		if ($user_id != 0) {
			$shift = $this->M_pengguna->get_pengguna_by_id($user_id);
			$shift = $shift->name;
		}

		$date_start = date('d-m-Y', strtotime($date['start']));
		$date_end = date('d-m-Y', strtotime($date['end']));

		if (empty($outlet)) {
			$outlet = 'Semua Outlet';
		} else {
			$outlet = 'Outlet ' . $outlet;
		}

		$this->load->library('Pdf');
		$pdf = new FPDF('p', 'mm', 'A4');
		$pdf->AddPage();
		$pdf->SetFont('Arial', 'B', 16);
		$pdf->Cell(190, 7, "LAPORAN TRANSAKSI {$shift}", 0, 1, 'C');
		$pdf->SetFont('Arial', 'B', 12);
		$pdf->Cell(190, 7, "{$outlet} Tanggal {$date_start} sampai {$date_end}", 0, 1, 'C');

		#AKSESORIS
		$pdf->Cell(190, 5, '', 0, 1);
		$pdf->SetFont('Arial', 'B', 12);
		$pdf->Cell(190, 10, 'Aksesoris', 0, 1);
		$pdf->SetFont('Arial', 'B', 7);
		$pdf->Cell(35, 6, 'Kategori', 1, 0);
		$pdf->Cell(45, 6, 'Nama Barang', 1, 0);
		$pdf->Cell(25, 6, 'Harga', 1, 0);
		$pdf->Cell(20, 6, 'Stok', 1, 0);
		$pdf->Cell(20, 6, '+/-', 1, 0);
		$pdf->Cell(20, 6, 'Laku', 1, 0);
		$pdf->Cell(20, 6, 'Sisa', 1, 1);
		$pdf->SetFont('Arial', '', 7);

		foreach ($transaction['aksesoris'] as $data) {
			$pdf->Cell(35, 6, $data->category, 1, 0);
			$pdf->Cell(45, 6, $data->product, 1, 0);
			$pdf->Cell(25, 6, $data->cost, 1, 0);
			$pdf->Cell(20, 6, $data->awl, 1, 0);
			$pdf->Cell(20, 6, $data->plus, 1, 0);
			$pdf->Cell(20, 6, $data->lk, 1, 0);
			$pdf->Cell(20, 6, $data->ak, 1, 1);
		}

		#PERDANA
		$pdf->Cell(190, 5, '', 0, 1);
		$pdf->SetFont('Arial', 'B', 12);
		$pdf->Cell(190, 10, 'Perdana', 0, 1);
		$pdf->SetFont('Arial', 'B', 7);
		$pdf->Cell(35, 6, 'Kategori', 1, 0);
		$pdf->Cell(45, 6, 'Nama Barang', 1, 0);
		$pdf->Cell(25, 6, 'Harga', 1, 0);
		$pdf->Cell(20, 6, 'Stok', 1, 0);
		$pdf->Cell(20, 6, '+/-', 1, 0);
		$pdf->Cell(20, 6, 'Laku', 1, 0);
		$pdf->Cell(20, 6, 'Sisa', 1, 1);
		$pdf->SetFont('Arial', '', 7);

		foreach ($transaction['perdana'] as $data) {
			$pdf->Cell(35, 6, $data->category, 1, 0);
			$pdf->Cell(45, 6, $data->product, 1, 0);
			$pdf->Cell(25, 6, $data->cost, 1, 0);
			$pdf->Cell(20, 6, $data->awl, 1, 0);
			$pdf->Cell(20, 6, $data->plus, 1, 0);
			$pdf->Cell(20, 6, $data->lk, 1, 0);
			$pdf->Cell(20, 6, $data->ak, 1, 1);
		}

		#VOUCHER
		$pdf->Cell(190, 5, '', 0, 1);
		$pdf->SetFont('Arial', 'B', 12);
		$pdf->Cell(190, 10, 'Voucher', 0, 1);
		$pdf->SetFont('Arial', 'B', 7);
		$pdf->Cell(35, 6, 'Kategori', 1, 0);
		$pdf->Cell(45, 6, 'Nama Barang', 1, 0);
		$pdf->Cell(25, 6, 'Harga', 1, 0);
		$pdf->Cell(20, 6, 'Stok', 1, 0);
		$pdf->Cell(20, 6, '+/-', 1, 0);
		$pdf->Cell(20, 6, 'Laku', 1, 0);
		$pdf->Cell(20, 6, 'Sisa', 1, 1);
		$pdf->SetFont('Arial', '', 7);

		foreach ($transaction['voucher'] as $data) {
			$pdf->Cell(35, 6, $data->category, 1, 0);
			$pdf->Cell(45, 6, $data->product, 1, 0);
			$pdf->Cell(25, 6, $data->cost, 1, 0);
			$pdf->Cell(20, 6, $data->awl, 1, 0);
			$pdf->Cell(20, 6, $data->plus, 1, 0);
			$pdf->Cell(20, 6, $data->lk, 1, 0);
			$pdf->Cell(20, 6, $data->ak, 1, 1);
		}

		$pdf->Output();
	}

	function generate_date_filter($start_date, $end_date)
	{
		if (empty($start_date) && empty($end_date)) {
			$filter = [
				'start' => date('Y-m-d', strtotime("-1 days")),
				'end' => date('Y-m-d')
			];
		} else if (date('Y-m-d', strtotime($start_date)) == date('Y-m-d', strtotime($end_date))) {
			$filter = [
				'start' => date('Y-m-d', strtotime($start_date)),
				'end' => date('Y-m-d', strtotime($end_date))
			];
		} else {
			$filter = [
				'start' => date('Y-m-d', strtotime($start_date)),
				'end' => date('Y-m-d', strtotime($end_date))
			];
		}

		return $filter;
	}
}
