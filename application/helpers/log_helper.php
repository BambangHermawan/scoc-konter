<?php

function created_by()
{
	return $_SESSION['name'];
}

function created_date()
{
	return Date('Y-m-d h:i:s');
}

function modified_by()
{
	return $_SESSION['name'];
}

function modified_date()
{
	return Date('Y-m-d h:i:s');
}

function deleted_by()
{
	return $_SESSION['name'];
}

function deleted_date()
{
	return Date('Y-m-d h:i:s');
}
