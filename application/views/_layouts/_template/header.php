<!DOCTYPE html>
<html lang="en">
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title><?php echo $title ?> | <?php echo $_SESSION['status_user'] ?></title>

	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url('assets/fontawesome-free/css/all.min.css') ?>">

	<!-- Ionicons -->
	<link rel="stylesheet" href="<?php echo base_url('assets/ionicons-2.0.1/css/ionicons.min.css') ?>">

	<!-- iCheck -->
	<link rel="stylesheet" href="<?php echo base_url('assets/icheck-bootstrap/icheck-bootstrap.min.css') ?>">

	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url('assets/adminlte/css/adminlte.min.css') ?>">

	<!-- overlayScrollbars -->
	<link rel="stylesheet" href="<?php echo base_url('assets/overlayScrollbars/css/OverlayScrollbars.min.css') ?>">

	<!-- Daterange picker -->
	<link rel="stylesheet" href="<?php echo base_url('assets/daterangepicker/daterangepicker.css') ?>">

	<!-- Select2 -->
	<link rel="stylesheet" href="<?php echo base_url('assets/select2/css/select2.css') ?>"/>

	<!-- SweetAlert2 Theme -->
	<link rel="stylesheet" href="<?php echo base_url('assets/sweetalert2/dist/sweetalert2.min.css') ?>">

	<!-- icheck bootstrap -->
	<link rel="stylesheet" href="<?php echo base_url('assets/icheck-bootstrap/icheck-bootstrap.min.css') ?>">

	<!-- DataTables -->
	<link rel="stylesheet" href="<?php echo base_url('assets/datatables/datatables.min.css') ?>">

	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url('assets/adminlte/css/adminlte.min.css') ?>">

	<!-- Google Font -->
	<link rel="stylesheet"
		  href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

	<!-- REQUIRED SCRIPTS -->

	<!-- jQuery -->
	<script src="<?php echo base_url('assets/jquery/jquery.min.js') ?>"></script>

	<!-- jQuery UI 1.11.4 -->
	<script src="<?php echo base_url('assets/jquery-ui/jquery-ui.min.js') ?>"></script>

	<!-- Bootstrap 4 -->
	<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>

	<!-- Select2 -->
	<script src="<?php echo base_url('assets/select2/js/select2.min.js') ?>"></script>

	<!-- ChartJS -->
	<script src="<?php echo base_url('assets/chart.js/chart.min.js') ?>"></script>

	<!-- Instascan -->
	<script src="<?php echo base_url('assets/instascan/instascan.min.js') ?>"></script>

	<!-- Moment -->
	<script src="<?php echo base_url('assets/moment/moment.min.js') ?>"></script>

	<!-- Datatables -->
	<script src="<?php echo base_url('assets/datatables/jquery.datatables.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/datatables/datatables.min.js') ?>"></script>

	<!-- daterangepicker -->
	<script src="<?php echo base_url('assets/daterangepicker/daterangepicker.js') ?>"></script>

	<!-- overlayScrollbars -->
	<script src="<?php echo base_url('assets/overlayScrollbars/js/jquery.overlayScrollbars.min.js') ?>"></script>

	<!-- Sweetalert2 -->
	<script src="<?php echo base_url('assets/sweetalert2/dist/sweetalert2.all.min.js') ?>"></script>

	<!-- Jquery Validation -->
	<script src="<?php echo base_url('assets/jquery-validation/jquery.validate.js') ?>"></script>
	<script src="<?php echo base_url('assets/jquery-validation/additional-methods.js') ?>"></script>
	<script src="<?php echo base_url('assets/jquery-validation/localization/messages_id.min.js') ?>"></script>

	<!-- AdminLTE App -->
	<script src="<?php echo base_url('assets/adminlte/js/adminlte.min.js') ?>"></script>

	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
		$.widget.bridge('uibutton', $.ui.button)
	</script>

</head>
<body class="sidebar-mini layout-fixed layout-navbar-fixed control-sidebar-open text-sm">

<div class="wrapper">

	<!-- Navbar -->
	<?php $this->load->view('_layouts/_template/navbar') ?>
	<!-- /.navbar -->

	<!-- Sidebar -->
	<?php
	if ($_SESSION['status_user'] == 'ADMIN') {
		$this->load->view('_layouts/_admin/sidebar');
	} else if ($_SESSION['status_user'] == 'OWNER') {
		$this->load->view('_layouts/_owner/sidebar');
	} else {
		$this->load->view('_layouts/_kasir/sidebar');
	}
	?>
	<div class="content-wrapper">
		<div class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 class="m-0 text-dark"><?php print $title; ?></h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="#"><?php print $title; ?></a></li>
							<li class="breadcrumb-item active"><?php print $site_navigation; ?></li>
						</ol>
					</div>
				</div>
			</div>
		</div>
		<section class="content">
