<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kategori extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_kategori', '', true);

		if (!isset($_SESSION)) {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'ADMIN') {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'OWNER') {
			redirect('auth/login');
		} elseif ($_SESSION['status_user'] == 'KASIR') {
			redirect('auth/login');
		}
	}

	function index()
	{
		$data['title'] = 'Kategori';
		$data['site_navigation'] = 'Kategori';

		return $this->load->view('kategori/index', $data);
	}

	function get_all_category()
	{
		$result = $this->M_kategori->get_datatables();

		$response = [
			'success' => true,
			'draw' => $_REQUEST['draw'],
			'recordsTotal' => $this->M_kategori->count_all(),
			'recordsFiltered' => $this->M_kategori->count_filtered(),
			'data' => $result
		];

		return to_json($response);
	}

	function get_category_by_id($item_type_id)
	{
		$result = $this->M_kategori->get_category_by_id($item_type_id);

		if ($result) {

			$response = [
				'success' => true,
				'data' => $result,
				'total' => count($result)
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function get_category_by_outlet_id($outlet_id)
	{
		$result = $this->M_kategori->get_category_by_outlet_id($outlet_id);

		if ($result) {

			$response = [
				'success' => true,
				'data' => $result,
				'total' => count($result)
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function add_category()
	{
		$type_id = $this->input->post('type_id');
		$category_name = $this->input->post('category_name');

		$result = $this->M_kategori->add_category($type_id, $category_name);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Tambah kategori berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Tambah kategori gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function edit_category()
	{
		$id = $this->input->post('category_id');
		$type_id = $this->input->post('type_id');
		$name = $this->input->post('name');

		$result = $this->M_kategori->edit_category($id, $type_id, $name);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Ubah kategori ' . $name . ' berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Ubah kategori ' . $name . ' gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function delete_category()
	{
		$id = $this->input->post('id');
		$result = $this->M_kategori->delete_category($id);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Hapus kategori berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Hapus kategori gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}
}
