<?php $this->load->view('_layouts/_template/header') ?>

<style>
	#scanner_qrcode {
		max-width: 100%;
		height: auto;
	}
</style>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-3">
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Scan QRCode</h3>
				</div>
				<div class="card-body p-1">
					<video id="scanner_qrcode"></video>
					<div id="label_scanner_qrcode"></div>
				</div>
				<div class="card-footer">
					<select class="form-control" id="options" name="options">
						<option value="1">Kamera Depan</option>
						<option value="2">Kamera Belakang</option>
					</select>
				</div>
			</div>

			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Info Transaksi</h3>
				</div>
				<div class="card-body">
					<input type="text" name="user_id" id="user_id" value="<?php echo $_SESSION['id']; ?>" hidden>
					<input type="text" name="outlet_id" id="outlet_id" value="<?php echo $outlet[0]->id ?>" hidden>
					<input type="text" name="type_id" id="type_id" value="pulsa" hidden>

					<div class="form-group">
						<label class="control-label">Outlet</label>
						<input type="text" name="outlet_name" id="outlet_name" class="form-control input-sm"
							   value="<?php echo $outlet[0]->name; ?>" readonly>
					</div>

					<div class="form-group">
						<label class="control-label">Nomor Nota (Otomatis)</label>
						<input type="text" name="nomor_transaksi" id="nomor_transaksi" class="form-control input-sm"
							   value="<?php echo strtoupper('TRNSK' . uniqid()) . $_SESSION['id']; ?>" readonly>
					</div>
					<div class="form-group">
						<label class="control-label">Tanggal</label>
						<input type="text" name="tanggal_transaksi" id="tanggal_transaksi" class="form-control input-sm"
							   value="<?php echo date('d-m-Y'); ?>" disabled>
					</div>
					<div class="form-group">
						<label class="control-label">Kasir</label>
						<input type="text" name="user_name" id="user_name" class="form-control input-sm"
							   value="<?php echo $_SESSION['name']; ?>" disabled>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-9">
			<div class="card card-primary card-outline">
				<div class="card-header">
					<h3 class="card-title">Keranjang Belanja Pulsa Non Unit</b></h3>
					<div class="float-right">
						<div class="form-group">
							<button class="btn btn-success btn-sm" id="btn_product_pulsa_outlet"><i
										class="fa fa-search"></i> Stok Pulsa Tersedia
							</button>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-hover dt-responsive display nowrap"
							   style="width:100%">
							<tr>
								<td>Operator</td>
								<td colspan="5" style="padding-left:40px">
									<div class="row" id="item_pulsa">
									</div>
								</td>
							</tr>
							<tr>
								<td>Nominal Pulsa</td>
								<td colspan="5" style="padding-left:40px">
									<div class="row" id="item_pulsa_nominal">
									</div>
								</td>
								<input type="text" id="item_outlet_id" hidden>
							</tr>
							<tr>
								<td>Harga</td>
								<td>
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
										</div>
										<input class="form-control form-control-sm" id="pulsa_cost" readonly>
										<input class="form-control form-control-sm" id="pulsa_cost_data" hidden>
									</div>
								</td>
								<td>Sisa Stok Pulsa</td>
								<td>
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
										</div>
										<input class="form-control form-control-sm" id="pulsa_sisa_stok" readonly>
										<input class="form-control form-control-sm" id="pulsa_sisa_stok_data" readonly
											   hidden>
									</div>
								</td>
							</tr>
							<tr>
								<td>Diskon</td>
								<td>
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<span class="input-group-text">%</span>
										</div>
										<input type="number" class="form-control form-control-sm" id="pulsa_discount"
											   readonly>
									</div>
								</td>
								<td>Nomor HP Tujuan</td>
								<td>
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<span class="input-group-text">HP</span>
										</div>
										<input type="number" class="form-control form-control-sm" id="nomor_hp_tujuan"
											   required>
									</div>
								</td>
								<input type="hidden" class="form-control form-control-sm" id="modal">
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="card card-primary card-outline">
				<div class="card-header">
					<h3 class="card-title">Transaksi Penjualan Pulsa Non Unit</h3>
					<div class="float-right">
						<select name="transaction_type" id="transaction_type" class="form-control">
							<option value="1">Produk Fisik</option>
							<option value="2">Produk Pulsa</option>
						</select>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-6">
							<div class="alert alert-info text-center">
								<h3><span id="total_diskon">Diskon : Rp. 0</span></h3>
								<input type="hidden" id="total_diskon_data">
							</div>
						</div>

						<div class="col-md-6">
							<div class="alert alert-info text-center">
								<h3><span id="total_bayar">Total : Rp. 0</span></h3>
								<input type="hidden" id="total_bayar_data">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Jumlah Bayar</label>
								<input type="text" name="uang_cash" id="uang_cash" class="form-control"
									   onkeypress="return check_int(event)">
							</div>
							<div class="form-group">
								<label class="control-label">Kembali</label>
								<input type="text" name="uang_kembalian" id="uang_kembalian" class="form-control"
									   disabled>
								<input type="hidden" id="uang_kembalian_data">
							</div>
							<div class="row">
								<div class="col-sm-6">
									<button type="button" class="btn btn-warning btn-block"
											name="btn_save_print_transaction_submit"
											id="btn_save_print_transaction_submit">
										<i class="fa fa-print"></i> Cetak & Simpan
									</button>
								</div>
								<div class="col-sm-6">
									<button type="button" class="btn btn-primary btn-block"
											name="btn_save_transaction_submit"
											id="btn_save_transaction_submit">
										<i class="fa fa-save"></i> Simpan Transaksi
									</button>
								</div>
							</div>
						</div>
					</div>
					<br/>
				</div>
			</div>
		</div>
	</div>
</div>

<!--Modal tambah barang keranjang-->
<div class="modal fade" id="modal_product_pulsa_outlet" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content modal-xl">
			<div class="modal-header">
				<h5 class="modal-title">Informasi Stok Pulsa Non Unit</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="table_stok_pulsa" class="table table-striped table-hover dt-responsive display nowrap"
						   style="width:100%">
						<thead>
						<tr>
							<th class="nomor" data-orderable="false">No</th>
							<th>Tipe</th>
							<th>Kategori</th>
							<th>Nama</th>
							<th>Banyak</th>
						</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Selesai</button>
			</div>
		</div>
	</div>
</div>

<script>
	jenis_transaksi = 2;

	//init transaksi
	$(document).ready(function () {
		var today = new Date();
		date = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2)

		$('#transaction_type').val(jenis_transaksi);
		$("#date_begin").val(date)
		$("#date_end").val(date)

		init_operator_pulsa()

		$('#btn_save_transaction_submit').prop('disabled', true);
		$('#btn_save_print_transaction_submit').prop('disabled', true);
	})

	//ganti jenis transaksi
	$('#transaction_type').change(function () {
		outlet_id = "<?= $this->uri->segment(4); ?>";

		if ($(this).val() == 1) {
			link = "<?= base_url(); ?>kasir/transaksi/index/" + outlet_id
			window.location.href = link;
		} else if ($(this).val() == 2) {
			link = "<?= base_url(); ?>kasir/transaksi/pulsa/" + outlet_id
			window.location.href = link;
		}
	})

	//simpan data transaksi
	$("#btn_save_transaction_submit").click(function () {

		if ($('#pulsa_cost_data').val() > $('#pulsa_sisa_stok_data').val()) {
			Swal.fire({
				title: 'Transaksi gagal',
				text: 'Sisa saldo pulsa tidak mencukupi!',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1000
			});
		} else if ($('#nomor_hp_tujuan').val().length < 1) {

			Swal.fire({
				title: 'Peringatan',
				text: 'Nomor Hp Tujuan Wajib di isi!',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1000
			});

			$('#nomor_hp_tujuan').focus();

		} else {
			var user_id = $("#user_id").val()
			var outlet_id = $("#outlet_id").val()
			var type_id = $("#type_id").val()
			var item_outlet_id = $("#item_outlet_id").val()
			var nomor_transaksi = $("#nomor_transaksi").val()
			var total_bayar = $("#total_bayar_data").val()
			var total_diskon = $("#total_diskon_data").val()
			var uang_cash = $("#uang_cash").val()
			var uang_kembalian = $("#uang_kembalian_data").val()
			var catatan = $("#nomor_hp_tujuan").val()

			if (uang_cash.length < 1) {
				Swal.fire({
					title: 'Peringatan!',
					text: 'Harap masukan jumlah bayar',
					icon: 'warning',
					showConfirmButton: false,
					timer: 1000
				});
			} else {
				Swal.fire({
					title: 'Peringatan!',
					text: 'Apakah Anda yakin akan memproses transaksi pulsa ?',
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, proses transaksi ini!'
				}).then((result) => {
					if (result.value) {
						$.ajax({
							url: "<?= base_url(); ?>kasir/transaksi/save_transaction",
							type: "POST",
							data: {
								user_id: user_id,
								outlet_id: outlet_id,
								type_id: type_id,
								item_outlet_id: item_outlet_id,
								nomor_transaksi: nomor_transaksi,
								total_bayar: total_bayar,
								total_diskon: total_diskon,
								uang_cash: uang_cash,
								uang_kembalian: uang_kembalian,
								catatan: catatan
							},
							success(result) {
								if (result.success) {
									Swal.fire({
										title: 'Berhasil',
										text: result.message,
										icon: result.type,
										showConfirmButton: false,
										timer: 600
									}).then(function () {
										location.reload();
									});

								} else {
									Swal.fire({
										title: 'gagal',
										text: result.message,
										icon: result.type,
										showConfirmButton: false,
										timer: 1000
									});
								}
							}
						})
					}
				})
			}
		}
	})

	//simpan dan cetak transaksi
	$("#btn_save_print_transaction_submit").click(function () {

		if ($('#pulsa_cost_data').val() > $('#pulsa_sisa_stok_data').val()) {
			Swal.fire({
				title: 'Transaksi gagal',
				text: 'Sisa saldo pulsa tidak mencukupi!',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1000
			});
		} else if ($('#nomor_hp_tujuan').val().length < 1) {

			Swal.fire({
				title: 'Peringatan',
				text: 'Nomor Hp Tujuan Wajib di isi!',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1000
			});

			$('#nomor_hp_tujuan').focus();

		} else {
			var user_id = $("#user_id").val()
			var outlet_id = $("#outlet_id").val()
			var type_id = $("#type_id").val()
			var item_outlet_id = $("#item_outlet_id").val()
			var nomor_transaksi = $("#nomor_transaksi").val()
			var total_bayar = $("#total_bayar_data").val()
			var total_diskon = $("#total_diskon_data").val()
			var uang_cash = $("#uang_cash").val()
			var uang_kembalian = $("#uang_kembalian_data").val()
			var catatan = $("#nomor_hp_tujuan").val()

			if (uang_cash.length < 1) {
				Swal.fire({
					title: 'Peringatan!',
					text: 'Harap masukan jumlah bayar!',
					icon: 'warning',
					showConfirmButton: false,
					timer: 1000
				});
			} else {
				Swal.fire({
					title: 'Peringatan!',
					text: 'Apakah Anda yakin akan memproses transaksi pulsa ?',
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, proses transaksi ini!'
				}).then((result) => {
					if (result.value) {
						$.ajax({
							url: "<?= base_url(); ?>kasir/transaksi/save_transaction",
							type: "POST",
							data: {
								user_id: user_id,
								outlet_id: outlet_id,
								type_id: type_id,
								item_outlet_id: item_outlet_id,
								nomor_transaksi: nomor_transaksi,
								total_bayar: total_bayar,
								total_diskon: total_diskon,
								uang_cash: uang_cash,
								uang_kembalian: uang_kembalian,
								catatan: catatan
							},
							success(result) {

								if (result.success) {

									link = "<?= base_url(); ?>transaksi/print_transaction_detail/" + result.transaction_code
									window.open(link, '_blank');

									Swal.fire({
										title: 'Berhasil',
										text: result.message,
										icon: result.type,
										showConfirmButton: false,
										timer: 600
									}).then(function () {
										location.reload();
									});

								} else {
									Swal.fire({
										title: 'gagal',
										text: result.message,
										icon: result.type,
										showConfirmButton: false,
										timer: 1000
									});
								}
							}
						})
					}
				})
			}
		}
	})

	//hitung uang kembalian
	$('#uang_cash').keyup(function () {
		hitung_total_kembalian();
	});

	//mengambil data nominal pulsa per operator
	$("#item_pulsa").click(function () {
		item_outlet_category_id = $("#item_pulsa").find(":checked").val()

		$.ajax({
			url: "<?= base_url(); ?>kasir/transaksi/get_pulsa_nominal/" + item_outlet_category_id,
			success: function (result) {
				if (result.success) {
					data = '';
					result.data.forEach(function (item) {
						data += "<div class='col-lg-2'><input type='radio' class='form-check-input' value='" + item.id + "' name='item_pulsa_nominal' id='item_pulsa_nominal_" + item.id + "'><p><b>" + item.name + "</b></p></div></div>"
					})
					$('#item_pulsa_nominal').html(data)
				} else {
					Swal.fire({
						title: 'gagal',
						text: 'Nominal pulsa tidak ditemukan',
						icon: 'warning',
						showConfirmButton: false,
						timer: 1000
					});
				}
			}
		})
	})

	$("#item_pulsa_nominal").click(function () {
		item_product_id = $("#item_pulsa_nominal").find(":checked").val()

		$.ajax({
			url: "<?= base_url(); ?>kasir/transaksi/get_pulsa_cost_detail/" + item_product_id,
			success: function (result) {
				if (result.success) {

					$('#item_outlet_id').val(result.data.item_outlet_id);
					$('#pulsa_cost').val(to_rupiah(result.data.cost)).text(to_rupiah(result.data.cost))
					$('#pulsa_cost_data').val(result.data.cost).text(result.data.cost)
					$('#pulsa_discount').val(result.data.discount).text(result.data.cost)

					sisa_pulsa = parseInt(result.data.many) - parseInt(result.data.cost);

					$('#pulsa_sisa_stok').val(to_rupiah(sisa_pulsa)).text(to_rupiah(sisa_pulsa))
					$('#pulsa_sisa_stok_data').val(sisa_pulsa).text(sisa_pulsa)

					hitung_total_bayar()

				} else {
					Swal.fire({
						title: 'gagal',
						text: 'Nominal pulsa tidak ditemukan',
						icon: 'warning',
						showConfirmButton: false,
						timer: 1000
					});
				}
			}
		})
	})

	$('#btn_product_pulsa_outlet').click(function () {
		init_table_stok_pulsa();
		$('#modal_product_pulsa_outlet').modal({backdrop: 'static', keyboard: false})
	})

	//init operator pulsa
	function init_operator_pulsa() {
		$.ajax({
			url: "<?= base_url(); ?>kasir/transaksi/get_item_pulsa/" + "<?=$this->uri->segment(4);?>",
			success: function (result) {
				if (result.success) {
					data = '';
					result.data.forEach(function (item) {
						data += "<div class='col-lg-2'><input type='radio' class='form-check-input' value='" + item.id + "' name='item_pulsa' id='item_pulsa_" + item.id + "'><p><b>" + item.name + "</b></p></div>"
					})
					$('#item_pulsa').html(data)
				} else {
					Swal.fire({
						title: 'gagal',
						text: 'Produk pulsa tidak ditemukan',
						icon: 'warning',
						showConfirmButton: false,
						timer: 1000
					});
				}
			}
		})
	}

	//init datatables table peringatan
	function init_table_stok_pulsa() {
		table_stok_pulsa = $("#table_stok_pulsa").DataTable({
			destroy: true,
			autoWidth: true,
			searching: false,
			ajax: "<?= base_url(); ?>kasir/_gudang/outlet/get_stok_peringatan/<?= $this->uri->segment(4); ?>",
			columns: [
				{
					data: "id",
					width: "0.8%",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: "type_name"
				},
				{
					data: "category_name"
				},
				{
					data: "product_name"
				},
				{
					data: "many"
				}
			]
		})
	}

	//hitung uang total bayar
	function hitung_total_bayar() {
		total = $('#pulsa_cost_data').val();
		discount = ($('#pulsa_discount').val() / 100 * total);

		var total_semua = 0;
		total_semua = parseInt(total_semua) + (parseInt(total) - parseInt(discount));

		$('#total_diskon').html('Diskon : ' + to_rupiah(discount));
		$('#total_diskon_data').val(discount);

		$('#total_bayar').html('Total : ' + to_rupiah(total_semua));
		$('#total_bayar_data').val(total_semua);

		$('#uang_cash').val('');
		$('#uang_kembalian').val('Rp. 0');
		$('#uang_kembalian_data').val(0);

		$('#btn_save_transaction_submit').prop('disabled', true);
		$('#btn_save_print_transaction_submit').prop('disabled', true);
	}

	//hitung uang total kembalian
	function hitung_total_kembalian() {
		var cash = $('#uang_cash').val();
		var total_bayar = $('#total_bayar_data').val();

		if (parseInt(cash) >= parseInt(total_bayar)) {
			var selisih = parseInt(cash) - parseInt(total_bayar);
			$('#uang_kembalian').val(to_rupiah(selisih));
			$('#uang_kembalian_data').val(selisih);
			$('#btn_save_transaction_submit').prop('disabled', false);
			$('#btn_save_print_transaction_submit').prop('disabled', false);
		} else if (parseInt(cash) < parseInt(total_bayar)) {
			var selisih = parseInt(cash) - parseInt(total_bayar);
			$('#uang_kembalian').val(to_rupiah(selisih));
			$('#uang_kembalian_data').val(selisih);
			$('#btn_save_transaction_submit').prop('disabled', true);
			$('#btn_save_print_transaction_submit').prop('disabled', true);
		} else {
			$('#uang_kembalian').val('');
			$('#uang_kembalian_data').val(0);
		}
	}

	//konversi angka ke format rupiah
	function to_rupiah(angka) {
		var rev = parseInt(angka, 10).toString().split('').reverse().join('');

		var rev2 = '';
		for (var i = 0; i < rev.length; i++) {
			rev2 += rev[i];
			if ((i + 1) % 3 === 0 && i !== (rev.length - 1)) {
				rev2 += ',';
			}
		}

		$('#btn_save_transaction_submit').prop('disabled', false);
		$('#btn_save_print_transaction_submit').prop('disabled', false);

		return 'Rp. ' + rev2.split('').reverse().join('');
	}

	//cek inputan angka atau bukan
	function check_int(event) {
		var charCode = (event.which) ? event.which : event.keyCode;
		return (charCode >= 48 && charCode <= 57 || charCode === 8);
	}

	//init camera
	var scanner = new Instascan.Scanner({
		video: document.getElementById('scanner_qrcode'),
		scanPeriod: 5,
		mirror: false
	});

	//init scanner
	scanner.addListener('scan', function (content) {

		outlet_id = "<?= $this->uri->segment(4); ?>";

		if (content.length > 0) {
			$.ajax({
				url: "<?= base_url(); ?>kasir/transaksi/get_product_pulsa_by_qrcode/" + outlet_id + "/" + content,
				success: function (result) {
					if (result.success) {
						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 600
						});

						$('#item_pulsa_' + result.data.item_outlet_category_id).prop('checked', true);

						var id_nominal = result.data.id

						$.ajax({
							url: "<?= base_url(); ?>kasir/transaksi/get_pulsa_nominal/" + result.data.item_outlet_category_id,
							success: function (result) {
								if (result.success) {
									data = '';
									result.data.forEach(function (item) {
										if (id_nominal === item.id) {
											data += "<div class='col-lg-2'><input checked type='radio' class='form-check-input' value='" + item.id + "' name='item_pulsa_nominal' id='item_pulsa_nominal_" + item.id + "'><p><b>" + item.name + "</b></p></div></div>"
										} else {
											data += "<div class='col-lg-2'><input type='radio' class='form-check-input' value='" + item.id + "' name='item_pulsa_nominal' id='item_pulsa_nominal_" + item.id + "'><p><b>" + item.name + "</b></p></div></div>"
										}
									})
									$('#item_pulsa_nominal').html(data)
								}
							}
						})

						setTimeout(() => {
							item_product_id = $("#item_pulsa_nominal").find(":checked").val()

							$.ajax({
								url: "<?= base_url(); ?>kasir/transaksi/get_pulsa_cost_detail/" + item_product_id,
								success: function (result) {
									if (result.success) {

										$('#item_outlet_id').val(result.data.item_outlet_id);
										$('#pulsa_cost').val(to_rupiah(result.data.cost)).text(to_rupiah(result.data.cost))
										$('#pulsa_cost_data').val(result.data.cost).text(result.data.cost)
										$('#pulsa_discount').val(result.data.discount).text(result.data.cost)

										sisa_pulsa = parseInt(result.data.many) - parseInt(result.data.cost);

										$('#pulsa_sisa_stok').val(to_rupiah(sisa_pulsa)).text(to_rupiah(sisa_pulsa))
										$('#pulsa_sisa_stok_data').val(sisa_pulsa).text(sisa_pulsa)

										hitung_total_bayar()
									} else {
										Swal.fire({
											title: 'gagal',
											text: 'Nominal pulsa tidak ditemukan',
											icon: 'warning',
											showConfirmButton: false,
											timer: 1000
										});
									}
								}
							})
						}, 500)

					} else {
						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1000
						});
					}
				}
			})
		} else {
			Swal.fire({
				title: 'gagal',
				text: 'Produk tidak ditemukan',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1000
			});
		}
	});

	//init reader
	Instascan.Camera.getCameras().then(function (cameras) {
		if (cameras.length > 0) {
			scanner.start(cameras[0]);
			$('[name="options"]').change(function () {
				if ($('[name="options"]').val() == 1) {
					if (cameras[0] != "") {
						scanner.start(cameras[0]);
					} else {
						alert('No Front camera found!');
					}
				} else if ($(this).val() == 2) {
					if (cameras[1] != "") {
						scanner.start(cameras[1]);
					} else {
						alert('No Back camera found!');
					}
				}
			});
		} else {
			console.error('No cameras found.');
			alert('No cameras found.');
		}
	}).catch(function (e) {
		console.error(e);
		alert(e);
	});

</script>

<?php $this->load->view('_layouts/_template/js') ?>
