<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Item extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_item', '', true);

		if (!isset($_SESSION)) {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'ADMIN') {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'OWNER') {
			redirect('auth/login');
		} elseif ($_SESSION['status_user'] == 'KASIR') {
			redirect('auth/login');
		}
	}

	function index()
	{
		$data['title'] = 'Item';
		$data['site_navigation'] = 'Item';

		return $this->load->view('item/index', $data);
	}

	function get_all_item()
	{
		$result = $this->M_item->get_datatables();

		$response = [
			'success' => true,
			'draw' => $_REQUEST['draw'],
			'recordsTotal' => $this->M_item->count_all(),
			'recordsFiltered' => $this->M_item->count_filtered(),
			'data' => $result
		];

		return to_json($response);
	}

	function get_item()
	{
		$result = $this->M_item->get_item();

		if (count($result) > 0) {

			$response = [
				'success' => true,
				'data' => $result,
				'total' => count($result)
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function add_item()
	{
		$name = $this->input->post('name');

		$result = $this->M_item->add_item($name);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Tambah item berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Tambah item gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function edit_item()
	{
		$id = $this->input->post('id');
		$name = $this->input->post('name');

		$result = $this->M_item->edit_item($id, $name);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Ubah item ' . $name . ' berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Ubah item ' . $name . ' gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function delete_item()
	{
		$id = $this->input->post('id');
		$result = $this->M_item->delete_item($id);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Hapus item berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Hapus item gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}
}
