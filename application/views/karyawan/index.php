<?php $this->load->view('_layouts/_template/header') ?>

<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div class="card card-outline card-primary">
				<div class="card-header">
					<h3 class="card-title"><b>karyawan</b></h3>
					<div class="float-right">
						<button class="btn btn-danger btn-sm" id="btn_reset_password_karyawan_outlet"><i
									class="fa fa-pencil-alt"></i> Reset Password
						</button>
						<button class="btn btn-primary btn-sm" id="btn_add_karyawan_outlet"><i class="fa fa-plus"></i>
							karyawan
							Outlet
						</button>
					</div>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table id="table_karyawan" class="table table-striped table-hover dt-responsive display nowrap"
							   style="width:100%">
							<thead>
							<tr>
								<th class="nomor" data-orderable="false">No</th>
								<th>Username</th>
								<th>Nama</th>
								<th>Wewenang</th>
								<th>Outlet</th>
								<th>Alamat Outlet</th>
								<th data-orderable="false">Aksi</th>
							</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--modal tambah karyawan-->
<div class="modal fade" id="modal_add_karyawan_outlet" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Tambah karyawan Outlet</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="add_karyawan_outlet_form">
					<div class="form-group">
						<div class="col-lg-4">Outlet</div>
						<div class="col-lg-12">
							<select class="form-control form-control-sm" id="add_karyawan_outlet"
									name="add_karyawan_outlet">
							</select>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Nama Karyawan</div>
						<div class="col-lg-12">
							<select class="form-control form-control-sm" id="add_karyawan_outlet_name">
							</select>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"
						id="save_add_karyawan_outlet_submit">Simpan
				</button>
			</div>
		</div>
	</div>
</div>

<!--modal ubah karyawan-->
<div class="modal fade" id="modal_edit_karyawan_outlet" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Ubah karyawan Outlet</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="edit_karyawan_outlet_form">
					<input type="text" id="edit_karyawan_outlet_id" name="edit_karyawan_outlet_id" hidden>
					<div class="form-group">
						<div class="col-lg-4">Outlet</div>
						<div class="col-lg-12">
							<select class="form-control form-control-sm" id="edit_karyawan_outlet"
									name="edit_karyawan_outlet">
							</select>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Nama Karyawan</div>
						<div class="col-lg-12">
							<select class="form-control form-control-sm" id="edit_karyawan_outlet_name"
									name="edit_karyawan_outlet_name" disabled>
							</select>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"
						id="save_edit_karyawan_outlet_submit">Simpan
				</button>
			</div>
		</div>
	</div>
</div>

<!--modal reset password karyawan-->
<div class="modal fade" id="modal_reset_password_karyawan_outlet" tabindex="-1" role="dialog"
	 aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Reset Password Karyawan Outlet</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="reset_password_karyawan_outlet_form">
					<div class="form-group">
						<div class="col-lg-4">Karyawan</div>
						<div class="col-lg-12">

							<select class="form-control form-control-sm" id="reset_password_karyawan_outlet_id"
									name="reset_password_karyawan_outlet_id" required>
							</select>
						</div>
					</div>
				</form>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"
						id="btn_reset_password_karyawan_outlet_submit">Reset Password
				</button>
			</div>
		</div>
	</div>
</div>

<script>
	//init table karyawan
	$(document).ready(function () {
		get_user()
		get_outlet()
		get_outlet_employee()
		init_table_karyawan()
	})

	//validasi form reset password pengguna
	$("#reset_password_karyawan_outlet_form").validate({
		rules: {
			reset_password_karyawan_outlet_id: {
				required: true
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//modal reset password pengguna
	$("#btn_reset_password_karyawan_outlet").on('click', function () {
		$("#modal_reset_password_karyawan_outlet").modal({backdrop: 'static', keyboard: false})
	})

	//simpan reset password pengguna
	$('#btn_reset_password_karyawan_outlet_submit').click(function () {

		if ($('#reset_password_karyawan_outlet_form').valid()) {

			user_id = $('#reset_password_karyawan_outlet_id option:selected').val();

			nama = $('#reset_password_karyawan_outlet_id option:selected').text();

			Swal.fire({
				title: 'Peringatan!',
				text: 'Apakah Anda yakin akan mereset password karyawan ' + nama + ' ?',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Ya, reset password karyawan ini!'
			}).then((result) => {
				if (result.value) {

					$.ajax({
						url: "<?= base_url(); ?>karyawan/reset_password",
						type: "POST",
						data: {id: user_id},
						success(result) {

							if (result.success) {
								Swal.fire({
									title: 'Berhasil',
									text: result.message,
									icon: result.type,
									showConfirmButton: false,
									timer: 1500
								});

								init_table_karyawan()
							} else {
								Swal.fire({
									title: 'gagal',
									text: result.message,
									icon: result.type,
									showConfirmButton: false,
									timer: 1500
								});
							}
						}
					})
				}
			})

		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});
		}
	});

	//validasi form tambah data karyawan
	$("#add_karyawan_outlet_form").validate({
		rules: {
			add_karyawan_outlet: {
				required: true
			},
			add_karyawan_outlet_name: {
				required: true
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//modal tambah data karyawan
	$("#btn_add_karyawan_outlet").on('click', function () {
		$("#modal_add_karyawan_outlet").modal({backdrop: 'static', keyboard: false})
	})

	//simpan tambah data karyawan
	$("#save_add_karyawan_outlet_submit").on('click', function () {

		if ($('#add_karyawan_outlet_form').valid()) {

			$.ajax({
				url: "<?= base_url(); ?>karyawan/add_karyawan",
				type: "POST",
				data: {
					outlet_id: $("#add_karyawan_outlet").val(),
					user_id: $("#add_karyawan_outlet_name").val()
				},
				success: function (result) {

					if (result.success) {
						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						init_table_karyawan()

					} else {
						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});
					}
				}
			})
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});

			init_table_karyawan()
		}
	})

	//validasi form edit data karyawan
	$("#edit_karyawan_outlet_form").validate({
		rules: {
			edit_karyawan_outlet: {
				required: true
			},
			edit_karyawan_outlet_name: {
				required: true
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//modal ubah data karyawan
	$("#table_karyawan").on('click', 'tr #btn_edit_click', function () {

		tr = $(this).closest('tr')
		data = table_karyawan.row(tr).data()

		$("#edit_karyawan_outlet_id").val(data.employee_outlet_id)
		$("#edit_karyawan_outlet").val(data.outlet_id)
		$("#edit_karyawan_outlet_name").val(data.user_id)

		$('#modal_edit_karyawan_outlet').modal({backdrop: 'static', keyboard: false})
	})

	//ubah data karyawan
	$("#save_edit_karyawan_outlet_submit").on('click', function () {

		if ($('#edit_karyawan_outlet_form').valid()) {
			$.ajax({
				url: "<?= base_url(); ?>karyawan/edit_karyawan",
				type: "POST",
				data: {
					outlet_employee_id: $("#edit_karyawan_outlet_id").val(),
					outlet_id: $("#edit_karyawan_outlet").val(),
					user_id: $("#edit_karyawan_outlet_name").val()
				},
				success: function (result) {

					if (result.success) {
						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						init_table_karyawan()

					} else {
						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});
					}
				}
			})
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});

			init_table_karyawan()
		}
	})

	//hapus data karyawan
	$("#table_karyawan").on('click', 'tr #btn_delete_click', function () {
		tr = $(this).closest('tr')
		data = table_karyawan.row(tr).data()

		id = data.employee_outlet_id

		Swal.fire({
			title: 'Peringatan!',
			text: 'Apakah Anda yakin akan menghapus karyawan ' + data.user_name + ' ?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya, hapus data ini!'
		}).then((result) => {
			if (result.value) {

				$.ajax({
					url: "<?= base_url(); ?>karyawan/delete_karyawan",
					type: "POST",
					data: {id: id},
					success(result) {

						if (result.success) {
							Swal.fire({
								title: 'Berhasil',
								text: result.message,
								icon: result.type,
								showConfirmButton: false,
								timer: 1500
							});

							init_table_karyawan()
						} else {
							Swal.fire({
								title: 'gagal',
								text: result.message,
								icon: result.type,
								showConfirmButton: false,
								timer: 1500
							});
						}
					}
				})
			}
		})
	})

	//init datatables table karyawan
	function init_table_karyawan() {
		button = "&nbsp;<button class='btn btn-warning btn-sm' id='btn_edit_click'>\
            <i class='fas fa-edit'></i>&nbsp;Ubah</button>"
		button += "&nbsp;<button class='btn btn-danger btn-sm' id='btn_delete_click'>\
            <i class='fas fa-trash'></i>&nbsp;Hapus</button>"

		table_karyawan = $("#table_karyawan").DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			autoWidth: true,
			width: "0.8%",
			ajax: "<?= base_url(); ?>karyawan/get_all_karyawan",
			columns: [
				{
					data: "id",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: 'user_username'
				},
				{
					data: 'user_name'
				},
				{
					data: 'user_roles_name'
				},
				{
					data: 'outlet_name'
				},
				{
					data: 'outlet_address'
				},
				{
					defaultContent: button
				},
			]
		})
	}

	//dropdown pilih outlet
	function get_outlet() {
		$.ajax({
			url: "<?= base_url(); ?>outlet/get_all_outlet",
			success: function (result) {
				if (result.success) {
					var outlet_list = '<option value="">Pilih...</option>'
					result.data.forEach(function (outlet) {
						outlet_list += "<option value='" + outlet['id'] + "'>" + outlet['name'] + "</option>";
					})

					$('#add_karyawan_outlet').html(outlet_list)
					$('#edit_karyawan_outlet').html(outlet_list)
				}
			}
		})
	}

	//dropdown pilih pengguna
	function get_user() {
		$.ajax({
			url: "<?= base_url(); ?>pengguna/get_pengguna",
			success: function (result) {
				if (result.success) {
					var user_list = '<option value="">Pilih...</option>'
					result.data.forEach(function (outlet) {
						if (outlet['wewenang'] !== 'ADMIN' && outlet['wewenang'] !== 'OWNER') {
							user_list += "<option value='" + outlet['user_id'] + "'>" + outlet['name'] + "</option>";
						}
					})
					$('#add_karyawan_outlet_name').html(user_list)
					$('#edit_karyawan_outlet_name').html(user_list)
				}
			}
		})
	}

	//dropdown pilih pengguna
	function get_outlet_employee() {
		$.ajax({
			url: "<?= base_url(); ?>karyawan/get_karyawan/0",
			success: function (result) {
				if (result.success) {
					var user_list = '<option value="">Pilih...</option>'
					result.data.forEach(function (outlet) {
						if (outlet['wewenang'] !== 'ADMIN' && outlet['wewenang'] !== 'OWNER') {
							user_list += "<option value='" + outlet['outlet_employee_id'] + "'>" + "nama : " + outlet['name'] + " | outlet : " + outlet['outlet_name'] + " | username : " + outlet['username'] + "</option>";
						}
					})
					$('#reset_password_karyawan_outlet_id').html(user_list)
				}
			}
		})
	}

</script>


<?php $this->load->view('_layouts/_template/js') ?>
