<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_cart extends CI_Model
{
	var $column_order = array('category.name', 'cart.name', 'product.modal', 'product.many', 'cart.cost', 'cart.quantity');
	var $column_search = array('category.name', 'cart.name', 'product.modal', 'product.many', 'cart.cost', 'cart.quantity');
	var $order = array('cart.name' => 'asc');

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_datatables($outlet_id)
	{
		$this->_get_datatables_query($outlet_id);

		if ($_REQUEST['length'] != -1)
			$this->db->limit($_REQUEST['length'], $_REQUEST['start']);
		$query = $this->db->get();

		return $query->result();
	}

	private function _get_datatables_query($outlet_id)
	{
		if ($outlet_id != 0) {
			$this->db->select('cart.id as cart_id, category.id as category_id, category.name as category_name, cart.item_product_id, cart.name as product_name, format(product.modal,0) as modal, format(item_product_outlet.many,0) as stock, format(cart.cost,0) as cost, format(cart.quantity,0) as quantity')
				->from('item_transfer_cart as cart, item_product as product, item_product_outlet as item_product_outlet, item_category as category')
				->where('cart.item_product_id = product.id')
				->where('product.item_category_id = category.id')
				->where('product.id = item_product_outlet.item_product_id')
				->where('cart.outlet_id', $outlet_id)
				->where('item_product_outlet.outlet_id', $outlet_id)
				->where('cart.deleted_date is null');
		} else {
			$this->db->select('cart.id as cart_id, category.id as category_id, category.name as category_name, cart.item_product_id, cart.name as product_name, format(product.modal,0) as modal, format(product.many,0) as stock, format(cart.cost,0) as cost, format(cart.quantity,0) as quantity')
				->from('item_transfer_cart as cart, item_product as product, item_category as category')
				->where('cart.item_product_id = product.id')
				->where('product.item_category_id = category.id')
				->where('cart.outlet_id is null')
				->where('cart.deleted_date is null');
		}

		$i = 0;
		foreach ($this->column_search as $item) {
			if ($_REQUEST['search']['value']) {

				if ($i === 0) {
					$this->db->group_start();
					$this->db->like($item, $_REQUEST['search']['value']);
				} else {
					$this->db->or_like($item, $_REQUEST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if (isset($_REQUEST['order'])) {
			$this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function count_filtered($outlet_id)
	{
		$this->_get_datatables_query($outlet_id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_all($outlet_id)
	{
		if ($outlet_id != 0) {
			$this->db->select('cart.id as cart_id, category.id as category_id, category.name as category_name, cart.item_product_id, cart.name as product_name, format(product.modal,0) as modal, format(item_product_outlet.many,0) as stock, format(cart.cost,0) as cost, format(cart.quantity,0) as quantity')
				->from('item_transfer_cart as cart, item_product as product, item_product_outlet as item_product_outlet, item_category as category')
				->where('cart.item_product_id = product.id')
				->where('product.item_category_id = category.id')
				->where('product.id = item_product_outlet.item_product_id')
				->where('cart.outlet_id', $outlet_id)
				->where('item_product_outlet.outlet_id', $outlet_id)
				->where('cart.deleted_date is null');
		} else {
			$this->db->select('cart.id as cart_id, category.id as category_id, category.name as category_name, cart.item_product_id, cart.name as product_name, format(product.modal,0) as modal, format(product.many,0) as stock, format(cart.cost,0) as cost, format(cart.quantity,0) as quantity')
				->from('item_transfer_cart as cart, item_product as product, item_category as category')
				->where('cart.item_product_id = product.id')
				->where('product.item_category_id = category.id')
				->where('cart.outlet_id is null')
				->where('cart.deleted_date is null');
		}

		return $this->db->count_all_results();
	}

	//BULK TRANSFER GUDANG
	function get_all_cart_transfer($sender_outlet_id, $status)
	{
		if ($sender_outlet_id != 0) {
			return $this->db->query("select * from item_transfer_cart where outlet_id='$sender_outlet_id' and status='$status' and deleted_date is null")->result();
		} else {
			return $this->db->query("select * from item_transfer_cart where outlet_id is null and status='$status' and deleted_date is null")->result();
		}
	}

	function add_cart_transfer($product_id, $name, $modal, $cost, $quantity)
	{
		$created_by = created_by();
		$created_date = created_date();

		$gudang_product = $this->db->query("select many from item_product where id='$product_id' and deleted_date is null")->result()[0];

		if ($quantity > $gudang_product->many) {
			return false;
		} else {

			$check = $this->db->query("select id, quantity from item_transfer_cart where item_product_id ='$product_id' and deleted_date is null")->result();

			if (count($check) != null) {

				$total = $check[0]->quantity + $quantity;

				return $this->db->query("
				update item_transfer_cart set cost='$cost', quantity='$total', modified_by='$created_by', modified_date='$created_date' where id='{$check[0]->id}' and deleted_date is null");
			} else {
				return $this->db->query("
				insert into item_transfer_cart (item_product_id, name, modal, cost, quantity, status, created_by, created_date)
				values ('$product_id', '$name', '$modal', '$cost', '$quantity', 'transfer_from_pusat','$created_by','$created_date')");
			}
		}
	}

	function edit_cart_transfer($product_id, $cost, $quantity)
	{
		$modified_by = modified_by();
		$modified_date = modified_date();

		$keranjang = $this->db->query("select item_product_id from item_transfer_cart where id='$product_id' and deleted_date is null")->result()[0];
		$gudang = $this->db->query("select many from item_product where id='{$keranjang->item_product_id}' and deleted_date is null")->result()[0];

		if ($quantity > $gudang->many) {
			return false;
		} else {
			return $this->db->query("
				update item_transfer_cart set cost='$cost', quantity='$quantity', modified_by='$modified_by', modified_date='$modified_date' where id='$product_id' and deleted_date is null");
		}
	}

	function bulk_transfer_to_outlet($outlet_id, $item_product_id, $cost, $quantity, $product_image)
	{
		$created_by = created_by();
		$created_date = created_date();
		$modified_by = modified_by();
		$modified_date = modified_date();

		$this->db->trans_begin();

		$product = $this->db->query("select many from item_product where id='$item_product_id' and deleted_date is null")->result();

		if (count($product) > 0) {
			$product_many = $product[0]->many - $quantity;

			if ($product_many < 0) {
				return false;
			} else {
				$barang_outlet = $this->db->query("select many from item_product_outlet where outlet_id='$outlet_id' and  item_product_id='$item_product_id' and deleted_date is null")->result();

				if ($barang_outlet) {
					$item = $this->db->query("
					select item_product.name as product_name, item_category.name as category_name,
					item_type.name as type_name, item_product.modal, item_product_outlet.cost,
					item_product_outlet.many as awl
					from item_product_outlet, item_product, item_category, item_type
					where item_product_outlet.item_product_id=item_product.id
					and item_product.item_category_id=item_category.id 
					and item_category.item_type_id=item_type.id 
					and item_product_outlet.deleted_date is null 
					and item_product.deleted_date is null 
					and item_category.deleted_date is null 
					and item_type.deleted_date is null 
					and item_product.id='$item_product_id'
					and item_product_outlet.outlet_id='$outlet_id'
					")->result()[0];

					$total = $item->awl + $quantity;

					$this->db->query("insert into transaction (user_id, outlet_id, item_outlet_id, type, category, product, modal, cost, many, awl, plus, ak, status, from_outlet, unit, created_by, created_date)  
					values('{$_SESSION['id']}','$outlet_id','$item_product_id','$item->type_name','$item->category_name','$item->product_name','$item->modal','$item->cost','$quantity','$item->awl','$quantity','$total','tambah_unit','$outlet_id','Unit','$created_by','$created_date')");

					$transaction_id = $this->db->insert_id();

					$this->db->query("update item_product_outlet set many='$total', cost='$cost', modified_by='$modified_by', modified_date='$modified_date' where outlet_id='$outlet_id' and item_product_id='$item_product_id' and deleted_date is null");

					$this->db->query("update item_product set many='$product_many', modified_by='$modified_by', modified_date='$modified_date' where id='$item_product_id' and deleted_date is null");

					$this->db->query("delete from item_transfer_cart where status='transfer_from_pusat' and deleted_date is null");

					$this->db->trans_complete();

					return $transaction_id;
				} else {

					$outlet_many = $quantity;
					$this->db->query("insert into item_product_outlet (outlet_id, item_product_id, many, cost, qrcode, created_by, created_date)
                	values('$outlet_id', '$item_product_id', '$outlet_many', '$cost','$product_image', '$created_by', '$created_date')");

					$item = $this->db->query("
                	select item_product.name as product_name, item_category.name as category_name, item_type.name as type_name, item_product.modal
                	from item_product, item_category, item_type
                	where item_product.item_category_id=item_category.id 
                	and item_category.item_type_id=item_type.id 
					and item_product.deleted_date is null 
					and item_category.deleted_date is null 
					and item_type.deleted_date is null 
                	and item_product.id='$item_product_id'")->result()[0];

					$this->db->query("insert into transaction (user_id, outlet_id, item_outlet_id, type, category,
                	product, modal,cost,many,awl, plus, ak, status, from_outlet, unit, created_by, created_date)  
                	values('{$_SESSION['id']}', '$outlet_id','$item_product_id','$item->type_name','$item->category_name','$item->product_name', '$item->modal', '0', '$quantity', '0', '$quantity', '$quantity', 'tambah_unit', '$outlet_id', 'Unit', '$created_by', '$created_date')");

					$transaction_id = $this->db->insert_id();

					$this->db->query("update item_product set many='$product_many', modified_by='$modified_by', modified_date='$modified_date' where id='$item_product_id' and deleted_date is null");

					$this->db->query("delete from item_transfer_cart where status='transfer_from_pusat' and deleted_date is null");

					$this->db->trans_complete();

					return $transaction_id;
				}
			}
		}
		return false;
	}

	//BULK TRANSFER OUTLET
	function add_cart_transfer_from_outlet($outlet_id, $outlet_product_id, $item_product_id, $name, $modal, $cost, $quantity)
	{
		$created_by = created_by();
		$created_date = created_date();

		$gudang_product = $this->db->query("select many from item_product_outlet where outlet_id='$outlet_id' and id='$outlet_product_id' and deleted_date is null")->result();

		if (count($gudang_product) > 0) {
			if ($quantity > $gudang_product[0]->many) {
				return false;
			} else {

				$check = $this->db->query("select id, quantity from item_transfer_cart where outlet_id='$outlet_id' and item_product_id ='$item_product_id' and deleted_date is null")->result();

				if (count($check) != null) {

					$total = $check[0]->quantity + $quantity;

					return $this->db->query("
				update item_transfer_cart set cost='$cost', quantity='$total', modified_by='$created_by', modified_date='$created_date' where id='{$check[0]->id}' and deleted_date is null");
				} else {
					return $this->db->query("
				insert into item_transfer_cart (outlet_id, item_product_id, name, modal, cost, quantity, status, created_by, created_date)
				values ('$outlet_id','$item_product_id', '$name', '$modal', '$cost', '$quantity', 'transfer_from_outlet','$created_by','$created_date')");
				}
			}
		}
		return false;
	}

	function edit_cart_transfer_from_outlet($outlet_id, $cart_id, $item_product_id, $cost, $quantity)
	{
		$modified_by = modified_by();
		$modified_date = modified_date();

		$cart = $this->db->query("select id, quantity from item_transfer_cart where outlet_id='$outlet_id' and item_product_id='$item_product_id' and id='$cart_id' and deleted_date is null")->result();

		if (count($cart) > 0) {
			$outlet_product = $this->db->query("select many from item_product_outlet where outlet_id='$outlet_id' and item_product_id='$item_product_id' and deleted_date is null")->result();

			if (count($outlet_product) > 0) {
				if ($quantity > $outlet_product[0]->many) {
					return false;
				} else {

					return $this->db->query("
				update item_transfer_cart set cost='$cost', quantity='$quantity', modified_by='$modified_by', modified_date='$modified_date' where outlet_id='$outlet_id' and item_product_id ='$item_product_id' and status='transfer_from_outlet' and deleted_date is null");
				}
			}
		}
		return false;
	}

	function bulk_transfer_to_outlet_from_outlet($sender_outlet_id, $destination_outlet_id, $product_id, $cost, $many, $product_image, $note)
	{
		$created_by = created_by();
		$created_date = created_date();
		$modified_by = modified_by();
		$modified_date = modified_date();

		$this->db->trans_begin();

		$from_outlet_many = $this->db->query("select many from item_product_outlet where outlet_id='$sender_outlet_id' and item_product_id='$product_id' and deleted_date is null")->result()[0]->many;

		$from_outlet_many -= $many;

		if ($from_outlet_many < 0) {
			return false;
		} else {

			$cek_barang_outlet = $this->db->query("select many from item_product_outlet where outlet_id='$destination_outlet_id' and item_product_id='$product_id' and deleted_date is null")->result();

			if ($cek_barang_outlet) {

				$item_product_outlet_id = $this->db->query("select id from item_product_outlet where outlet_id='$sender_outlet_id' and item_product_id='$product_id' and deleted_date is null")->result()[0]->id;

				$item1 = $this->db->query("
                select item_product.name as product_name, item_category.name as category_name, item_type.name as type_name, item_product.modal, item_product_outlet.cost, item_product_outlet.qrcode, item_product_outlet.many as awl
                from item_product_outlet, item_product, item_category, item_type
                where item_product_outlet.item_product_id=item_product.id 
                and item_product.item_category_id=item_category.id 
                and item_category.item_type_id=item_type.id 
                and item_product_outlet.deleted_date is null
                and item_product.deleted_date is null
                and item_category.deleted_date is null
                and item_type.deleted_date is null
                and item_product_outlet.id='$item_product_outlet_id'
                ")->result()[0];

				$ak1 = $item1->awl - $many;

				$this->db->query("insert into transaction (user_id, outlet_id, item_outlet_id, type, category, product, modal, cost, many, awl, transfer, ak, note, status, unit, from_outlet, created_by, created_date)  
				values('{$_SESSION['id']}', '$sender_outlet_id','$item_product_outlet_id','$item1->type_name','$item1->category_name', '$item1->product_name', '$item1->modal','$item1->cost','$many','$item1->awl','$many','$ak1','$note','transfer_unit','Unit', '$destination_outlet_id', '$created_by','$created_date')");

				$item1_product_outlet_id = $this->db->query("select id from item_product_outlet where outlet_id='$destination_outlet_id' and item_product_id='$product_id' and deleted_date is null")->result()[0]->id;

				$item = $this->db->query("
                select item_product.name as product_name, item_category.name as category_name, item_type.name as type_name, item_product.modal, item_product_outlet.cost, item_product_outlet.qrcode, item_product_outlet.many as awl
                from item_product_outlet, item_product, item_category, item_type
                where item_product_outlet.item_product_id=item_product.id 
                and item_product.item_category_id=item_category.id 
                and item_category.item_type_id=item_type.id 
                and item_product_outlet.deleted_date is null
                and item_product.deleted_date is null
                and item_category.deleted_date is null
                and item_type.deleted_date is null
                and item_product_outlet.id='$item1_product_outlet_id'
                ")->result()[0];

				$ak = $item->awl + $many;

				$this->db->query("insert into transaction (user_id, outlet_id, item_outlet_id, type, category, product, modal,cost, many, awl, plus, ak, note, status, unit, from_outlet, created_by, created_date)  
				values('{$_SESSION['id']}', '$destination_outlet_id','$item_product_outlet_id','$item->type_name','$item->category_name', '$item->product_name', '$item->modal','$item->cost','$many','$item->awl','$many','$ak', '$note','transfer_unit','Unit', '$sender_outlet_id', '$created_by','$created_date')");

				$transaction_id = $this->db->insert_id();

				$outlet_many = $many + $cek_barang_outlet[0]->many;

				$this->db->query("update item_product_outlet set cost='$cost', many='$outlet_many', modified_by='$modified_by', modified_date='$modified_date' where outlet_id='$destination_outlet_id' and item_product_id='$product_id' and deleted_date is null");

				$this->db->query("update item_product_outlet set many='$from_outlet_many', modified_by='$modified_by', modified_date='$modified_date' where outlet_id='$sender_outlet_id' and item_product_id='$product_id' and deleted_date is null");

				$this->db->query("delete from item_transfer_cart where outlet_id='$sender_outlet_id' and status='transfer_from_outlet' and deleted_date is null");

				$this->db->trans_complete();

				return $transaction_id;

			} else {

				$this->db->query("insert into item_product_outlet (outlet_id, item_product_id, cost, many, qrcode, created_by, created_date) 
				values('$destination_outlet_id','$product_id','$cost','$many', '$product_image','$created_by','$created_date')");

				$item_product_outlet_id = $this->db->query("select id from item_product_outlet where outlet_id='$sender_outlet_id' and item_product_id='$product_id' and deleted_date is null")->result()[0]->id;

				$item1 = $this->db->query("
                select item_product.name as product_name, item_category.name as category_name, item_type.name as type_name, item_product.modal, item_product_outlet.cost, item_product_outlet.qrcode, item_product_outlet.many as awl
                from item_product_outlet, item_product, item_category, item_type
                where item_product_outlet.item_product_id=item_product.id 
                and item_product.item_category_id=item_category.id 
                and item_category.item_type_id=item_type.id
                and item_product_outlet.deleted_date is null
                and item_product.deleted_date is null
                and item_category.deleted_date is null
                and item_type.deleted_date is null
				and item_product_outlet.id='$item_product_outlet_id'
                ")->result()[0];

				$ak = $item1->awl - $many;

				$this->db->query("insert into transaction (user_id, outlet_id, item_outlet_id, type, category, product, modal, cost, many, awl, transfer, ak, note, status, unit, from_outlet, created_by, created_date)  
                values('{$_SESSION['id']}', '$sender_outlet_id','$item_product_outlet_id','$item1->type_name','$item1->category_name', '$item1->product_name', '$item1->modal','$item1->cost','$many','$item1->awl','$many','$ak','$note','transfer_unit','Unit', '$destination_outlet_id', '$created_by','$created_date')");

				$item1_product_outlet_id = $this->db->query("select id from item_product_outlet where outlet_id='$destination_outlet_id' and item_product_id='$product_id' and deleted_date is null")->result()[0]->id;

				$item = $this->db->query("
                select item_product.name as product_name, item_category.name as category_name, item_type.name as type_name, item_product.modal, item_product_outlet.cost, item_product_outlet.qrcode, item_product_outlet.many as awl
                from item_product_outlet, item_product, item_category, item_type
                where item_product_outlet.item_product_id=item_product.id 
                and item_product.item_category_id=item_category.id 
                and item_category.item_type_id=item_type.id 
                and item_product_outlet.deleted_date is null
                and item_product.deleted_date is null
                and item_category.deleted_date is null
                and item_type.deleted_date is null
				and item_product_outlet.id='$item1_product_outlet_id'
                ")->result()[0];

				$this->db->query("insert into transaction (user_id, outlet_id, item_outlet_id, type, category, product, modal, cost, many, awl, plus, ak, note, status, unit, from_outlet, created_by, created_date)  
                values('{$_SESSION['id']}', '$destination_outlet_id','$item1_product_outlet_id','$item->type_name','$item->category_name', '$item->product_name', '$item->modal','$item->cost','$many','0','$many','$many','$note','transfer_unit','Unit', '$sender_outlet_id','$created_by','$created_date')");

				$transaction_id = $this->db->insert_id();

				$this->db->query("update item_product_outlet set many='$from_outlet_many', modified_by='$modified_by', modified_date='$modified_date' where outlet_id='$sender_outlet_id' and item_product_id='$product_id' and deleted_date is null");

				$this->db->query("delete from item_transfer_cart where outlet_id='$sender_outlet_id' and status='transfer_from_outlet' and deleted_date is null");

				$this->db->trans_complete();

				return $transaction_id;
			}
		}
	}

	function delete_cart_transfer($cart_id)
	{
		return $this->db->query("delete from item_transfer_cart where id='$cart_id' and deleted_date is null");
	}
}
