<?php $this->load->view('_layouts/_template/header') ?>

<div class="container-fluid">

	<div class="row">
		<div class="col-12">
			<div class="card card-outline card-primary">
				<div class="card-header">
					<h3 class="card-title"><b>Kategori</b></h3>
					<div class="float-right">
						<button class="btn btn-primary btn-sm" id="btn_add_category"><i class="fa fa-plus"></i> Kategori
						</button>
					</div>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table id="table_category" class="table table-striped table-hover dt-responsive display nowrap"
							   style="width:100%">
							<thead>
							<tr>
								<th class="nomor" data-orderable="false">No</th>
								<th>Kategori</th>
								<th>Tipe item</th>
								<th data-orderable="false">Aksi</th>
							</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--Modal tambah kategori baru-->
<div class="modal fade" id="modal_add_category">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Tambah Kategori Baru</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="add_category_form">
					<div class="form-group">
						<div class="col-lg-4">Tipe</div>
						<div class="col-lg-12">
							<select class="form-control form-control-sm" id="add_category_type_id"
									name="add_category_type_id"></select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-4">Kategori</div>
						<div class="col-lg-12">
							<input type="text" class="form-control form-control-sm" id="add_category_name"
								   name="add_category_name">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"
						id="btn_new_category">Simpan
				</button>
			</div>
		</div>
	</div>
</div>

<!--Modal ubah kategori baru-->
<div class="modal fade" id="modal_edit_category">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Ubah Kategori Baru</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="edit_category_form">
					<div class="form-group">
						<div class="col-lg-4">Tipe item</div>
						<div class="col-lg-12">
							<select class="form-control form-control-sm" id="edit_category_type_id"
									name="edit_category_type_id"></select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-4">Kategori</div>
						<div class="col-lg-12">
							<input type="text" class="form-control form-control-sm" id="edit_category_name"
								   name="edit_category_name">
						</div>
					</div>
					<div class="form-group" hidden>
						<div class="col-lg-12">
							<input type="text" class="form-control form-control-sm" id="edit_category_id"
								   name="edit_category_id">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"
						id="save_edit_category">Simpan
				</button>
			</div>
		</div>
	</div>
</div>

<script>
	//init table kategori
	$(document).ready(function () {
		get_item_type()
		init_table_category()
	})

	//modal tambah kategori
	$("#btn_add_category").on('click', function () {
		$("#modal_add_category").modal({backdrop: 'static', keyboard: false})
	})

	//validasi form tambah kategori
	$("#add_category_form").validate({
		rules: {
			add_category_type_id: {
				required: true,
				maxlength: 2
			},
			add_category_name: {
				required: true,
				maxlength: 50
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//tambah kategori baru
	$("#btn_new_category").click(function () {

		if ($('#add_category_form').valid()) {
			$.ajax({
				url: "<?= base_url(); ?>kategori/add_category",
				type: "POST",
				data: {
					type_id: $('#add_category_type_id').val(),
					category_name: $('#add_category_name').val()
				},
				success: function (result) {

					if (result.success) {

						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						init_table_category();

					} else {

						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						init_table_category();

					}
				}
			})
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});

			init_table_category();
		}

	})

	//validasi form ubah kategori
	$("#edit_category_form").validate({
		rules: {
			edit_category_type_id: {
				required: true,
				maxlength: 2
			},
			edit_category_name: {
				required: true,
				maxlength: 50
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//modal ubah data kategori
	$("#table_category").on('click', 'tr #btn_edit_click', function () {

		tr = $(this).closest('tr')
		data = table_category.row(tr).data()

		$("#edit_category_type_id").val(data.item_type_id)
		$("#edit_category_name").val(data.item_category_name)
		$("#edit_category_id").val(data.item_category_id)

		$('#modal_edit_category').modal({backdrop: 'static', keyboard: false})
	})

	//ubah data kategori
	$("#save_edit_category").on('click', function () {

		if ($('#edit_category_form').valid()) {
			$.ajax({
				url: "<?= base_url(); ?>kategori/edit_category",
				type: "POST",
				data: {
					'type_id': $("#edit_category_type_id").val(),
					'category_id': $("#edit_category_id").val(),
					'name': $("#edit_category_name").val()
				},
				success: function (result) {

					if (result.success) {
						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});
						init_table_category()

					} else {
						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						init_table_category()
					}
				}
			})
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});

			init_table_category()
		}

	})

	//hapus data kategori
	$("#table_category").on('click', 'tr #btn_delete_click', function () {
		tr = $(this).closest('tr')
		data = table_category.row(tr).data()
		id = data.item_category_id

		Swal.fire({
			title: 'Peringatan!',
			text: 'Apakah Anda yakin akan menghapus kategori ' + data.item_category_name + ' ?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya, hapus data ini!'
		}).then((result) => {
			if (result.value) {

				$.ajax({
					url: "<?= base_url(); ?>kategori/delete_category",
					type: "POST",
					data: {id: id},
					success(result) {

						if (result.success) {
							Swal.fire({
								title: 'Berhasil',
								text: result.message,
								icon: result.type,
								showConfirmButton: false,
								timer: 1500
							});

							init_table_category()
						} else {
							Swal.fire({
								title: 'gagal',
								text: result.message,
								icon: result.type,
								showConfirmButton: false,
								timer: 1500
							});

							init_table_category()
						}

					}
				})
			}
		})
	})

	//init datatables table wewenang
	function init_table_category() {
		button = "&nbsp;<button class='btn btn-warning btn-sm' id='btn_edit_click'>\
            <i class='fas fa-edit'></i>&nbsp;Ubah</button>"
		button += "&nbsp;<button class='btn btn-danger btn-sm' id='btn_delete_click'>\
            <i class='fas fa-trash'></i>&nbsp;Hapus</button>"

		table_category = $("#table_category").DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			autoWidth: true,
			width: "0.8%",
			ajax: "<?= base_url(); ?>kategori/get_all_category",
			columns: [
				{
					data: "id",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: 'item_type_name'
				},
				{
					data: 'item_category_name'
				},
				{
					defaultContent: button
				},
			]
		})
	}

	//dropdown tipe item
	function get_item_type() {
		$.ajax({
			url: "<?= base_url(); ?>item/get_item",
			type: 'GET',
			success: function (result) {

				if (result.success) {

					dropdown_item = '<option value="">Pilih...</option>'

					result.data.forEach(function (data) {
						if (data.id != 4) {
							dropdown_item += "<option value='" + data.id + "'>" + data.name + "</option>";
						}
					})

					$('#add_category_type_id').html(dropdown_item)
					$('#edit_category_type_id').html(dropdown_item)
				}
			}
		})
	}
</script>


<?php $this->load->view('_layouts/_template/js') ?>
