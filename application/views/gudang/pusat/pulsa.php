<div class="table-responsive">
	<table id="table_pulsa" class="table table-striped table-hover dt-responsive display nowrap" style="width:100%">
		<thead>
		<tr>
			<th class="nomor" data-orderable="false">No</th>
			<th>Outlet</th>
			<th>Kategori</th>
			<th>Nama</th>
			<th>Modal</th>
			<th>Harga Jual</th>
			<th>Qrcode</th>
			<th data-orderable="false">Aksi</th>
		</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>

<script>
	$(document).ready(function () {
		init_table_pulsa()
	})

	//button modal ubah data pulsa
	$("#table_pulsa").on('click', 'tr #btn_edit_click', function () {

		tr = $(this).closest('tr')
		data = table_pulsa.row(tr).data()

		$("#input_edit_pulsa_outlet").val(data.outlet_name)
		$("#input_edit_pulsa_name").val(data.name)
		$("#input_edit_pulsa_category").val(data.category_name)
		$("#input_edit_pulsa_cost").val(data.cost.replace(',', ''))
		$("#input_edit_pulsa_modal").val(data.modal.replace(',', ''))
		$("#input_edit_pulsa_type_id").val('Pulsa')

		$('#modal_pusat_pulsa').modal({backdrop: 'static', keyboard: false})
	})

	//validasi form transfer barang
	$("#edit_pulsa_form").validate({
		rules: {
			input_edit_pulsa_category_id: {
				required: true
			},
			input_edit_pulsa_product_id: {
				required: true
			},
			input_edit_pulsa_name: {
				required: true
			},
			input_edit_pulsa_modal: {
				required: true,
				digits: true,
				maxlength: 10
			},
			input_edit_pulsa_cost: {
				required: true,
				digits: true,
				maxlength: 7
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//ubah data produk pulsa
	$("#btn_edit_pulsa_pusat_submit").click(function () {

		if ($('#edit_pulsa_form').valid()) {

			$.ajax({
				url: "<?= base_url(); ?>_gudang/pulsa/edit_pulsa",
				type: "POST",
				data: {
					product: $('#input_edit_pulsa_name').val(),
					category: $('#input_edit_pulsa_category').val(),
					modal: $('#input_edit_pulsa_modal').val(),
					cost: $('#input_edit_pulsa_cost').val()
				},
				success: function (result) {

					if (result.success) {
						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						init_table_pulsa()

					} else {
						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});
					}
				}
			})
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});
		}
	});

	//hapus data produk pulsa
	$("#table_pulsa").on('click', 'tr #btn_delete_click', function () {
		tr = $(this).closest('tr')
		data = table_pulsa.row(tr).data()
		id = data.id

		Swal.fire({
			title: 'Peringatan!',
			text: 'Apakah Anda yakin akan menghapus produk ' + data.name + ' ?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya, hapus data ini!'
		}).then((result) => {
			if (result.value) {
				$.ajax({
					url: "<?= base_url(); ?>_gudang/pulsa/delete_pulsa",
					type: "POST",
					data: {id: id},
					success(result) {

						if (result.success) {
							Swal.fire({
								title: 'Berhasil',
								text: result.message,
								icon: result.type,
								showConfirmButton: false,
								timer: 1500
							});

							init_table_pulsa()

						} else {
							Swal.fire({
								title: 'gagal',
								text: result.message,
								icon: result.type,
								showConfirmButton: false,
								timer: 1500
							});
						}
					}
				})
			}
		})
	})

	//modal preview qrcode
	$("#table_pulsa").on('click', 'tr #qrcode_image', function () {
		$('#qrcode_preview').attr('src', $(this).attr('src'));
		$('#modal_qrcode').modal({backdrop: 'static', keyboard: false});
	});

	//init datatables table perdana
	function init_table_pulsa() {
		button = "&nbsp;<button class='btn btn-warning btn-sm' id='btn_edit_click'>\
            <i class='fas fa-edit'></i>&nbsp;Ubah</button>"
		button += "&nbsp;<button class='btn btn-danger btn-sm' id='btn_delete_click'>\
            <i class='fas fa-trash'></i>&nbsp;Hapus</button>"

		table_pulsa = $("#table_pulsa").DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			autoWidth: true,
			ajax: "<?= base_url(); ?>_gudang/pulsa/get_all_pulsa",
			columns: [
				{
					data: "id",
					width: "0.8%",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: "outlet_name"
				},
				{
					data: "category_name"
				},
				{
					data: "name"
				},
				{
					data: "modal"
				},
				{
					data: "cost"
				},
				{
					data: "qrcode",
					targets: 6,
					render: function (url, type, full) {
						return '<img onerror="this.onerror=null; this.remove();" class="center image_' + full['id'] + '"id="qrcode_image" height="100px" width="100px" src="' + "<?= base_url(); ?>uploads/qrcode/" + full['qrcode'] + '"/>';
					}
				},
				{
					defaultContent: button
				},
			]
		})

	}
</script>
