<?php $this->load->view('_layouts/_template/header') ?>

<style>
	#scanner_qrcode {
		max-width: 100%;
		height: auto;
	}
</style>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-3">
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Scan QRCode</h3>
				</div>
				<div class="card-body p-1">
					<video id="scanner_qrcode"></video>
					<div id="label_scanner_qrcode"></div>
				</div>
				<div class="card-footer">
					<select class="form-control" id="options" name="options">
						<option value="1">Kamera Depan</option>
						<option value="2">Kamera Belakang</option>
					</select>
				</div>
			</div>

			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Info Transaksi</h3>
				</div>
				<div class="card-body">
					<input type="text" name="user_id" id="user_id" value="<?php echo $_SESSION['id']; ?>" hidden>
					<input type="text" name="outlet_id" id="outlet_id" value="<?php echo $outlet[0]->id ?>" hidden>
					<input type="text" name="type_id" id="type_id" value="fisik" hidden>

					<div class="form-group">
						<label class="control-label">Outlet</label>
						<input type="text" name="outlet_name" id="outlet_name" class="form-control input-sm"
							   value="<?php echo $outlet[0]->name; ?>" readonly>
					</div>

					<div class="form-group">
						<label class="control-label">Nomor Nota (Otomatis)</label>
						<input type="text" name="nomor_transaksi" id="nomor_transaksi" class="form-control input-sm"
							   value="<?php echo strtoupper('TRNSK' . uniqid()) . $_SESSION['id']; ?>" readonly>
					</div>
					<div class="form-group">
						<label class="control-label">Tanggal</label>
						<input type="text" name="tanggal_transaksi" id="tanggal_transaksi" class="form-control input-sm"
							   value="<?php echo date('d-m-Y'); ?>" disabled>
					</div>
					<div class="form-group">
						<label class="control-label">Kasir</label>
						<input type="text" name="user_name" id="user_name" class="form-control input-sm"
							   value="<?php echo $_SESSION['name']; ?>" disabled>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-9">
			<div class="card card-primary card-outline">
				<div class="card-header">
					<h3 class="card-title">Keranjang Belanja</b></h3>
					<div class="float-right">
						<div class="form-group">
							<button class="btn btn-success btn-sm" id="btn_search_product_outlet"><i
										class="fa fa-search"></i> Cari Produk Manual
							</button>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table id="table_transaksi_cart"
							   class="table table-striped table-hover dt-responsive display nowrap"
							   style="width:100%">
							<thead>
							<tr>
								<th class="nomor" data-orderable="false">No</th>
								<th>Kategori</th>
								<th>Produk</th>
								<th>Harga</th>
								<th>Diskon (%)</th>
								<th>Jumlah Beli</th>
								<th>Stok Tersedia</th>
								<th class="nomor" data-orderable="false">Aksi</th>
							</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="card card-primary card-outline">
				<div class="card-header">
					<h3 class="card-title">Transaksi Penjualan Fisik</h3>
					<div class="float-right">
						<select name="transaction_type" id="transaction_type" class="form-control">
							<option value="1">Produk Fisik</option>
							<option value="2">Produk Pulsa</option>
						</select>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-6">
							<div class="alert alert-info text-center">
								<h3><span id="total_diskon">Diskon : Rp. 0</span></h3>
								<input type="hidden" id="total_diskon_data">
							</div>
						</div>

						<div class="col-md-6">
							<div class="alert alert-info text-center">
								<h3><span id="total_bayar">Total : Rp. 0</span></h3>
								<input type="hidden" id="total_bayar_data">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Catatan</label>
								<textarea name="catatan" id="catatan" class="form-control" rows="5"
										  placeholder="catatan transaksi (jika ada)"></textarea>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Jumlah Bayar</label>
								<input type="text" name="uang_cash" id="uang_cash" class="form-control"
									   onkeypress="return check_int(event)">
							</div>
							<div class="form-group">
								<label class="control-label">Kembali</label>
								<input type="text" name="uang_kembalian" id="uang_kembalian" class="form-control"
									   disabled>
								<input type="hidden" id="uang_kembalian_data">
							</div>
							<div class="row">
								<div class="col-sm-6">
									<button type="button" class="btn btn-warning btn-block"
											name="btn_save_print_transaction_submit"
											id="btn_save_print_transaction_submit">
										<i class="fa fa-print"></i> Cetak & Simpan
									</button>
								</div>
								<div class="col-sm-6">
									<button type="button" class="btn btn-primary btn-block"
											name="btn_save_transaction_submit"
											id="btn_save_transaction_submit">
										<i class="fa fa-save"></i> Simpan Transaksi
									</button>
								</div>
							</div>
						</div>
					</div>
					<br/>
				</div>
			</div>
		</div>
	</div>
</div>

<!--Modal tambah barang keranjang-->
<div class="modal fade" id="modal_edit_many_cart" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content modal-sm">
			<div class="modal-header">
				<h5 class="modal-title">Tambah Jumlah Barang</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="add_modal_outlet_bluk_form">
					<input type="text" id="edit_many_cart_id" hidden>
					<div class="form-group">
						<div class="col-lg-4">Jumlah</div>
						<div class="col-lg-12">
							<input type="number" class="form-control form-control-sm"
								   id="edit_many_cart_many"
								   name="edit_many_cart_many" value='0'>
						</div>
					</div>
				</form>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" id="btn_edit_cart_many_submit"
						data-dismiss="modal">Simpan
				</button>
			</div>
		</div>
	</div>
</div>

<!--Modal cari produk manual-->
<div class="modal fade" id="modal_search_product_outlet" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Cari Barang outlet</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="table_cari_product" class="table table-striped table-hover dt-responsive display nowrap"
						   style="width:100%">
						<thead>
						<tr>
							<th class="nomor" data-orderable="false">No</th>
							<th>Kategori</th>
							<th>Nama</th>
							<th>Harga</th>
							<th>Diskon (%)</th>
							<th>D. Sampai</th>
							<th>Stok Tersedia</th>
							<th data-orderable="false">Aksi</th>
						</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Selesai</button>
			</div>
		</div>
	</div>
</div>

<script>
	jenis_transaksi = 1;

	//init transaksi
	$(document).ready(function () {
		var today = new Date();
		date = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2)

		$('#transaction_type').val(jenis_transaksi);
		$("#date_begin").val(date)
		$("#date_end").val(date)

		init_table_transaksi_cart()
		init_table_search_product()
		hitung_total_bayar()

		$('#btn_save_transaction_submit').prop('disabled', true);
		$('#btn_save_print_transaction_submit').prop('disabled', true);
	})

	//ganti jenis transaksi
	$('#transaction_type').change(function () {
		outlet_id = "<?= $this->uri->segment(4); ?>";

		if ($(this).val() == 1) {
			link = "<?= base_url(); ?>kasir/transaksi/index/" + outlet_id
			window.location.href = link;
		} else if ($(this).val() == 2) {
			link = "<?= base_url(); ?>kasir/transaksi/pulsa/" + outlet_id
			window.location.href = link;
		}
	})

	//modal cari barang manual
	$("#btn_search_product_outlet").click(function () {
		$("#modal_search_product_outlet").modal({backdrop: 'static', keyboard: false});
	})

	//simpan cari barang manual
	$("#table_cari_product").on('click', 'tr #btn_add_to_cart_click', function () {

		tr = $(this).closest('tr')
		data = table_cari_product.row(tr).data()

		$.ajax({
			url: "<?= base_url(); ?>kasir/transaksi/add_transaction_cart",
			type: "POST",
			data: {
				outlet_id: "<?= $this->uri->segment(4); ?>",
				item_outlet_id: data.id,
				cost: data.cost
			},
			success(result) {

				if (result.success) {
					Swal.fire({
						title: 'Berhasil',
						text: result.message,
						icon: result.type,
						showConfirmButton: false,
						timer: 600
					});

					init_table_transaksi_cart()
					init_table_search_product()
					hitung_total_bayar()

				} else {
					Swal.fire({
						title: 'gagal',
						text: result.message,
						icon: result.type,
						showConfirmButton: false,
						timer: 1000
					});
				}
			}
		})
	})

	//modal tambah produk keranjang
	$("#table_transaksi_cart").on('click', 'tr #btn_add_click', function () {

		tr = $(this).closest('tr')
		data = table_transaksi_cart.row(tr).data()

		$('#edit_many_cart_id').val(data.cart_id)

		$('#modal_edit_many_cart').modal({backdrop: 'static', keyboard: false})
	})

	//simpan tambah produk keranjang
	$('#btn_edit_cart_many_submit').click(function () {

		$.ajax({
			url: "<?= base_url(); ?>kasir/transaksi/edit_product_from_cart",
			type: "POST",
			data: {
				cart_id: $('#edit_many_cart_id').val(),
				many: $('#edit_many_cart_many').val()
			},
			success(result) {
				if (result.success) {
					Swal.fire({
						title: 'Berhasil',
						text: result.message,
						icon: result.type,
						showConfirmButton: false,
						timer: 600
					});

					init_table_transaksi_cart()
					init_table_search_product()
					hitung_total_bayar()

				} else {
					Swal.fire({
						title: 'gagal',
						text: result.message,
						icon: result.type,
						showConfirmButton: false,
						timer: 1000
					});
				}
			}
		})
	})

	//button hapus produk keranjang
	$("#table_transaksi_cart").on('click', 'tr #btn_delete_click', function () {

		tr = $(this).closest('tr')
		data = table_transaksi_cart.row(tr).data()
		id = data.cart_id

		Swal.fire({
			title: 'Peringatan!',
			text: 'Apakah Anda yakin akan menghapus produk ' + data.product_name + ' ?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya, hapus data ini!'
		}).then((result) => {
			if (result.value) {
				$.ajax({
					url: "<?= base_url(); ?>kasir/transaksi/delete_product_from_cart",
					type: "POST",
					data: {id: id},
					success(result) {

						if (result.success) {
							Swal.fire({
								title: 'Berhasil',
								text: result.message,
								icon: result.type,
								showConfirmButton: false,
								timer: 600
							}).then(function () {
								location.reload();
							});

						} else {
							Swal.fire({
								title: 'gagal',
								text: result.message,
								icon: result.type,
								showConfirmButton: false,
								timer: 1000
							});
						}
					}
				})
			}
		})
	})

	//simpan data transaksi
	$("#btn_save_transaction_submit").click(function () {

		var total_row = $('#table_transaksi_cart tbody tr td').length;

		if (total_row < 2) {
			Swal.fire({
				title: 'Transaksi gagal',
				text: 'Tidak ada produk di keranjang belanja',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1000
			});
		} else {
			var user_id = $("#user_id").val()
			var outlet_id = $("#outlet_id").val()
			var type_id = $("#type_id").val()
			var nomor_transaksi = $("#nomor_transaksi").val()
			var total_bayar = $("#total_bayar_data").val()
			var total_diskon = $("#total_diskon_data").val()
			var uang_cash = $("#uang_cash").val()
			var uang_kembalian = $("#uang_kembalian_data").val()
			var catatan = $("#catatan").val()

			if (uang_cash.length < 1) {
				Swal.fire({
					title: 'Peringatan!',
					text: 'Harap masukan jumlah bayar',
					icon: 'warning',
					showConfirmButton: false,
					timer: 1000
				});
			} else {
				Swal.fire({
					title: 'Peringatan!',
					text: 'Apakah Anda yakin akan memproses transaksi semua produk di keranjang ?',
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, proses transaksi ini!'
				}).then((result) => {
					if (result.value) {
						$.ajax({
							url: "<?= base_url(); ?>kasir/transaksi/save_transaction",
							type: "POST",
							data: {
								user_id: user_id,
								outlet_id: outlet_id,
								type_id: type_id,
								nomor_transaksi: nomor_transaksi,
								total_bayar: total_bayar,
								total_diskon: total_diskon,
								uang_cash: uang_cash,
								uang_kembalian: uang_kembalian,
								catatan: catatan
							},
							success(result) {

								if (result.success) {
									Swal.fire({
										title: 'Berhasil',
										text: result.message,
										icon: result.type,
										showConfirmButton: false,
										timer: 600
									}).then(function () {
										location.reload();
									});

								} else {
									Swal.fire({
										title: 'gagal',
										text: result.message,
										icon: result.type,
										showConfirmButton: false,
										timer: 1000
									});
								}
							}
						})
					}
				})
			}
		}
	})

	//simpan dan cetak transaksi
	$("#btn_save_print_transaction_submit").click(function () {

		var total_row = $('#table_transaksi_cart tbody tr td').length;

		if (total_row < 2) {
			Swal.fire({
				title: 'Transaksi gagal',
				text: 'Tidak ada produk di keranjang belanja',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1000
			});
		} else {
			var user_id = $("#user_id").val()
			var outlet_id = $("#outlet_id").val()
			var type_id = $("#type_id").val()
			var nomor_transaksi = $("#nomor_transaksi").val()
			var total_bayar = $("#total_bayar_data").val()
			var total_diskon = $("#total_diskon_data").val()
			var uang_cash = $("#uang_cash").val()
			var uang_kembalian = $("#uang_kembalian_data").val()
			var catatan = $("#catatan").val()

			if (uang_cash.length < 1) {
				Swal.fire({
					title: 'Peringatan!',
					text: 'Harap masukan jumlah bayar',
					icon: 'warning',
					showConfirmButton: false,
					timer: 1000
				});
			} else {
				Swal.fire({
					title: 'Peringatan!',
					text: 'Apakah Anda yakin akan memproses transaksi semua produk di keranjang ?',
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, proses transaksi ini!'
				}).then((result) => {
					if (result.value) {
						$.ajax({
							url: "<?= base_url(); ?>kasir/transaksi/save_transaction",
							type: "POST",
							data: {
								user_id: user_id,
								outlet_id: outlet_id,
								type_id: type_id,
								nomor_transaksi: nomor_transaksi,
								total_bayar: total_bayar,
								total_diskon: total_diskon,
								uang_cash: uang_cash,
								uang_kembalian: uang_kembalian,
								catatan: catatan
							},
							success(result) {

								if (result.success) {

									link = "<?= base_url(); ?>transaksi/print_transaction_detail/" + result.transaction_code
									window.open(link, '_blank');

									Swal.fire({
										title: 'Berhasil',
										text: result.message,
										icon: result.type,
										showConfirmButton: false,
										timer: 600
									}).then(function () {
										location.reload();
									});

								} else {
									Swal.fire({
										title: 'gagal',
										text: result.message,
										icon: result.type,
										showConfirmButton: false,
										timer: 1000
									});
								}
							}
						})
					}
				})
			}
		}
	})

	//hitung uang kembalian
	$('#uang_cash').keyup(function () {
		hitung_total_kembalian();
	});

	//init datatables table transaksi cart
	function init_table_transaksi_cart() {
		button = "&nbsp;<button class='btn btn-success btn-sm' id='btn_add_click'>\
            <i class='fas fa-plus'></i>&nbsp;Tambah</button>"
		button += "&nbsp;<button class='btn btn-danger btn-sm' id='btn_delete_click'>\
            <i class='fas fa-trash'></i>&nbsp;Hapus</button>";

		table_transaksi_cart = $("#table_transaksi_cart").DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			autoWidth: true,
			ajax: "<?= base_url(); ?>kasir/transaksi/get_all_transaction_cart/" + "<?= $this->uri->segment(4); ?>",
			columns: [
				{
					data: "id",
					width: "0.8%",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: 'category_name'
				},
				{
					data: 'product_name'
				},
				{
					data: 'cost'
				},
				{
					data: 'discount'
				},
				{
					data: 'many'
				},
				{
					data: 'stock'
				},
				{
					defaultContent: button
				}
			]
		})
	}

	//init datatables table search produk
	function init_table_search_product() {
		button = "<button class='btn btn-info btn-sm' id='btn_add_to_cart_click'>\
            <i class='fas fa-plus'></i>&nbsp;Tambah</button>"

		table_cari_product = $("#table_cari_product").DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			autoWidth: true,
			width: "0.8%",
			ajax: "<?= base_url(); ?>kasir/_gudang/outlet/get_all_product_by_outlet_id/" + 0 + "/<?= $this->uri->segment(4); ?>",
			columns: [
				{
					data: "id",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: "category_name"
				},
				{
					data: "name"
				},
				{
					data: "cost"
				},
				{
					data: "discount"
				},
				{
					data: "discount_until"
				},
				{
					data: "many"
				},
				{
					defaultContent: button
				}
			]
		})
	}

	//hitung uang total bayar
	function hitung_total_bayar() {

		$.ajax({
			url: "<?= base_url(); ?>kasir/transaksi/get_total_payment/" + $("#user_id").val() + "/" + $("#outlet_id").val(),
			success: function (result) {
				if (result.success) {

					var total = 0;
					var discount = 0;
					result.data.forEach(function (data) {
						total += data['total'] << 0;
						discount += data['discount'] << 0;
					});

					var total_semua = 0;
					total_semua = parseInt(total_semua) + (parseInt(total) - parseInt(discount));

					$('#total_diskon').html('Diskon : ' + to_rupiah(discount));
					$('#total_diskon_data').val(discount);

					$('#total_bayar').html('Total : ' + to_rupiah(total_semua));
					$('#total_bayar_data').val(total_semua);

					$('#uang_cash').val('');
					$('#uang_kembalian').val('Rp. 0');
					$('#uang_kembalian_data').val(0);

					$('#btn_save_transaction_submit').prop('disabled', true);
					$('#btn_save_print_transaction_submit').prop('disabled', true);
				}
			}
		})
	}

	//hitung uang total kembalian
	function hitung_total_kembalian() {
		var cash = $('#uang_cash').val();
		var total_bayar = $('#total_bayar_data').val();

		if (parseInt(cash) >= parseInt(total_bayar)) {
			var selisih = parseInt(cash) - parseInt(total_bayar);
			$('#uang_kembalian').val(to_rupiah(selisih));
			$('#uang_kembalian_data').val(selisih);
			$('#btn_save_transaction_submit').prop('disabled', false);
			$('#btn_save_print_transaction_submit').prop('disabled', false);
		} else if (parseInt(cash) < parseInt(total_bayar)) {
			var selisih = parseInt(cash) - parseInt(total_bayar);
			$('#uang_kembalian').val(to_rupiah(selisih));
			$('#uang_kembalian_data').val(selisih);
			$('#btn_save_transaction_submit').prop('disabled', true);
			$('#btn_save_print_transaction_submit').prop('disabled', true);
		} else {
			$('#uang_kembalian').val('');
			$('#uang_kembalian_data').val(0);
		}
	}

	//konversi angka ke format rupiah
	function to_rupiah(angka) {
		var rev = parseInt(angka, 10).toString().split('').reverse().join('');

		var rev2 = '';
		for (var i = 0; i < rev.length; i++) {
			rev2 += rev[i];
			if ((i + 1) % 3 === 0 && i !== (rev.length - 1)) {
				rev2 += ',';
			}
		}

		$('#btn_save_transaction_submit').prop('disabled', false);
		$('#btn_save_print_transaction_submit').prop('disabled', false);

		return 'Rp. ' + rev2.split('').reverse().join('');
	}

	//cek inputan angka atau bukan
	function check_int(event) {
		var charCode = (event.which) ? event.which : event.keyCode;
		return (charCode >= 48 && charCode <= 57 || charCode === 8);
	}

	//init camera
	var scanner = new Instascan.Scanner({
		video: document.getElementById('scanner_qrcode'),
		scanPeriod: 5,
		mirror: false
	});

	//init scanner
	scanner.addListener('scan', function (content) {

		outlet_id = "<?= $this->uri->segment(4); ?>";

		if (content.length > 0) {
			$.ajax({
				url: "<?= base_url(); ?>kasir/transaksi/get_product_by_qrcode/" + outlet_id + "/" + content,
				success: function (result) {
					if (result.success) {
						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 600
						});

						init_table_transaksi_cart()
						init_table_search_product()
						hitung_total_bayar()

					} else {
						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1000
						});
					}
				}
			})
		} else {
			Swal.fire({
				title: 'gagal',
				text: 'Produk tidak ditemukan',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1000
			});
		}
	});

	//init reader
	Instascan.Camera.getCameras().then(function (cameras) {
		if (cameras.length > 0) {
			scanner.start(cameras[0]);
			$('[name="options"]').change(function () {
				if ($('[name="options"]').val() == 1) {
					if (cameras[0] != "") {
						scanner.start(cameras[0]);
					} else {
						alert('No Front camera found!');
					}
				} else if ($(this).val() == 2) {
					if (cameras[1] != "") {
						scanner.start(cameras[1]);
					} else {
						alert('No Back camera found!');
					}
				}
			});
		} else {
			console.error('No cameras found.');
			alert('No cameras found.');
		}
	}).catch(function (e) {
		console.error(e);
		alert(e);
	});

</script>

<?php $this->load->view('_layouts/_template/js') ?>
