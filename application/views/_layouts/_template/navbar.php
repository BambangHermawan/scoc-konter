<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-dark navbar-purple">
	<!-- Left navbar links -->
	<ul class="navbar-nav">
		<li class="nav-item">
			<a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
		</li>
	</ul>

	<!-- Right navbar links -->
	<ul class="navbar-nav ml-auto">
		<li class="nav-item dropdown">
			<a class="nav-link" data-toggle="dropdown" href="#">
				<i class="fas fa-th-large"></i>
			</a>
			<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
				<a href="#" class="dropdown-item text-left">
					<i class="fas fa-user"></i> Profile
				</a>
				<div class="dropdown-divider"></div>
				<a href="<?= base_url(); ?>auth/login/logout" class="dropdown-item text-left">
					<i class="fas fa-reply"></i> Logout
				</a>
			</div>
		</li>
	</ul>
</nav>
<!-- /.navbar -->
