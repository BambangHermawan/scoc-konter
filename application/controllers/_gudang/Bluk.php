<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bluk extends MY_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('M_auth', 'M_auth', true);
		$this->load->model('M_wewenang', 'M_wewenang', true);
		$this->load->model('M_outlet', 'M_outlet', true);
		$this->load->model('M_log', 'M_log', true);
		$this->load->model('_gudang/M_bluk', 'M_bluk', true);

		if (!isset($_SESSION)) {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'ADMIN') {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'OWNER') {
			redirect('auth/login');
		} elseif ($_SESSION['status_user'] == 'KASIR') {
			redirect('auth/login');
		}
	}

	function get_all_bluk($outlet_id)
	{
		$result = $this->M_bluk->get_datatables($outlet_id);

		$response = [
			'success' => true,
			'draw' => $_REQUEST['draw'],
			'recordsTotal' => $this->M_bluk->count_all($outlet_id),
			'recordsFiltered' => $this->M_bluk->count_filtered($outlet_id),
			'data' => $result
		];

		return to_json($response);
	}

	function set_cost_bluk()
	{
		$product_id = $this->input->post('product_id');
		$cost = $this->input->post('cost');

		$result = $this->M_bluk->set_cost_bluk($product_id, $cost);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Ubah harga produk berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Ubah harga produk gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function set_discount_bluk()
	{
		$product_id = $this->input->post('product_id');
		$discount = $this->input->post('discount');

		$result = $this->M_bluk->set_discount_bluk($product_id, $discount);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Ubah diskon produk berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Ubah diskon produk gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function delete_product_bluk()
	{
		$product_id = $this->input->post('product_id');

		$result = $this->M_bluk->delete_product_bluk($product_id);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Hapus produk berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Hapus produk gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function add_modal_bluk()
	{
		$outlet_id = $this->input->post('outlet_id');
		$product_id = $this->input->post('product_id');
		$modal = $this->input->post('modal');

		$result = $this->M_bluk->add_modal_bluk($product_id, $modal);

		if ($result) {

			$product = $this->M_log->get_item_outlet_category($product_id);
			$outlet = $this->M_log->get_outlet($outlet_id);

			$activity = "Outlet $outlet->name menambahkan saldo \"$product->name sebesar $modal\"";
			$this->M_log->insert_log($_SESSION['id'], $outlet_id, 'transaction', $result, $activity, '-', 'tambah_saldo_pulsa_outlet');

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Tambah modal berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Tambah modal gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function edit_modal_bluk()
	{
		$outlet_id = $this->input->post('outlet_id');
		$product_id = $this->input->post('product_id');
		$modal = $this->input->post('modal');

		$result = $this->M_bluk->edit_modal_bluk($product_id, $modal);

		if ($result) {

			$product = $this->M_log->get_item_outlet_category($product_id);
			$outlet = $this->M_log->get_outlet($outlet_id);

			$activity = "Outlet $outlet->name mengubah saldo \"$product->name sebesar $modal\"";
			$this->M_log->insert_log($_SESSION['id'], $outlet_id, 'transaction', $result, $activity, '-', 'ubah_saldo_pulsa_outlet');

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Ubah modal berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Ubah modal gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function add_product_bluk()
	{
		$category_id = $this->input->post('category_id');
		$name = $this->input->post('name');
		$modal = $this->input->post('modal');
		$cost = $this->input->post('cost');
		$unit = $this->input->post('unit');

		$this->load->library('ciqrcode');

		$config['cacheable'] = true;
		$config['cachedir'] = 'uploads/';
		$config['errorlog'] = 'uploads/';
		$config['imagedir'] = 'uploads/qrcode/';
		$config['quality'] = true;
		$config['size'] = '1024';
		$config['black'] = array(224, 255, 255);
		$config['white'] = array(70, 130, 180);
		$this->ciqrcode->initialize($config);

		$product_image = md5(time() . '_' . uniqid()) . '.png';

		$result = $this->M_bluk->add_product_bluk($category_id, $name, $modal, $cost, $unit, $product_image);

		if ($result) {

			$params['data'] = $product_image;
			$params['level'] = 'H';
			$params['size'] = 10;
			$params['savename'] = FCPATH . $config['imagedir'] . $product_image;
			$this->ciqrcode->generate($params);

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Tambah produk ' . $name . ' berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Tambah produk ' . $name . ' gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}
}
