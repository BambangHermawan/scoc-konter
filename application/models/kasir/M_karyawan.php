<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_karyawan extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_all_karyawan($outlet_id)
	{
		if ($outlet_id != 0) {
			return $this->db->query("select outlet_employee.id as outlet_employee_id, user.id as user_id, user.name as name, user.username as username, outlet.id as outlet_id, outlet.name as outlet_name from outlet_employee, outlet, user where outlet_employee.user_id = user.id and outlet_employee.outlet_id = outlet.id and outlet_employee.outlet_id = '$outlet_id' and user.user_roles_id = 3 and outlet_employee.deleted_date is null and user.deleted_date is null order by user.id asc")->result();
		}

		return $this->db->query("select outlet_employee.id as outlet_employee_id, user.id as user_id, user.name as name, user.username as username, outlet.id as outlet_id, outlet.name as outlet_name from outlet_employee, outlet, user where outlet_employee.user_id = user.id and outlet_employee.outlet_id = outlet.id and user.user_roles_id = 3 and outlet_employee.deleted_date is null and user.deleted_date is null order by user.id asc")->result();
	}
}
