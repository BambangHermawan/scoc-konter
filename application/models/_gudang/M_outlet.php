<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_outlet extends CI_Model
{
	var $column_order = array('item_type.name', 'item_product_outlet.id', 'item_product_outlet.item_product_id', 'item_product_outlet.qrcode', 'item_product.name', 'item_product.modal', 'item_product_outlet.cost', 'item_product_outlet.discount', 'item_product_outlet.discount_until', 'item_product_outlet.many', 'item_category.name');
	var $column_search = array('item_type.name', 'item_product_outlet.id', 'item_product_outlet.item_product_id', 'item_product_outlet.qrcode', 'item_product.name', 'item_product.modal', 'item_product_outlet.cost', 'item_product_outlet.discount', 'item_product_outlet.discount_until', 'item_product_outlet.many', 'item_category.name');
	var $order = array('item_product.name' => 'desc');

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query($type, $outlet_id)
	{
		if ($type != 0) {
			$this->db->select('item_type.name as type_name, item_product_outlet.id, item_product_outlet.item_product_id, item_product_outlet.qrcode, item_product.name, format(item_product.modal,0) as modal, format(item_product_outlet.cost,0) as cost, format(item_product_outlet.discount,0) as discount, DATE_FORMAT(item_product_outlet.discount_until, "%d-%m-%Y") as discount_until, item_product_outlet.many, item_category.name as category_name')
				->from('item_product_outlet, item_product, item_category, item_type')
				->where('item_category.item_type_id = item_type.id')
				->where('item_product_outlet.item_product_id = item_product.id')
				->where('item_product.item_category_id = item_category.id')
				->where('item_product_outlet.deleted_date is null')
				->where('item_product.deleted_date is null')
				->where('item_category.deleted_date is null')
				->where('item_type.deleted_date is null')
				->where('item_product_outlet.outlet_id', $outlet_id)
				->where('item_type.id', $type);

		} else {
			$this->db->select('item_type.name as type_name, item_product_outlet.id, item_product_outlet.item_product_id, item_product_outlet.qrcode, item_product.name, format(item_product.modal,0) as modal, format(item_product_outlet.cost,0) as cost, format(item_product_outlet.discount,0) as discount, DATE_FORMAT(item_product_outlet.discount_until, "%d-%m-%Y") as discount_until, item_product_outlet.many, item_category.name as category_name')
				->from('item_product_outlet, item_product, item_category, item_type')
				->where('item_category.item_type_id = item_type.id')
				->where('item_product_outlet.item_product_id = item_product.id')
				->where('item_product.item_category_id = item_category.id')
				->where('item_product_outlet.deleted_date is null')
				->where('item_product.deleted_date is null')
				->where('item_category.deleted_date is null')
				->where('item_type.deleted_date is null')
				->where('item_product_outlet.outlet_id', $outlet_id);
		}

		$i = 0;
		foreach ($this->column_search as $item) {
			if ($_REQUEST['search']['value']) {

				if ($i === 0) {
					$this->db->group_start();
					$this->db->like($item, $_REQUEST['search']['value']);
				} else {
					$this->db->or_like($item, $_REQUEST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if (isset($_REQUEST['order'])) {
			$this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($type, $outlet_id)
	{
		$this->_get_datatables_query($type, $outlet_id);

		if ($_REQUEST['length'] != -1)
			$this->db->limit($_REQUEST['length'], $_REQUEST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($type, $outlet_id)
	{
		$this->_get_datatables_query($type, $outlet_id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_all($type, $outlet_id)
	{
		if ($type != 0) {
			$this->db->select('item_type.name as type_name, item_product_outlet.id, item_product_outlet.item_product_id, item_product_outlet.qrcode, item_product.name, format(item_product.modal,0) as modal, format(item_product_outlet.cost,0) as cost, format(item_product_outlet.discount,0) as discount, DATE_FORMAT(item_product_outlet.discount_until, "%d-%m-%Y - %h:%i") as discount_until, item_product_outlet.many, item_category.name as category_name')
				->from('item_product_outlet, item_product, item_category, item_type')
				->where('item_category.item_type_id = item_type.id')
				->where('item_product_outlet.item_product_id = item_product.id')
				->where('item_product.item_category_id = item_category.id')
				->where('item_product_outlet.deleted_date is null')
				->where('item_product.deleted_date is null')
				->where('item_category.deleted_date is null')
				->where('item_type.deleted_date is null')
				->where('item_product_outlet.outlet_id', $outlet_id)
				->where('item_type.id', $type);

		} else {
			$this->db->select('item_type.name as type_name, item_product_outlet.id, item_product_outlet.item_product_id, item_product_outlet.qrcode, item_product.name, format(item_product.modal,0) as modal, format(item_product_outlet.cost,0) as cost, format(item_product_outlet.discount,0) as discount, DATE_FORMAT(item_product_outlet.discount_until, "%d-%m-%Y - %h:%i") as discount_until, item_product_outlet.many, item_category.name as category_name')
				->from('item_product_outlet, item_product, item_category, item_type')
				->where('item_category.item_type_id = item_type.id')
				->where('item_product_outlet.item_product_id = item_product.id')
				->where('item_product.item_category_id = item_category.id')
				->where('item_product_outlet.deleted_date is null')
				->where('item_product.deleted_date is null')
				->where('item_category.deleted_date is null')
				->where('item_type.deleted_date is null')
				->where('item_product_outlet.outlet_id', $outlet_id);
		}

		return $this->db->count_all_results();
	}

	function transfer_from_outlet($product_id, $sender_outlet_id, $destination_outlet_id, $many, $note, $product_image)
	{
		$created_by = created_by();
		$created_date = created_date();
		$modified_by = modified_by();
		$modified_date = modified_date();

		$this->db->trans_begin();

		$from_outlet_many = $this->db->query("select many from item_product_outlet where outlet_id='$sender_outlet_id' and item_product_id='$product_id' and deleted_date is null")->result()[0]->many;

		$from_outlet_many -= $many;

		if ($from_outlet_many < 0) {
			return false;
		} else {

			$cek_barang_outlet = $this->db->query("select many from item_product_outlet where outlet_id='$destination_outlet_id' and item_product_id='$product_id' and deleted_date is null")->result();

			if ($cek_barang_outlet) {

				$item_product_outlet_id = $this->db->query("select id from item_product_outlet where outlet_id='$sender_outlet_id' and item_product_id='$product_id' and deleted_date is null")->result()[0]->id;

				$item1 = $this->db->query("
                select item_product.name as product_name, item_category.name as category_name, item_type.name as type_name, item_product.modal, item_product_outlet.cost, item_product_outlet.qrcode, item_product_outlet.many as awl
                from item_product_outlet, item_product, item_category, item_type
                where item_product_outlet.item_product_id=item_product.id 
                and item_product.item_category_id=item_category.id 
                and item_category.item_type_id=item_type.id 
                and item_product_outlet.deleted_date is null
                and item_product.deleted_date is null
                and item_category.deleted_date is null
                and item_type.deleted_date is null
                and item_product_outlet.id='$item_product_outlet_id'
                ")->result()[0];

				$ak1 = $item1->awl - $many;

				$this->db->query("insert into transaction (user_id, outlet_id, item_outlet_id, type, category, product, modal, cost, many, awl, transfer, ak, note, status, unit, from_outlet, created_by, created_date)  
				values('{$_SESSION['id']}', '$sender_outlet_id','$item_product_outlet_id','$item1->type_name','$item1->category_name', '$item1->product_name', '$item1->modal','$item1->cost','$many','$item1->awl','$many','$ak1','$note','transfer_unit','Unit', '$destination_outlet_id', '$created_by','$created_date')");

				$item1_product_outlet_id = $this->db->query("select id from item_product_outlet where outlet_id='$destination_outlet_id' and item_product_id='$product_id' and deleted_date is null")->result()[0]->id;

				$item = $this->db->query("
                select item_product.name as product_name, item_category.name as category_name, item_type.name as type_name, item_product.modal, item_product_outlet.cost, item_product_outlet.qrcode, item_product_outlet.many as awl
                from item_product_outlet, item_product, item_category, item_type
                where item_product_outlet.item_product_id=item_product.id 
                and item_product.item_category_id=item_category.id 
                and item_category.item_type_id=item_type.id 
                and item_product_outlet.deleted_date is null
                and item_product.deleted_date is null
                and item_category.deleted_date is null
                and item_type.deleted_date is null
                and item_product_outlet.id='$item1_product_outlet_id'
                ")->result()[0];

				$ak = $item->awl + $many;

				$this->db->query("insert into transaction (user_id, outlet_id, item_outlet_id, type, category, product, modal,cost, many, awl, plus, ak, note, status, unit, from_outlet, created_by, created_date)  
				values('{$_SESSION['id']}', '$destination_outlet_id','$item_product_outlet_id','$item->type_name','$item->category_name', '$item->product_name', '$item->modal','$item->cost','$many','$item->awl','$many','$ak', '$note','transfer_unit','Unit', '$sender_outlet_id', '$created_by','$created_date')");

				$transaction_id = $this->db->insert_id();

				$outlet_many = $many + $cek_barang_outlet[0]->many;

				$this->db->query("update item_product_outlet set many='$outlet_many', modified_by='$modified_by', modified_date='$modified_date' where outlet_id='$destination_outlet_id' and item_product_id='$product_id' and deleted_date is null");

				$this->db->query("update item_product_outlet set many='$from_outlet_many', modified_by='$modified_by', modified_date='$modified_date' where outlet_id='$sender_outlet_id' and item_product_id='$product_id' and deleted_date is null");

				$this->db->trans_complete();

				return $transaction_id;

			} else {

				$this->db->query("insert into item_product_outlet (outlet_id, item_product_id, many, qrcode, created_by, created_date) values('$destination_outlet_id','$product_id','$many', '$product_image','$created_by','$created_date')");

				$item_product_outlet_id = $this->db->query("select id from item_product_outlet where outlet_id='$sender_outlet_id' and item_product_id='$product_id' and deleted_date is null")->result()[0]->id;

				$item1 = $this->db->query("
                select item_product.name as product_name, item_category.name as category_name, item_type.name as type_name, item_product.modal, item_product_outlet.cost, item_product_outlet.qrcode, item_product_outlet.many as awl
                from item_product_outlet, item_product, item_category, item_type
                where item_product_outlet.item_product_id=item_product.id 
                and item_product.item_category_id=item_category.id 
                and item_category.item_type_id=item_type.id
                and item_product_outlet.deleted_date is null
                and item_product.deleted_date is null
                and item_category.deleted_date is null
                and item_type.deleted_date is null
				and item_product_outlet.id='$item_product_outlet_id'
                ")->result()[0];

				$ak = $item1->awl - $many;

				$this->db->query("insert into transaction (user_id, outlet_id, item_outlet_id, type, category, product, modal, cost, many, awl, transfer, ak, note, status, unit, from_outlet, created_by, created_date)  
                values('{$_SESSION['id']}', '$sender_outlet_id','$item_product_outlet_id','$item1->type_name','$item1->category_name', '$item1->product_name', '$item1->modal','$item1->cost','$many','$item1->awl','$many','$ak','$note','transfer_unit','Unit', '$destination_outlet_id', '$created_by','$created_date')");

				$item1_product_outlet_id = $this->db->query("select id from item_product_outlet where outlet_id='$destination_outlet_id' and item_product_id='$product_id' and deleted_date is null")->result()[0]->id;

				$item = $this->db->query("
                select item_product.name as product_name, item_category.name as category_name, item_type.name as type_name, item_product.modal, item_product_outlet.cost, item_product_outlet.qrcode, item_product_outlet.many as awl
                from item_product_outlet, item_product, item_category, item_type
                where item_product_outlet.item_product_id=item_product.id 
                and item_product.item_category_id=item_category.id 
                and item_category.item_type_id=item_type.id 
                and item_product_outlet.deleted_date is null
                and item_product.deleted_date is null
                and item_category.deleted_date is null
                and item_type.deleted_date is null
				and item_product_outlet.id='$item1_product_outlet_id'
                ")->result()[0];

				$this->db->query("insert into transaction (user_id, outlet_id, item_outlet_id, type, category, product, modal, cost, many, awl, plus, ak, note, status, unit, from_outlet, created_by, created_date)  
                values('{$_SESSION['id']}', '$destination_outlet_id','$item1_product_outlet_id','$item->type_name','$item->category_name', '$item->product_name', '$item->modal','$item->cost','$many','0','$many','$many','$note','transfer_unit','Unit', '$sender_outlet_id','$created_by','$created_date')");

				$transaction_id = $this->db->insert_id();

				$this->db->query("update item_product_outlet set many='$from_outlet_many', modified_by='$modified_by', modified_date='$modified_date' where outlet_id='$sender_outlet_id' and item_product_id='$product_id' and deleted_date is null");

				$this->db->trans_complete();

				return $transaction_id;
			}
		}
	}

	function set_product_cost($outlet_id, $product_id, $cost)
	{
		$modified_by = modified_by();
		$modified_date = modified_date();

		$this->db->trans_begin();

		$this->db->query("update item_product_outlet set cost='$cost', modified_by='$modified_by', modified_date='$modified_date' where id='$product_id' and outlet_id = '$outlet_id' and deleted_date is null");

		$cek_transaction_cart = $this->db->query("select id, cost from transaction_cart where item_outlet_id ='$product_id' and outlet_id='$outlet_id'")->result();

		if (count($cek_transaction_cart) > 0) {
			$this->db->query("update transaction_cart set cost='$cost', modified_by='$modified_by', modified_date='$modified_date' where id='{$cek_transaction_cart[0]->id}' and outlet_id='$outlet_id'");
		}

		$CI =& get_instance();
		$CI->load->model('M_log_produk');
		$detail = $CI->M_log_produk->get_detail_product_outlet($product_id);
		$outlet = $CI->M_log_produk->get_outlet($outlet_id);

		$activity = "Outlet $outlet->name mengubah produk \"$detail->type_name $detail->category_name $detail->product_name harga menjadi $cost\"";
		$CI->M_log_produk->insert_log_product($_SESSION['id'], $outlet_id, 'item_product_outlet', $product_id, NULL, $cost, NULL, NULL, NULL, NULL, $activity, '-', 'edit_cost_product_outlet');

		$this->db->trans_complete();

		return true;
	}

	function set_product_discount($outlet_id, $product_id, $discount, $discount_until)
	{
		$modified_by = modified_by();
		$modified_date = modified_date();

		$this->db->trans_begin();

		$this->db->query("update item_product_outlet set discount='$discount', discount_until='$discount_until', modified_by='$modified_by', modified_date='$modified_date' where id='$product_id' and outlet_id ='$outlet_id' and deleted_date is null");

		$cek_transaction_cart = $this->db->query("select id, discount from transaction_cart where item_outlet_id ='$product_id' and outlet_id='$outlet_id'")->result();

		if (count($cek_transaction_cart) > 0) {
			if (strtotime(date('Y-m-d')) > strtotime($discount_until)) {
				$this->db->query("update transaction_cart set discount='0', modified_by='$modified_by', modified_date='$modified_date' where id='{$cek_transaction_cart[0]->id}' and outlet_id='$outlet_id'");
			} else {
				$this->db->query("update transaction_cart set discount='$discount', modified_by='$modified_by', modified_date='$modified_date' where id='{$cek_transaction_cart[0]->id}' and outlet_id='$outlet_id'");
			}
		}

		$CI =& get_instance();
		$CI->load->model('M_log_produk');
		$detail = $CI->M_log_produk->get_detail_product_outlet($product_id);
		$outlet = $CI->M_log_produk->get_outlet($outlet_id);

		$activity = "Outlet $outlet->name mengatur diskon \"$detail->type_name $detail->category_name $detail->product_name sebanyak $discount %\"";
		$CI->M_log_produk->insert_log_product($_SESSION['id'], $outlet_id, 'item_product_outlet', $product_id, NULL, NULL, NULL, NULL, NULL, $discount, $activity, '-', 'edit_discount_product_outlet');

		$this->db->trans_complete();

		return true;
	}

	function delete_product($outlet_id, $product_id)
	{
		$deleted_by = deleted_by();
		$deleted_date = deleted_date();

		$this->db->trans_begin();

		$unlink = $this->db->query("select qrcode, many from item_product_outlet where id='$product_id' and deleted_date is null")->result();

		if (count($unlink) > 0) {
			unlink("uploads/qrcode/" . $unlink[0]->qrcode);

			$CI =& get_instance();
			$CI->load->model('M_log_produk');
			$detail = $CI->M_log_produk->get_detail_product_outlet($product_id);
			$outlet = $CI->M_log_produk->get_outlet($outlet_id);

			$activity = "Outlet $outlet->name menghapus \"$detail->type_name $detail->category_name $detail->product_name\"";
			$CI->M_log_produk->insert_log_product($_SESSION['id'], $outlet_id, 'item_product_outlet', $product_id, NULL, NULL, NULL, NULL, $unlink[0]->many, NULL, $activity, '-', 'delete_product_outlet');

			$this->db->query("update item_product_outlet set deleted_by='$deleted_by', deleted_date='$deleted_date' where id='$product_id' and outlet_id='$outlet_id' and deleted_date is null");

			$this->db->trans_complete();

			return true;
		} else {
			return false;
		}
	}

	function get_stok_peringatan($outlet_id)
	{
		return $this->db->query("
		select io.id, io.name as product_name, ioc.name as category_name, it.name as type_name, FORMAT(io.many, 0) as many
        from item_outlet io, item_outlet_category ioc, item_type it
        where it.id=ioc.type_id 
        and io.item_outlet_category_id=ioc.id 
        and  ioc.outlet_id='$outlet_id' 
        and io.unit='unit' 
        and many < 10
        union all
        select io.id, ioc.name as product_name, 'Rp' as category_name, it.name as type_name, FORMAT(ioc.modal, 0) as many
        from item_outlet io, item_outlet_category ioc, item_type it
        where ioc.outlet_id='$outlet_id' 
        and io.item_outlet_category_id=ioc.id
        and ioc.type_id=it.id group by ioc.name")->result();
	}
}
