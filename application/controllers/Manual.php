<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Manual extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_impor', '', true);

		if (!isset($_SESSION)) {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'ADMIN') {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'OWNER') {
			redirect('auth/login');
		} elseif ($_SESSION['status_user'] == 'KASIR') {
			redirect('auth/login');
		}
	}

	function produk_fisik()
	{
		$data['item_categories'] = $this->M_impor->produk_fisik();

		$data['title'] = "Petunjuk Impor";
		$data['site_navigation'] = 'Produk Fisik';
		$this->load->view('manual/produk_fisik', $data);
	}

	function pulsa_unit()
	{
		$data['item_outlet_categories'] = $this->M_impor->pulsa_unit();

		$data['title'] = "Petunjuk Impor";
		$data['site_navigation'] = 'Pulsa Unit';
		$this->load->view('manual/pulsa_unit', $data);
	}

	function pulsa_non_unit()
	{
		$data['item_outlet_categories'] = $this->M_impor->pulsa_non_unit();

		$data['title'] = "Petunjuk Impor";
		$data['site_navigation'] = 'Pulsa non Unit';
		$this->load->view('manual/pulsa_non_unit', $data);
	}
}
