<footer class="main-footer text-sm">
	<strong>Copyright &copy; 2019 - <a href="http://adminlte.io">SC.OC Cellular</a>.</strong>
	<div class="float-right d-none d-sm-inline-block">
		All rights reserved.
	</div>
</footer>
