<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_transaksi extends CI_Model
{
	var $column_order = array('user.name', 'transaction_summary.transaction_code', 'transaction_summary.total_item', 'transaction_summary.sub_total', 'transaction_summary.discount_total', 'transaction_summary.payment', 'transaction_summary.change_payment', 'transaction_summary.created_date');
	var $column_search = array('user.name', 'transaction_summary.transaction_code', 'transaction_summary.total_item', 'transaction_summary.sub_total', 'transaction_summary.discount_total', 'transaction_summary.payment', 'transaction_summary.change_payment', 'transaction_summary.created_date');
	var $order = array('transaction_summary.created_date' => 'desc');

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_datatables($user_id, $outlet_id, $date)
	{
		$this->_get_datatables_query($user_id, $outlet_id, $date);

		if ($_REQUEST['length'] != -1)
			$this->db->limit($_REQUEST['length'], $_REQUEST['start']);
		$query = $this->db->get();

		return $query->result();
	}

	private function _get_datatables_query($user_id, $outlet_id, $date)
	{
		if ($user_id != 0 && $outlet_id != 0) {
			$this->db->select('user.id as user_id, user.name as user_name, outlet.id as outlet_id, outlet.name as outlet_name, transaction_summary.id as transaction_summary_id, transaction_summary.transaction_code, transaction_summary.total_item, format(transaction_summary.sub_total, 0) as sub_total, format(transaction_summary.discount_total, 0) as discount_total, transaction_summary.payment, transaction_summary.change_payment, DATE_FORMAT(transaction_summary.created_date, "%d-%m-%Y - %h:%i") as created_date')
				->from('user, outlet, transaction, transaction_summary')
				->where('transaction.transaction_code = transaction_summary.transaction_code')
				->where('transaction.user_id = user.id')
				->where('transaction.outlet_id = outlet.id')
				->where('user.deleted_date is null')
				->where('outlet.deleted_date is null')
				->where('transaction.deleted_date is null')
				->where('transaction_summary.deleted_date is null')
				->where('transaction.user_id', $user_id)
				->where('transaction.outlet_id', $outlet_id)
				->where('transaction.status =', 'beli')
				->where('DATE(transaction_summary.created_date) BETWEEN "' . $date['start'] . '" and "' . $date['end'] . '"')
				->group_by('transaction.transaction_code');
		}

		if ($user_id == 0 && $outlet_id != 0) {
			$this->db->select('user.id as user_id, user.name as user_name, outlet.id as outlet_id, outlet.name as outlet_name, transaction_summary.id as transaction_summary_id, transaction_summary.transaction_code, transaction_summary.total_item, format(transaction_summary.sub_total, 0) as sub_total, format(transaction_summary.discount_total, 0) as discount_total, transaction_summary.payment, transaction_summary.change_payment, DATE_FORMAT(transaction_summary.created_date, "%d-%m-%Y - %h:%i") as created_date')
				->from('user, outlet, transaction, transaction_summary')
				->where('transaction.transaction_code = transaction_summary.transaction_code')
				->where('transaction.user_id = user.id')
				->where('transaction.outlet_id = outlet.id')
				->where('user.deleted_date is null')
				->where('outlet.deleted_date is null')
				->where('transaction.deleted_date is null')
				->where('transaction_summary.deleted_date is null')
				->where('transaction.outlet_id', $outlet_id)
				->where('transaction.status =', 'beli')
				->where('DATE(transaction_summary.created_date) BETWEEN "' . $date['start'] . '" and "' . $date['end'] . '"')
				->group_by('transaction_summary.transaction_code');
		}

		if ($user_id != 0 && $outlet_id == 0) {
			$this->db->select('user.id as user_id, user.name as user_name, outlet.id as outlet_id, outlet.name as outlet_name, transaction_summary.id as transaction_summary_id, transaction_summary.transaction_code, transaction_summary.total_item, format(transaction_summary.sub_total, 0) as sub_total, format(transaction_summary.discount_total, 0) as discount_total, transaction_summary.payment, transaction_summary.change_payment, DATE_FORMAT(transaction_summary.created_date, "%d-%m-%Y - %h:%i") as created_date')
				->from('user, outlet, transaction, transaction_summary')
				->where('transaction.transaction_code = transaction_summary.transaction_code')
				->where('transaction.user_id = user.id')
				->where('transaction.outlet_id = outlet.id')
				->where('user.deleted_date is null')
				->where('outlet.deleted_date is null')
				->where('transaction.deleted_date is null')
				->where('transaction_summary.deleted_date is null')
				->where('transaction.user_id', $user_id)
				->where('transaction.status =', 'beli')
				->where('DATE(transaction_summary.created_date) BETWEEN "' . $date['start'] . '" and "' . $date['end'] . '"')
				->group_by('transaction_summary.transaction_code');
		}

		$i = 0;
		foreach ($this->column_search as $item) {
			if ($_REQUEST['search']['value']) {

				if ($i === 0) {
					$this->db->group_start();
					$this->db->like($item, $_REQUEST['search']['value']);
				} else {
					$this->db->or_like($item, $_REQUEST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if (isset($_REQUEST['order'])) {
			$this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function count_filtered($user_id, $outlet_id, $date)
	{
		$this->_get_datatables_query($user_id, $outlet_id, $date);
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_all($user_id, $outlet_id, $date)
	{
		if ($user_id != 0 && $outlet_id != 0) {
			$this->db->select('user.id as user_id, user.name as user_name, outlet.id as outlet_id, outlet.name as outlet_name, transaction_summary.id as transaction_summary_id, transaction_summary.transaction_code, transaction_summary.total_item, format(transaction_summary.sub_total, 0) as sub_total, format(transaction_summary.discount_total, 0) as discount_total, transaction_summary.payment, transaction_summary.change_payment, DATE_FORMAT(transaction_summary.created_date, "%d-%m-%Y - %h:%i") as created_date')
				->from('user, outlet, transaction, transaction_summary')
				->where('transaction.transaction_code = transaction_summary.transaction_code')
				->where('transaction.user_id = user.id')
				->where('transaction.outlet_id = outlet.id')
				->where('user.deleted_date is null')
				->where('outlet.deleted_date is null')
				->where('transaction.deleted_date is null')
				->where('transaction_summary.deleted_date is null')
				->where('transaction.user_id', $user_id)
				->where('transaction.outlet_id', $outlet_id)
				->where('transaction.status =', 'beli')
				->where('DATE(transaction_summary.created_date) BETWEEN "' . $date['start'] . '" and "' . $date['end'] . '"')
				->group_by('transaction.transaction_code');
		}

		if ($user_id == 0 && $outlet_id != 0) {
			$this->db->select('user.id as user_id, user.name as user_name, outlet.id as outlet_id, outlet.name as outlet_name, transaction_summary.id as transaction_summary_id, transaction_summary.transaction_code, transaction_summary.total_item, format(transaction_summary.sub_total, 0) as sub_total, format(transaction_summary.discount_total, 0) as discount_total, transaction_summary.payment, transaction_summary.change_payment, DATE_FORMAT(transaction_summary.created_date, "%d-%m-%Y - %h:%i") as created_date')
				->from('user, outlet, transaction, transaction_summary')
				->where('transaction.transaction_code = transaction_summary.transaction_code')
				->where('transaction.user_id = user.id')
				->where('transaction.outlet_id = outlet.id')
				->where('user.deleted_date is null')
				->where('outlet.deleted_date is null')
				->where('transaction.deleted_date is null')
				->where('transaction_summary.deleted_date is null')
				->where('transaction.outlet_id', $outlet_id)
				->where('transaction.status =', 'beli')
				->where('DATE(transaction_summary.created_date) BETWEEN "' . $date['start'] . '" and "' . $date['end'] . '"')
				->group_by('transaction_summary.transaction_code');
		}

		if ($user_id != 0 && $outlet_id == 0) {
			$this->db->select('user.id as user_id, user.name as user_name, outlet.id as outlet_id, outlet.name as outlet_name, transaction_summary.id as transaction_summary_id, transaction_summary.transaction_code, transaction_summary.total_item, format(transaction_summary.sub_total, 0) as sub_total, format(transaction_summary.discount_total, 0) as discount_total, transaction_summary.payment, transaction_summary.change_payment, DATE_FORMAT(transaction_summary.created_date, "%d-%m-%Y - %h:%i") as created_date')
				->from('user, outlet, transaction, transaction_summary')
				->where('transaction.transaction_code = transaction_summary.transaction_code')
				->where('transaction.user_id = user.id')
				->where('transaction.outlet_id = outlet.id')
				->where('user.deleted_date is null')
				->where('outlet.deleted_date is null')
				->where('transaction.deleted_date is null')
				->where('transaction_summary.deleted_date is null')
				->where('transaction.user_id', $user_id)
				->where('transaction.status =', 'beli')
				->where('DATE(transaction_summary.created_date) BETWEEN "' . $date['start'] . '" and "' . $date['end'] . '"')
				->group_by('transaction_summary.transaction_code');
		}

		return $this->db->count_all_results();
	}

	function get_transaction_detail($transaction_code)
	{
		return $this->db->query("select *, format(cost, 0) as cost, format(cost*many, 0) as sub_total FROM transaction  where transaction_code ='$transaction_code'")->result();
	}

	function get_transaction_summary($transaction_code)
	{
		return $this->db->query("select user.name as user_name, outlet.name as outlet_name, format(transaction_summary.sub_total,0) as sub_total, format(transaction_summary.discount_total,0) as discount_total, format(transaction_summary.payment,0) as payment, format(transaction_summary.change_payment,0) as change_payment, DATE_FORMAT(transaction_summary.created_date, \"%h:%i - %d-%m-%Y\") as created_date 
		from user, outlet, transaction, transaction_summary
		where transaction.transaction_code = transaction_summary.transaction_code
		and transaction.user_id = user.id
		and transaction.outlet_id = outlet.id
		and user.deleted_date is null
		and outlet.deleted_date is null
		and transaction.deleted_date is null
		and transaction_summary.deleted_date is null
		and transaction.transaction_code = '$transaction_code'
		limit 1")->result();
	}
}
