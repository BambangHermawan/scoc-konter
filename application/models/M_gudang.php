<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class M_gudang
 */
class M_gudang extends CI_Model
{
	function transfer_from_pusat($product_id, $outlet_id, $many, $cost, $product_image)
	{
		$created_by = created_by();
		$created_date = created_date();
		$modified_by = modified_by();
		$modified_date = modified_date();

		$this->db->trans_begin();

		$product = $this->db->query("select many from item_product where id='$product_id' and deleted_date is null")->result();

		if (count($product) > 0) {
			$product_many = $product[0]->many - $many;

			if ($product_many < 0) {
				return false;
			} else {

				$barang_outlet = $this->db->query("select many from item_product_outlet where outlet_id='$outlet_id' and item_product_id='$product_id' and deleted_date is null")->result();

				if ($barang_outlet) {

					$item = $this->db->query("
					select item_product.name as product_name, item_category.name as category_name,
					item_type.name as type_name, item_product.modal, item_product_outlet.cost,
					item_product_outlet.many as awl
					from item_product_outlet, item_product, item_category, item_type
					where item_product_outlet.item_product_id=item_product.id 
					and item_product.item_category_id=item_category.id 
					and item_category.item_type_id=item_type.id 
					and item_product.id='$product_id'
					and item_product_outlet.outlet_id='$outlet_id'
					")->result()[0];

					$total = $many + $item->awl;

					$this->db->query("insert into transaction (user_id, outlet_id, item_outlet_id, type, category, product, modal, cost, many, awl, plus, ak, status, from_outlet, unit, created_by, created_date)  
					values('{$_SESSION['id']}','$outlet_id','$product_id','$item->type_name','$item->category_name','$item->product_name','$item->modal','$item->cost','$many','$item->awl','$many','$total','tambah_unit','$outlet_id','Unit','$created_by','$created_date')");

					$transaction_id = $this->db->insert_id();

					$this->db->query("update item_product_outlet set many='$total', cost='$cost', modified_by='$modified_by', modified_date='$modified_date' where outlet_id='$outlet_id' and item_product_id='$product_id' and deleted_date is null");

					$this->db->query("update item_product set many='$product_many', modified_by='$modified_by', modified_date='$modified_date' where id='$product_id' and deleted_date is null");

					$this->db->trans_complete();

					return $transaction_id;

				} else {

					$this->db->query("insert into item_product_outlet (outlet_id, item_product_id, many, cost, qrcode, created_by, created_date)
                	values('$outlet_id', '$product_id', '$many', '$cost', '$product_image', '$created_by', '$created_date')");

					$item = $this->db->query("
					select item_product.name as product_name, item_category.name as category_name, item_type.name as type_name, item_product.modal
					from item_product, item_category, item_type
					where item_product.deleted_date is null 
					and item_category.deleted_date is null 
					and item_type.deleted_date is null 
					and item_product.item_category_id=item_category.id 
					and item_category.item_type_id=item_type.id 
					and item_product.id='$product_id'")->result()[0];

					$this->db->query("insert into transaction (user_id, outlet_id, item_outlet_id, type, category, product, modal,cost,many,awl, plus, ak, status, from_outlet, unit, created_by, created_date)  values('{$_SESSION['id']}',
                	'$outlet_id','$product_id','$item->type_name','$item->category_name', '$item->product_name', '$item->modal', '0', '$many', '0', '$many', '$many', 'tambah_unit', '$outlet_id', 'Unit', '$created_by', '$created_date')");

					$transaction_id = $this->db->insert_id();

					$this->db->query("update item_product set many='$product_many', modified_by='$modified_by', modified_date='$modified_date' where id='$product_id' and deleted_date is null");

					$this->db->trans_complete();

					return $transaction_id;
				}
			}
		}

		return false;
	}

	function salah_transfer_from_pusat($product_id, $outlet_id, $many)
	{
		$created_by = created_by();
		$created_date = created_date();
		$modified_by = modified_by();
		$modified_date = modified_date();

		$this->db->trans_begin();

		$item = $this->db->query("
        select item_product.name as product_name, item_category.name as category_name, item_type.name as type_name,
        item_product.modal, item_product_outlet.cost, item_product_outlet.many as awl
        from item_product_outlet, item_product, item_category, item_type
        where item_product_outlet.item_product_id=item_product.id and item_product.item_category_id=item_category.id
        and item_product_outlet.deleted_date is null 
        and item_product.deleted_date is null 
        and item_category.deleted_date is null 
        and item_type.deleted_date is null 
        and item_category.item_type_id=item_type.id 
        and item_product.id='$product_id' 
        and item_product_outlet.outlet_id='$outlet_id'")->result();

		if (count($item) > 0) {
			$sisa_barang = $item[0]->awl - $many;

			if ($sisa_barang < 0) {
				return false;
			} else {

				$this->db->query("insert into transaction (user_id, outlet_id, item_outlet_id, type, category, product, modal, cost, many, awl, plus, ak, status, from_outlet, unit, created_by, created_date) 
				values('{$_SESSION['id']}','$outlet_id','$product_id','{$item[0]->type_name}','{$item[0]->category_name}','{$item[0]->product_name}','{$item[0]->modal}','{$item[0]->cost}', '$many','{$item[0]->awl}','-$many','$sisa_barang','salah_transfer', '$outlet_id', 'Unit', '$created_by', '$created_date')");

				$transaction_id = $this->db->insert_id();

				$this->db->query("update item_product_outlet set many='$sisa_barang', modified_by='$modified_by', modified_date='$modified_date' 
				where outlet_id='$outlet_id' and item_product_id='$product_id' and deleted_date is null");

				$this->db->query("update item_product set many= many + $many, modified_by='$modified_by', modified_date='$modified_date' where id='$product_id' and deleted_date is null");

				$this->db->trans_complete();

				return $transaction_id;
			}
		}

		return false;
	}
}
