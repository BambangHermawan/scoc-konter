<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Import extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_auth', '', true);
		$this->load->model('M_impor', '', true);
		$this->load->model('M_log', '', true);
		$this->load->helper('url');

		if (!isset($_SESSION)) {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'ADMIN') {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'OWNER') {
			redirect('auth/login');
		} elseif ($_SESSION['status_user'] == 'KASIR') {
			redirect('auth/login');
		}
	}

	function import_excel()
	{
		move_uploaded_file($_FILES['berkas']['tmp_name'], 'uploads/excels/impor.xls');
		$this->load->library('PHPExcel');
		$document = PHPExcel_IOFactory::load(FCPATH . 'uploads/excels/impor.xls');
		$worksheet = $document->getActiveSheet()->toArray(null, true, true, true);

		if ($this->input->post('tujuan') == 'item_product') {
			$numrow = 5;
			$data = array();
			foreach ($worksheet as $key => $row) {
				array_push($data, array(
					'ID_ITEM_TIPE' => $row['B'],
					'ID_ITEM_KATEGORI' => $row['C'],
					'NAMA_PRODUK' => $row['D'],
					'MODAL' => $row['E'],
					'BANYAK' => $row['F']
				));
			}


			if (count($data) > 0) {
				try {
					$i = 3;
					foreach ($data as $key => $item) {
						if ($key > $i) {
							$this->M_impor->insert_excel_add_new_product($item);
						}
					}
					redirect($this->input->post('redirect'));

				} catch (Exception $e) {
					$this->db->trans_rollback();
					echo $e;
				}
			}
		}

		if ($this->input->post('tujuan') == 'item_product_stok') {
			$data = array();
			foreach ($worksheet as $key => $row) {
				array_push($data, array(
					'ID_ITEM_TIPE' => $row['B'],
					'ID_ITEM_KATEGORI' => $row['C'],
					'NAMA_PRODUK' => $row['D'],
					'PENAMBAHAN_PRODUK' => intval($row['E'])
				));
			}

			if (count($data) > 0) {
				try {
					$i = 3;
					foreach ($data as $key => $item) {
						if ($key > $i) {
							$this->M_impor->insert_excel_add_product_stock($item);
						}
					}

					redirect($this->input->post('redirect'));

				} catch (Exception $e) {
					$this->db->trans_rollback();
					echo $e;
				}
			}
		}
	}

	function import_excel_bluk()
	{
		$outlet_id = $this->input->post('outlet_id');

		move_uploaded_file($_FILES['berkas']['tmp_name'], 'uploads/excels/impor_bluk.xls');
		$this->load->library('PHPExcel');
		$document = PHPExcel_IOFactory::load(FCPATH . 'uploads/excels/impor_bluk.xls');
		$worksheet = $document->getActiveSheet()->toArray(null, true, true, true);

		if ($this->input->post('tujuan') == 'item_outlet_modal') {
			$data = array();
			foreach ($worksheet as $row) {
				array_push($data, array(
					'OUTLET_NAMA' => $row['B'],
					'ID_ITEM_KATEGORI' => $row['C'],
					'ID_ITEM_TIPE' => $row['D'],
					'PENAMBAHAN_MODAL' => intval($row['E'])
				));
			}

			if (count($data) > 0) {
				try {
					$i = 3;
					foreach ($data as $key => $item) {
						if ($key > $i) {
							$this->M_impor->insert_excel_add_product_outlet_modal($item, $outlet_id);
						}
					}
					redirect($this->input->post('redirect'));
				} catch (Exception $e) {
					$this->db->trans_rollback();
					echo $e;
				}
			}
		}

		if ($this->input->post('tujuan') == 'item_outlet_nonunit') {
			$data = array();
			foreach ($worksheet as $row) {
				array_push($data, array(
					'OUTLET_NAMA' => $row['B'],
					'ID_ITEM_KATEGORI' => $row['C'],
					'UNIT_ITEM' => $row['D'],
					'NAMA_ITEM' => $row['E'],
					'MODAL' => intval($row['F']),
					'HARGA' => intval($row['G'])
				));
			}

			if (count($data) > 0) {
				try {
					$i = 3;
					foreach ($data as $key => $item) {
						if ($key > $i) {
							$this->M_impor->insert_excel_add_product_outlet_non_unit($item, $outlet_id);
						}
					}
					redirect($this->input->post('redirect'));

				} catch (Exception $e) {
					$this->db->trans_rollback();
					echo $e;
				}
			}
		}
	}
}
