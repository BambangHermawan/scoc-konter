<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_transaksi extends CI_Model
{
	var $column_order = array('user.name', 'transaction_summary.transaction_code', 'transaction_summary.total_item', 'transaction_summary.sub_total', 'transaction_summary.discount_total', 'transaction_summary.payment', 'transaction_summary.change_payment', 'transaction_summary.created_date');
	var $column_search = array('user.name', 'transaction_summary.transaction_code', 'transaction_summary.total_item', 'transaction_summary.sub_total', 'transaction_summary.discount_total', 'transaction_summary.payment', 'transaction_summary.change_payment', 'transaction_summary.created_date');
	var $order = array('transaction_summary.created_date' => 'desc');

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_datatables($user_id, $outlet_id, $date)
	{
		$this->_get_datatables_query($user_id, $outlet_id, $date);

		if ($_REQUEST['length'] != -1)
			$this->db->limit($_REQUEST['length'], $_REQUEST['start']);
		$query = $this->db->get();

		return $query->result();
	}

	private function _get_datatables_query($user_id, $outlet_id, $date)
	{
		if ($user_id != 0 && $outlet_id != 0) {
			$this->db->select('user.id as user_id, user.name as user_name, outlet.id as outlet_id, outlet.name as outlet_name, transaction_summary.id as transaction_summary_id, transaction_summary.transaction_code, transaction_summary.total_item, format(transaction_summary.sub_total, 0) as sub_total, format(transaction_summary.discount_total, 0) as discount_total, transaction_summary.payment, transaction_summary.change_payment, DATE_FORMAT(transaction_summary.created_date, "%d-%m-%Y - %h:%i") as created_date')
				->from('user, outlet, transaction, transaction_summary')
				->where('transaction.transaction_code = transaction_summary.transaction_code')
				->where('transaction.user_id = user.id')
				->where('transaction.outlet_id = outlet.id')
				->where('user.deleted_date is null')
				->where('outlet.deleted_date is null')
				->where('transaction.deleted_date is null')
				->where('transaction_summary.deleted_date is null')
				->where('transaction.user_id', $user_id)
				->where('transaction.outlet_id', $outlet_id)
				->where('transaction.status =', 'beli')
				->where('DATE(transaction_summary.created_date) BETWEEN "' . $date['start'] . '" and "' . $date['end'] . '"')
				->group_by('transaction.transaction_code');
		}

		if ($user_id == 0 && $outlet_id != 0) {
			$this->db->select('user.id as user_id, user.name as user_name, outlet.id as outlet_id, outlet.name as outlet_name, transaction_summary.id as transaction_summary_id, transaction_summary.transaction_code, transaction_summary.total_item, format(transaction_summary.sub_total, 0) as sub_total, format(transaction_summary.discount_total, 0) as discount_total, transaction_summary.payment, transaction_summary.change_payment, DATE_FORMAT(transaction_summary.created_date, "%d-%m-%Y - %h:%i") as created_date')
				->from('user, outlet, transaction, transaction_summary')
				->where('transaction.transaction_code = transaction_summary.transaction_code')
				->where('transaction.user_id = user.id')
				->where('transaction.outlet_id = outlet.id')
				->where('user.deleted_date is null')
				->where('outlet.deleted_date is null')
				->where('transaction.deleted_date is null')
				->where('transaction_summary.deleted_date is null')
				->where('transaction.outlet_id', $outlet_id)
				->where('transaction.status =', 'beli')
				->where('DATE(transaction_summary.created_date) BETWEEN "' . $date['start'] . '" and "' . $date['end'] . '"')
				->group_by('transaction_summary.transaction_code');
		}

		if ($user_id != 0 && $outlet_id == 0) {
			$this->db->select('user.id as user_id, user.name as user_name, outlet.id as outlet_id, outlet.name as outlet_name, transaction_summary.id as transaction_summary_id, transaction_summary.transaction_code, transaction_summary.total_item, format(transaction_summary.sub_total, 0) as sub_total, format(transaction_summary.discount_total, 0) as discount_total, transaction_summary.payment, transaction_summary.change_payment, DATE_FORMAT(transaction_summary.created_date, "%d-%m-%Y - %h:%i") as created_date')
				->from('user, outlet, transaction, transaction_summary')
				->where('transaction.transaction_code = transaction_summary.transaction_code')
				->where('transaction.user_id = user.id')
				->where('transaction.outlet_id = outlet.id')
				->where('user.deleted_date is null')
				->where('outlet.deleted_date is null')
				->where('transaction.deleted_date is null')
				->where('transaction_summary.deleted_date is null')
				->where('transaction.user_id', $user_id)
				->where('transaction.status =', 'beli')
				->where('DATE(transaction_summary.created_date) BETWEEN "' . $date['start'] . '" and "' . $date['end'] . '"')
				->group_by('transaction_summary.transaction_code');
		}

		$i = 0;
		foreach ($this->column_search as $item) {
			if ($_REQUEST['search']['value']) {

				if ($i === 0) {
					$this->db->group_start();
					$this->db->like($item, $_REQUEST['search']['value']);
				} else {
					$this->db->or_like($item, $_REQUEST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if (isset($_REQUEST['order'])) {
			$this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function count_filtered($user_id, $outlet_id, $date)
	{
		$this->_get_datatables_query($user_id, $outlet_id, $date);
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_all($user_id, $outlet_id, $date)
	{
		if ($user_id != 0 && $outlet_id != 0) {
			$this->db->select('user.id as user_id, user.name as user_name, outlet.id as outlet_id, outlet.name as outlet_name, transaction_summary.id as transaction_summary_id, transaction_summary.transaction_code, transaction_summary.total_item, format(transaction_summary.sub_total, 0) as sub_total, format(transaction_summary.discount_total, 0) as discount_total, transaction_summary.payment, transaction_summary.change_payment, DATE_FORMAT(transaction_summary.created_date, "%d-%m-%Y - %h:%i") as created_date')
				->from('user, outlet, transaction, transaction_summary')
				->where('transaction.transaction_code = transaction_summary.transaction_code')
				->where('transaction.user_id = user.id')
				->where('transaction.outlet_id = outlet.id')
				->where('user.deleted_date is null')
				->where('outlet.deleted_date is null')
				->where('transaction.deleted_date is null')
				->where('transaction_summary.deleted_date is null')
				->where('transaction.user_id', $user_id)
				->where('transaction.outlet_id', $outlet_id)
				->where('transaction.status =', 'beli')
				->where('DATE(transaction_summary.created_date) BETWEEN "' . $date['start'] . '" and "' . $date['end'] . '"')
				->group_by('transaction.transaction_code');
		}

		if ($user_id == 0 && $outlet_id != 0) {
			$this->db->select('user.id as user_id, user.name as user_name, outlet.id as outlet_id, outlet.name as outlet_name, transaction_summary.id as transaction_summary_id, transaction_summary.transaction_code, transaction_summary.total_item, format(transaction_summary.sub_total, 0) as sub_total, format(transaction_summary.discount_total, 0) as discount_total, transaction_summary.payment, transaction_summary.change_payment, DATE_FORMAT(transaction_summary.created_date, "%d-%m-%Y - %h:%i") as created_date')
				->from('user, outlet, transaction, transaction_summary')
				->where('transaction.transaction_code = transaction_summary.transaction_code')
				->where('transaction.user_id = user.id')
				->where('transaction.outlet_id = outlet.id')
				->where('user.deleted_date is null')
				->where('outlet.deleted_date is null')
				->where('transaction.deleted_date is null')
				->where('transaction_summary.deleted_date is null')
				->where('transaction.outlet_id', $outlet_id)
				->where('transaction.status =', 'beli')
				->where('DATE(transaction_summary.created_date) BETWEEN "' . $date['start'] . '" and "' . $date['end'] . '"')
				->group_by('transaction_summary.transaction_code');
		}

		if ($user_id != 0 && $outlet_id == 0) {
			$this->db->select('user.id as user_id, user.name as user_name, outlet.id as outlet_id, outlet.name as outlet_name, transaction_summary.id as transaction_summary_id, transaction_summary.transaction_code, transaction_summary.total_item, format(transaction_summary.sub_total, 0) as sub_total, format(transaction_summary.discount_total, 0) as discount_total, transaction_summary.payment, transaction_summary.change_payment, DATE_FORMAT(transaction_summary.created_date, "%d-%m-%Y - %h:%i") as created_date')
				->from('user, outlet, transaction, transaction_summary')
				->where('transaction.transaction_code = transaction_summary.transaction_code')
				->where('transaction.user_id = user.id')
				->where('transaction.outlet_id = outlet.id')
				->where('user.deleted_date is null')
				->where('outlet.deleted_date is null')
				->where('transaction.deleted_date is null')
				->where('transaction_summary.deleted_date is null')
				->where('transaction.user_id', $user_id)
				->where('transaction.status =', 'beli')
				->where('DATE(transaction_summary.created_date) BETWEEN "' . $date['start'] . '" and "' . $date['end'] . '"')
				->group_by('transaction_summary.transaction_code');
		}

		return $this->db->count_all_results();
	}

	function get_item_pulsa($outlet_id)
	{
		return $this->db->query("select * from item_outlet_category where outlet_id='$outlet_id' order by cast(name as UNSIGNED) ASC")->result();
	}

	function get_pulsa_nominal($item_outlet_category_id)
	{
		return $this->db->query("select id, name from item_outlet where item_outlet_category_id='$item_outlet_category_id' and deleted_date is null order by cast(name as UNSIGNED) ASC")->result();
	}

	function get_pulsa_cost_detail($item_product_id)
	{
		return $this->db->query("select *, many, item_outlet.id as item_outlet_id from item_outlet where id='$item_product_id' and unit='unit' union all
        select *, (select item_outlet_category.modal from item_outlet_category where item_outlet.item_outlet_category_id = item_outlet_category.id) as many, item_outlet.id as item_outlet_id from item_outlet
        where id='$item_product_id' and unit='bluk'")->result()[0];
	}

	function get_product_pulsa_by_qrcode($outlet_id, $qrcode)
	{
		return $this->db->query("select *, many, item_outlet.id as item_outlet_id from item_outlet where qrcode='$qrcode' union all
        select *, (select item_outlet_category.modal from item_outlet_category where item_outlet.item_outlet_category_id = item_outlet_category.id and item_outlet_category.outlet_id='$outlet_id') as many, item_outlet.id as item_outlet_id from item_outlet where unit='bluk'")->result()[0];
	}

	function get_transaction_summary($transaction_code)
	{
		return $this->db->query("select user.name as user_name, outlet.name as outlet_name, format(transaction_summary.sub_total,0) as sub_total, format(transaction_summary.discount_total,0) as discount_total, format(transaction_summary.payment,0) as payment, format(transaction_summary.change_payment,0) as change_payment, DATE_FORMAT(transaction_summary.created_date, \"%h:%i - %d-%m-%Y\") as created_date 
		from user, outlet, transaction, transaction_summary
		where transaction.transaction_code = transaction_summary.transaction_code
		and transaction.user_id = user.id
		and transaction.outlet_id = outlet.id
		and user.deleted_date is null
		and outlet.deleted_date is null
		and transaction.deleted_date is null
		and transaction_summary.deleted_date is null
		and transaction.transaction_code = '$transaction_code'
		limit 1")->result();
	}

	function get_transaction_detail($transaction_code)
	{
		return $this->db->query("select *, format(cost, 0) as cost, format(cost*many, 0) as sub_total from transaction where transaction_code ='$transaction_code'")->result();
	}

	function get_transaction_detail_by_date($user_id, $outlet_id, $date)
	{
		if ($user_id != 0) {
			return $this->db->query("select *, format(cost, 0) as cost, sum(lk) as lk, awl-sum(lk) as ak, format(cost*many, 0) as sub_total from transaction where user_id='$user_id' and outlet_id='$outlet_id' and status='beli' and unit='Unit' and DATE(created_date) between '{$date['start']}' and '{$date['end']}' GROUP BY category, product ")->result();
		} else {
			return $this->db->query("select *, format(cost, 0) as cost, sum(lk) as lk, awl-sum(lk) as ak, format(cost*many, 0) as sub_total from transaction where outlet_id='$outlet_id' and status='beli' and unit='Unit' and DATE(created_date) between '{$date['start']}' and '{$date['end']}' GROUP BY category, product ")->result();
		}
	}

	function save_transaction($user_id, $outlet_id, $type_id, $item_outlet_id, $nomor_transaksi, $total_bayar, $total_diskon, $uang_cash, $uang_kembalian, $catatan)
	{
		$created_by = created_by();
		$created_date = created_date();

		if ($type_id == 'fisik') {
			$this->db->trans_begin();

			$total_item = $this->db->query("select sum(many) as many from transaction_cart where user_id='$user_id' and outlet_id='$outlet_id'")->result();

			$this->db->query("insert into transaction_summary (transaction_code, total_item, sub_total, discount_total, payment, change_payment, created_by, created_date) 
				values ('$nomor_transaksi','{$total_item[0]->many}', '$total_bayar','$total_diskon','$uang_cash','$uang_kembalian','$created_by','$created_date')");

			$cart = $this->db->query("select * from transaction_cart where user_id='$user_id' and outlet_id='$outlet_id'")->result();

			if (count($cart) > 0) {
				foreach ($cart as $item) {
					$outlet_item = $this->db->query("
                select item_product.name as product_name, item_category.name as category_name, item_type.name as type_name, item_product.modal, item_product_outlet.id as item_outlet_id, item_product_outlet.cost, item_product_outlet.qrcode, item_product_outlet.many as awl
                from item_product_outlet, item_product, item_category, item_type
                where item_product_outlet.item_product_id=item_product.id 
                and item_product.item_category_id=item_category.id 
                and item_category.item_type_id=item_type.id 
                and item_product_outlet.deleted_date is null
                and item_product.deleted_date is null	
                and item_category.deleted_date is null
                and item_type.deleted_date is null
                and item_product_outlet.id='$item->item_outlet_id'
                and item_product_outlet.outlet_id='$outlet_id'
                ")->result()[0];

					$sisa_stok = $outlet_item->awl - $item->many;

					if ($sisa_stok < 0) {
						$this->db->trans_complete();
						return false;
					}

					$this->db->query("insert into transaction (transaction_code, user_id, outlet_id, item_outlet_id, type, category, product, modal, cost, discount, many, awl, lk, ak, note, status, from_outlet, unit, created_by, created_date)  
					values('$nomor_transaksi','{$_SESSION['id']}','$outlet_id','$outlet_item->item_outlet_id','$outlet_item->type_name','$outlet_item->category_name','$outlet_item->product_name','$outlet_item->modal','$item->cost','$item->discount','$item->many','$outlet_item->awl','$item->many','$sisa_stok', '$catatan','beli','$outlet_id','Unit','$created_by','$created_date')");

					$transaction_id = $this->db->insert_id();

					$this->db->query("update item_product_outlet set many='$sisa_stok', modified_by='$created_by', modified_date='$created_date' where outlet_id='$outlet_id' and id='$item->item_outlet_id' and deleted_date is null");

					$CI =& get_instance();
					$CI->load->model('M_log');

					$activity = "Transaksi beli \"$outlet_item->type_name $outlet_item->category_name $outlet_item->product_name $item->many buah\"";
					$this->M_log->insert_log($_SESSION['id'], $outlet_id, 'transaction', $transaction_id, $activity, '-', 'transaksi_beli');

					$this->db->query("delete from transaction_cart where user_id='$user_id' and outlet_id='$outlet_id'");
				}

				$this->db->trans_complete();

				return $nomor_transaksi;
			}
		} elseif ($type_id == 'pulsa') {

			$this->db->trans_begin();

			$this->db->query("insert into transaction_summary (transaction_code, total_item, sub_total, discount_total, payment, change_payment, created_by, created_date) 
			values ('$nomor_transaksi','1', '$total_bayar','$total_diskon','$uang_cash','$uang_kembalian','$created_by','$created_date')");

			$outlet_item = $this->db->query("
			select *, many, item_outlet.id as item_outlet_id from item_outlet
			where id='$item_outlet_id' and unit='unit' union all
        	select *, (select item_outlet_category.modal 
        	from item_outlet_category where item_outlet.item_outlet_category_id = item_outlet_category.id) as many, item_outlet.id as item_outlet_id from item_outlet
        	where id='$item_outlet_id' and unit='bluk'")->result()[0];

			$product_detail = $this->db->query("select item_outlet_category.name as category_name, item_type.name as type_name from item_outlet_category, item_type where item_outlet_category.id='$outlet_item->item_outlet_category_id' and item_outlet_category.type_id = item_type.id")->result()[0];

			$this->db->query("insert into transaction (transaction_code, user_id, outlet_id, item_outlet_id, type, category, product, modal, cost, discount, many, lk, note, status, from_outlet, unit, created_by, created_date)
					values('$nomor_transaksi','{$_SESSION['id']}','$outlet_id','$outlet_item->item_outlet_id','$product_detail->type_name','$product_detail->category_name','$outlet_item->name','$outlet_item->modal','$outlet_item->cost','$outlet_item->discount','1','1', '$catatan','beli','$outlet_id','$outlet_item->unit','$created_by','$created_date')");

			$transaction_id = $this->db->insert_id();

			$sisa_modal = $outlet_item->many - $total_bayar;

			$this->db->query("update item_outlet_category set modal='$sisa_modal', modified_by='$created_by', modified_date='$created_date' where outlet_id='$outlet_id' and id='$outlet_item->item_outlet_id' and deleted_date is null");

			$CI =& get_instance();
			$CI->load->model('M_log');

			$activity = "Transaksi beli \"$product_detail->type_name $product_detail->category_name 1 buah\"";
			$this->M_log->insert_log($_SESSION['id'], $outlet_id, 'transaction', $transaction_id, $activity, $catatan, 'transaksi_beli');

			$this->db->trans_complete();

			return $nomor_transaksi;
		}

		return false;
	}
}
