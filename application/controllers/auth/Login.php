<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_auth', '', true);
		$this->load->model('M_wewenang', '', true);
		$this->load->model('M_outlet', '', true);
	}

	function index()
	{
		if (isset($_SESSION['status_user']) && $_SESSION['status_user'] === 'ADMIN' || isset($_SESSION['status_user']) && $_SESSION['status_user'] === 'OWNER') {
			redirect('beranda');
		} elseif (isset($_SESSION['status_user']) && $_SESSION['status_user'] === 'KASIR') {
			redirect('kasir/beranda');
		} else {
			$data['title'] = 'Login';
			$data['site_navigation'] = 'Login';

			$this->load->view('auth/login', $data);
		}
	}

	function check_login()
	{
		$role = $this->input->post('user_roles_id');
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		if ($role == 3) {
			$kasir = $this->M_auth->check_login_kasir($role, $username, md5($password));

			if ($kasir) {
				foreach ($kasir as $key => $item) {
					$_SESSION['id'] = $item->user_id;
					$_SESSION['user_roles_id'] = $item->user_roles_id;
					$_SESSION['outlet_employee_id'] = $item->outlet_employee_id;
					$_SESSION['outlet_id'] = $item->outlet_id;
					$_SESSION['username'] = $item->username;
					$_SESSION['name'] = $item->name;
					$_SESSION['address'] = $item->address;
					$_SESSION['phone'] = $item->hp;
					$_SESSION['status_user'] = $item->role;
					$_SESSION['login_date'] = date('Y-m-d H:i:s');
					$_SESSION['menu'] = $this->M_outlet->get_outlet_by_id($item->outlet_id);

					$wewenang = $item->role;
					$outlet = $item->outlet_name;
					$shift = $item->name;
				}

				$response = [
					'success' => true,
					'type' => 'success',
					'message' => 'Anda telah berhasil login sebagai ' . $wewenang . ' ' . $outlet,
					'data' => $_SESSION['status_user'],
					'total' => count($kasir)
				];

				return to_json($response);
			}
		} else {

			$result = $this->M_auth->check_login($role, $username, md5($password));

			if ($result) {

				foreach ($result as $key => $item) {
					$_SESSION['id'] = $item->id;
					$_SESSION['user_roles_id'] = $item->user_roles_id;
					$_SESSION['username'] = $item->username;
					$_SESSION['name'] = $item->name;
					$_SESSION['address'] = $item->address;
					$_SESSION['phone'] = $item->hp;
					$_SESSION['status_user'] = $item->role;
					$_SESSION['login_date'] = date('Y-m-d H:i:s');
					$_SESSION['menu'] = $this->M_outlet->get_all_outlet();

					$role = $item->role;
				}

				$response = [
					'success' => true,
					'type' => 'success',
					'message' => 'Anda telah berhasil login sebagai ' . $role,
					'data' => $_SESSION['status_user'],
					'total' => count($result)
				];

				return to_json($response);
			}

		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Username atau password yang anda masukan salah',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function logout()
	{
		session_destroy();

		redirect('auth/login');
	}

	function get_all_wewenang()
	{
		$data = $this->M_wewenang->get_wewenang();

		if (count($data) > 0) {
			$no = 1;
			$result = [];
			foreach ($data as $item) {
				$column['no'] = $no++;
				$column['id'] = $item->id;
				$column['wewenang'] = $item->wewenang;
				$result[] = $column;
			}

			$response = [
				'success' => true,
				'data' => $result,
				'total' => count($result)
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}
}
