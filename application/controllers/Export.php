<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Export extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_export', '', true);

		if (!isset($_SESSION)) {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'ADMIN') {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'OWNER') {
			redirect('auth/login');
		} elseif ($_SESSION['status_user'] == 'KASIR') {
			redirect('auth/login');
		}
	}

	function export_excel()
	{
		$item_type_id = $this->input->post('tipe_item');
		$format_excel = $this->input->post('format_excel');

		$this->load->library('PHPExcel');

		$excel = new PHPExcel();

		$styleArray = array(
			'font' => array(
				'bold' => true,
			)
		);

		$style_col = array(
			'font' => array('bold' => true),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'borders' => array(
				'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
				'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
				'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
				'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			)
		);

		$style_row = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT
			),
			'borders' => array(
				'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
				'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
				'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
				'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			)
		);

		$result = $this->M_export->get_all_product_by_item_type_id($item_type_id);

		if ($format_excel == 'laporan_stok') {
			$excel->getProperties()->setCreator('087878242423')
				->setTitle("Data Produk Gudang")
				->setSubject("Data Produk Gudang")
				->setDescription("Laporan Data Produk Gudang")
				->setKeywords("Data Produk Gudang");

			$excel->setActiveSheetIndex(0)->setCellValue('A4', "NO");
			$excel->setActiveSheetIndex(0)->setCellValue('B4', "ID_ITEM_TIPE");
			$excel->setActiveSheetIndex(0)->setCellValue('C4', "ID_ITEM_KATEGORI");
			$excel->setActiveSheetIndex(0)->setCellValue('D4', "NAMA_PRODUK");
			$excel->setActiveSheetIndex(0)->setCellValue('E4', "MODAL");
			$excel->setActiveSheetIndex(0)->setCellValue('F4', "BANYAK");
			$excel->setActiveSheetIndex(0)->setCellValue('G4', "TANGGAL_MASUK");
			$excel->setActiveSheetIndex(0)->setTitle("Produk Gudang")->getStyle("A4:G4")->applyFromArray($styleArray);

			$excel->getActiveSheet()->getStyle('A4')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('B4')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('C4')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('D4')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('E4')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('F4')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('G4')->applyFromArray($style_col);

			$no = 1;
			$numrow = 5;
			foreach ($result as $item) {
				$excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no);
				$excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $item->item_type_name);
				$excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $item->item_category_name);
				$excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $item->item_product_name);
				$excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $item->modal);
				$excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, $item->many);
				$excel->setActiveSheetIndex(0)->setCellValue('G' . $numrow, $item->entry_date);

				$excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('G' . $numrow)->applyFromArray($style_row);

				$no++;
				$numrow++;
			}

			$excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
			$excel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
			$excel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
			$excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
			$excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
			$excel->getActiveSheet()->getColumnDimension('G')->setWidth(25);

			$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
			$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			$excel->getActiveSheet(0)->setTitle("Laporan Data Outlet");
			$excel->setActiveSheetIndex(0);

			ob_end_clean();
			header("Content-Type: application/vnd.ms-excel");
			header("Content-Disposition: attachment; filename=\"LAPORAN_STOK_GUDANG.xls\"");
			header("Cache-Control: max-age=0");
		}

		if ($format_excel == 'tambah_stok') {

			$excel->getProperties()->setCreator('087878242423')
				->setTitle("Format Tambah Stok Gudang")
				->setSubject("Format Tambah Stok Gudang")
				->setDescription("Format Tambah Stok Gudang")
				->setKeywords("Tambah Stok Gudang");

			$excel->setActiveSheetIndex(0)->setCellValue('A4', "NO");
			$excel->setActiveSheetIndex(0)->setCellValue('B4', "ID_ITEM_TIPE");
			$excel->setActiveSheetIndex(0)->setCellValue('C4', "ID_ITEM_KATEGORI");
			$excel->setActiveSheetIndex(0)->setCellValue('D4', "NAMA_PRODUK");
			$excel->setActiveSheetIndex(0)->setCellValue('E4', "PENAMBAHAN_PRODUK");
			$excel->setActiveSheetIndex(0)->setTitle("Tambah Stok Produk Gudang")->getStyle("A4:E4")->applyFromArray($styleArray);

			$excel->getActiveSheet()->getStyle('A4')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('B4')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('C4')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('D4')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('E4')->applyFromArray($style_col);

			$no = 1;
			$numrow = 5;
			foreach ($result as $item) {
				$excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no);
				$excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $item->item_type_name);
				$excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $item->item_category_name);
				$excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $item->item_product_name);
				$excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, null);

				$excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);

				$no++;
				$numrow++;
			}

			$excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
			$excel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
			$excel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
			$excel->getActiveSheet()->getColumnDimension('E')->setWidth(30);

			$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
			$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			$excel->getActiveSheet(0)->setTitle("Format Tambah Stok Gudang");
			$excel->setActiveSheetIndex(0);

			ob_end_clean();
			header("Content-Type: application/vnd.ms-excel");
			header("Content-Disposition: attachment; filename=\"FORMAT_TAMBAH_STOK_GUDANG.xls\"");
			header("Cache-Control: max-age=0");
		}

		if ($format_excel == 'tambah_item_produk') {
			$excel->getProperties()->setCreator('087878242423')
				->setTitle("Format Tambah Produk Baru")
				->setSubject("Format Tambah Produk Baru")
				->setDescription("Format Tambah Produk Baru")
				->setKeywords("Format Tambah Produk Baru");

			$excel->setActiveSheetIndex(0)->setCellValue('A4', "NO");
			$excel->setActiveSheetIndex(0)->setCellValue('B4', "ID_ITEM_TIPE");
			$excel->setActiveSheetIndex(0)->setCellValue('C4', "ID_ITEM_KATEGORI");
			$excel->setActiveSheetIndex(0)->setCellValue('D4', "NAMA_PRODUK");
			$excel->setActiveSheetIndex(0)->setCellValue('E4', "MODAL");
			$excel->setActiveSheetIndex(0)->setCellValue('F4', "BANYAK");
			$excel->setActiveSheetIndex(0)->setTitle("Tambah Stok Produk Gudang")->getStyle("A4:F4")->applyFromArray($styleArray);

			$excel->getActiveSheet()->getStyle('A4')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('B4')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('C4')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('D4')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('E4')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('F4')->applyFromArray($style_col);

			$format_product = $this->M_export->get_all_product_category_by_item_type_id($item_type_id);

			$no = 1;
			$numrow = 5;
			foreach ($format_product as $item) {
				$excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no);
				$excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $item->item_type_name);
				$excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $item->item_category_name);
				$excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, null);
				$excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, null);
				$excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, null);

				$excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);

				$no++;
				$numrow++;
			}

			$excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
			$excel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
			$excel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
			$excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
			$excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);

			$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
			$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			$excel->getActiveSheet(0)->setTitle("Format Tambah Produk Baru");
			$excel->setActiveSheetIndex(0);

			ob_end_clean();
			header("Content-Type: application/vnd.ms-excel");
			header("Content-Disposition: attachment; filename=\"FORMAT_TAMBAH_PRODUK_BARU_GUDANG.xls\"");
			header("Cache-Control: max-age=0");
		}

		$objWriter = PHPExcel_IOFactory::createWriter($excel, "Excel5");
		$objWriter->save("php://output");

		exit;
	}

	function export_excel_bluk()
	{
		$outlet_id = $this->input->post('outlet_id');
		$tujuan = $this->input->post('tujuan_export');

		$this->load->library('PHPExcel');

		$excel = new PHPExcel();

		$styleArray = array(
			'font' => array(
				'bold' => true,
			)
		);

		$style_col = array(
			'font' => array('bold' => true),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'borders' => array(
				'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
				'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
				'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
				'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			)
		);

		$style_row = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT
			),
			'borders' => array(
				'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
				'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
				'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
				'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			)
		);

		if ($tujuan == 'item_outlet_modal') {
			$modal = $this->M_export->get_item_outlet_pulsa_modal_by_outlet_id($outlet_id);

			$excel->getProperties()->setCreator('087878242423')
				->setTitle("Format Tambah Modal Pulsa")
				->setSubject("Format Tambah Modal Pulsa")
				->setDescription("Format Tambah Modal Pulsa")
				->setKeywords("Format Tambah Modal Pulsa");

			$excel->setActiveSheetIndex(0)->setCellValue('A4', "NO");
			$excel->setActiveSheetIndex(0)->setCellValue('B4', "OUTLET_NAMA");
			$excel->setActiveSheetIndex(0)->setCellValue('C4', "ID_ITEM_KATEGORI");
			$excel->setActiveSheetIndex(0)->setCellValue('D4', "ID_ITEM_TIPE");
			$excel->setActiveSheetIndex(0)->setCellValue('E4', "PENAMBAHAN_MODAL");
			$excel->setActiveSheetIndex(0)->setTitle("Produk Gudang")->getStyle("A4:F4")->applyFromArray($styleArray);

			$excel->getActiveSheet()->getStyle('A4')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('B4')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('C4')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('D4')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('E4')->applyFromArray($style_col);

			$no = 1;
			$numrow = 5;
			foreach ($modal as $item) {
				$excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no);
				$excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $item->outlet_name);
				$excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $item->item_outlet_category_name);
				$excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $item->item_type_name);
				$excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, null);

				$excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);

				$no++;
				$numrow++;
			}

			$excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
			$excel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
			$excel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
			$excel->getActiveSheet()->getColumnDimension('E')->setWidth(30);

			$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
			$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			$excel->getActiveSheet(0)->setTitle("Format Tambah Modal Pulsa");
			$excel->setActiveSheetIndex(0);

			ob_end_clean();
			header("Content-Type: application/vnd.ms-excel");
			header("Content-Disposition: attachment; filename=\"FORMAT_TAMBAH_MODAL_PULSA_OUTLET.xls\"");
			header("Cache-Control: max-age=0");
		}

		if ($tujuan == 'item_outlet_nonunit') {
			$item_data = $this->M_export->get_item_outlet_pulsa_by_outlet_id($outlet_id);

			$excel->getProperties()->setCreator('087878242423')
				->setTitle("Format Tambah Item Pulsa")
				->setSubject("Format Tambah Item Pulsa")
				->setDescription("Format Tambah Item Pulsa")
				->setKeywords("Format Tambah Item Pulsa");

			$excel->setActiveSheetIndex(0)->setCellValue('A4', "NO");
			$excel->setActiveSheetIndex(0)->setCellValue('B4', "OUTLET_NAMA");
			$excel->setActiveSheetIndex(0)->setCellValue('C4', "ID_ITEM_KATEGORI");
			$excel->setActiveSheetIndex(0)->setCellValue('D4', "UNIT_ITEM");
			$excel->setActiveSheetIndex(0)->setCellValue('E4', "NAMA_ITEM");
			$excel->setActiveSheetIndex(0)->setCellValue('F4', "MODAL");
			$excel->setActiveSheetIndex(0)->setCellValue('G4', "HARGA");
			$excel->setActiveSheetIndex(0)->setTitle("Item Pulsa")->getStyle("A4:G4")->applyFromArray($styleArray);

			$excel->getActiveSheet()->getStyle('A4')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('B4')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('C4')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('D4')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('E4')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('F4')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('G4')->applyFromArray($style_col);

			$no = 1;
			$numrow = 5;
			foreach ($item_data as $item) {
				$excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no);
				$excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $item->outlet_name);
				$excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $item->item_outlet_category_name);
				$excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $item->unit);
				$excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, null);
				$excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, null);
				$excel->setActiveSheetIndex(0)->setCellValue('G' . $numrow, null);

				$excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);
				$excel->getActiveSheet()->getStyle('G' . $numrow)->applyFromArray($style_row);

				$no++;
				$numrow++;
			}

			$excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
			$excel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
			$excel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
			$excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
			$excel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
			$excel->getActiveSheet()->getColumnDimension('G')->setWidth(30);

			$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
			$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			$excel->getActiveSheet(0)->setTitle("Format Tambah Item Pulsa");
			$excel->setActiveSheetIndex(0);

			ob_end_clean();
			header("Content-Type: application/vnd.ms-excel");
			header("Content-Disposition: attachment; filename=\"FORMAT_TAMBAH_ITEM_PULSA_OUTLET.xls\"");
			header("Cache-Control: max-age=0");
		}

		$objWriter = PHPExcel_IOFactory::createWriter($excel, "Excel5");
		$objWriter->save("php://output");
	}
}
