<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_outlet extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_outlet_by_id($outlet_id)
	{
		return $this->db->query("select id, name from outlet where deleted_date is null and id='$outlet_id' LIMIT 1")->result();
	}

	function get_all_outlet()
	{
		return $this->db->query("select id, name from outlet where deleted_date is null")->result();
	}
}
