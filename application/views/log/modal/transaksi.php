<!--Modal rekapan transfer dari gudang ke outlet-->

<!--Modal rekapan transfer dari outlet ke outlet-->

<!--Modal rekapan tranfer saldo pulsa non unit outlet-->

<!--modal detail transfer produk unit-->
<div class="modal fade" id="modal_show_detail_transfer_product_unit">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header"><h5>Detail Log Transfer</h5></div>
			<div class="modal-body">
				<div class="row mt-2">
					<div class="col-lg-6">Outlet Pengirim</div>
					<div class="col-lg-6" id="detail_log_sender"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-6">Outlet Penerima</div>
					<div class="col-lg-6" id="detail_log_destination"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-6">Tipe</div>
					<div class="col-lg-6" id="detail_log_type"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-6">Kategori</div>
					<div class="col-lg-6" id="detail_log_kategori"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-6">Produk</div>
					<div class="col-lg-6" id="detail_log_produk"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-6">Stok Awal Penerima</div>
					<div class="col-lg-6" id="detail_log_many"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-6">Jumlah +/-</div>
					<div class="col-lg-6" id="detail_log_plus"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-6">Stok Akhir Penerima</div>
					<div class="col-lg-6" id="detail_log_after_many"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Selesai</button>
			</div>
		</div>
	</div>
</div>

<!--modal detail tambah saldo pulsa gudang-->
<div class="modal fade" id="modal_show_detail_add_saldo_pulsa">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header"><h5>Detail Log Transfer</h5></div>
			<div class="modal-body">
				<div class="row mt-2">
					<div class="col-lg-6">Outlet Pengirim</div>
					<div class="col-lg-6" id="detail_add_saldo_sender"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-6">Outlet Penerima</div>
					<div class="col-lg-6" id="detail_add_saldo_destination"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-6">Tipe</div>
					<div class="col-lg-6" id="detail_add_saldo_type"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-6">Kategori</div>
					<div class="col-lg-6" id="detail_add_saldo_kategori"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-6">Produk</div>
					<div class="col-lg-6" id="detail_add_saldo_produk"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-6">Jumlah Awal</div>
					<div class="col-lg-6" id="detail_add_saldo_many"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-6">Jumlah +/-</div>
					<div class="col-lg-6" id="detail_add_saldo_plus"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-6">Jumlah Akhir</div>
					<div class="col-lg-6" id="detail_add_saldo_after_many"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Selesai</button>
			</div>
		</div>
	</div>
</div>
