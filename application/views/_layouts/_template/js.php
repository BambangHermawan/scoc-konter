</section>
</div>
<!-- /.content-wrapper -->

<!-- Footer -->
<?php $this->load->view('_layouts/_template/footer') ?>
<!-- /.Footer -->

</div>

<script>
	/** add active class and stay opened when selected */
	var url = window.location;

	/** for sidebar menu entirely but not cover treeview */
	$('ul.nav-sidebar a').filter(function () {
		return this.href == url;
	}).addClass('active');

	/** for treeview */
	$('ul.nav-treeview a').filter(function () {
		return this.href == url;
	}).parentsUntil(".nav-sidebar > .nav-treeview").addClass('menu-open').prev('a').addClass('active');
</script>

</body>
</html>
