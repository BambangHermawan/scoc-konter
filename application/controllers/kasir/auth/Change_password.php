<?php
defined('BASEPATH') or exit('No direct script access allowed');

class change_password extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_auth', '', true);

		if (!isset($_SESSION)) {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'KASIR') {
			redirect('auth/login');
		}
	}

	function index()
	{
		$data['title'] = 'Ubah Password';
		$data['site_navigation'] = 'Ubah Password';

		$this->load->view('kasir/auth/change_password', $data);
	}

	function change_password()
	{
		$user_id = $_SESSION['id'];
		$outlet_employee_id = $_SESSION['outlet_employee_id'];
		$current_password = $this->input->post('current_password');
		$confirm_password = $this->input->post('confirm_password');

		$result = $this->M_auth->change_password_kasir($outlet_employee_id, $user_id, md5($current_password), md5($confirm_password));

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Berhasil mengubah password. silahkan login kembali dengan password baru anda.',
				'data' => $result,
				'total' => count($result)
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Gagal mengubah password',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

}
