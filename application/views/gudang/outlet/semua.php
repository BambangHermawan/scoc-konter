<div class="table-responsive">
	<table id="table_outlet_semua" class="table table-striped table-hover dt-responsive display nowrap"
		   style="width:100%">
		<thead>
		<tr>
			<th class="nomor" data-orderable="false">No</th>
			<th>Kategori</th>
			<th>Nama</th>
			<th>Modal</th>
			<th>Harga Jual</th>
			<th>Banyak</th>
			<th>Diskon (%)</th>
			<th>Disc. Sampai</th>
			<th>Qrcode</th>
			<th data-orderable="false">Aksi</th>
		</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>

<script>
	$(document).ready(function () {
		init_table_outlet_semua()
	})

	//button modal transfer produk
	$("#table_outlet_semua").on('click', 'tr #btn_outlet_transfer_click', function () {
		tr = $(this).closest('tr')
		data = table_outlet_semua.row(tr).data()

		$("#input_transfer_outlet_product_id").val(data.item_product_id)
		$("#input_transfer_outlet_product_name").val(data.name)
		$("#input_transfer_outlet_stock").val(data.many)
		$("#input_transfer_outlet_product_qrcode").val(data.qrcode)
		$("#input_transfer_outlet_type_id").val(data.type_name)
		$('#modal_transfer_outlet').modal({backdrop: 'static', keyboard: false})
	})

	//button modal atur harga produk
	$("#table_outlet_semua").on('click', 'tr #btn_outlet_cost_click', function () {

		tr = $(this).closest('tr')
		data = table_outlet_semua.row(tr).data()

		$("#input_cost_outlet_product_id").val(data.id)
		$("#input_cost_outlet_product_name").val(data.name)
		$("#input_cost_outlet_cost").val(data.cost.replace(',', ''))
		$("#input_cost_outlet_type_id").val(data.type_name)
		$('#modal_cost_outlet').modal({backdrop: 'static', keyboard: false})
	})

	//button modal diskon produk
	$("#table_outlet_semua").on('click', 'tr #btn_outlet_discount_click', function () {

		tr = $(this).closest('tr')
		data = table_outlet_semua.row(tr).data()

		$("#input_discount_outlet_product_id").val(data.id)
		$("#input_outlet_discount_product_name").val(data.name)
		$("#input_outlet_discount_cost").val(data.cost.replace(',', ''))
		$("#input_outlet_discount_type_id").val(data.type_name)
		$("#input_outlet_discount_many").val(data.discount)
		$("#input_outlet_discount_until").val(data.discount_until)
		$('#modal_discount_outlet').modal({backdrop: 'static', keyboard: false})
	})

	//hapus data produk
	$("#table_outlet_semua").on('click', 'tr #btn_outlet_delete_click', function () {
		tr = $(this).closest('tr')
		data = table_outlet_semua.row(tr).data()
		product_id = data.id

		Swal.fire({
			title: 'Peringatan!',
			text: 'Apakah Anda yakin akan menghapus produk ' + data.name + ' ?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya, hapus data ini!'
		}).then((result) => {
			if (result.value) {
				$.ajax({
					url: "<?= base_url(); ?>_gudang/outlet/delete_product",
					type: "POST",
					data: {
						product_id: product_id,
						outlet_id: "<?= $this->uri->segment(4); ?>",
					},
					success(result) {

						if (result.success) {
							Swal.fire({
								title: 'Berhasil',
								text: result.message,
								icon: result.type,
								showConfirmButton: false,
								timer: 1500
							});

							init_table_outlet_semua()
						} else {
							Swal.fire({
								title: 'gagal',
								text: result.message,
								icon: result.type,
								showConfirmButton: false,
								timer: 1500
							});
						}
					}
				})
			}
		})
	})

	//modal preview qrcode
	$("#table_outlet_semua").on('click', 'tr #qrcode_image', function () {
		$('#qrcode_preview').attr('src', $(this).attr('src'));
		$('#modal_qrcode').modal({backdrop: 'static', keyboard: false});
	});

	//init datatables table semua
	function init_table_outlet_semua() {
		button = "<button class='btn btn-info btn-sm' id='btn_outlet_transfer_click'>\
            <i class='fas fa-exchange-alt'></i>&nbsp;Transfer</button>"
		button += "&nbsp;<button class='btn btn-success btn-sm' id='btn_outlet_cost_click'>\
            <i class='fas fa-edit'></i>&nbsp;Harga</button>"
		button += "&nbsp;<button class='btn btn-warning btn-sm' id='btn_outlet_discount_click'>\
            <i class='fas fa-edit'></i>&nbsp;Diskon</button>"
		button += "&nbsp;<button class='btn btn-danger btn-sm' id='btn_outlet_delete_click'>\
            <i class='fas fa-trash'></i>&nbsp;Hapus</button>"

		table_outlet_semua = $("#table_outlet_semua").DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			autoWidth: true,
			width: "0.8%",
			ajax: "<?= base_url(); ?>_gudang/outlet/get_all_product_by_outlet_id/" + 0 + "/<?= $this->uri->segment(4); ?>",
			columns: [
				{
					data: "id",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: "category_name"
				},
				{
					data: "name"
				},
				{
					data: "modal"
				},
				{
					data: "cost"
				},
				{
					data: "many"
				},
				{
					data: "discount"
				},
				{
					data: "discount_until"
				},
				{
					data: "qrcode",
					"targets": 6,
					"render": function (url, type, full) {
						return '<img onerror="this.onerror=null; this.remove();" class="center image_' + full['id'] + '"id="qrcode_image" height="100px" width="100px" src="' + "<?= base_url(); ?>uploads/qrcode/" + full['qrcode'] + '" />';
					}
				},
				{
					defaultContent: button
				}
			]
		})
	}
</script>
