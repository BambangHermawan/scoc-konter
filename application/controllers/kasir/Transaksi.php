<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaksi extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('kasir/M_transaksi', 'M_transaksi', true);
		$this->load->model('kasir/M_transaksi_cart', 'M_transaksi_cart', true);
		$this->load->model('kasir/M_outlet', 'M_outlet', true);

		if (!isset($_SESSION)) {
			redirect('auth/login');
		} elseif ($_SESSION['status_user'] != 'KASIR') {
			redirect('auth/login');
		}
	}

	function index($outlet_id)
	{
		$data['title'] = 'Transaksi Fisik';
		$data['site_navigation'] = 'Transaksi';

		$data['outlet'] = $this->M_outlet->get_outlet_by_id($outlet_id);

		$this->load->view('kasir/transaksi/fisik', $data);
	}

	function pulsa($outlet_id)
	{
		$data['title'] = 'Transaksi Pulsa';
		$data['site_navigation'] = 'Transaksi';

		$data['outlet'] = $this->M_outlet->get_outlet_by_id($outlet_id);

		$this->load->view('kasir/transaksi/pulsa', $data);
	}

	function riwayat($outlet_id)
	{
		$data['title'] = 'Riwayat Transaksi';
		$data['site_navigation'] = 'Detail';

		$data['outlet'] = $this->M_outlet->get_outlet_by_id($outlet_id);

		$this->load->view('kasir/transaksi/riwayat', $data);
	}

	function get_all_transaction($user_id, $outlet_id, $start_date, $end_date)
	{
		$filter = $this->generate_date_filter($start_date, $end_date);

		$result = $this->M_transaksi->get_datatables($user_id, $outlet_id, $filter);

		$response = [
			'success' => true,
			'draw' => $_REQUEST['draw'],
			'recordsTotal' => $this->M_transaksi->count_all($user_id, $outlet_id, $filter),
			'recordsFiltered' => $this->M_transaksi->count_filtered($user_id, $outlet_id, $filter),
			'data' => $result
		];

		return to_json($response);
	}

	function get_item_pulsa($outlet_id)
	{
		$result = $this->M_transaksi->get_item_pulsa($outlet_id);

		if ($result) {

			$response = [
				'success' => true,
				'data' => $result,
				'total' => count($result)
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function get_pulsa_nominal($item_category_id)
	{
		$result = $this->M_transaksi->get_pulsa_nominal($item_category_id);

		if ($result) {

			$response = [
				'success' => true,
				'data' => $result,
				'total' => count($result)
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function get_pulsa_cost_detail($item_product_id)
	{
		$result = $this->M_transaksi->get_pulsa_cost_detail($item_product_id);

		if ($result) {

			$response = [
				'success' => true,
				'data' => $result,
				'total' => count($result)
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function get_transaction_detail($transaction_code)
	{
		$result = $this->M_transaksi->get_transaction_detail($transaction_code);

		if ($result) {

			$response = [
				'success' => true,
				'data' => $result,
				'total' => count($result)
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function print_transaction_detail($transaction_code)
	{
		$this->load->library('Pdf');
		$pdf = new FPDF('P', 'mm', 'A5');

		$result = $this->M_transaksi->get_transaction_detail($transaction_code);
		$summary = $this->M_transaksi->get_transaction_summary($transaction_code);

		$pdf->AddPage();
		$pdf->SetFont('Arial', '', 10);

		$pdf->Cell(25, 5, 'Nota', 0, 0, 'L');
		$pdf->Cell(85, 5, $transaction_code, 0, 0, 'L');
		$pdf->Ln();
		$pdf->Cell(25, 5, 'Tanggal', 0, 0, 'L');
		$pdf->Cell(85, 5, $summary[0]->created_date, 0, 0, 'L');
		$pdf->Ln();
		$pdf->Cell(25, 5, 'Kasir', 0, 0, 'L');
		$pdf->Cell(85, 5, $summary[0]->user_name, 0, 0, 'L');
		$pdf->Ln();
		$pdf->Cell(25, 5, 'Outlet', 0, 0, 'L');
		$pdf->Cell(85, 5, $summary[0]->outlet_name, 0, 0, 'L');
		$pdf->Ln();

		$pdf->Cell(130, 5, '-----------------------------------------------------------------------------------------------------------', 0, 0, 'L');
		$pdf->Ln();

		$pdf->Cell(50, 5, 'Item', 0, 0, 'L');
		$pdf->Cell(25, 5, 'Harga', 0, 0, 'L');
		$pdf->Cell(15, 5, 'Qty', 0, 0, 'L');
		$pdf->Cell(15, 5, 'Disc', 0, 0, 'L');
		$pdf->Cell(25, 5, 'Sub total', 0, 0, 'L');
		$pdf->Ln();

		$pdf->Cell(130, 5, '-----------------------------------------------------------------------------------------------------------', 0, 0, 'L');
		$pdf->Ln();

		$no = 0;
		foreach ($result as $item) {
			$pdf->Cell(50, 5, $item->product, 0, 0, 'L');
			$pdf->Cell(25, 5, $item->cost, 0, 0, 'L');
			$pdf->Cell(15, 5, $item->many);
			$pdf->Cell(15, 5, $item->discount . '%');
			$pdf->Cell(25, 5, $item->sub_total, 0, 0, 'L');
			$pdf->Ln();
			$no++;
		}

		$pdf->Cell(130, 5, '-----------------------------------------------------------------------------------------------------------', 0, 0, 'L');
		$pdf->Ln();

		$pdf->Cell(105, 5, 'Total Bayar', 0, 0, 'R');
		$pdf->Cell(25, 5, $summary[0]->sub_total);
		$pdf->Ln();

		$pdf->Cell(105, 5, 'Cash', 0, 0, 'R');
		$pdf->Cell(25, 5, $summary[0]->payment);
		$pdf->Ln();

		$pdf->Cell(105, 5, 'Kembali', 0, 0, 'R');
		$pdf->Cell(25, 5, $summary[0]->change_payment);
		$pdf->Ln();
		$pdf->Cell(130, 5, '-----------------------------------------------------------------------------------------------------------', 0, 0, 'L');
		$pdf->Ln();
		$pdf->Ln();
		$pdf->Cell(130, 5, "Terimakasih telah berbelanja dengan kami", 0, 0, 'C');
		$pdf->Ln();

		$pdf->Output();
	}

	function get_all_transaction_cart($outlet_id)
	{
		$user_id = $_SESSION['id'];

		$result = $this->M_transaksi_cart->get_datatables($user_id, $outlet_id);

		$response = [
			'success' => true,
			'draw' => $_REQUEST['draw'],
			'recordsTotal' => $this->M_transaksi_cart->count_all($user_id, $outlet_id),
			'recordsFiltered' => $this->M_transaksi_cart->count_filtered($user_id, $outlet_id),
			'data' => $result
		];

		return to_json($response);
	}

	function get_product_by_qrcode($outlet_id, $qrcode)
	{
		$user_id = $_SESSION['id'];

		$data = $this->M_transaksi_cart->get_product_by_qrcode($outlet_id, $qrcode);

		if (count($data) > 0) {
			foreach ($data as $item) {
				$result = $this->M_transaksi_cart->add_transaction_cart($user_id, $outlet_id, $item->id, $item->cost);
			}

			if ($result) {

				$response = [
					'success' => true,
					'type' => 'success',
					'message' => 'Tambah produk berhasil',
					'data' => $result,
					'total' => $result
				];

				return to_json($response);
			}
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'produk tidak ditemukan / Stok tidak cukup',
			'data' => [],
			'total' => 0
		];

		return to_json($response);

	}

	function get_product_pulsa_by_qrcode($outlet_id, $qrcode)
	{
		$result = $this->M_transaksi->get_product_pulsa_by_qrcode($outlet_id, $qrcode);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Produk berhasil ditemukan',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'produk tidak ditemukan / Stok tidak cukup',
			'data' => [],
			'total' => 0
		];

		return to_json($response);

	}

	function get_total_payment($user_id, $outlet_id)
	{
		$result = $this->M_transaksi_cart->get_total_payment_cart($user_id, $outlet_id);

		if ($result) {

			$response = [
				'success' => true,
				'data' => $result,
				'total' => count($result)
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function get_transaction_detail_by_date($user_id, $outlet_id, $start_date, $end_date)
	{
		$filter = $this->generate_date_filter($start_date, $end_date);

		$result = $this->M_transaksi->get_transaction_detail_by_date($user_id, $outlet_id, $filter);

		if ($result) {

			$response = [
				'success' => true,
				'data' => $result,
				'total' => count($result)
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function add_transaction_cart()
	{
		$user_id = $_SESSION['id'];
		$outlet_id = $this->input->post('outlet_id');
		$item_outlet_id = $this->input->post('item_outlet_id');
		$cost = $this->input->post('cost');

		$result = $this->M_transaksi_cart->add_transaction_cart($user_id, $outlet_id, $item_outlet_id, $cost);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Tambah produk berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Stok tidak mencukupi',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function edit_product_from_cart()
	{
		$cart_id = $this->input->post('cart_id');
		$many = $this->input->post('many');

		$result = $this->M_transaksi_cart->edit_product_from_cart($cart_id, $many);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Tambah produk berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Stok tidak mencukupi',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function delete_product_from_cart()
	{
		$transaction_cart_id = $this->input->post('id');

		$result = $this->M_transaksi_cart->delete_product_from_cart($transaction_cart_id);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Hapus produk berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Hapus produk gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function save_transaction()
	{
		$user_id = $this->input->post('user_id');
		$outlet_id = $this->input->post('outlet_id');
		$type_id = $this->input->post('type_id');
		$item_outlet_id = $this->input->post('item_outlet_id');
		$nomor_transaksi = $this->input->post('nomor_transaksi');
		$total_bayar = $this->input->post('total_bayar');
		$total_diskon = $this->input->post('total_diskon');
		$uang_cash = $this->input->post('uang_cash');
		$uang_kembalian = $this->input->post('uang_kembalian');
		$catatan = $this->input->post('catatan');

		if ($type_id == 'fisik') {
			$result = $this->M_transaksi->save_transaction($user_id, $outlet_id, $type_id, $item_outlet_id, $nomor_transaksi, $total_bayar, $total_diskon, $uang_cash, $uang_kembalian, $catatan);

			if ($result) {
				$response = [
					'success' => true,
					'transaction_code' => $nomor_transaksi,
					'type' => 'success',
					'message' => 'Transaksi pembelian berhasil',
					'data' => $result,
					'total' => $result
				];

				return to_json($response);
			}

			$response = [
				'success' => false,
				'type' => 'warning',
				'message' => 'Transaksi pembelian gagal',
				'data' => [],
				'total' => 0
			];

			return to_json($response);

		} else if ($type_id == 'pulsa') {
			$result = $this->M_transaksi->save_transaction($user_id, $outlet_id, $type_id, $item_outlet_id, $nomor_transaksi, $total_bayar, $total_diskon, $uang_cash, $uang_kembalian, $catatan);

			if ($result) {
				$response = [
					'success' => true,
					'transaction_code' => $nomor_transaksi,
					'type' => 'success',
					'message' => 'Transaksi pembelian berhasil',
					'data' => $result,
					'total' => $result
				];

				return to_json($response);
			}

			$response = [
				'success' => false,
				'type' => 'warning',
				'message' => 'Transaksi pembelian gagal',
				'data' => [],
				'total' => 0
			];

			return to_json($response);
		} else {
			$response = [
				'success' => false,
				'type' => 'warning',
				'message' => 'Terjadi kesalahan.',
				'data' => [],
				'total' => 0
			];

			return to_json($response);
		}
	}

	function generate_date_filter($start_date, $end_date)
	{
		if (empty($start_date) && empty($end_date)) {
			$filter = [
				'start' => date('Y-m-d', strtotime("-1 days")),
				'end' => date('Y-m-d')
			];
		} else if (date('Y-m-d', strtotime($start_date)) == date('Y-m-d', strtotime($end_date))) {
			$filter = [
				'start' => date('Y-m-d', strtotime($start_date)),
				'end' => date('Y-m-d', strtotime($end_date))
			];
		} else {
			$filter = [
				'start' => date('Y-m-d', strtotime($start_date)),
				'end' => date('Y-m-d', strtotime($end_date))
			];
		}

		return $filter;
	}

}
