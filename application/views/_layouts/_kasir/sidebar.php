<aside class="main-sidebar elevation-4 sidebar-light-purple">
	<a href="<?php echo base_url('kasir/beranda'); ?>" class="brand-link navbar-purple">
		<img src="<?php echo base_url('assets/image/logo.png'); ?>" alt="AdminLTE Logo"
			 class="brand-image img-circle elevation-3"
			 style="opacity: .8">
		<span class="brand-text font-weight-bold font-weight-lighter text-light">SC.OC Cellular</span>
	</a>

	<div class="sidebar">
		<div class="user-panel mt-3 pb-3 mb-3 d-flex">
			<div class="image" style="margin-top: 9.5px">
				<img src="<?php echo base_url('assets/image/anonim.png'); ?>" class="img-circle elevation-2"
					 alt="User Image">
			</div>
			<div class="info float-right" style="margin-left: 10px">
				<a class="d-block text-bold"><?php echo $_SESSION['name']; ?></a>
				<a class="d-block text-bold"><?php echo $_SESSION['status_user']; ?></a>
			</div>
		</div>

		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column text-sm nav-compact" data-widget="treeview" role="menu"
				data-accordion="false">
				<li class="nav-item has-treeview">
					<a href="#" class="nav-link active">
						<i class="nav-icon fas fa-tachometer-alt"></i>
						<p>
							Beranda
							<i class="right fas fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="<?php echo base_url('kasir/beranda') ?>" class="nav-link active">
								<i class="far fa-circle nav-icon"></i>
								<p>Beranda</p>
							</a>
						</li>
					</ul>
				</li>

				<!--Transaksi-->
				<li class="nav-item has-treeview">
					<a href="#" class="nav-link">
						<i class="nav-icon fas fa-cash-register"></i>
						<p>
							Transaksi
							<i class="fas fa-angle-left right"></i>
						</p>
					</a>
					<?php foreach ($_SESSION['menu'] as $menu) { ?>
						<ul class="nav nav-treeview">
							<li class="nav-item">
								<a href="<?= base_url(); ?>kasir/transaksi/index/<?= $menu->id; ?>"
								   class="nav-link">
									<i class="far fa-circle nav-icon"></i>
									<p>Transaksi</p>
								</a>
							</li>
							<li class="nav-item">
								<a href="<?= base_url(); ?>kasir/transaksi/riwayat/<?= $menu->id; ?>"
								   class="nav-link">
									<i class="far fa-circle nav-icon"></i>
									<p>Riwayat Transaksi</p>
								</a>
							</li>
						</ul>
					<?php } ?>
				</li>

				<!--Gudang-->
				<li class="nav-item has-treeview">
					<a href="#" class="nav-link">
						<i class="nav-icon fas fa-box"></i>
						<p>
							Gudang Outlet
							<i class="fas fa-angle-left right"></i>
						</p>
					</a>
					<?php foreach ($_SESSION['menu'] as $menu) { ?>
						<ul class="nav nav-treeview">
							<li class="nav-item">
								<a href="<?= base_url(); ?>kasir/_gudang/outlet/index/<?= $menu->id; ?>"
								   class="nav-link">
									<i class="far fa-circle nav-icon"></i>
									<p><?= $menu->name; ?></p>
								</a>
							</li>
						</ul>
					<?php } ?>
				</li>

				<!--Laporan-->
				<li class="nav-item has-treeview">
					<a href="#" class="nav-link">
						<i class="nav-icon fas fa-newspaper"></i>
						<p>
							Laporan Outlet
							<i class="fas fa-angle-left right"></i>
						</p>
					</a>
					<?php foreach ($_SESSION['menu'] as $menu) { ?>
						<ul class="nav nav-treeview">
							<li class="nav-item">
								<a href="<?= base_url(); ?>kasir/laporan/index/<?= $menu->id; ?>"
								   class="nav-link">
									<i class="far fa-circle nav-icon"></i>
									<p>Laporan Penjualan</p>
								</a>
							</li>
						</ul>
					<?php } ?>
					<?php foreach ($_SESSION['menu'] as $menu) { ?>
						<ul class="nav nav-treeview">
							<li class="nav-item">
								<a href="<?= base_url(); ?>kasir/laporan/pendapatan/<?= $menu->id; ?>"
								   class="nav-link">
									<i class="far fa-circle nav-icon"></i>
									<p>Laporan Pendapatan</p>
								</a>
							</li>
						</ul>
					<?php } ?>
				</li>

				<!--Log Transaksi-->
				<li class="nav-item has-treeview">
					<a href="#" class="nav-link">
						<i class="nav-icon fas fa-history"></i>
						<p>
							Log Aktifitas
							<i class="fas fa-angle-left right"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<?php foreach ($_SESSION['menu'] as $menu) { ?>
								<a href="<?= base_url(); ?>kasir/log/index/<?= $menu->id; ?>"
								   class="nav-link">
									<i class="far fa-circle nav-icon"></i>
									<p><?= $menu->name; ?></p>
								</a>
							<?php } ?>
						</li>
					</ul>
				</li>

				<!--Profile-->
				<li class="nav-item has-treeview">
					<a href="#" class="nav-link">
						<i class="nav-icon fas fa-user"></i>
						<p>
							Profile
							<i class="fas fa-angle-left right"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="<?= base_url('kasir/auth/change_password') ?>" class="nav-link">
								<i class="far fa-circle nav-icon"></i>
								<p>Ubah Password</p>
							</a>
						</li>
					</ul>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="<?php echo base_url('auth/login/logout') ?>" class="nav-link">
								<i class="far fa-circle nav-icon"></i>
								<p>Keluar</p>
							</a>
						</li>
					</ul>
				</li>
			</ul>
		</nav>
	</div>
</aside>
