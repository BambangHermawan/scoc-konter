<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_laporan extends CI_Model
{
	function get_statistic_by_date($user_id, $type, $outlet_id, $filter)
	{
		$query = '';
		if ($type != "Pulsa") {
			if ($user_id != 0 && $outlet_id != 0) {
				$query = $this->db->query(
					"SELECT category, product, sum(lk) as lk,
                cost*sum(lk)+cost*sum(retur_c)+sum(retur_u) as omset,
                (cost-modal)*sum(lk) as advantage, cost*sum(retur_br) as rugi
                FROM transaction 
				WHERE type='$type' 
                AND user_id='$user_id' 
                AND outlet_id='$outlet_id' 
                AND DATE(transaction.created_date) BETWEEN '{$filter['start']}' AND '{$filter['end']}'
                GROUP BY category, product"
				)->result();
			}

			if ($user_id != 0 && $outlet_id == 0) {
				$query = $this->db->query(
					"SELECT category, product, sum(lk) as lk,
                cost*sum(lk)+cost*sum(retur_c)+sum(retur_u) as omset,
                (cost-modal)*sum(lk) as advantage, cost*sum(retur_br) as rugi
                FROM transaction 
				WHERE type='$type' 
                AND user_id='$user_id' 
                AND DATE(transaction.created_date) BETWEEN '{$filter['start']}' AND '{$filter['end']}'
                GROUP BY category, product"
				)->result();
			}

			if ($outlet_id != 0 && $user_id == 0) {
				$query = $this->db->query(
					"SELECT category, product, sum(lk) as lk,
                cost*sum(lk)+cost*sum(retur_c)+sum(retur_u) as omset,
                (cost-modal)*sum(lk) as advantage, cost*sum(retur_br) as rugi
                FROM transaction 
				WHERE type='$type' 
                AND outlet_id='$outlet_id' 
                AND DATE(transaction.created_date) BETWEEN '{$filter['start']}' AND '{$filter['end']}'
                GROUP BY category, product"
				)->result();
			}

			if ($outlet_id == 0 && $user_id == 0) {
				$query = $this->db->query(
					"SELECT category, product, sum(lk) as lk,
                cost*sum(lk)+cost*sum(retur_c)+sum(retur_u) as omset,
                (cost-modal)*sum(lk) as advantage, cost*sum(retur_br) as rugi
                FROM transaction 
				WHERE type='$type' 
                AND DATE(transaction.created_date) BETWEEN '{$filter['start']}' AND '{$filter['end']}'
                GROUP BY category, product"
				)->result();
			}

			if ($query) {
				$omset = 0;
				foreach ($query as $data) {
					$omset += $data->omset;
				}
				return $omset;
			} else return 0;
		} else {

			if ($user_id != 0 && $outlet_id != 0) {
				$query = $this->db->query("
                SELECT transaction.id, sum(retur_gbb) as retur_gbb, sum(retur_bu) as retur_bu,sum(retur_c) as retur_c,
                sum(retur_u) as retur_u,
                type, category, product, format(cost,0) as cost, format(modal,0) as modal, awl, sum(plus) as plus,
                sum(retur_br) as retur_br, sum(lk) as lk, awl-sum(lk)+sum(plus)-sum(retur_gbb) as ak,
                format(cost*sum(lk)+cost*sum(retur_c)+sum(retur_u),0) as omset,
                format((cost-modal)*sum(lk),0) as advantage, format(cost*sum(retur_br),0) as rugi,
                format(modal*ak,0) as asset
                FROM transaction 
				WHERE type = 'Pulsa' 
                AND unit = 'Unit' 
                AND user_id='$user_id'
                AND outlet_id='$outlet_id'
                AND DATE(transaction.created_date) BETWEEN '{$filter['start']}' AND '{$filter['end']}'
                GROUP BY category, product, type
            
                UNION ALL
                
                SELECT transaction.id, sum(retur_gbb) as retur_gbb, sum(retur_bu) as retur_bu, sum(retur_c) as retur_c,
                sum(retur_u) as retur_u, type, category, product, 
                format(cost,0) as cost, format(modal,0) as modal, '-' as awl, sum(plus) as plus,
                '-' as retur_br, sum(lk) as lk, '-' as ak,
                cost*sum(lk)+cost*sum(retur_c)+sum(retur_u) as omset,
                format((cost-modal)*sum(lk),0) as advantage, '-' as rugi, '-' as asset
                FROM transaction 
				WHERE type = 'Pulsa' 
                AND unit = 'Bluk' 
                AND status != 'tambah_saldo'
                AND user_id = '$user_id'
                AND outlet_id = '$outlet_id'
                AND DATE(transaction.created_date) BETWEEN '{$filter['start']}' AND '{$filter['end']}'
                GROUP BY category, product, type"
				)->result();
			}

			if ($user_id != 0 && $outlet_id == 0) {
				$query = $this->db->query("
                SELECT transaction.id, sum(retur_gbb) as retur_gbb, sum(retur_bu) as retur_bu,sum(retur_c) as retur_c,
                sum(retur_u) as retur_u,
                type, category, product, format(cost,0) as cost, format(modal,0) as modal, awl, sum(plus) as plus,
                sum(retur_br) as retur_br, sum(lk) as lk, awl-sum(lk)+sum(plus)-sum(retur_gbb) as ak,
                format(cost*sum(lk)+cost*sum(retur_c)+sum(retur_u),0) as omset,
                format((cost-modal)*sum(lk),0) as advantage, format(cost*sum(retur_br),0) as rugi,
                format(modal*ak,0) as asset
                FROM transaction 
				WHERE type = 'Pulsa' 
                AND unit = 'Unit' 
                AND user_id='$user_id'
                AND DATE(transaction.created_date) BETWEEN '{$filter['start']}' AND '{$filter['end']}'
                GROUP BY category, product, type
                UNION ALL
                SELECT id, sum(retur_gbb) as retur_gbb, sum(retur_bu) as retur_bu, sum(retur_c) as retur_c,
                sum(retur_u) as retur_u, type, category, product, 
                format(cost,0) as cost, format(modal,0) as modal, '-' as awl, sum(plus) as plus,
                '-' as retur_br, sum(lk) as lk, '-' as ak,
                cost*sum(lk)+cost*sum(retur_c)+sum(retur_u) as omset,
                format((cost-modal)*sum(lk),0) as advantage, '-' as rugi, '-' as asset
                FROM transaction 
				WHERE type = 'Pulsa' 
                AND unit = 'Bluk' 
                AND status != 'tambah_saldo'
                AND user_id = '$user_id'
                AND DATE(transaction.created_date) BETWEEN '{$filter['start']}' AND '{$filter['end']}'
                GROUP BY category, product, type"
				)->result();
			}

			if ($outlet_id != 0 && $user_id == 0) {
				$query = $this->db->query("
                SELECT transaction.id, sum(retur_gbb) as retur_gbb, sum(retur_bu) as retur_bu,sum(retur_c) as retur_c,
                sum(retur_u) as retur_u,
                type, category, product, format(cost,0) as cost, format(modal,0) as modal, awl, sum(plus) as plus,
                sum(retur_br) as retur_br, sum(lk) as lk, awl-sum(lk)+sum(plus)-sum(retur_gbb) as ak,
                format(cost*sum(lk)+cost*sum(retur_c)+sum(retur_u),0) as omset,
                format((cost-modal)*sum(lk),0) as advantage, format(cost*sum(retur_br),0) as rugi,
                format(modal*ak,0) as asset
                FROM transaction 
				WHERE type = 'Pulsa' 
                AND unit = 'Unit' 
                AND user_id='$user_id'
                AND outlet_id='$outlet_id'
                AND DATE(transaction.created_date) BETWEEN '{$filter['start']}' AND '{$filter['end']}'
                GROUP BY category, product, type
                UNION ALL
                SELECT transaction.id, sum(retur_gbb) as retur_gbb, sum(retur_bu) as retur_bu, sum(retur_c) as retur_c,
                sum(retur_u) as retur_u, type, category, product, 
                format(cost,0) as cost, format(modal,0) as modal, '-' as awl, sum(plus) as plus,
                '-' as retur_br, sum(lk) as lk, '-' as ak,
                cost*sum(lk)+cost*sum(retur_c)+sum(retur_u) as omset,
                format((cost-modal)*sum(lk),0) as advantage, '-' as rugi, '-' as asset
                FROM transaction 
				WHERE type = 'Pulsa' 
                AND unit = 'Bluk' 
                AND status != 'tambah_saldo'
                AND user_id = '$user_id'
                AND DATE(transaction.created_date) BETWEEN '{$filter['start']}' AND '{$filter['end']}'
                GROUP BY category, product, type"
				)->result();
			}

			if ($outlet_id == 0 && $user_id == 0) {
				$query = $this->db->query("
                SELECT transaction.id, sum(retur_gbb) as retur_gbb, sum(retur_bu) as retur_bu,sum(retur_c) as retur_c,
                sum(retur_u) as retur_u,
                type, category, product, format(cost,0) as cost, format(modal,0) as modal, awl, sum(plus) as plus,
                sum(retur_br) as retur_br, sum(lk) as lk, awl-sum(lk)+sum(plus)-sum(retur_gbb) as ak,
                format(cost*sum(lk)+cost*sum(retur_c)+sum(retur_u),0) as omset,
                format((cost-modal)*sum(lk),0) as advantage, format(cost*sum(retur_br),0) as rugi,
                format(modal*ak,0) as asset
                FROM transaction 
				WHERE type = 'Pulsa' 
                AND unit = 'Unit' 
                AND user_id='$user_id'
                AND outlet_id='$outlet_id'
                AND DATE(transaction.created_date) BETWEEN '{$filter['start']}' AND '{$filter['end']}'
                GROUP BY category, product, type
                UNION ALL
                SELECT transaction.id, sum(retur_gbb) as retur_gbb, sum(retur_bu) as retur_bu, sum(retur_c) as retur_c,
                sum(retur_u) as retur_u, type, category, product, 
                format(cost,0) as cost, format(modal,0) as modal, '-' as awl, sum(plus) as plus,
                '-' as retur_br, sum(lk) as lk, '-' as ak,
                cost*sum(lk)+cost*sum(retur_c)+sum(retur_u) as omset,
                format((cost-modal)*sum(lk),0) as advantage, '-' as rugi, '-' as asset
                FROM transaction 
				WHERE type = 'Pulsa' 
                AND unit = 'Bluk' 
                AND status != 'tambah_saldo'
                AND DATE(transaction.created_date) BETWEEN '{$filter['start']}' AND '{$filter['end']}'
                GROUP BY category, product, type"
				)->result();
			}

			if ($query) {
				$omset = 0;
				foreach ($query as $data) {
					$omset += $data->omset;
				}
				return $omset;
			} else return 0;
		}
	}

	function get_transaction_earning_by_date($user_id, $outlet_id, $start_date)
	{
		if ($user_id != 0 && $outlet_id != 0) {
			return $this->db->query("
            SELECT sum(lk) as laku,
            format(sum(cost*lk),0) as total_uang  
            FROM transaction 
			WHERE user_id = '$user_id'
            AND outlet_id = '$outlet_id'
            AND status = 'beli' 
            AND DATE(transaction.created_date) = '$start_date'
        ")->result()[0];
		}

		if ($user_id != 0 && $outlet_id == 0) {
			return $this->db->query("
            SELECT sum(lk) as laku, 
            format(sum(cost*lk),0) as total_uang  
            FROM transaction 
			WHERE user_id = '$user_id'
            AND status = 'beli' 
            AND DATE(transaction.created_date) = '$start_date'
            ")->result()[0];
		}

		if ($user_id == 0 && $outlet_id != 0) {
			return $this->db->query("
            SELECT sum(lk) as laku,
            format(sum(cost*lk),0) as total_uang  
            FROM transaction 
			WHERE outlet_id = '$outlet_id'
            AND status = 'beli'   
            AND DATE(transaction.created_date) = '$start_date'
            ")->result()[0];
		}

		if ($user_id == 0 && $outlet_id == 0) {
			return $this->db->query("
            SELECT sum(lk) as laku,
            format(sum(cost*lk),0) as total_uang  
            FROM transaction 
			WHERE DATE(transaction.created_date) = '$start_date'
			AND status = 'beli' 
            ")->result()[0];
		}

		return false;
	}

	function get_transaction_total_by_date($user_id, $outlet_id, $start_date, $end_date)
	{
		$filter = $this->generate_date_filter($start_date, $end_date);

		if ($user_id != 0 && $outlet_id != 0) {
			return $this->db->query("
            SELECT COUNT(*) as count FROM (
    		SELECT count(transaction.id) as count 
			FROM transaction 
    		WHERE user_id = '$user_id' 
    		AND outlet_id = '$outlet_id' 
    		AND status = 'beli' 
            AND DATE(transaction.created_date) between '{$filter["start"]}' and '{$filter["end"]}'
			GROUP BY transaction_code DESC
			) count
        ")->result()[0];
		}

		if ($user_id != 0 && $outlet_id == 0) {
			return $this->db->query("
            SELECT COUNT(*) as count FROM (
    		SELECT count(transaction.id) as count 
			FROM transaction 
    		WHERE user_id = '$user_id' 
    		AND status = 'beli' 
            AND DATE(transaction.created_date) between '{$filter["start"]}' and '{$filter["end"]}'
			GROUP BY transaction_code DESC
			) count
        ")->result()[0];
		}

		if ($user_id == 0 && $outlet_id != 0) {
			return $this->db->query("
            SELECT COUNT(*) as count FROM (
    		SELECT count(transaction.id) as count 
			FROM transaction 
    		WHERE outlet_id = '$outlet_id' 
    		AND status = 'beli' 
            AND DATE(transaction.created_date) between '{$filter["start"]}' and '{$filter["end"]}'
			GROUP BY transaction_code DESC
			) count
        ")->result()[0];
		}

		if ($user_id == 0 && $outlet_id == 0) {
			return $this->db->query("
            SELECT COUNT(*) as count FROM (
    		SELECT count(transaction.id) as count 
			FROM transaction 
    		WHERE status = 'beli' 
            AND DATE(transaction.created_date) between '{$filter["start"]}' and '{$filter["end"]}'
			GROUP BY transaction_code DESC
			) count
        ")->result()[0];
		}

		return false;
	}

	function get_transaction_unit_by_duration($user_id, $outlet_id, $start_date, $end_date)
	{
		$filter = $this->generate_date_filter($start_date, $end_date);

		if ($user_id != 0 && $outlet_id != 0) {
			return $this->db->query("
            SELECT sum(transaction.many) as count 
            FROM transaction 
            WHERE user_id = '$user_id'
            and outlet_id = '$outlet_id'
            AND status = 'beli' 
            AND type !='Pulsa' 
            AND DATE(transaction.created_date) between '{$filter["start"]}' and '{$filter["end"]}'
        ")->result()[0];
		}

		if ($user_id == 0 && $outlet_id != 0) {
			return $this->db->query("
            SELECT sum(transaction.many) as count 
            FROM transaction 
            WHERE outlet_id = '$outlet_id'
            AND status = 'beli' 
            AND type !='Pulsa' 
            AND DATE(transaction.created_date) between '{$filter["start"]}' and '{$filter["end"]}'
        ")->result()[0];
		}

		if ($user_id != 0 && $outlet_id == 0) {
			return $this->db->query("
            SELECT sum(transaction.many) as count  
            FROM transaction 
            WHERE user_id = '$user_id'
            AND status = 'beli' 
            AND type !='Pulsa' 
            AND DATE(transaction.created_date) between '{$filter["start"]}' and '{$filter["end"]}'
        ")->result()[0];
		}

		if ($user_id == 0 && $outlet_id == 0) {
			return $this->db->query("
            SELECT sum(transaction.many) as count 
            FROM transaction 
            WHERE status = 'beli' 
            AND type !='Pulsa' 
            AND DATE(transaction.created_date) between '{$filter["start"]}' and '{$filter["end"]}'
        ")->result()[0];
		}

		return false;
	}

	function get_transaction_detail_by_date($user_id, $type, $outlet_id, $date)
	{
		if ($user_id != 0 && $outlet_id != 0) {
			return $this->db->query("select *, format(cost, 0) as cost, sum(plus) as plus, sum(lk) as lk, awl-sum(lk) as ak, format(cost*many, 0) as sub_total from transaction where user_id='$user_id' and outlet_id='$outlet_id' and type='$type' and status='beli' and DATE(created_date) between '{$date['start']}' and '{$date['end']}' GROUP BY category, product ")->result();
		}

		if ($user_id != 0 && $outlet_id == 0) {
			return $this->db->query("select *, format(cost, 0) as cost, sum(plus) as plus, sum(lk) as lk, awl-sum(lk) as ak, format(cost*many, 0) as sub_total from transaction where user_id='$user_id' and type='$type' and status='beli' and DATE(created_date) between '{$date['start']}' and '{$date['end']}' GROUP BY category, product ")->result();
		}

		if ($user_id == 0 && $outlet_id != 0) {
			return $this->db->query("select *, format(cost, 0) as cost, sum(plus) as plus, sum(lk) as lk, awl-sum(lk) as ak, format(cost*many, 0) as sub_total from transaction where outlet_id='$outlet_id' and type='$type' and status='beli' and DATE(created_date) between '{$date['start']}' and '{$date['end']}' GROUP BY category, product ")->result();
		}

		if ($user_id == 0 && $outlet_id == 0) {
			return $this->db->query("select *, format(cost, 0) as cost, sum(plus) as plus, sum(lk) as lk, awl-sum(lk) as ak, format(cost*many, 0) as sub_total from transaction where type='$type' and status='beli' and DATE(created_date) between '{$date['start']}' and '{$date['end']}' GROUP BY category, product ")->result();
		}
	}

	function get_statistic_by_day($type, $user_id, $outlet_id, $today)
	{
		if ($type != "Pulsa") {
			$query = $this->db->query("
                SELECT sum(many) as many
                FROM transaction 
				WHERE status = 'beli'
                AND unit = 'Unit'
                AND type = '$type'
                AND user_id = '$user_id'
                AND outlet_id = '$outlet_id'
                AND DATE(transaction.created_date) = '$today'
				GROUP BY DATE(transaction.created_date)"
			)->result();

			if ($query) {
				$total_penjualan = [];
				foreach ($query as $data) {
					$total_penjualan[] = $data->many;
				}
				return $total_penjualan;
			} else return 0;
		} else {
			$query = $this->db->query("
                SELECT transaction.id, sum(many) as many
                FROM transaction 
				WHERE type = 'Pulsa'
                AND status = 'beli'
                AND unit = 'Unit'
                AND user_id = '$user_id'
                AND outlet_id = '$outlet_id'
                AND DATE(transaction.created_date) = '$today'
                GROUP BY  DATE(transaction.created_date)
                UNION ALL
                SELECT transaction.id, sum(many) as many
                FROM transaction 
				WHERE type = 'Pulsa'
                AND unit = 'Bluk'
                AND status != 'tambah_saldo'
                AND user_id = '$user_id'
                AND outlet_id = '$outlet_id'
                AND DATE(transaction.created_date) = '$today'
				GROUP BY DATE(transaction.created_date)"
			)->result();

			if ($query) {
				$total_penjualan = [];
				foreach ($query as $data) {
					$total_penjualan[] = $data->many;
				}
				return $total_penjualan;
			} else return 0;
		}
	}

	function get_statistic_by_month($type, $user_id, $outlet_id, $filter)
	{
		if ($type != "Pulsa") {
			$query = $this->db->query("
                SELECT sum(many) as many
                FROM transaction 
				WHERE status = 'beli' 
                AND unit = 'Unit' 
                AND type = '$type' 
                AND user_id = '$user_id' 
                AND outlet_id = '$outlet_id' 
                AND DATE(transaction.created_date) BETWEEN '{$filter['start']}' AND '{$filter['end']}'
				GROUP BY DATE(transaction.created_date)"
			)->result();

			if ($query) {
				$total_penjualan = [];
				foreach ($query as $data) {
					$total_penjualan[] = $data->many;
				}
				return $total_penjualan;
			} else return 0;
		} else {
			$query = $this->db->query("
                SELECT transaction.id, sum(many) as many
                FROM transaction 
				WHERE type = 'Pulsa'
                AND status = 'beli'
                AND unit = 'Unit'
                AND user_id = '$user_id' 
                AND outlet_id = '$outlet_id' 
                AND DATE(transaction.created_date) BETWEEN '{$filter['start']}' AND '{$filter['end']}'
                GROUP BY  DATE(transaction.created_date)
                UNION ALL
                SELECT transaction.id, sum(many) as many
                FROM transaction 
				WHERE type = 'Pulsa'
                AND unit = 'Bluk'
                AND status != 'tambah_saldo'
                AND user_id = '$user_id' 
                AND outlet_id = '$outlet_id' 
                AND DATE(transaction.created_date) BETWEEN '{$filter['start']}' AND '{$filter['end']}'
				GROUP BY DATE(transaction.created_date)"
			)->result();

			if ($query) {
				$total_penjualan = [];
				foreach ($query as $data) {
					$total_penjualan[] = $data->many;
				}
				return $total_penjualan;
			} else return 0;
		}
	}

	function generate_date_filter($start_date, $end_date)
	{
		if (empty($start_date) && empty($end_date)) {
			$filter = [
				'start' => date('Y-m-d', strtotime("-1 days")),
				'end' => date('Y-m-d')
			];
		} else if (date('Y-m-d', strtotime($start_date)) == date('Y-m-d', strtotime($end_date))) {
			$filter = [
				'start' => date('Y-m-d', strtotime('-1 day', strtotime($start_date))),
				'end' => date('Y-m-d', strtotime($end_date))
			];
		} else {
			$filter = [
				'start' => date('Y-m-d', strtotime($start_date)),
				'end' => date('Y-m-d', strtotime($end_date))
			];
		}

		return $filter;
	}
}
