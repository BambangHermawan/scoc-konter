<aside class="main-sidebar elevation-4 sidebar-light-purple">
	<a href="<?= base_url(); ?>" class="brand-link navbar-purple">
		<img src="<?php echo base_url('assets/image/logo.png'); ?>" alt="AdminLTE Logo"
			 class="brand-image img-circle elevation-3"
			 style="opacity: .8">
		<span class="brand-text font-weight-bold font-weight-lighter text-light">SC.OC Cellular</span>
	</a>

	<div class="sidebar">
		<div class="user-panel mt-3 pb-3 mb-3 d-flex">
			<div class="image" style="margin-top: 9.5px">
				<img src="<?php echo base_url('assets/image/anonim.png'); ?>" class="img-circle elevation-2"
					 alt="User Image">
			</div>
			<div class="info float-right" style="margin-left: 10px">
				<a class="d-block text-bold"><?php echo $_SESSION['name']; ?></a>
				<a class="d-block text-bold"><?php echo $_SESSION['status_user']; ?></a>
			</div>
		</div>

		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column text-sm nav-compact" data-widget="treeview" role="menu"
				data-accordion="false">
				<li class="nav-item has-treeview">
					<a href="#" class="nav-link active">
						<i class="nav-icon fas fa-tachometer-alt"></i>
						<p>
							Beranda
							<i class="right fas fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="<?php echo '/' ?>" class="nav-link active">
								<i class="far fa-circle nav-icon"></i>
								<p>Beranda</p>
							</a>
						</li>
					</ul>
				</li>

				<!--Gudang-->
				<li class="nav-item has-treeview">
					<a href="#" class="nav-link">
						<i class="nav-icon fas fa-warehouse"></i>
						<p>
							Gudang
							<i class="fas fa-angle-left right"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="<?php echo base_url('gudang') ?>" class="nav-link">
								<i class="far fa-circle nav-icon"></i>
								<p>Gudang Pusat</p>
							</a>
						</li>
					</ul>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<?php foreach ($_SESSION['menu'] as $menu) { ?>
								<a href="<?= base_url(); ?>_gudang/outlet/index/<?= $menu->id; ?>"
								   class="nav-link">
									<i class="far fa-circle nav-icon"></i>
									<p><?= $menu->name; ?></p>
								</a>
							<?php } ?>
						</li>
					</ul>
				</li>

				<!--Transaksi-->
				<li class="nav-item has-treeview">
					<a href="#" class="nav-link">
						<i class="nav-icon fas fa-cash-register"></i>
						<p>
							Transaksi
							<i class="fas fa-angle-left right"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<?php foreach ($_SESSION['menu'] as $menu) { ?>
							<li class="nav-item has-treeview">
								<a href="#" class="nav-link">
									<i class="far fa-circle nav-icon"></i>
									<p>
									<p><?= $menu->name; ?></p>
									<i class="right fas fa-angle-left"></i>
									</p>
								</a>
								<ul class="nav nav-treeview">
									<li class="nav-item">
										<a href="<?= base_url(); ?>transaksi/index/<?= $menu->id; ?>"
										   class="nav-link">
											<i class="far fa-dot-circle nav-icon"></i>
											<p>Riwayat Transaksi</p>
										</a>
									</li>
								</ul>
							</li>
						<?php } ?>
					</ul>
				</li>

				<!--Log Transaksi-->
				<li class="nav-item has-treeview">
					<a href="#" class="nav-link">
						<i class="nav-icon fas fa-history"></i>
						<p>
							Log Aktifitas
							<i class="fas fa-angle-left right"></i>
						</p>
					</a>

					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="<?= base_url(); ?>log/index/0" class="nav-link">
								<i class="far fa-circle nav-icon"></i>
								<p>Gudang Pusat</p>
							</a>
						</li>

						<li class="nav-item">
							<?php foreach ($_SESSION['menu'] as $menu) { ?>
								<a href="<?= base_url(); ?>log/outlet/<?= $menu->id; ?>"
								   class="nav-link">
									<i class="far fa-circle nav-icon"></i>
									<p><?= $menu->name; ?></p>
								</a>
							<?php } ?>
						</li>
					</ul>
				</li>

				<!--Manajemen-->
				<li class="nav-item has-treeview">
					<a href="#" class="nav-link">
						<i class="nav-icon fas fa-tasks"></i>
						<p>
							Manajemen
							<i class="fas fa-angle-left right"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="<?php echo base_url('pengguna') ?>" class="nav-link">
								<i class="far fa-circle nav-icon"></i>
								<p>Pengguna</p>
							</a>
						</li>
					</ul>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="<?php echo base_url('outlet') ?>" class="nav-link">
								<i class="far fa-circle nav-icon"></i>
								<p>Outlet</p>
							</a>
						</li>
					</ul>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="<?php echo base_url('karyawan') ?>" class="nav-link">
								<i class="far fa-circle nav-icon"></i>
								<p>pengguna Outlet</p>
							</a>
						</li>
					</ul>
				</li>

				<!--Master Data-->
				<li class="nav-item has-treeview">
					<a href="#" class="nav-link">
						<i class="nav-icon fas fa-database"></i>
						<p>
							Master Data
							<i class="fas fa-angle-left right"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="<?php echo base_url('wewenang') ?>" class="nav-link">
								<i class="far fa-circle nav-icon"></i>
								<p>Wewenang</p>
							</a>
						</li>
					</ul>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="<?php echo base_url('item') ?>" class="nav-link">
								<i class="far fa-circle nav-icon"></i>
								<p>Item</p>
							</a>
						</li>
					</ul>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="<?php echo base_url('kategori') ?>" class="nav-link">
								<i class="far fa-circle nav-icon"></i>
								<p>Kategori</p>
							</a>
						</li>
					</ul>
				</li>

				<!--Petunjuk impor-->
				<li class="nav-item has-treeview">
					<a href="#" class="nav-link">
						<i class="nav-icon fas fa-file-import"></i>
						<p>
							Manual Impor
							<i class="fas fa-angle-left right"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="<?php echo base_url('manual/produk_fisik') ?>" class="nav-link">
								<i class="far fa-circle nav-icon"></i>
								<p>Produk Fisik</p>
							</a>
						</li>
					</ul>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="<?php echo base_url('manual/pulsa_unit') ?>" class="nav-link">
								<i class="far fa-circle nav-icon"></i>
								<p>Pulsa Unit</p>
							</a>
						</li>
					</ul>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="<?php echo base_url('manual/pulsa_non_unit') ?>" class="nav-link">
								<i class="far fa-circle nav-icon"></i>
								<p>Pulsa Non Unit</p>
							</a>
						</li>
					</ul>
				</li>

				<!--Profile-->
				<li class="nav-item has-treeview">
					<a href="#" class="nav-link">
						<i class="nav-icon fas fa-user"></i>
						<p>
							Profile
							<i class="fas fa-angle-left right"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="<?php echo base_url('auth/Change_password') ?>" class="nav-link">
								<i class="far fa-circle nav-icon"></i>
								<p>Ubah Password</p>
							</a>
						</li>
					</ul>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="<?php echo base_url('auth/login/logout') ?>" class="nav-link">
								<i class="far fa-circle nav-icon"></i>
								<p>Keluar</p>
							</a>
						</li>
					</ul>
				</li>
			</ul>
		</nav>
	</div>
</aside>
