<!--Modal rekapan transfer dari gudang ke outlet-->

<!--Modal rekapan transfer dari outlet ke outlet-->

<!--Modal rekapan tranfer saldo pulsa non unit outlet-->

<!--modal detail tambah produk baru/hapus produk gudang-->
<div class="modal fade" id="modal_show_detail_new_product_gudang">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header"><h5>Detail Log Aktifitas</h5></div>
			<div class="modal-body">
				<div class="row mt-2">
					<div class="col-lg-4">Pengguna</div>
					<div class="col-lg-6" id="detail_log_new_product_username"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-4">Tipe</div>
					<div class="col-lg-6" id="detail_log_new_product_type"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-4">Kategori</div>
					<div class="col-lg-6" id="detail_log_new_product_category"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-4">Produk</div>
					<div class="col-lg-6" id="detail_log_new_product_name"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-4">Jumlah Stok</div>
					<div class="col-lg-6" id="detail_log_new_product_many"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Selesai</button>
			</div>
		</div>
	</div>
</div>

<!--modal detail tambah stok produk gudang-->
<div class="modal fade" id="modal_show_detail_add_stock_product_gudang">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header"><h5>Detail Log Aktifitas</h5></div>
			<div class="modal-body">
				<div class="row mt-2">
					<div class="col-lg-4">Pengguna</div>
					<div class="col-lg-6" id="detail_add_stock_product_username"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-4">Tipe</div>
					<div class="col-lg-6" id="detail_add_stock_product_type"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-4">Kategori</div>
					<div class="col-lg-6" id="detail_add_stock_product_category"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-4">Produk</div>
					<div class="col-lg-6" id="detail_add_stock_product_name"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-4">Stok Awal</div>
					<div class="col-lg-6" id="detail_add_stock_product_many"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-4">Jumlah +/-</div>
					<div class="col-lg-6" id="detail_add_stock_product_plus"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-4">Stok Akhir</div>
					<div class="col-lg-6" id="detail_add_stock_product_pre_plus"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Selesai</button>
			</div>
		</div>
	</div>
</div>

<!--modal detail edit stok produk gudang-->
<div class="modal fade" id="modal_show_detail_edit_stock_product_gudang">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header"><h5>Detail Log Aktifitas</h5></div>
			<div class="modal-body">
				<div class="row mt-2">
					<div class="col-lg-4">Pengguna</div>
					<div class="col-lg-6" id="detail_edit_stock_product_username"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-4">Tipe</div>
					<div class="col-lg-6" id="detail_edit_stock_product_type"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-4">Kategori</div>
					<div class="col-lg-6" id="detail_edit_stock_product_category"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-4">Produk</div>
					<div class="col-lg-6" id="detail_edit_stock_product_name"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-4">Modal</div>
					<div class="col-lg-6" id="detail_edit_stock_product_modal"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-4">Stok</div>
					<div class="col-lg-6" id="detail_edit_stock_product_pre_plus"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Selesai</button>
			</div>
		</div>
	</div>
</div>

<!--modal detail edit stok produk outlet-->
<div class="modal fade" id="modal_show_detail_edit_cost_product_outlet">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header"><h5>Detail Log Aktifitas</h5></div>
			<div class="modal-body">
				<div class="row mt-2">
					<div class="col-lg-4">Pengguna</div>
					<div class="col-lg-6" id="detail_edit_cost_product_username"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-4">Tipe</div>
					<div class="col-lg-6" id="detail_edit_cost_product_type"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-4">Kategori</div>
					<div class="col-lg-6" id="detail_edit_cost_product_category"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-4">Produk</div>
					<div class="col-lg-6" id="detail_edit_cost_product_name"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-4">Harga</div>
					<div class="col-lg-6" id="detail_edit_cost_product_cost"></div>
				</div>
				<div class="row mt-2 stok_product">
					<div class="col-lg-4">Stok</div>
					<div class="col-lg-6" id="detail_edit_cost_product_many"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Selesai</button>
			</div>
		</div>
	</div>
</div>

<!--modal detail edit diskon produk outlet-->
<div class="modal fade" id="modal_show_detail_edit_discount_product_outlet">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header"><h5>Detail Log Aktifitas</h5></div>
			<div class="modal-body">
				<div class="row mt-2">
					<div class="col-lg-4">Pengguna</div>
					<div class="col-lg-6" id="detail_edit_discount_product_username"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-4">Tipe</div>
					<div class="col-lg-6" id="detail_edit_discount_product_type"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-4">Kategori</div>
					<div class="col-lg-6" id="detail_edit_discount_product_category"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-4">Produk</div>
					<div class="col-lg-6" id="detail_edit_discount_product_name"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-4">Diskon</div>
					<div class="col-lg-6" id="detail_edit_discount_product_many"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Selesai</button>
			</div>
		</div>
	</div>
</div>

<!--modal detail hapus produk outlet-->
<div class="modal fade" id="modal_show_detail_delete_product_outlet">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header"><h5>Detail Log Aktifitas</h5></div>
			<div class="modal-body">
				<div class="row mt-2">
					<div class="col-lg-4">Pengguna</div>
					<div class="col-lg-6" id="detail_edit_delete_product_username"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-4">Tipe</div>
					<div class="col-lg-6" id="detail_edit_delete_product_type"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-4">Kategori</div>
					<div class="col-lg-6" id="detail_edit_delete_product_category"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-4">Produk</div>
					<div class="col-lg-6" id="detail_edit_delete_product_name"></div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-4">Stok</div>
					<div class="col-lg-6" id="detail_edit_delete_product_many"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Selesai</button>
			</div>
		</div>
	</div>
</div>

