<?php $this->load->view('_layouts/_template/header') ?>

<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div class="card card-outline card-primary">
				<div class="card-header">
					<h3 class="card-title"><b>pengguna</b></h3>
					<div class="float-right">
						<button class="btn btn-danger btn-sm" id="btn_reset_password_pengguna"><i
									class="fa fa-pencil-alt"></i> Reset Password
						</button>
						<button class="btn btn-primary btn-sm" id="btn_add_pengguna"><i class="fa fa-plus"></i> pengguna
						</button>
					</div>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table id="table_pengguna" class="table table-striped table-hover dt-responsive display nowrap"
							   style="width:100%">
							<thead>
							<tr>
								<th class="nomor" data-orderable="false">No</th>
								<th>Username</th>
								<th>Nama</th>
								<th>Wewenang</th>
								<th>Hp</th>
								<th>Alamat</th>
								<th data-orderable="false">Aksi</th>
							</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--modal tambah pengguna-->
<div class="modal fade" id="modal_add_pengguna" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Tambah pengguna</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="add_pengguna_form">
					<div class="form-group">
						<div class="col-lg-4">Nama</div>
						<div class="col-lg-12">
							<input type="text" class="form-control form-control-sm" id="add_pengguna_name"
								   name="add_pengguna_name">
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-4">Hp</div>
						<div class="col-lg-12">
							<input type="number" class="form-control form-control-sm" id="add_pengguna_phone"
								   name="add_pengguna_name">
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-4">Alamat</div>
						<div class="col-lg-12">
							<textarea class="form-control" id="add_pengguna_address"
									  name="add_pengguna_address"></textarea>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-4">Hak akses</div>
						<div class="col-lg-12">
							<select class="form-control form-control-sm" id="add_pengguna_role"
									name="add_pengguna_role">
							</select>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"
						id="save_add_pengguna">Simpan
				</button>
			</div>
		</div>
	</div>
</div>

<!--modal ubah pengguna-->
<div class="modal fade" id="modal_edit_pengguna" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Ubah pengguna</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="edit_pengguna_form">
					<div class="form-group">
						<div class="col-lg-4">Username</div>
						<div class="col-lg-12">
							<input type="text" class="form-control form-control-sm" id="edit_pengguna_username"
								   name="edit_pengguna_username">
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-4">Nama</div>
						<div class="col-lg-12">
							<input type="text" class="form-control form-control-sm" id="edit_pengguna_name"
								   name="edit_pengguna_name">
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-4">Hp</div>
						<div class="col-lg-12">
							<input type="number" class="form-control form-control-sm" id="edit_pengguna_phone"
								   name="edit_pengguna_phone">
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-4">Alamat</div>
						<div class="col-lg-12">
							<textarea class="form-control" id="edit_pengguna_address"
									  name="edit_pengguna_address"></textarea>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-4">Tipe</div>
						<div class="col-lg-12">
							<select class="form-control form-control-sm" id="edit_pengguna_role"
									name="edit_pengguna_role">
							</select>
						</div>
					</div>
					<div class="form-group" hidden>
						<div class="col-lg-4">Type</div>
						<div class="col-lg-12">
							<input type="text" class="form-control form-control-sm" id="edit_pengguna_id"
								   name="edit_pengguna_id"></div>
					</div>
				</form>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"
						id="save_edit_pengguna">Simpan
				</button>
			</div>
		</div>
	</div>
</div>

<!--modal reset password pengguna-->
<div class="modal fade" id="modal_reset_password_pengguna" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Reset Password Pengguna</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="reset_password_pengguna_form">

					<div class="form-group">
						<div class="col-lg-4">Pengguna</div>
						<div class="col-lg-12">

							<select class="form-control form-control-sm" id="reset_password_pengguna_id"
									name="reset_password_pengguna_id" required>
							</select>
						</div>
					</div>
				</form>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"
						id="btn_reset_password_pengguna_submit">Reset Password
				</button>
			</div>
		</div>
	</div>
</div>

<script>
	//init table pengguna
	$(document).ready(function () {
		get_user()
		get_user_roles()
		init_table_pengguna()
	})

	//validasi form reset password pengguna
	$("#reset_password_pengguna_form").validate({
		rules: {
			reset_password_pengguna_id: {
				required: true
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//modal reset password pengguna
	$("#btn_reset_password_pengguna").on('click', function () {
		$("#modal_reset_password_pengguna").modal({backdrop: 'static', keyboard: false})
	})

	//simpan reset password pengguna
	$('#btn_reset_password_pengguna_submit').click(function () {

		if ($('#reset_password_pengguna_form').valid()) {

			user_id = $('#reset_password_pengguna_id option:selected').val();

			nama = $('#reset_password_pengguna_id option:selected').text();

			Swal.fire({
				title: 'Peringatan!',
				text: 'Apakah Anda yakin akan mereset password pengguna ' + nama + ' ?',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Ya, reset password pengguna ini!'
			}).then((result) => {
				if (result.value) {

					$.ajax({
						url: "<?= base_url(); ?>pengguna/reset_password",
						type: "POST",
						data: {id: user_id},
						success(result) {

							if (result.success) {
								Swal.fire({
									title: 'Berhasil',
									text: result.message,
									icon: result.type,
									showConfirmButton: false,
									timer: 1500
								});

								init_table_pengguna()
							} else {
								Swal.fire({
									title: 'gagal',
									text: result.message,
									icon: result.type,
									showConfirmButton: false,
									timer: 1500
								});
								init_table_pengguna()
							}

						}
					})
				}
			})

		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});

			init_table_pengguna()
		}
	});

	//validasi form tambah data pengguna
	$("#add_pengguna_form").validate({
		rules: {
			add_pengguna_name: {
				required: true,
				maxlength: 30
			},
			add_pengguna_phone: {
				required: true,
				maxlength: 13,
				digits: true
			},
			add_pengguna_address: {
				required: true,
				maxlength: 100
			},
			add_pengguna_role: {
				required: true,
				maxlength: 10
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//modal tambah data pengguna
	$("#btn_add_pengguna").on('click', function () {
		$("#modal_add_pengguna").modal({backdrop: 'static', keyboard: false})
	})

	//simpan tambah data pengguna
	$("#save_add_pengguna").on('click', function () {

		if ($('#add_pengguna_form').valid()) {
			$.ajax({
				url: "<?= base_url(); ?>pengguna/add_pengguna",
				type: "POST",
				data: {
					name: $("#add_pengguna_name").val(),
					phone: $("#add_pengguna_phone").val(),
					address: $("#add_pengguna_address").val(),
					role: $("#add_pengguna_role").val()
				},
				success: function (result) {

					if (result.success) {
						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});
						init_table_pengguna()

					} else {
						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});
						init_table_pengguna()
					}
				}
			})
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});
		}
	})

	//validasi form edit data pengguna
	$("#edit_pengguna_form").validate({
		rules: {
			edit_pengguna_username: {
				required: true,
				maxlength: 5
			},
			edit_pengguna_name: {
				required: true,
				maxlength: 30
			},
			edit_pengguna_phone: {
				required: true,
				maxlength: 13,
				digits: true
			},
			edit_pengguna_address: {
				required: true,
				maxlength: 100
			},
			edit_pengguna_role: {
				required: true,
				maxlength: 10
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//modal ubah data pengguna
	$("#table_pengguna").on('click', 'tr #btn_edit_click', function () {

		tr = $(this).closest('tr')
		data = table_pengguna.row(tr).data()

		$("#edit_pengguna_id").val(data.id)
		$("#edit_pengguna_role").val(data.user_roles_id)
		$("#edit_pengguna_username").val(data.username)
		$("#edit_pengguna_name").val(data.name)
		$("#edit_pengguna_phone").val(data.hp)
		$("#edit_pengguna_address").val(data.address)
		$('#modal_edit_pengguna').modal({backdrop: 'static', keyboard: false})
	})

	//ubah data pengguna
	$("#save_edit_pengguna").on('click', function () {

		if ($('#edit_pengguna_form').valid()) {
			$.ajax({
				url: "<?= base_url(); ?>pengguna/edit_pengguna",
				type: "POST",
				data: {
					id: $("#edit_pengguna_id").val(),
					name: $("#edit_pengguna_name").val(),
					phone: $("#edit_pengguna_phone").val(),
					address: $("#edit_pengguna_address").val(),
					role: $("#edit_pengguna_role").val()
				},
				success: function (result) {

					if (result.success) {
						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						init_table_pengguna()

					} else {
						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						init_table_pengguna()
					}
				}
			})
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});

			init_table_pengguna()
		}

	})

	//hapus data pengguna
	$("#table_pengguna").on('click', 'tr #btn_delete_click', function () {
		tr = $(this).closest('tr')
		data = table_pengguna.row(tr).data()
		id = data.id

		Swal.fire({
			title: 'Peringatan!',
			text: 'Apakah Anda yakin akan menghapus pengguna ' + data.name + ' ?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya, hapus data ini!'
		}).then((result) => {
			if (result.value) {

				$.ajax({
					url: "<?= base_url(); ?>pengguna/delete_pengguna",
					type: "POST",
					data: {id: id},
					success(result) {

						if (result.success) {
							Swal.fire({
								title: 'Berhasil',
								text: result.message,
								icon: result.type,
								showConfirmButton: false,
								timer: 1500
							});

							init_table_pengguna()
						} else {
							Swal.fire({
								title: 'gagal',
								text: result.message,
								icon: result.type,
								showConfirmButton: false,
								timer: 1500
							});
							init_table_pengguna()
						}

					}
				})
			}
		})
	})

	//init datatables table pengguna
	function init_table_pengguna() {
		button = "&nbsp;<button class='btn btn-warning btn-sm' id='btn_edit_click'>\
            <i class='fas fa-edit'></i>&nbsp;Ubah</button>"
		button += "&nbsp;<button class='btn btn-danger btn-sm' id='btn_delete_click'>\
            <i class='fas fa-trash'></i>&nbsp;Hapus</button>"

		table_pengguna = $("#table_pengguna").DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			autoWidth: true,
			width: "0.8%",
			ajax: "<?= base_url(); ?>pengguna/get_all_pengguna",
			columns: [
				{
					data: "id",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: 'username'
				},
				{
					data: 'name'
				},
				{
					data: 'wewenang'
				},
				{
					data: 'hp'
				},
				{
					data: 'address'
				},
				{
					defaultContent: button
				},
			]
		})
	}

	//dropdown user roles
	function get_user_roles() {
		$.ajax({
			url: "<?= base_url(); ?>wewenang/get_wewenang",
			type: 'GET',
			success: function (result) {

				if (result.success) {
					var user_roles = '<option value="">Pilih...</option>'
					result.data.forEach(function (data) {
						user_roles += "<option value='" + data.id + "'>" + data.wewenang + "</option>";
					})

					$('#add_pengguna_role').html(user_roles)
					$('#edit_pengguna_role').html(user_roles)
				}
			}
		})
	}

	//dropdown pilih pengguna
	function get_user() {
		$.ajax({
			url: "<?= base_url(); ?>pengguna/get_pengguna",
			success: function (result) {
				if (result.success) {
					var user_list = '<option value="">Pilih...</option>'
					result.data.forEach(function (outlet) {
						if (outlet.wewenang !== 'KASIR') {
							user_list += "<option value='" + outlet['user_id'] + "'>" + "nama : " + outlet['name'] + " | wewenang : " + outlet['wewenang'] + "</option>";
						}
					})
					$('#reset_password_pengguna_id').html(user_list)
				}
			}
		})
	}

</script>


<?php $this->load->view('_layouts/_template/js') ?>
