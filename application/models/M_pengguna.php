<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pengguna extends CI_Model
{
	var $column_order = array('usr.id', 'usrle.id', 'usrle.wewenang', 'usr.name', 'usr.address', 'usr.hp');
	var $column_search = array('usr.id', 'usrle.id', 'usrle.wewenang', 'usr.name', 'usr.address', 'usr.hp');
	var $order = array('usr.name' => 'asc');

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_datatables()
	{
		$this->_get_datatables_query();

		if ($_REQUEST['length'] != -1)
			$this->db->limit($_REQUEST['length'], $_REQUEST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	private function _get_datatables_query()
	{
		$this->db->select('usr.id as id, usrle.id as user_roles_id, ,usrle.wewenang, username, address, name, hp, password')
			->from('user as usr')
			->join('user_roles as usrle', 'usr.user_roles_id = usrle.id')
			->where('usr.deleted_date is null')
			->where('usrle.deleted_date is null');

		$i = 0;
		foreach ($this->column_search as $item) {
			if ($_REQUEST['search']['value']) {

				if ($i === 0) {
					$this->db->group_start();
					$this->db->like($item, $_REQUEST['search']['value']);
				} else {
					$this->db->or_like($item, $_REQUEST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if (isset($_REQUEST['order'])) {
			$this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_all()
	{
		$this->db->select('usr.id as id, usrle.id as user_roles_id, ,usrle.wewenang, username, address, name, hp, password')
			->from('user as usr')
			->join('user_roles as usrle', 'usr.user_roles_id = usrle.id')
			->where('usr.deleted_date is null')
			->where('usrle.deleted_date is null');

		return $this->db->count_all_results();
	}

	function get_all_pengguna()
	{
		return $this->db->query("select user.id as user_id, user_roles.id as user_roles_id, user_roles.wewenang as wewenang, username, user.name from user, user_roles where user.user_roles_id = user_roles.id and user.deleted_date is null and user_roles.deleted_date is null")->result();
	}

	function get_pengguna_by_id($user_id)
	{
		return $this->db->query("select user.id as user_id, user_roles.id as user_roles_id, user_roles.wewenang as wewenang, username, user.name from user, user_roles where user.user_roles_id = user_roles.id and user.id = '$user_id' and user.deleted_date is null and user_roles.deleted_date is null")->result()[0];
	}

	function add_pengguna($username, $name, $hp, $address, $role, $password)
	{
		$created_by = created_by();
		$created_date = created_date();

		return $this->db->query("insert into user (username, user_roles_id, password, name, hp, address, created_by, created_date) values('$username','$role','$password','$name','$hp','$address','$created_by','$created_date')");
	}

	function edit_pengguna($id, $name, $hp, $address, $role)
	{
		$modified_by = modified_by();
		$modified_date = modified_date();

		return $this->db->query("update user set user_roles_id ='$role', name='$name', address='$address', hp='$hp', modified_by='$modified_by', modified_date='$modified_date' where id='$id' and deleted_date is null");
	}

	function reset_password_pengguna($id)
	{
		$modified_by = modified_by();
		$modified_date = modified_date();

		$user = $this->db->query("select id, username from user where id='$id' and deleted_date is null")->result();

		if (count($user) > 0) {
			$password = md5($user[0]->username);

			return $this->db->query("update user set password='$password', modified_by='$modified_by', modified_date='$modified_date' where id='$id' and deleted_date is null");
		} else {
			return false;
		}
	}

	function delete_pengguna($id)
	{
		$deleted_by = deleted_by();
		$deleted_date = deleted_date();
		return $this->db->query("update user set deleted_by='$deleted_by', deleted_date='$deleted_date' where id='$id' and deleted_date is null");
	}
}
