<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bluk extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_log', 'M_log', true);
		$this->load->model('kasir/_gudang/M_bluk', 'M_bluk', true);

		if (!isset($_SESSION)) {
			redirect('auth/login');
		} elseif ($_SESSION['status_user'] != 'KASIR') {
			redirect('auth/login');
		}
	}

	function get_all_bluk($outlet_id)
	{
		$result = $this->M_bluk->get_datatables($outlet_id);

		$response = [
			'success' => true,
			'draw' => $_REQUEST['draw'],
			'recordsTotal' => $this->M_bluk->count_all($outlet_id),
			'recordsFiltered' => $this->M_bluk->count_filtered($outlet_id),
			'data' => $result
		];

		return to_json($response);
	}

	function add_modal_bluk()
	{
		$outlet_id = $this->input->post('outlet_id');
		$product_id = $this->input->post('product_id');
		$modal = $this->input->post('modal');

		$result = $this->M_bluk->add_modal_bluk($product_id, $modal);

		if ($result) {

			$product = $this->M_log->get_item_outlet_category($product_id);
			$outlet = $this->M_log->get_outlet($outlet_id);

			$activity = "Outlet $outlet->name menambahkan saldo \"$product->name sebesar $modal\"";
			$this->M_log->insert_log($_SESSION['id'], $outlet_id, 'transaction', $result, $activity, '-', 'tambah_saldo_pulsa_outlet');

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Tambah modal berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Tambah modal gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function edit_modal_bluk()
	{
		$outlet_id = $this->input->post('outlet_id');
		$product_id = $this->input->post('product_id');
		$modal = $this->input->post('modal');

		$result = $this->M_bluk->edit_modal_bluk($product_id, $modal);

		if ($result) {

			$product = $this->M_log->get_item_outlet_category($product_id);
			$outlet = $this->M_log->get_outlet($outlet_id);

			$activity = "Outlet $outlet->name mengubah saldo \"$product->name sebesar $modal\"";
			$this->M_log->insert_log($_SESSION['id'], $outlet_id, 'transaction', $result, $activity, '-', 'ubah_saldo_pulsa_outlet');

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Ubah modal berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Ubah modal gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}
}
