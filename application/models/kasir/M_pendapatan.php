<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pendapatan extends CI_Model
{
	var $column_order = array('user.name', 'outlet.name', 'outlet_earning.total_uang_penjualan', 'outlet_earning.total_uang_cash', 'outlet_earning.total_uang_selisih', 'outlet_earning.note', 'outlet_earning.created_date');
	var $column_search = array('user.name', 'outlet.name', 'outlet_earning.total_uang_penjualan', 'outlet_earning.total_uang_cash', 'outlet_earning.total_uang_selisih', 'outlet_earning.note', 'outlet_earning.created_date');
	var $order = array('outlet_earning.created_date' => 'desc');

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_datatables($user_id, $outlet_id, $date)
	{
		$this->_get_datatables_query($user_id, $outlet_id, $date);

		if ($_REQUEST['length'] != -1)
			$this->db->limit($_REQUEST['length'], $_REQUEST['start']);
		$query = $this->db->get();

		return $query->result();
	}

	private function _get_datatables_query($user_id, $outlet_id, $date)
	{
		$this->db->select('user.id as user_id, user.name as user_name, outlet.id as outlet_id, outlet.name as outlet_name, FORMAT(outlet_earning.total_uang_penjualan,0) as total_uang_penjualan, FORMAT(outlet_earning.total_uang_cash,0) as total_uang_cash, FORMAT(outlet_earning.total_uang_selisih,0) as total_uang_selisih, outlet_earning.note as catatan, DATE_FORMAT(outlet_earning.created_date, "%d-%m-%Y - %h:%i") as created_date')
			->from('user, outlet, outlet_earning')
			->where('outlet_earning.user_id = user.id')
			->where('outlet_earning.outlet_id = outlet.id')
			->where('outlet_earning.deleted_date is null')
			->where('user.deleted_date is null')
			->where('outlet.deleted_date is null')
			->where('outlet_earning.user_id', $user_id)
			->where('outlet_earning.outlet_id', $outlet_id)
			->where('DATE(outlet_earning.created_date) BETWEEN "' . $date['start'] . '" and "' . $date['end'] . '"');

		$i = 0;
		foreach ($this->column_search as $item) {
			if ($_REQUEST['search']['value']) {

				if ($i === 0) {
					$this->db->group_start();
					$this->db->like($item, $_REQUEST['search']['value']);
				} else {
					$this->db->or_like($item, $_REQUEST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if (isset($_REQUEST['order'])) {
			$this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function count_filtered($user_id, $outlet_id, $date)
	{
		$this->_get_datatables_query($user_id, $outlet_id, $date);
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_all($user_id, $outlet_id, $date)
	{
		$this->db->select('user.id as user_id, user.name as user_name, outlet.id as outlet_id, outlet.name as outlet_name, FORMAT(outlet_earning.total_uang_penjualan,0) as total_uang_penjualan, FORMAT(outlet_earning.total_uang_cash,0) as total_uang_cash, FORMAT(outlet_earning.total_uang_selisih,0) as total_uang_selisih, outlet_earning.note as catatan, DATE_FORMAT(outlet_earning.created_date, "%d-%m-%Y - %h:%i") as created_date')
			->from('user, outlet, outlet_earning')
			->where('outlet_earning.user_id = user.id')
			->where('outlet_earning.outlet_id = outlet.id')
			->where('outlet_earning.deleted_date is null')
			->where('user.deleted_date is null')
			->where('outlet.deleted_date is null')
			->where('outlet_earning.user_id', $user_id)
			->where('outlet_earning.outlet_id', $outlet_id)
			->where('DATE(outlet_earning.created_date) BETWEEN "' . $date['start'] . '" and "' . $date['end'] . '"');

		return $this->db->count_all_results();
	}

	function add_laporan_pendapatan($user_id, $outlet_id, $total, $uang_cash, $selisih, $catatan)
	{
		$created_by = created_by();
		$created_date = created_date();

		return $this->db->query("insert into outlet_earning (user_id, outlet_id, total_uang_penjualan, total_uang_cash, total_uang_selisih, note, created_by, created_date) values('$user_id','$outlet_id','$total','$uang_cash','$selisih','$catatan','$created_by','$created_date')");
	}
}
