<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_produk extends CI_Model
{
	var $column_order = array('itm_prdk.id', 'itm_ctgy.id', 'itm_ctgy.name', 'itm_prdk.name', 'itm_prdk.modal', '.itm_prdk.many', 'itm_prdk.qrcode', 'itm_prdk.entry_date');
	var $column_search = array('itm_prdk.id', 'itm_ctgy.id', 'itm_ctgy.name', 'itm_prdk.name', 'itm_prdk.modal', 'itm_prdk.many', 'itm_prdk.qrcode', 'itm_prdk.entry_date');
	var $order = array('itm_prdk.name' => 'asc');

	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$CI =& get_instance();
		$CI->load->model('M_log');
	}

	private function _get_datatables_query($type_id)
	{
		if ($type_id != 0) {

			$this->db->select('format(itm_prdk.modal,0) as modal, itm_prdk.id, itm_prdk.name as product_name, format(itm_prdk.many,0) as many, itm_prdk.qrcode, DATE_FORMAT(itm_prdk.entry_date, "%d-%m-%Y") as entry_date, itm_ctgy.name as category_name')
				->from('item_product as itm_prdk, item_category as itm_ctgy')
				->where('itm_prdk.item_category_id = itm_ctgy.id')
				->where('itm_prdk.deleted_date is null')
				->where('itm_ctgy.deleted_date is null')
				->where('itm_ctgy.item_type_id', $type_id);

		} else {

			$this->db->select('format(itm_prdk.modal,0) as modal, itm_prdk.id, itm_prdk.name as product_name, format(itm_prdk.many,0) as many, itm_prdk.qrcode, DATE_FORMAT(itm_prdk.entry_date, "%d-%m-%Y") as entry_date, itm_ctgy.name as category_name')
				->from('item_product as itm_prdk, item_category as itm_ctgy')
				->where('itm_prdk.item_category_id = itm_ctgy.id')
				->where('itm_prdk.deleted_date is null')
				->where('itm_ctgy.deleted_date is null');
		}

		$i = 0;
		foreach ($this->column_search as $item) {
			if ($_REQUEST['search']['value']) {

				if ($i === 0) {
					$this->db->group_start();
					$this->db->like($item, $_REQUEST['search']['value']);
				} else {
					$this->db->or_like($item, $_REQUEST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if (isset($_REQUEST['order'])) {
			$this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($type_id)
	{
		$this->_get_datatables_query($type_id);

		if ($_REQUEST['length'] != -1)
			$this->db->limit($_REQUEST['length'], $_REQUEST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($type_id)
	{
		$this->_get_datatables_query($type_id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_all($type_id)
	{
		if ($type_id != 0) {

			$this->db->select('format(itm_prdk.modal,0) as modal, itm_prdk.id, itm_prdk.name as product_name, format(itm_prdk.many,0) as many, itm_prdk.qrcode, DATE_FORMAT(itm_prdk.entry_date, "%d-%m-%Y") as entry_date, itm_ctgy.name as category_name')
				->from('item_product as itm_prdk, item_category as itm_ctgy')
				->where('itm_prdk.item_category_id = itm_ctgy.id')
				->where('itm_prdk.deleted_date is null')
				->where('itm_ctgy.deleted_date is null')
				->where('itm_ctgy.item_type_id', $type_id);

		} else {

			$this->db->select('format(itm_prdk.modal,0) as modal, itm_prdk.id, itm_prdk.name as product_name, format(itm_prdk.many,0) as many, itm_prdk.qrcode, DATE_FORMAT(itm_prdk.entry_date, "%d-%m-%Y") as entry_date, itm_ctgy.name as category_name')
				->from('item_product as itm_prdk, item_category as itm_ctgy')
				->where('itm_prdk.item_category_id = itm_ctgy.id')
				->where('itm_prdk.deleted_date is null')
				->where('itm_ctgy.deleted_date is null');
		}

		return $this->db->count_all_results();
	}

	function add_product($category_id, $name, $modal, $many, $product_image, $entry_date)
	{
		$created_by = created_by();
		$created_date = created_date();

		$this->db->trans_begin();

		$check = $this->db->query("select id from item_product where name='$name' and
        item_category_id='$category_id' and deleted_date is null")->result();

		if ($check) return false;
		else {
			$this->db->query("insert into item_product (item_category_id, name, modal, many, qrcode, entry_date, created_by, created_date)
            values('$category_id','$name','$modal','$many', '$product_image','$entry_date','$created_by', '$created_date')");

			$product_id = $this->db->insert_id();

			$CI =& get_instance();
			$CI->load->model('M_log_produk');
			$detail = $CI->M_log_produk->get_detail_product($product_id);

			$activity = "Gudang pusat menambahkan produk baru \"$detail->type_name $detail->product_name sebanyak $many pcs\"";
			$CI->M_log_produk->insert_log_product($_SESSION['id'], 16, 'item_product', $product_id, $modal, NULL, NULL, NULL, $many, NULL, $activity, '-', 'add_new_product_gudang');

			$this->db->trans_complete();

			return true;
		}
	}

	function add_stock_product($product_id, $many)
	{
		$modified_by = modified_by();
		$modified_date = modified_date();

		$this->db->trans_begin();

		$product = $this->db->query("select many from item_product where id='$product_id' and deleted_date is null")->result();

		if (count($product) > 0) {
			$total = $product[0]->many + $many;

			$this->db->query("update item_product set many='$total', modified_by='$modified_by', modified_date='$modified_date' where id='$product_id' and deleted_date is null");

			$CI =& get_instance();
			$CI->load->model('M_log_produk');
			$detail = $CI->M_log_produk->get_detail_product($product_id);

			$activity = "Gudang pusat menambahkan stok \"$detail->type_name $detail->category_name $detail->product_name sebanyak $many pcs\"";
			$CI->M_log_produk->insert_log_product($_SESSION['id'], 16, 'item_product', $product_id, NULL, NULL, $product[0]->many, $many, $total, NULL, $activity, '-', 'add_stock_product_gudang');

			$this->db->trans_complete();

			return true;

		} else {
			return false;
		}
	}

	function edit_product($name, $many, $modal, $id, $note)
	{
		$modified_by = modified_by();
		$modified_date = modified_date();

		$this->db->trans_begin();

		$this->db->query("update item_product set name='$name', many='$many', modal='$modal', modified_by='$modified_by', modified_date='$modified_date' where id='$id' and deleted_date is null");

		$CI =& get_instance();
		$CI->load->model('M_log_produk');
		$detail = $CI->M_log_produk->get_detail_product($id);

		$activity = "Gudang pusat mengubah produk \"$detail->type_name $detail->category_name $detail->product_name modal $modal dan banyak $many\"";
		$CI->M_log_produk->insert_log_product($_SESSION['id'], 5, 'item_product', $id, $modal, NULL, NULL, NULL, $many, NULL, $activity, $note, 'edit_stock_product_gudang');

		$this->db->trans_complete();

		return true;
	}

	function delete_product($id)
	{
		$deleted_by = deleted_by();
		$deleted_date = deleted_date();

		$this->db->trans_begin();

		$unlink = $this->db->query("select qrcode, many from item_product where id='$id' and deleted_date is null")->result();

		if (count($unlink) > 0) {

			unlink("uploads/qrcode/" . $unlink[0]->qrcode);

			$CI =& get_instance();
			$CI->load->model('M_log_produk');
			$detail = $CI->M_log_produk->get_detail_product($id);

			$activity = "Gudang pusat menghapus produk \"$detail->type_name $detail->category_name $detail->product_name\"";
			$CI->M_log_produk->insert_log_product($_SESSION['id'], 16, 'item_product', $id, NULL, NULL, NULL, NULL, $unlink[0]->many, NULL, $activity, '-', 'delete_product_gudang');

			$this->db->query("update item_product set deleted_by='" . $deleted_by . "', deleted_date = '" . $deleted_date . "' where id='$id' and deleted_date is null");

			$this->db->trans_complete();

			return true;
		} else {
			return false;
		}
	}

}
