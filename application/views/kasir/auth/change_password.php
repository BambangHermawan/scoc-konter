<?php $this->load->view('_layouts/_template/header') ?>

<div class="container-fluid">

	<div class="row">
		<div class="col-12">
			<div class="card card-outline card-primary">
				<div class="card-header">
					<h3 class="card-title"><b>Ubah password</b></h3>
				</div>
				<div class="card-body">
					<div class="col-md-6">
						<form id="change_password_form">
							<div class="form-group">
								<div class="col-lg-4">Password sekarang</div>
								<div class="col-lg-12">
									<input type="password" class="form-control" id="edit_current_password"
										   name="edit_current_password">
								</div>
							</div>

							<div class="form-group">
								<div class="col-lg-4">Password baru</div>
								<div class="col-lg-12">
									<input type="password" class="form-control" id="edit_new_password"
										   name="edit_new_password">
								</div>
							</div>

							<div class="form-group">
								<div class="col-lg-4">Konfirmasi password baru</div>
								<div class="col-lg-12">
									<input type="password" class="form-control" id="edit_confirm_password"
										   name="edit_confirm_password">
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="card-footer">
					<button type="button" class="btn btn-primary" id="submit" name="submit"><i
								class='fa fa-save'></i>
						Ubah password
					</button>
					<a href="<?= base_url(); ?>beranda" class="btn btn-danger text-white"><i
								class='fa fa-reply'></i> Beranda</a>
				</div>
			</div>
		</div>
	</div>
</div>

<script>

	//validasi login form
	$("#change_password_form").validate({
		rules: {
			edit_current_password: {
				required: true,
				maxlength: 100
			},
			edit_new_password: {
				required: true,
				maxlength: 100
			},
			edit_confirm_password: {
				required: true,
				maxlength: 100,
				equalTo: '#edit_new_password'
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				element.next().after(error);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//check login
	$("#submit").on('click', function () {

		if ($('#change_password_form').valid()) {
			$.ajax({
				url: "<?= base_url(); ?>kasir/auth/Change_password/change_password",
				type: 'POST',
				data: {
					'current_password': $("#edit_current_password").val(),
					'confirm_password': $("#edit_confirm_password").val()
				},
				success: function (result) {

					if (result.success) {
						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: true,
						}).then(function () {
							window.location.href = "<?= base_url(); ?>auth/login/logout";
						});


					} else {

						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: true,
						}).then(function () {
							window.location.href = "<?= base_url(); ?>auth/change_password";
						});

					}

				}
			});
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});
		}

	});
</script>

<?php $this->load->view('_layouts/_template/js') ?>
