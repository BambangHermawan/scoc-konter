<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class Gudang
 */
class M_export extends CI_Model
{
	function get_all_product_by_item_type_id($item_type_id)
	{
		if ($item_type_id != 0) {
			return $this->db->query("
                select item_type.name as item_type_name, item_category.name as item_category_name, item_product.name as item_product_name, modal, many, entry_date
				from item_product, item_category, item_type
                where item_product.item_category_id = item_category.id 
                and item_category.item_type_id = item_type.id 
                and item_type.id='$item_type_id'
                and item_product.deleted_date is null
                and item_category.deleted_date is null
                and item_type.deleted_date is null
                ")->result();
		}

		return $this->db->query("
                select item_type.name as item_type_name, item_category.name as item_category_name, item_product.name as item_product_name, modal, many, entry_date
				from item_product, item_category, item_type
                where item_product.item_category_id = item_category.id 
                and item_category.item_type_id = item_type.id
                and item_product.deleted_date is null
                and item_category.deleted_date is null
                and item_type.deleted_date is null
                ")->result();
	}

	function get_all_product_category_by_item_type_id($item_type_id)
	{
		if ($item_type_id != 0) {
			return $this->db->query("
                select item_type.name as item_type_name, item_category.name as item_category_name, item_product.name as item_product_name, modal, many, entry_date
				from item_product, item_category, item_type
                where item_product.item_category_id = item_category.id 
                and item_category.item_type_id = item_type.id 
                and item_type.id='$item_type_id'
                and item_product.deleted_date is null
                and item_category.deleted_date is null
                and item_type.deleted_date is null
                group by item_category.name ASC
                ")->result();
		}

		return $this->db->query("
                select item_type.name as item_type_name, item_category.name as item_category_name, item_product.name as item_product_name, modal, many, entry_date
				from item_product, item_category, item_type
                where item_product.item_category_id = item_category.id 
                and item_category.item_type_id = item_type.id
                and item_product.deleted_date is null
                and item_category.deleted_date is null
                and item_type.deleted_date is null
                group by item_category.name ASC
                ")->result();
	}

	function get_item_outlet_pulsa_modal_by_outlet_id($outlet_id)
	{
		return $this->db->query("
		select  outlet.id as outlet_id, outlet.name as outlet_name, item_outlet_category.id as item_outlet_category_id, item_outlet_category.name as item_outlet_category_name, item_type.id as item_type_id, item_type.name as item_type_name, item_outlet_category.modal as item_category_modal
		from item_outlet_category, item_type, outlet
		where item_outlet_category.type_id = item_type.id
		and item_outlet_category.outlet_id = outlet.id
		and item_type.id = 4
		and outlet_id = '$outlet_id'
		and item_outlet_category.deleted_date is null
        and item_type.deleted_date is null
        and outlet.deleted_date is null
		")->result();
	}

	function get_item_outlet_pulsa_by_outlet_id($outlet_id)
	{
		return $this->db->query("
		SELECT item_outlet_category.name as item_outlet_category_name, item_outlet.unit as unit, outlet.name as outlet_name
		FROM scoc_konter.item_outlet_category, item_outlet, outlet, item_type 
		where item_outlet_category.id = item_outlet.item_outlet_category_id
		and item_outlet_category.outlet_id = outlet.id
		and item_outlet_category.type_id = item_type.id
		and outlet_id = '$outlet_id'
		")->result();
	}
}
