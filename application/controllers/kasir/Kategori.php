<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kategori extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('kasir/M_kategori', 'M_kategori', true);

		if (!isset($_SESSION)) {
			redirect('auth/login');
		} elseif ($_SESSION['status_user'] != 'KASIR') {
			redirect('auth/login');
		}
	}

	function get_all_category()
	{
		$result = $this->M_kategori->get_datatables();

		$response = [
			'success' => true,
			'draw' => $_REQUEST['draw'],
			'recordsTotal' => $this->M_kategori->count_all(),
			'recordsFiltered' => $this->M_kategori->count_filtered(),
			'data' => $result
		];

		return to_json($response);
	}

	function get_category_by_id($item_type_id)
	{
		$result = $this->M_kategori->get_category_by_id($item_type_id);

		if ($result) {

			$response = [
				'success' => true,
				'data' => $result,
				'total' => count($result)
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function get_category_by_outlet_id($outlet_id)
	{
		$result = $this->M_kategori->get_category_by_outlet_id($outlet_id);

		if ($result) {

			$response = [
				'success' => true,
				'data' => $result,
				'total' => count($result)
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}
}
