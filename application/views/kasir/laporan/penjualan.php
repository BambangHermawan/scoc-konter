<?php $this->load->view('_layouts/_template/header') ?>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-3">
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Filter Laporan <b><?= $outlet[0]->name ?></b></h3>
				</div>
				<div class="card-body p-2">
					<div class="form-group">
						<div class="col-lg-4"><label for="date_begin">Mulai</label></div>
						<div class="col-lg-12">
							<input type="date" class="form-control form-control-sm"
								   name="date_begin"
								   id="date_begin" required>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4"><label for="date_end">Sampai</label></div>
						<div class="col-lg-12">
							<input type="date" class="form-control form-control-sm"
								   name="date_end"
								   id="date_end" required>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4"><label for="outlet_employee_id">Pengguna</label></div>
						<div class="col-lg-12">
							<select type="date" class="form-control form-control-sm"
									name="outlet_employee_id"
									id="outlet_employee_id">
							</select>
						</div>
					</div>

				</div>
				<div class="card-footer">
					<div class="float-right">
						<a class="btn btn-sm btn-warning" id="print_laporan"><i
									class="fa fa-print"></i> Cetak Laporan</a>
						<button type="submit" class="btn btn-primary btn-sm" id="btn_search_all">Filter
							Data
						</button>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-9">
			<div class="card card-primary card-outline">
				<div class="card-header">
					<h3 class="card-title">Laporan Penjualan</b></h3>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table id="table_laporan"
							   class="table table-striped table-hover dt-responsive display nowrap"
							   style="width:100%">
							<thead>
							<tr>
								<th class="nomor" data-orderable="false">No</th>
								<th>Kategori</th>
								<th>Produk</th>
								<th>Harga</th>
								<th>Stok</th>
								<th>Terjual</th>
								<th>Sisa Stok</th>
							</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	//init laporan
	$(document).ready(function () {
		var today = new Date();
		date = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2)

		$("#date_begin").val(date)
		$("#date_end").val(date)

		init_table_laporan()
		get_outlet_employee()
	})

	//print laporan
	$('#print_laporan').click(function () {

		user_id = $('#outlet_employee_id option:selected').val();
		outlet_id = "<?= $_SESSION['outlet_id'] ?>";

		link = "<?= base_url(); ?>kasir/laporan/outlet/" + user_id + "/" + outlet_id + "/" + $('#date_begin').val() + "/" + $('#date_end').val()
		window.open(link, '_blank');
	})

	//filter laporan
	$('#btn_search_all').click(function () {

		var user_id = $('#outlet_employee_id option:selected').val();

		table_laporan = $("#table_laporan").DataTable({
			destroy: true,
			autoWidth: true,
			ajax: "<?= base_url(); ?>kasir/transaksi/get_transaction_detail_by_date/" + user_id + "/" + "<?= $this->uri->segment(4); ?>" + "/" + $('#date_begin').val() + "/" + $('#date_end').val(),
			columns: [
				{
					data: "id",
					width: "0.8%",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: 'category'
				},
				{
					data: 'product'
				},
				{
					data: 'cost'
				},
				{
					data: 'awl'
				},
				{
					data: 'lk'
				},
				{
					data: 'ak'
				}
			]
		})
	})

	//init datatables table laporan
	function init_table_laporan() {

		var user_id = "<?= $_SESSION['id'] ?>";

		button = "&nbsp;<button class='btn btn-info btn-sm' id='btn_detail_transaction_click'>\
            <i class='fas fa-search'></i>&nbsp;Detail</button>"

		table_laporan = $("#table_laporan").DataTable({
			destroy: true,
			autoWidth: true,
			ajax: "<?= base_url(); ?>kasir/transaksi/get_transaction_detail_by_date/" + user_id + "/" + "<?= $this->uri->segment(4); ?>" + "/" + $('#date_begin').val() + "/" + $('#date_end').val(),
			columns: [
				{
					data: "id",
					width: "0.8%",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: 'category'
				},
				{
					data: 'product'
				},
				{
					data: 'cost'
				},
				{
					data: 'awl'
				},
				{
					data: 'lk'
				},
				{
					data: 'ak'
				}
			]
		})
	}

	//dropdown pilih pengguna outlet
	function get_outlet_employee() {
		user_id = "<?= $_SESSION['id'] ?>"
		outlet_id = "<?= $this->uri->segment(4) ?>"

		$.ajax({
			url: "<?= base_url(); ?>kasir/karyawan/get_karyawan/" + outlet_id,
			success: function (result) {
				if (result.success) {

					var user_list = ''

					result.data.forEach(function (outlet) {
						if (outlet['wewenang'] !== 'ADMIN' && outlet['wewenang'] !== 'OWNER' && outlet['outlet_id'] === outlet_id && outlet['user_id'] === user_id) {
							user_list += "<option value='" + outlet['user_id'] + "'>" + outlet['name'] + "</option>";
						}
					})
					$('#outlet_employee_id').html(user_list)
				}
			}
		})
	}

	//konversi angka ke format rupiah
	function to_rupiah(angka) {
		var rev = parseInt(angka, 10).toString().split('').reverse().join('');

		var rev2 = '';
		for (var i = 0; i < rev.length; i++) {
			rev2 += rev[i];
			if ((i + 1) % 3 === 0 && i !== (rev.length - 1)) {
				rev2 += ',';
			}
		}

		return 'Rp. ' + rev2.split('').reverse().join('');
	}

</script>

<?php $this->load->view('_layouts/_template/js') ?>
