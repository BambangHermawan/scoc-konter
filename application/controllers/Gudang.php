<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Gudang extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_gudang', '', true);
		$this->load->model('M_log', '', true);

		if (!isset($_SESSION)) {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'ADMIN') {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'OWNER') {
			redirect('auth/login');
		} elseif ($_SESSION['status_user'] == 'KASIR') {
			redirect('auth/login');
		}
	}

	function index()
	{
		$data['title'] = 'Gudang Pusat';
		$data['site_navigation'] = 'Gudang';

		$this->load->view('gudang/pusat/index', $data);
	}

	function transfer_from_pusat()
	{
		$product_id = $this->input->post('product_id');
		$outlet_id = $this->input->post('outlet_id');
		$many = $this->input->post('many');
		$cost = $this->input->post('cost');

		$this->load->library('ciqrcode');

		$config['cacheable'] = true;
		$config['cachedir'] = 'uploads/';
		$config['errorlog'] = 'uploads/';
		$config['imagedir'] = 'uploads/qrcode/';
		$config['quality'] = true;
		$config['size'] = '1024';
		$config['black'] = array(224, 255, 255);
		$config['white'] = array(70, 130, 180);
		$this->ciqrcode->initialize($config);

		$product_image = md5(time() . '_' . uniqid()) . '.png';

		$result = $this->M_gudang->transfer_from_pusat($product_id, $outlet_id, $many, $cost, $product_image);

		if ($result) {

			$params['data'] = $product_image;
			$params['level'] = 'H';
			$params['size'] = 10;
			$params['savename'] = FCPATH . $config['imagedir'] . $product_image;
			$this->ciqrcode->generate($params);

			$product = $this->M_log->get_detail_product($product_id);
			$outlet = $this->M_log->get_outlet($outlet_id);

			$activity = "Gudang pusat transfer \"$product->type_name $product->category_name $product->product_name sebanyak $many pcs\" ke outlet $outlet->name";
			$this->M_log->insert_log($_SESSION['id'], $outlet_id, 'transaction', $result, $activity, '-', 'transfer_from_gudang');

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Transfer produk berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Jumlah barang tidak mencukupi.',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function salah_transfer_from_pusat()
	{
		$product_id = $this->input->post('product_id');
		$outlet_id = $this->input->post('outlet_id');
		$many = $this->input->post('many');

		$result = $this->M_gudang->salah_transfer_from_pusat($product_id, $outlet_id, $many);

		if ($result) {

			$product = $this->M_log->get_detail_product($product_id);
			$outlet = $this->M_log->get_outlet($outlet_id);

			$activity = "Gudang pusat menarik barang \"$product->type_name $product->category_name $product->product_name sebanyak $many pcs\" dari outlet $outlet->name";
			$this->M_log->insert_log($_SESSION['id'], $outlet_id, 'transaction', $result, $activity, '-', 'return_from_outlet_to_gudang');

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Data salah transfer produk berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Data salah transfer produk gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}
}
