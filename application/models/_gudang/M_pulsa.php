<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pulsa extends CI_Model
{
	var $column_order = array('itm_otlt.id', 'itm_otlt_ctgy.id', 'itm_otlt_ctgy.name', 'itm_otlt.name', 'itm_otlt.modal', '.itm_otlt.many', 'itm_otlt.qrcode', 'otlt.name');
	var $column_search = array('itm_otlt.id', 'itm_otlt_ctgy.id', 'itm_otlt_ctgy.name', 'itm_otlt.name', 'itm_otlt.modal', 'itm_otlt.many', 'itm_otlt.qrcode', 'otlt.name');
	var $order = array('itm_otlt.name' => 'asc');

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		$this->db->select('itm_otlt.id, itm_otlt.item_outlet_category_id, itm_otlt.name, itm_otlt.unit, format(itm_otlt.modal,0) as modal, format(itm_otlt.cost,0) as cost, DATE_FORMAT(itm_otlt.discount, "%d-%m-%Y") as discount, format(itm_otlt.many,0) as many, itm_otlt.qrcode, itm_otlt_ctgy.name as category_name, otlt.name as outlet_name')
			->from('item_outlet as itm_otlt, item_outlet_category as itm_otlt_ctgy')
			->where('itm_otlt.item_outlet_category_id = itm_otlt_ctgy.id')
			->join('outlet as otlt', 'itm_otlt_ctgy.outlet_id = otlt.id')
			->where('itm_otlt.deleted_date is null')
			->where('itm_otlt_ctgy.deleted_date is null');

		$i = 0;
		foreach ($this->column_search as $item) {
			if ($_REQUEST['search']['value']) {

				if ($i === 0) {
					$this->db->group_start();
					$this->db->like($item, $_REQUEST['search']['value']);
				} else {
					$this->db->or_like($item, $_REQUEST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if (isset($_REQUEST['order'])) {
			$this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();

		if ($_REQUEST['length'] != -1)
			$this->db->limit($_REQUEST['length'], $_REQUEST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_all()
	{
		$this->db->select('itm_otlt.id, itm_otlt.item_outlet_category_id, itm_otlt.name, itm_otlt.unit, format(itm_otlt.modal,0) as modal, format(itm_otlt.cost,0) as cost, DATE_FORMAT(itm_otlt.discount, "%d-%m-%Y") as discount, format(itm_otlt.many,0) as many, itm_otlt.qrcode, itm_otlt_ctgy.name as category_name, otlt.name as outlet_name')
			->from('item_outlet as itm_otlt, item_outlet_category as itm_otlt_ctgy')
			->where('itm_otlt.item_outlet_category_id = itm_otlt_ctgy.id')
			->join('outlet as otlt', 'itm_otlt_ctgy.outlet_id = otlt.id')
			->where('itm_otlt.deleted_date is null')
			->where('itm_otlt_ctgy.deleted_date is null');

		return $this->db->count_all_results();
	}

	function edit_pulsa($product, $category, $modal, $cost)
	{
		return $this->db->query("update item_outlet io, item_outlet_category ioc set io.cost='$cost', io.modal='$modal'
        where io.name='$product' and ioc.name='$category' and io.item_outlet_category_id=ioc.id and io.deleted_date is null and ioc.deleted_date is null");
	}

	function delete_pulsa($id)
	{
		$deleted_by = deleted_by();
		$deleted_date = deleted_date();

		return $this->db->query("update item_outlet set deleted_by='$deleted_by', deleted_date='$deleted_date' where id='$id' and deleted_date is null");
	}
}
