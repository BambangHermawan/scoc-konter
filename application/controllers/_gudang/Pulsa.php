<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pulsa extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('_gudang/M_pulsa', 'M_pulsa', true);

		if (!isset($_SESSION)) {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'ADMIN') {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'OWNER') {
			redirect('auth/login');
		} elseif ($_SESSION['status_user'] == 'KASIR') {
			redirect('auth/login');
		}
	}

	function get_all_pulsa()
	{
		$result = $this->M_pulsa->get_datatables();

		$response = [
			'success' => true,
			'draw' => $_REQUEST['draw'],
			'recordsTotal' => $this->M_pulsa->count_all(),
			'recordsFiltered' => $this->M_pulsa->count_filtered(),
			'data' => $result
		];

		return to_json($response);
	}

	function edit_pulsa()
	{
		$product = $this->input->post('product');
		$category = $this->input->post('category');
		$modal = $this->input->post('modal');
		$cost = $this->input->post('cost');

		$result = $this->M_pulsa->edit_pulsa($product, $category, $modal, $cost);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Ubah produk pulsa ' . $product . ' berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Ubah produk pulsa' . $product . ' gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function delete_pulsa()
	{
		$id = $this->input->post('id');

		$result = $this->M_pulsa->delete_pulsa($id);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Hapus pulsa berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Hapus produk pulsa gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}
}
