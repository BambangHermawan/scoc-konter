<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_auth extends CI_Model
{
	function check_login($role, $username, $password)
	{
		return $this->db->query("select user.id, username, name, address, hp, user_roles_id, user_roles.wewenang as role from user, user_roles where user.user_roles_id = user_roles.id and user_roles_id='$role' and username='$username' and password='$password' and user.deleted_date is null and user_roles.deleted_date is null")->result();
	}

	function check_login_kasir($role, $username, $password)
	{
		return $this->db->query("select outlet_employee.id as outlet_employee_id, user.id as user_id, user.name as name, user.username as username, user.address as address, user.hp as hp, outlet.id as outlet_id, outlet.name as outlet_name, user_roles.id as user_roles_id, user_roles.wewenang as role 
		from outlet_employee, outlet, user, user_roles
		where outlet_employee.user_id = user.id 
		and outlet_employee.outlet_id = outlet.id 
		and user_roles.id = user.user_roles_id
		and user.username = '$username' 
		and outlet_employee.password = '$password' 
		and user.user_roles_id = '$role' 
		and outlet_employee.deleted_date is null 
		and outlet.deleted_date is null 
		and user.deleted_date is null 
		and user_roles.deleted_date is null 
		and user.deleted_date is null")->result();
	}

	function change_password($id, $current_password, $confirm_password)
	{
		$check = $this->db->query("select id from user where id='$id' and password='$current_password' and deleted_date is null")->result();

		if ($check == false) return false;
		else {
			$modified_by = modified_by();
			$modified_date = modified_date();
			return $this->db->query("update user set password='$confirm_password', modified_by='$modified_by', modified_date='$modified_date' where id='$id' and deleted_date is null");
		}
	}

	function change_password_kasir($outlet_employee_id, $user_id, $current_password, $confirm_password)
	{
		$check = $this->db->query("select id from outlet_employee where id='$outlet_employee_id' and user_id='$user_id' and password='$current_password' and deleted_date is null")->result();

		if ($check == false) return false;
		else {
			$modified_by = modified_by();
			$modified_date = modified_date();
			return $this->db->query("update outlet_employee set password='$confirm_password', modified_by='$modified_by', modified_date='$modified_date' where id='$outlet_employee_id' and user_id='$user_id' and deleted_date is null");
		}
	}

//	function get_roles_by_id($role)
//	{
//		return $this->db->query("select id, wewenang from user_roles where id='$role' and deleted_date is null")->result();
//	}

}
