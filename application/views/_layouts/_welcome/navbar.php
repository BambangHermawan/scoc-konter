<!-- Navbar -->
<nav class="main-header navbar navbar-expand-md navbar-dark navbar-purple text-sm">
	<div class="container">
		<a href="<?php echo base_url('auth/login'); ?>" class="navbar-brand">
			<img src="<?php echo base_url('assets/image/logo.png') ?>" alt="AdminLTE Logo"
				 class="brand-image img-circle elevation-3"
				 style="opacity: .8">
			<span class="brand-text font-weight-light">SC.OC Cellular</span>
		</a>

		<button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse"
				aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<!-- Right navbar links -->
		<ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
			<!-- Notifications Dropdown Menu -->
			<li class="nav-item dropdown">
				<a class="nav-link" data-toggle="dropdown" href="#">
					<i class="fa fa-th-large"></i>
				</a>
				<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right text-center">
					<a href="<?php echo base_url('/auth/login') ?>" class="dropdown-item text-left">
						<i class="fas fa-sign-in-alt mr-2"></i> LOGIN
					</a>
				</div>
			</li>
		</ul>
	</div>
</nav>
<!-- /.navbar -->
