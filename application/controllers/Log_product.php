<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Log_product extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_laporan', '', true);
		$this->load->model('M_log_produk', '', true);

		if (!isset($_SESSION)) {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'ADMIN') {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'OWNER') {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'KASIR') {
			redirect('auth/login');
		}
	}

	function get_all_log_product($user_id, $outlet_id, $start_date, $end_date)
	{
		$filter = $this->generate_date_filter($start_date, $end_date);

		$result = $this->M_log_produk->get_datatables($user_id, $outlet_id, $filter);

		$response = [
			'success' => true,
			'draw' => $_REQUEST['draw'],
			'recordsTotal' => $this->M_log_produk->count_all($user_id, $outlet_id, $filter),
			'recordsFiltered' => $this->M_log_produk->count_filtered($user_id, $outlet_id, $filter),
			'data' => $result
		];

		return to_json($response);
	}

	function get_detail_log_activity($target, $object_id)
	{
		$result = $this->M_log_produk->get_detail_log_activity($target, $object_id);

		if (count($result) > 0) {
			$response = [
				'success' => 'true',
				'data' => $result,
				'count' => count($result)
			];

			return to_json($response);
		}

		$response = [
			'success' => 'false',
			'data' => [],
			'count' => 0
		];

		return to_json($response);
	}

	function get_detail_product_outlet($object_id)
	{
		$result = $this->M_log_produk->get_detail_product_outlet($object_id);

		if (count($result) > 0) {
			$response = [
				'success' => 'true',
				'data' => $result,
				'count' => count($result)
			];

			return to_json($response);
		}

		$response = [
			'success' => 'false',
			'data' => [],
			'count' => 0
		];

		return to_json($response);
	}

	function get_detail_product_pulsa_bluk_outlet($object_id)
	{
		$result = $this->M_log_produk->get_detail_product_pulsa_bluk_outlet($object_id);

		if (count($result) > 0) {
			$response = [
				'success' => 'true',
				'data' => $result,
				'count' => count($result)
			];

			return to_json($response);
		}

		$response = [
			'success' => 'false',
			'data' => [],
			'count' => 0
		];

		return to_json($response);
	}

	function generate_date_filter($start_date, $end_date)
	{
		if (empty($start_date) && empty($end_date)) {
			$filter = [
				'start' => date('Y-m-d', strtotime("-1 days")),
				'end' => date('Y-m-d')
			];
		} else if (date('Y-m-d', strtotime($start_date)) == date('Y-m-d', strtotime($end_date))) {
			$filter = [
				'start' => date('Y-m-d', strtotime('-1 day', strtotime($start_date))),
				'end' => date('Y-m-d', strtotime($end_date))
			];
		} else {
			$filter = [
				'start' => date('Y-m-d', strtotime($start_date)),
				'end' => date('Y-m-d', strtotime($end_date))
			];
		}

		return $filter;
	}
}
