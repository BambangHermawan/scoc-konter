<!--Modal tambah produk baru-->
<div class="modal fade" id="modal_add_product" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Tambah Produk</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="add_product_form">
					<div class="form-group">
						<div class="col-lg-4">Tipe</div>
						<div class="col-lg-12">
							<select class="form-control form-control-sm" id="input_product_type_id"
									name="input_product_type_id">
								<option>Pilih</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Kategori</div>
						<div class="col-lg-12">
							<select class="form-control form-control-sm" id="input_product_category_id"
									name="input_product_category_id"></select>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Nama</div>
						<div class="col-lg-12">
							<input type="text" class="form-control form-control-sm" id="input_product_name"
								   name="input_product_name">
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Modal</div>
						<div class="col-lg-12">
							<input type="number" class="form-control form-control-sm"
								   id="input_product_modal" name="input_product_modal">
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Banyak</div>
						<div class="col-lg-12">
							<input type="number" class="form-control form-control-sm"
								   id="input_product_many" name="input_product_many">
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-6">Tanggal Masuk</div>
						<div class="col-lg-12">
							<input type="date" class="form-control form-control-sm"
								   id="input_product_entry_date" name="input_product_entry_date">
						</div>
					</div>

				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"
						id="btn_add_new_product">Simpan
				</button>
			</div>
		</div>
	</div>
</div>

<!--Modal tambah stok barang-->
<div class="modal fade" id="modal_stock" tabindex="-1" role="dialog" aria-labelledby="modelTitleId">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Tambah Stok Produk</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="add_stock_form">
					<input type="text" id="input_add_stock_id" name="input_add_stock_id" hidden>
					<input type="text" id="input_add_stock_type_id" name="input_add_stock_type_id" hidden>

					<div class="form-group">
						<div class="col-lg-4">Produk</div>
						<div class="col-lg-12"><input type="text" class="form-control form-control-sm"
													  id="input_add_stock_name" name="input_add_stock_name" readonly>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-6">Stok Sekarang</div>
						<div class="col-lg-12"><input type="number" class="form-control form-control-sm"
													  id="input_add_stock_many" name="input_add_stock_many" readonly>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-6">Stok</div>
						<div class="col-lg-12"><input type="number" class="form-control form-control-sm"
													  id="input_add_stock" name="input_add_stock"></div>
					</div>
				</form>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"
						id="btn_add_stock_submit">Simpan
				</button>
			</div>
		</div>
	</div>
</div>

<!--Modal ubah data barang-->
<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="modelTitleId">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Ubah Data Produk</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="edit_product_form">
					<input type="text" id="input_edit_type_id" name="input_edit_type_id" hidden>

					<div class="form-group">
						<div class="col-lg-4">Nama</div>
						<div class="col-lg-12"><input type="text" class="form-control form-control-sm"
													  id="input_edit_name" name="input_edit_name" readonly></div>
					</div>
					<div class="form-group">
						<div class="col-lg-4">Banyak</div>
						<div class="col-lg-12"><input type="number" class="form-control form-control-sm"
													  id="input_edit_many" name="input_edit_many"></div>
					</div>
					<div class="form-group">
						<div class="col-lg-4">Modal</div>
						<div class="col-lg-12"><input type="number" class="form-control form-control-sm"
													  id="input_edit_modal" name="input_edit_modal"></div>
					</div>
					<div class="form-group">
						<div class="col-lg-4">Catatan</div>
						<div class="col-lg-12"><textarea class="form-control form-control-sm" id="input_edit_note"
														 name="input_edit_note"></textarea></div>
					</div>
					<div class="form-group" hidden>
						<div class="col-lg-4">Id</div>
						<div class="col-lg-12"><input type="text" class="form-control form-control-sm"
													  id="input_edit_id"></div>
					</div>
			</div>
			</form>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"
						id="btn_save_edit">Simpan
				</button>
			</div>
		</div>
	</div>
</div>

<!--Modal transfer barang-->
<div class="modal fade" id="modal_transfer" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Transfer Barang</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="transfer_barang_form">
					<input type="text" id="input_transfer_product_id" hidden>
					<div class="form-group">
						<div class="col-lg-4">Barang</div>
						<div class="col-lg-12"><input type="text" class="form-control form-control-sm"
													  id="input_transfer_product_name" readonly></div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Tujuan</div>
						<div class="col-lg-12">
							<select class="form-control form-control-sm" id="input_transfer_outlet_id"
									name="input_transfer_outlet_id"></select>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-6">stok Tersedia</div>
						<div class="col-lg-12"><input type="text" class="form-control form-control-sm"
													  id="input_transfer_product_stock"
													  name="input_transfer_product_stock" readonly>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Kuantitas</div>
						<div class="col-lg-12"><input type="number" class="form-control form-control-sm"
													  id="input_transfer_many" name="input_transfer_many"></div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Harga</div>
						<div class="col-lg-12"><input type="number" class="form-control form-control-sm"
													  id="input_transfer_cost" name="input_transfer_cost"></div>
					</div>

					<div class="form-group" hidden>
						<div class="col-lg-4">Tipe</div>
						<div class="col-lg-12">
							<input type="text" class="form-control form-control-sm" id="input_transfer_type_id"
								   name="input_transfer_type_id">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal" id="btn_salah_transfer">Tarik
					Barang
				</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"
						id="btn_transfer">Transfer
				</button>
			</div>
		</div>
	</div>
</div>

<!--Modal ubah pulsa-->
<div class="modal fade" id="modal_pusat_pulsa" tabindex="-1" role="dialog" aria-labelledby="modelTitleId">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Ubah Pulsa</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="edit_pulsa_form">
					<input type="text" id="input_edit_pulsa_category" name="input_edit_pulsa_category" readonly
						   hidden>
					<input type="text" id="input_edit_pulsa_product" name="input_edit_pulsa_product" readonly
						   hidden>

					<div class="form-group">
						<div class="col-lg-4">Outlet</div>
						<div class="col-lg-12"><input type="text" class="form-control form-control-sm"
													  id="input_edit_pulsa_outlet" name="input_edit_pulsa_outlet"
													  readonly>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Produk</div>
						<div class="col-lg-12"><input type="text" class="form-control form-control-sm"
													  id="input_edit_pulsa_name" name="input_edit_pulsa_name" readonly>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Modal</div>
						<div class="col-lg-12"><input type="number" class="form-control form-control-sm"
													  id="input_edit_pulsa_modal" name="input_edit_pulsa_modal"></div>
					</div>
					<div class="form-group">
						<div class="col-lg-4">Harga Jual</div>
						<div class="col-lg-12"><input type="number" class="form-control form-control-sm"
													  id="input_edit_pulsa_cost" name="input_edit_pulsa_cost"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"
						id="btn_edit_pulsa_pusat_submit">Simpan
				</button>
			</div>
		</div>
	</div>
</div>

<!--Modal import barang-->
<div class="modal fade" id="modal_impor" tabindex="-1" role="dialog" aria-labelledby="modelTitleId">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="<?= base_url(); ?>import/import_excel" method="post" enctype="multipart/form-data">
				<div class="modal-header">
					<h5 class="modal-title">Impor Barang</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<div class="col-lg-4"><label for="tujuan">Tujuan</label></div>
						<div class="col-lg-12">
							<select name="tujuan" id="tujuan"
									class="form-control form-control-sm" required>
								<option value="">Pilih...</option>
								<option value="item_product">Tambah Produk Baru</option>
								<option value="item_product_stok">Tambah Stok Produk</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-4"><label for="input_impor_sheet">Sheet</label></div>
						<div class="col-lg-12">
							<input type="number" name="sheet"
								   class="form-control form-control-sm"
								   id="input_impor_sheet" min="1" value="1" required>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4"><label for="input_impor_berkas">Berkas</label></div>
						<div class="col-lg-12">
							<input type="file" name="berkas" id="input_impor_berkas">
						</div>
					</div>
					<hr>
					<div class="form-group">
						<div class="col-lg-12"><b>*Bacalah aturan petunjuk import</b></div>
					</div>
					<div class="modal-footer">
						<input type="text" name="redirect" value="<?= current_url(); ?>" hidden>
						<button type="reset" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
						<button type="submit" class="btn btn-primary btn-sm" id="btn_impor_data">Import Data</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<!--Modal export produk-->
<div class="modal fade" id="modal_export" tabindex="-1" role="dialog" aria-labelledby="modelTitleId">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="<?= base_url(); ?>export/export_excel" method="post" enctype="multipart/form-data">
				<div class="modal-header">
					<h5 class="modal-title">Export Produk</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<div class="col-lg-4"><label for="tipe_item">Tipe Item</label></div>
						<div class="col-lg-12">
							<select name="tipe_item" id="tipe_item"
									class="form-control form-control-sm" required>
								<option value="">Pilih...</option>
								<option value="0">Semua Tipe Item</option>
								<option value="1">Aksesoris</option>
								<option value="2">Perdana</option>
								<option value="3">Voucher</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4"><label for="format_excel">Format Excel</label></div>
						<div class="col-lg-12">
							<select name="format_excel" id="format_excel"
									class="form-control form-control-sm" required>
								<option value="">Pilih...</option>
								<option value="laporan_stok">Laporan Stok Produk</option>
								<option value="tambah_item_produk">Tambah Produk Baru</option>
								<option value="tambah_stok">Tambah Stok Produk</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-12"><b>* Gunakan Format Excel ini untuk menu import produk</b></div>
					</div>

					<div class="modal-footer">
						<input type="text" name="redirect" value="<?= current_url(); ?>" hidden>
						<button type="reset" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
						<button type="submit" class="btn btn-primary btn-sm" id="btn_export_data">Export Data</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<!--Modal preview qrcode-->
<div class="modal fade" id="modal_qrcode" tabindex="-1" role="dialog" aria-labelledby="modelTitleId">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Scan Qrcode</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<img src="" id="qrcode_preview" width="100%" height="100%">
			</div>
			<div class="modal-footer">
				<button type="reset" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
			</div>
			</form>
		</div>
	</div>
</div>
