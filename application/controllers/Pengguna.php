<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengguna extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_pengguna', '', true);

		if (!isset($_SESSION)) {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'ADMIN') {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'OWNER') {
			redirect('auth/login');
		} elseif ($_SESSION['status_user'] == 'KASIR') {
			redirect('auth/login');
		}
	}

	function index()
	{
		$data['title'] = 'pengguna';
		$data['site_navigation'] = 'pengguna';

		return $this->load->view('pengguna/index', $data);
	}

	function get_all_pengguna()
	{
		$result = $this->M_pengguna->get_datatables();

		$response = [
			'success' => true,
			'draw' => $_REQUEST['draw'],
			'recordsTotal' => $this->M_pengguna->count_all(),
			'recordsFiltered' => $this->M_pengguna->count_filtered(),
			'data' => $result
		];

		return to_json($response);

	}

	function get_pengguna()
	{
		$result = $this->M_pengguna->get_all_pengguna();

		if (count($result) > 0) {

			$response = [
				'success' => true,
				'data' => $result,
				'total' => count($result)
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function add_pengguna()
	{
		$username = '';
		$str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		for ($i = 0; $i < 5; $i++) $username .= $str[rand(0, 62)];

		$name = $this->input->post('name');
		$hp = $this->input->post('phone');
		$address = $this->input->post('address');
		$role = $this->input->post('role');
		$password = md5($username);

		$result = $this->M_pengguna->add_pengguna($username, $name, $hp, $address, $role, $password);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Tambah pengguna ' . $name . ' berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Tambah pengguna ' . $name . ' gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function edit_pengguna()
	{
		$id = $this->input->post('id');
		$name = $this->input->post('name');
		$hp = $this->input->post('phone');
		$address = $this->input->post('address');
		$role = $this->input->post('role');

		$result = $this->M_pengguna->edit_pengguna($id, $name, $hp, $address, $role);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Ubah pengguna ' . $name . ' berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Ubah pengguna ' . $name . ' gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function reset_password()
	{
		$id = $this->input->post('id');

		$result = $this->M_pengguna->reset_password_pengguna($id);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Reset password pengguna berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Reset password pengguna gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function delete_pengguna()
	{
		$id = $this->input->post('id');
		$result = $this->M_pengguna->delete_pengguna($id);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Hapus pengguna berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Hapus pengguna gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}
}
