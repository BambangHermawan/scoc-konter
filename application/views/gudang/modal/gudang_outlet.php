<!--Modal transfer barang outlet-->
<div class="modal fade" id="modal_transfer_outlet" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Transfer Barang</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="transfer_barang_outlet_form">
					<input type="text" id="input_transfer_outlet_product_id" hidden>
					<input type="text" id="input_transfer_outlet_type_id" hidden>
					<input type="text" id="input_transfer_outlet_product_qrcode" hidden>

					<div class="form-group">
						<div class="col-lg-4">Barang</div>
						<div class="col-lg-12"><input type="text" class="form-control form-control-sm"
													  id="input_transfer_outlet_product_name" readonly></div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Tujuan</div>
						<div class="col-lg-12">
							<select class="form-control form-control-sm" id="input_transfer_outlet_outlet_id"
									name="input_transfer_outlet_outlet_id"></select>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-6">Stok Tersedia</div>
						<div class="col-lg-12"><input type="text" class="form-control form-control-sm"
													  id="input_transfer_outlet_stock" readonly></div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Kuantitas</div>
						<div class="col-lg-12"><input type="number" class="form-control form-control-sm"
													  id="input_transfer_outlet_many" name="input_transfer_outlet_many">
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Catatan</div>
						<div class="col-lg-12">
							<textarea class="form-control form-control-sm" id="input_transfer_outlet_note"
									  name="input_transfer_outlet_note"></textarea>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"
						id="btn_transfer_outlet_submit">Transfer
				</button>
			</div>
		</div>
	</div>
</div>

<!--Modal atur harga barang outlet-->
<div class="modal fade" id="modal_cost_outlet" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content modal-sm">
			<div class="modal-header">
				<h5 class="modal-title">Ubah Harga</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="cost_barang_outlet_form">
					<input type="text" id="input_cost_outlet_product_id" hidden>
					<input type="text" id="input_cost_outlet_type_id" hidden>

					<div class="form-group">
						<div class="col-lg-4">Barang</div>
						<div class="col-lg-12"><input type="text" class="form-control form-control-sm"
													  id="input_cost_outlet_product_name"
													  name="input_cost_outlet_product_name" readonly></div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Harga</div>
						<div class="col-lg-12"><input type="number" class="form-control form-control-sm"
													  id="input_cost_outlet_cost" name="input_cost_outlet_cost"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"
						id="btn_cost_outlet_submit">Simpan
				</button>
			</div>
		</div>
	</div>
</div>

<!--Modal diskon barang outlet-->
<div class="modal fade" id="modal_discount_outlet" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content modal-sm">
			<div class="modal-header">
				<h5 class="modal-title">Ubah Diskon</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="discount_barang_outlet_form">
					<input type="text" id="input_discount_outlet_product_id" hidden>
					<input type="text" id="input_outlet_discount_type_id" hidden>

					<div class="form-group">
						<div class="col-lg-4">Barang</div>
						<div class="col-lg-12">
							<input type="text" class="form-control form-control-sm"
								   id="input_outlet_discount_product_name"
								   name="input_outlet_discount_product_name" readonly>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Harga</div>
						<div class="col-lg-12">
							<input type="text" class="form-control form-control-sm"
								   id="input_outlet_discount_cost"
								   name="input_outlet_discount_cost" readonly>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Diskon</div>
						<div class="col-lg-12">
							<input type="number" class="form-control form-control-sm"
								   id="input_outlet_discount_many"
								   name="input_outlet_discount_many">
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Sampai</div>
						<div class="col-lg-12">
							<input type="date" class="form-control form-control-sm"
								   id="input_outlet_discount_until"
								   name="input_outlet_discount_untul" required>
						</div>
					</div>
				</form>
			</div>

			<div class="modal-footer">
				<input type="text" id='input-discount-item_product_outlet_id-fisik' hidden>
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" id="btn_discount_outlet_submit"
						data-dismiss="modal">Simpan
				</button>
			</div>
		</div>
	</div>
</div>

<!--Modal tambah barang bluk-->
<div class="modal fade" id="modal_outlet_add_product_bluk" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content modal-sm">
			<div class="modal-header">
				<h5 class="modal-title">Tambah Produk</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="modal_outlet_add_product_bluk_form">
					<div class="form-group">
						<div class="col-lg-4">Kategori</div>
						<div class="col-lg-12">
							<select class="form-control form-control-sm"
									id="input_outlet_product_category_bluk"
									name="input_outlet_product_category_bluk">
							</select>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Nama</div>
						<div class="col-lg-12">
							<input type="text" class="form-control form-control-sm"
								   id="input_outlet_product_name_bluk"
								   name="input_outlet_product_name_bluk">
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Modal</div>
						<div class="col-lg-12">
							<input type="number" class="form-control form-control-sm"
								   id="input_outlet_product_modal_bluk"
								   name="input_outlet_product_modal_bluk">
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Harga</div>
						<div class="col-lg-12">
							<input type="number" class="form-control form-control-sm"
								   id="input_outlet_product_cost_bluk"
								   name="input_outlet_product_cost_bluk">
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Unit</div>
						<div class="col-lg-12">
							<select class="form-control form-control-sm" id="input_outlet_product_unit_bluk"
									name="input_outlet_product_unit_bluk">
								<option value="">Pilih...</option>
								<option value="bluk">Bluk</option>
							</select>
						</div>
					</div>
				</form>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" id="btn_outlet_add_product_bluk_submit"
						data-dismiss="modal">Simpan
				</button>
			</div>
		</div>
	</div>
</div>

<!--Modal impor barang bluk-->
<div class="modal fade" id="modal_outlet_import_product_bluk" tabindex="-1" role="dialog"
	 aria-labelledby="modelTitleId">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="<?= base_url(); ?>import/import_excel_bluk" method="post" enctype="multipart/form-data">
				<div class="modal-header">
					<h5 class="modal-title">Impor Produk Pulsa</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<input name="outlet_id" id="outlet_id" value="<?= $this->uri->segment(4); ?>" hidden>

						<div class="form-group">
							<div class="col-lg-4"><label for="tujuan">Tujuan</label></div>
							<div class="col-lg-12">
								<select name="tujuan" id="tujuan"
										class="form-control form-control-sm">
									<option value="">Pilih...</option>
									<option value="item_outlet_modal">Tambah Modal Pulsa Outlet</option>
									<option value="item_outlet_nonunit">Tambah Harga Pulsa Non Unit</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-4"><label for="input_outlet_import_sheet_bluk">Sheet</label></div>
							<div class="col-lg-12">
								<input type="number" name="input_outlet_import_sheet_bluk"
									   id="input_outlet_import_sheet_bluk"
									   class="form-control form-control-sm"
									   min="1" value="1">
							</div>
						</div>

						<div class="form-group">
							<div class="col-lg-4"><label for="input_impor_berkas">Berkas</label></div>
							<div class="col-lg-12">
								<input type="file" name="berkas"
									   id="berkas" required>
							</div>
						</div>
						<hr>
						<div class="form-group">
							<div class="col-lg-12"><b>*Bacalah aturan petunjuk import</b></div>
						</div>
					</div>
					<div class="modal-footer">
						<input type="text" name="redirect" value="<?= current_url(); ?>" hidden>
						<button type="reset" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
						<button type="submit" class="btn btn-primary btn-sm" id="btn_outlet_import_file_bluk">Import
							Data
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<!--Modal export barang bluk-->
<div class="modal fade" id="modal_outlet_export_product_bluk" tabindex="-1" role="dialog"
	 aria-labelledby="modelTitleId">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="<?= base_url(); ?>export/export_excel_bluk" method="post" enctype="multipart/form-data">
				<div class="modal-header">
					<h5 class="modal-title">Export Produk Pulsa</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<input name="outlet_id" id="outlet_id" value="<?= $this->uri->segment(4); ?>" hidden>

						<div class="form-group">
							<div class="col-lg-4"><label for="tujuan">Format Excel</label></div>
							<div class="col-lg-12">
								<select name="tujuan_export" id="tujuan_export"
										class="form-control form-control-sm" required>
									<option value="">Pilih...</option>
									<option value="item_outlet_modal">Tambah Modal Pulsa Outlet</option>
									<option value="item_outlet_nonunit">Tambah Item Pulsa Non Unit</option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<div class="col-lg-4"><label for="input_outlet_export_sheet_bluk">Sheet</label></div>
							<div class="col-lg-12">
								<input type="number" name="input_outlet_export_sheet_bluk"
									   id="input_outlet_export_sheet_bluk"
									   class="form-control form-control-sm"
									   min="1" value="1" required>
							</div>
						</div>
						<hr>

						<div class="form-group">
							<div class="col-lg-12"><b>* Gunakan Format Excel ini untuk menu import produk pulsa</b>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<input type="text" name="redirect" value="<?= current_url(); ?>" hidden>
						<button type="reset" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
						<button type="submit" class="btn btn-primary btn-sm" id="btn_outlet_export_file_bluk">Export
							Data
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<!--Modal atur harga barang outlet-->
<div class="modal fade" id="modal_outlet_cost_bluk" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content modal-sm">
			<div class="modal-header">
				<h5 class="modal-title">Ubah Harga</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="cost_barang_outlet_bluk_form">
					<input type="text" id="input_cost_outlet_bluk_product_id" hidden>

					<div class="form-group">
						<div class="col-lg-4">Barang</div>
						<div class="col-lg-12"><input type="text" class="form-control form-control-sm"
													  id="input_cost_outlet_product_name_bluk"
													  name="input_cost_outlet_product_name_bluk" readonly></div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Kategori</div>
						<div class="col-lg-12">
							<input type="text" class="form-control form-control-sm"
								   id="input_cost_outlet_product_category_bluk"
								   name="input_cost_outlet_product_category_bluk" readonly>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Harga</div>
						<div class="col-lg-12"><input type="text" class="form-control form-control-sm"
													  id="input_cost_outlet_product_cost_bluk"
													  name="input_cost_outlet_product_cost_bluk"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"
						id="btn_cost_outlet_bluk_submit">Simpan
				</button>
			</div>
		</div>
	</div>
</div>

<!--Modal diskon barang outlet-->
<div class="modal fade" id="modal_outlet_discount_bluk" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content modal-sm">
			<div class="modal-header">
				<h5 class="modal-title">Ubah Diskon</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="discount_barang_outlet_bluk_form">
					<input type="text" id="input_discount_outlet_bluk_product_id" hidden>
					<input type="text" id="input_discount_outlet_bluk_type_id" hidden>

					<div class="form-group">
						<div class="col-lg-4">Barang</div>
						<div class="col-lg-12">
							<input type="text" class="form-control form-control-sm"
								   id="input_discount_outlet_product_name_bluk"
								   name="input_discount_outlet_product_name_bluk" readonly>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Kategori</div>
						<div class="col-lg-12">
							<input type="text" class="form-control form-control-sm"
								   id="input_discount_outlet_product_category_bluk"
								   name="input_discount_outlet_product_category_bluk" readonly>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Harga</div>
						<div class="col-lg-12">
							<input type="text" class="form-control form-control-sm"
								   id="input_discount_outlet_product_cost_bluk"
								   name="input_discount_outlet_product_cost_bluk" readonly>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Diskon</div>
						<div class="col-lg-12">
							<input type="number" class="form-control form-control-sm"
								   id="input_discount_outlet_product_discount_bluk"
								   name="input_discount_outlet_product_discount_bluk">
						</div>
					</div>
				</form>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" id="btn_discount_outlet_bluk_submit"
						data-dismiss="modal">Simpan
				</button>
			</div>
		</div>
	</div>
</div>

<!--Modal tambah modal barang outlet-->
<div class="modal fade" id="modal_outlet_add_modal_bluk" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content modal-sm">
			<div class="modal-header">
				<h5 class="modal-title">Tambah Modal</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="add_modal_outlet_bluk_form">
					<input type="text" id="input_add_outlet_modal_bluk_product_id" hidden>
					<div class="form-group">
						<div class="col-lg-4">Modal</div>
						<div class="col-lg-12">
							<input type="number" class="form-control form-control-sm"
								   id="input_add_outlet_modal_bluk"
								   name="input_add_outlet_modal_bluk" value='0'>
						</div>
					</div>
				</form>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" id="btn_add_outlet_modal_submit"
						data-dismiss="modal">Simpan
				</button>
			</div>
		</div>
	</div>
</div>

<!--Modal edit modal barang outlet-->
<div class="modal fade" id="modal_outlet_edit_modal_bluk" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content modal-sm">
			<div class="modal-header">
				<h5 class="modal-title">Pengurangan Modal</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="edit_modal_outlet_bluk_form">
					<input type="text" id="input_edit_outlet_modal_bluk_product_id" hidden>
					<div class="form-group">
						<div class="col-lg-4">Modal</div>
						<div class="col-lg-12">
							<input type="number" class="form-control form-control-sm"
								   id="input_edit_outlet_modal_bluk"
								   name="input_edit_outlet_modal_bluk" value='0'>
						</div>
					</div>
				</form>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" id="btn_edit_outlet_modal_submit"
						data-dismiss="modal">Simpan
				</button>
			</div>
		</div>
	</div>
</div>

<!--Modal preview qrcode-->
<div class="modal fade" id="modal_qrcode" tabindex="-1" role="dialog" aria-labelledby="modelTitleId">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Scan Qrcode</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<img src="" id="qrcode_preview" width="100%" height="100%">
			</div>
			<div class="modal-footer">
				<button type="reset" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
			</div>
			</form>
		</div>
	</div>
</div>
