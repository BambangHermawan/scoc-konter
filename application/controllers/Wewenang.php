<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Wewenang extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_wewenang', '', true);
		$this->load->model('M_outlet', '', true);

		if (!isset($_SESSION)) {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'ADMIN') {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'OWNER') {
			redirect('auth/login');
		} elseif ($_SESSION['status_user'] == 'KASIR') {
			redirect('auth/login');
		}
	}

	function index()
	{
		$data['title'] = 'Wewenang';
		$data['site_navigation'] = 'Wewenang';

		$this->load->view('wewenang/index', $data);
	}

	function get_all_wewenang()
	{
		$result = $this->M_wewenang->get_datatables();

		$response = [
			'success' => true,
			'draw' => $_REQUEST['draw'],
			'recordsTotal' => $this->M_wewenang->count_all(),
			'recordsFiltered' => $this->M_wewenang->count_filtered(),
			'data' => $result
		];

		return to_json($response);

	}

	function get_wewenang()
	{
		$result = $this->M_wewenang->get_wewenang();

		if (count($result) > 0) {

			$response = [
				'success' => true,
				'data' => $result,
				'total' => count($result)
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'data' => [],
			'total' => 0
		];

		return to_json($response);

	}

	function add_wewenang()
	{
		$wewenang = $this->input->post('wewenang');

		$result = $this->M_wewenang->add_wewenang($wewenang);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Tambah wewenang ' . $wewenang . ' berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Tambah wewenang ' . $wewenang . ' gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function edit_wewenang()
	{
		$id = $this->input->post('id');
		$wewenang = $this->input->post('wewenang');

		$result = $this->M_wewenang->edit_wewenang($id, $wewenang);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Ubah wewenang ' . $wewenang . ' berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Ubah wewenang ' . $wewenang . ' gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function delete_wewenang()
	{
		$id = $this->input->post('id');
		$result = $this->M_wewenang->delete_wewenang($id);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Hapus wewenang berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Hapus wewenang gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}
}
