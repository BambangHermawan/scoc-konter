<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_outlet extends CI_Model
{
	var $column_order = array('id', 'name', 'address');
	var $column_search = array('id', 'name', 'address');
	var $order = array('name' => 'asc');

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		$this->db->select('id, name, address')
			->from('outlet')
			->where('deleted_date is null');

		$i = 0;
		foreach ($this->column_search as $item) {
			if ($_REQUEST['search']['value']) {

				if ($i === 0) {
					$this->db->group_start();
					$this->db->like($item, $_REQUEST['search']['value']);
				} else {
					$this->db->or_like($item, $_REQUEST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if (isset($_REQUEST['order'])) {
			$this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();

		if ($_REQUEST['length'] != -1)
			$this->db->limit($_REQUEST['length'], $_REQUEST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_all()
	{
		$this->db->select('id, name, address')
			->from('outlet')
			->where('deleted_date is null');

		return $this->db->count_all_results();
	}

	function get_all_outlet_detail($outlet_id)
	{
		$digital_bluk = $this->db->query("select sum(modal) as modal_bluk from item_outlet_category where outlet_id='$outlet_id' and deleted_date is null")->result();
		$asset_bluk = $digital_bluk ? $digital_bluk[0]->modal_bluk : 0;

		$digital_unit = $this->db->query("select sum(item_outlet.modal * item_outlet.many) as modal_unit from item_outlet,
            item_outlet_category where item_outlet.item_outlet_category_id=item_outlet_category.id and
            item_outlet_category.outlet_id='$outlet_id' and item_outlet.unit='unit' and item_outlet.deleted_date is null and item_outlet_category.deleted_date is null")->result();
		$asset_unit = $digital_unit ? $digital_unit[0]->modal_unit : 0;

		$fisik = $this->db->query("select sum(item_product.modal * item_product_outlet.many) as modal_fisik from item_product,
            item_product_outlet where item_product_outlet.item_product_id=item_product.id and
            item_product_outlet.outlet_id='$outlet_id' and item_product.deleted_date is null and item_product_outlet.deleted_date is null")->result();
		$asset_fisik = $fisik ? $fisik[0]->modal_fisik : 0;

		$total_assets = $asset_bluk + $asset_unit + $asset_fisik;

		return number_format($total_assets);
	}

	function get_outlet_by_id($outlet_id)
	{
		return $this->db->query("select id, name from outlet where deleted_date is null and id='$outlet_id' LIMIT 1")->result();
	}

	function get_all_outlet()
	{
		return $this->db->query("select id, name from outlet where deleted_date is null")->result();
	}

	function add_outlet($name, $address)
	{
		$this->db->trans_begin();

		$created_by = created_by();
		$created_date = created_date();

		$check = $this->db->query("select id from outlet where name='$name' and deleted_date is null")->result();
		if ($check) return false;
		else {
			$this->db->query("insert into outlet (name, address, created_by, created_date) values('$name','$address','$created_by','$created_date')");

			$outlet_id = $this->db->insert_id();

			$this->db->query("insert into item_outlet_category(name, outlet_id, created_by, created_date) 
			values('Telkomsel','$outlet_id','$created_by','$created_date'),
				  ('Indosat','$outlet_id','$created_by','$created_date'),
				  ('XL','$outlet_id','$created_by','$created_date'),
				  ('Three','$outlet_id','$created_by','$created_date'),
				  ('Smartfren','$outlet_id','$created_by','$created_date'),
				  ('Elang','$outlet_id','$created_by','$created_date'),
				  ('Shopee','$outlet_id','$created_by','$created_date'),
				  ('EBlanja','$outlet_id','$created_by','$created_date')");

			$category_id = $this->db->query("select id, name from item_outlet_category where outlet_id='$outlet_id' and deleted_date is null")->result();

			$unit = 'bluk';
			$many = 0;
			$initial = 5;

			foreach ($category_id as $item) {
				$this->load->library('ciqrcode');

				$config['cacheable'] = true;
				$config['cachedir'] = 'uploads/';
				$config['errorlog'] = 'uploads/';
				$config['imagedir'] = 'uploads/qrcode/';
				$config['quality'] = true;
				$config['size'] = '1024';
				$config['black'] = array(224, 255, 255);
				$config['white'] = array(70, 130, 180);
				$this->ciqrcode->initialize($config);

				$product_image = md5(time() . '_' . uniqid()) . '.png';

				$params['data'] = $product_image;
				$params['level'] = 'H';
				$params['size'] = 10;
				$params['savename'] = FCPATH . $config['imagedir'] . $product_image;
				$this->ciqrcode->generate($params);

				$this->db->query("insert into item_outlet(item_outlet_category_id, name, unit, modal, cost, many, qrcode, created_by, created_date)
				values('$item->id','$initial','$unit','5300','6000','$many','$product_image','$created_by','$created_date')");
			}

			return $this->db->trans_complete();
		}
	}

	function add_item_category_outlet()
	{
		$this->db->trans_begin();

		$created_by = created_by();
		$created_date = created_date();

		$outlet_id = 5;
		if (!$outlet_id) return false;
		else {
			$this->db->query("insert into item_outlet_category(name, outlet_id, created_by, created_date) 
			values('Elang','$outlet_id','$created_by','$created_date'),
				  ('Shopee','$outlet_id','$created_by','$created_date'),
				  ('EBelanja','$outlet_id','$created_by','$created_date')");

			$category_id = $this->db->query("select id, name from item_outlet_category where outlet_id='$outlet_id' and deleted_date is null")->result();

			$unit = 'bluk';
			$many = 0;
			$initial = 5;

			foreach ($category_id as $item) {
				$this->load->library('ciqrcode');

				$config['cacheable'] = true;
				$config['cachedir'] = 'uploads/';
				$config['errorlog'] = 'uploads/';
				$config['imagedir'] = 'uploads/qrcode/';
				$config['quality'] = true;
				$config['size'] = '1024';
				$config['black'] = array(224, 255, 255);
				$config['white'] = array(70, 130, 180);
				$this->ciqrcode->initialize($config);

				$product_image = md5(time() . '_' . uniqid()) . '.png';

				$params['data'] = $product_image;
				$params['level'] = 'H';
				$params['size'] = 10;
				$params['savename'] = FCPATH . $config['imagedir'] . $product_image;
				$this->ciqrcode->generate($params);

				$this->db->query("insert into item_outlet(item_outlet_category_id, name, unit, modal, cost, many, qrcode, created_by, created_date)
				values('$item->id','$initial','$unit','5300','6000','$many','$product_image','$created_by','$created_date')");
			}

			return $this->db->trans_complete();
		}
	}

	function edit_outlet($id, $name, $address)
	{
		$modified_by = modified_by();
		$modified_date = modified_date();

		return $this->db->query("update outlet set name='$name', address='$address', modified_by='$modified_by', modified_date='$modified_date' where id='$id' and deleted_date is null");
	}

	function delete_outlet($id)
	{
		$deleted_by = deleted_by();
		$deleted_date = deleted_date();

		return $this->db->query("update outlet set deleted_by='$deleted_by', deleted_date='$deleted_date' where id='$id' and deleted_date is null");
	}
}
