<?php $this->load->view('_layouts/_template/header') ?>

<div class="container-fluid">

	<div class="row">
		<div class="col-12">
			<div class="card card-outline card-primary">
				<div class="card-header">
					<h3 class="card-title"><b>Petunjuk Impor Pulsa Non Unit Fisik</b></h3>
				</div>
				<div class="card-body">
					<p><b>Petunjuk:</b></p>
					<ul style="margin-bottom:10px; width:80%">
						<li>Buatlah sheet dengan baris pertama kolom Id Kategori, nama produk yang akan diimpor, modal,
							harga, dan unitnya.
							(nama kolom pada baris pertama terserah)
						</li>
						<li>Kemudian pada baris kedua dan seterusnya diisi Id Kategori (kolom petama),
							nama produk yang akan diimpor (kolom kedua), modal (kolom ketiga), harga (kolom keempat),
							dan unitnya (bluk, tembak_data, tembak_telepon pada kolom kelima).
						</li>
						<li>Id Kategori sendiri dapat dilihat pada tabel dibawah ini:</li>
					</ul>
					<hr>
					<div class="table-responsive">
						<table id="table_pulsa_non_unit"
							   class="table table-striped table-hover dt-responsive display nowrap"
							   style="width:100%">
							<thead>
							<tr>
								<th style="width:100px">ID KATEGORI</th>
								<th>NAMA KATEGORI</th>
								<th>OUTLET</th>
							</tr>
							</thead>
							<tbody>
							<?php
							$no = 1;
							foreach ($item_outlet_categories as $item_outlet_category) {
								echo "<tr>";
								echo "<td>$item_outlet_category->id</td>";
								echo "<td>$item_outlet_category->name</td>";
								echo "<td>$item_outlet_category->outlet_name</td>";
								echo "</tr>";
							}
							?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$("#table_pulsa_non_unit").DataTable()
</script>

<?php $this->load->view('_layouts/_template/js') ?>
