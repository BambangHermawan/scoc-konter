<?php $this->load->view('_layouts/_template/header') ?>

<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div class="card card-primary">
				<div class="card-header">
					<h3 class="card-title"><b><?php echo $outlet ?></b></h3>
				</div>
				<div class="card-body">
					<ul class="nav nav-tabs" id="outlet_header_tab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="semua-tab" data-toggle="pill"
							   href="#semua" role="tab" aria-controls="semua"
							   aria-selected="true">Semua</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="aksesoris-tab" data-toggle="pill"
							   href="#aksesoris" role="tab" aria-controls="aksesoris"
							   aria-selected="false">Aksesoris</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="perdana-tab" data-toggle="pill"
							   href="#perdana" role="tab"
							   aria-controls="perdana" aria-selected="false">Perdana</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="voucher-tab" data-toggle="pill"
							   href="#voucher" role="tab"
							   aria-controls="voucher" aria-selected="false">Voucher</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="pulsa-tab" data-toggle="pill"
							   href="#pulsa" role="tab"
							   aria-controls="pulsa" aria-selected="false">Pulsa</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="peringatan-tab" data-toggle="pill"
							   href="#peringatan" role="tab"
							   aria-controls="peringatan" aria-selected="false">Peringatan</a>
						</li>
					</ul>
				</div>

				<div class="card-body">
					<div class="tab-content" id="outlet_tab">
						<div class="tab-pane fade show active" id="semua" role="tabpanel"
							 aria-labelledby="semua-tab">
							<?php $this->load->view('kasir/gudang/semua') ?>
						</div>
						<div class="tab-pane fade" id="aksesoris" role="tabpanel"
							 aria-labelledby="aksesoris-tab">
							<?php $this->load->view('kasir/gudang/aksesoris') ?>
						</div>
						<div class="tab-pane fade" id="perdana" role="tabpanel"
							 aria-labelledby="perdana-tab">
							<?php $this->load->view('kasir/gudang/perdana') ?>
						</div>
						<div class="tab-pane fade" id="voucher" role="tabpanel"
							 aria-labelledby="voucher-tab">
							<?php $this->load->view('kasir/gudang/voucher') ?>
						</div>
						<div class="tab-pane fade" id="pulsa" role="tabpanel"
							 aria-labelledby="pulsa-tab">
							<?php $this->load->view('kasir/gudang/pulsa') ?>
						</div>
						<div class="tab-pane fade" id="peringatan" role="tabpanel"
							 aria-labelledby="peringatan-tab">
							<?php $this->load->view('kasir/gudang/peringatan') ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--Modal transfer barang outlet-->
<div class="modal fade" id="modal_transfer_outlet" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Transfer Barang</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="transfer_barang_outlet_form">
					<input type="text" id="input_transfer_outlet_product_id" hidden>
					<input type="text" id="input_transfer_outlet_type_id" hidden>
					<input type="text" id="input_transfer_outlet_product_qrcode" hidden>

					<div class="form-group">
						<div class="col-lg-4">Barang</div>
						<div class="col-lg-12"><input type="text" class="form-control form-control-sm"
													  id="input_transfer_outlet_product_name" readonly></div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Tujuan</div>
						<div class="col-lg-12">
							<select class="form-control form-control-sm" id="input_transfer_outlet_outlet_id"
									name="input_transfer_outlet_outlet_id"></select>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-6">Stok Tersedia</div>
						<div class="col-lg-12"><input type="text" class="form-control form-control-sm"
													  id="input_transfer_outlet_stock" readonly></div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Kuantitas</div>
						<div class="col-lg-12"><input type="number" class="form-control form-control-sm"
													  id="input_transfer_outlet_many" name="input_transfer_outlet_many">
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4">Catatan</div>
						<div class="col-lg-12">
							<textarea class="form-control form-control-sm" id="input_transfer_outlet_note"
									  name="input_transfer_outlet_note"></textarea>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"
						id="btn_transfer_outlet_submit">Transfer
				</button>
			</div>
		</div>
	</div>
</div>

<!--Modal preview qrcode-->
<div class="modal fade" id="modal_qrcode" tabindex="-1" role="dialog" aria-labelledby="modelTitleId">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Scan Qrcode</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<img src="" id="qrcode_preview" width="100%" height="100%">
			</div>
			<div class="modal-footer">
				<button type="reset" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
			</div>
			</form>
		</div>
	</div>
</div>

<?php $this->load->view('_layouts/_template/js') ?>

<script>
	//init halaman index gudang
	$(document).ready(function () {
		get_outlet()
	})

	//validasi form transfer barang
	$("#transfer_barang_outlet_form").validate({
		rules: {
			input_transfer_outlet_outlet_id: {
				required: true,
				maxlength: 11
			},
			input_transfer_outlet_many: {
				required: true,
				maxlength: 3
			},
			input_transfer_outlet_note: {
				maxlength: 50
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `invalid-feedback` class to the error element
			error.addClass("invalid-feedback");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.next("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid").removeClass("is-valid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).addClass("is-valid").removeClass("is-invalid");
		}
	});

	//transfer dari outlet ke outlet
	$("#btn_transfer_outlet_submit").click(function () {

		type = $("#input_transfer_outlet_type_id").val();

		if ($('#transfer_barang_outlet_form').valid()) {

			$.ajax({
				url: "<?= base_url(); ?>kasir/_gudang/outlet/transfer_from_outlet",
				type: "POST",
				data: {
					product_id: $('#input_transfer_outlet_product_id').val(),
					from_outlet_id: "<?= $this->uri->segment(5); ?>",
					to_outlet_id: $('#input_transfer_outlet_outlet_id').val(),
					many: $('#input_transfer_outlet_many').val(),
					note: $('#input_transfer_outlet_note').val()
				},
				success: function (result) {

					if (result.success) {
						Swal.fire({
							title: 'Berhasil',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});

						if (type === 'Aksesoris') {
							init_table_outlet_aksesoris()
						} else if (type === 'Perdana') {
							init_table_outlet_perdana()
						} else if (type === 'Voucher') {
							init_table_outlet_voucher()
						} else if (type === 'Pulsa') {
							init_table_outlet_bluk()
						}

						init_table_outlet_semua()

					} else {
						Swal.fire({
							title: 'gagal',
							text: result.message,
							icon: result.type,
							showConfirmButton: false,
							timer: 1500
						});
					}
				}
			})
		} else {
			Swal.fire({
				title: 'Peringatan',
				text: 'Data yang di isi belum sesuai.',
				icon: 'warning',
				showConfirmButton: false,
				timer: 1500
			});
		}
	});

	//dropdown pilih outlet
	function get_outlet() {
		$.ajax({
			url: "<?= base_url(); ?>kasir/outlet/get_all_outlet",
			success: function (result) {

				if (result.success) {

					var outlet_list = '<option value="">Pilih...</option>'
					result.data.forEach(function (outlet) {

						if ("<?= $this->uri->segment(5); ?>" !== outlet['id']) {
							outlet_list += "<option value='" + outlet['id'] + "'>" + outlet['name'] + "</option>";
						}

					})

					$('#input_transfer_outlet_outlet_id').html(outlet_list)

				}

			}
		})
	}

	//reset kolom setiap modal ditutup
	$('[data-dismiss=modal]').on('click', function (e) {

		$('.form-control').removeClass("is-valid");
		$('.form-control').removeClass("is-invalid");

		var $t = $(this),
				target = $t[0].href || $t.data("target") || $t.parents('.modal') || [];

		$(target)
				.find("input,textarea,select")
				.val('')
				.end()
				.find("input[type=checkbox], input[type=radio]")
				.prop("checked", "")
				.end();
	})

</script>
