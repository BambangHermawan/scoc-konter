<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_retur extends CI_Model
{
	var $column_order = array('user.name', 'transaction_summary.transaction_code', 'transaction_summary.total_item', 'transaction_summary.sub_total', 'transaction_summary.discount_total', 'transaction_summary.payment', 'transaction_summary.change_payment', 'transaction_summary.created_date');
	var $column_search = array('user.name', 'transaction_summary.transaction_code', 'transaction_summary.total_item', 'transaction_summary.sub_total', 'transaction_summary.discount_total', 'transaction_summary.payment', 'transaction_summary.change_payment', 'transaction_summary.created_date');
	var $order = array('transaction_summary.created_date' => 'desc');

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_datatables($outlet_id, $date)
	{
		$this->_get_datatables_query($outlet_id, $date);

		if ($_REQUEST['length'] != -1)
			$this->db->limit($_REQUEST['length'], $_REQUEST['start']);
		$query = $this->db->get();

		return $query->result();
	}

	private function _get_datatables_query($outlet_id, $date)
	{
		if ($outlet_id != 0) {
			$this->db->select('user.id as user_id, user.name as user_name, outlet.id as outlet_id, outlet.name as outlet_name, transaction.transaction_code, transaction.category as category, transaction.product as product, transaction.many as total_item, transaction_retur.retur_item_outlet_id as retur_item_outlet_id, transaction_retur.retur_br, transaction_retur.retur_gbb, transaction_retur.retur_bu, transaction_retur.retur_c, transaction_retur.retur_u, transaction_retur.note as retur_note, transaction.note as transaction_note, transaction.discount as discount_total, transaction.cost as sub_total, DATE_FORMAT(transaction.created_date, "%d-%m-%Y - %h:%i") as created_date')
				->from('user, outlet, transaction, transaction_retur')
				->where('transaction.user_id = user.id')
				->where('transaction.outlet_id = outlet.id')
				->where('transaction.id = transaction_retur.transaction_id')
				->where('user.deleted_date is null')
				->where('outlet.deleted_date is null')
				->where('transaction.deleted_date is null')
				->where('transaction.outlet_id', $outlet_id)
				->where('transaction.status =', 'beli')
				->where('DATE(transaction.created_date) BETWEEN "' . $date['start'] . '" and "' . $date['end'] . '"');
		}

		if ($outlet_id == 0) {
			$this->db->select('user.id as user_id, user.name as user_name, outlet.id as outlet_id, outlet.name as outlet_name, transaction.transaction_code, transaction.category as category, transaction.product as product, transaction.many as total_item, transaction_retur.retur_item_outlet_id as retur_item_outlet_id, transaction_retur.retur_br, transaction_retur.retur_gbb, transaction_retur.retur_bu, transaction_retur.retur_c, transaction_retur.retur_u, transaction_retur.note as retur_note, transaction.note as transaction_note, transaction.discount as discount_total, transaction.cost as sub_total, DATE_FORMAT(transaction.created_date, "%d-%m-%Y - %h:%i") as created_date')
				->from('user, outlet, transaction, transaction_retur')
				->where('transaction.user_id = user.id')
				->where('transaction.outlet_id = outlet.id')
				->where('transaction.id = transaction_retur.transaction_id')
				->where('user.deleted_date is null')
				->where('outlet.deleted_date is null')
				->where('transaction.deleted_date is null')
				->where('transaction.status =', 'beli')
				->where('DATE(transaction.created_date) BETWEEN "' . $date['start'] . '" and "' . $date['end'] . '"');
		}

		$i = 0;
		foreach ($this->column_search as $item) {
			if ($_REQUEST['search']['value']) {

				if ($i === 0) {
					$this->db->group_start();
					$this->db->like($item, $_REQUEST['search']['value']);
				} else {
					$this->db->or_like($item, $_REQUEST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if (isset($_REQUEST['order'])) {
			$this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function count_filtered($outlet_id, $date)
	{
		$this->_get_datatables_query($outlet_id, $date);
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_all($outlet_id, $date)
	{
		if ($outlet_id != 0) {
			$this->db->select('user.id as user_id, user.name as user_name, outlet.id as outlet_id, outlet.name as outlet_name, transaction.transaction_code, transaction.category as category, transaction.product as product, transaction.many as total_item, transaction_retur.retur_item_outlet_id as retur_item_outlet_id, transaction_retur.retur_br, transaction_retur.retur_gbb, transaction_retur.retur_bu, transaction_retur.retur_c, transaction_retur.retur_u, transaction_retur.note as retur_note, transaction.note as transaction_note, transaction.discount as discount_total, transaction.cost as sub_total, DATE_FORMAT(transaction.created_date, "%d-%m-%Y - %h:%i") as created_date')
				->from('user, outlet, transaction, transaction_retur')
				->where('transaction.user_id = user.id')
				->where('transaction.outlet_id = outlet.id')
				->where('transaction.id = transaction_retur.transaction_id')
				->where('user.deleted_date is null')
				->where('outlet.deleted_date is null')
				->where('transaction.deleted_date is null')
				->where('transaction.outlet_id', $outlet_id)
				->where('transaction.status =', 'beli')
				->where('DATE(transaction.created_date) BETWEEN "' . $date['start'] . '" and "' . $date['end'] . '"');
		}

		if ($outlet_id == 0) {
			$this->db->select('user.id as user_id, user.name as user_name, outlet.id as outlet_id, outlet.name as outlet_name, transaction.transaction_code, transaction.category as category, transaction.product as product, transaction.many as total_item, transaction_retur.retur_item_outlet_id as retur_item_outlet_id, transaction_retur.retur_br, transaction_retur.retur_gbb, transaction_retur.retur_bu, transaction_retur.retur_c, transaction_retur.retur_u, transaction_retur.note as retur_note, transaction.note as transaction_note, transaction.discount as discount_total, transaction.cost as sub_total, DATE_FORMAT(transaction.created_date, "%d-%m-%Y - %h:%i") as created_date')
				->from('user, outlet, transaction, transaction_retur')
				->where('transaction.user_id = user.id')
				->where('transaction.outlet_id = outlet.id')
				->where('transaction.id = transaction_retur.transaction_id')
				->where('user.deleted_date is null')
				->where('outlet.deleted_date is null')
				->where('transaction.deleted_date is null')
				->where('transaction.status =', 'beli')
				->where('DATE(transaction.created_date) BETWEEN "' . $date['start'] . '" and "' . $date['end'] . '"');
		}

		return $this->db->count_all_results();
	}


}
