<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Karyawan extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('kasir/M_karyawan', 'M_karyawan', true);

		if (!isset($_SESSION)) {
			redirect('auth/login');
		} elseif ($_SESSION['status_user'] != 'KASIR') {
			redirect('auth/login');
		}
	}

	function get_karyawan($outlet_id)
	{
		$result = $this->M_karyawan->get_all_karyawan($outlet_id);

		if (count($result) > 0) {
			$response = [
				'success' => 'true',
				'data' => $result,
				'count' => count($result)
			];

			return to_json($response);
		}

		$response = [
			'success' => 'false',
			'data' => [],
			'count' => 0
		];

		return to_json($response);
	}
}
