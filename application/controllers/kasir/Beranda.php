<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Beranda extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('kasir/M_laporan', 'M_laporan', true);

		if (!isset($_SESSION)) {
			redirect('auth/login');
		} elseif ($_SESSION['status_user'] != 'KASIR') {
			redirect('auth/login');
		}
	}

	function index()
	{
		$data['title'] = 'Beranda';
		$data['site_navigation'] = 'Beranda';

		$data['start_date'] = date('Y-m-d');
		$data['end_date'] = date('Y-m-d');

		//transaksi
		$data['transaction'] = $this->M_laporan->get_transaction_total_by_date($_SESSION['id'], $_SESSION['outlet_id'], $data['start_date'], $data['end_date'])->count;
		$data['transaction_unit'] = $this->M_laporan->get_transaction_unit_by_duration($_SESSION['id'], $_SESSION['outlet_id'], $data['start_date'], $data['end_date'])->count;
		$data['transaction_earning'] = $this->M_laporan->get_transaction_earning_by_date($_SESSION['id'], $_SESSION['outlet_id'], $data['start_date']);

		$this->load->view('kasir/beranda/index', $data);
	}
}
