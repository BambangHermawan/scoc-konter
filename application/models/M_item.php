<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class M_kategori
 */
class M_item extends CI_Model
{
	var $column_order = array('id', 'name');
	var $column_search = array('id', 'name');
	var $order = array('name' => 'asc');

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		$this->db->select('id, name')
			->from('item_type')
			->where('deleted_date is null');

		$i = 0;
		foreach ($this->column_search as $item) {
			if ($_REQUEST['search']['value']) {

				if ($i === 0) {
					$this->db->group_start();
					$this->db->like($item, $_REQUEST['search']['value']);
				} else {
					$this->db->or_like($item, $_REQUEST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if (isset($_REQUEST['order'])) {
			$this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();

		if ($_REQUEST['length'] != -1)
			$this->db->limit($_REQUEST['length'], $_REQUEST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_all()
	{
		$this->db->select('id, name')
			->from('item_type')
			->where('deleted_date is null');

		return $this->db->count_all_results();
	}

	function get_item()
	{
		return $this->db->query("select id, name from item_type where item_type.deleted_date is null order by id desc")->result();
	}

	function add_item($name)
	{
		$created_by = created_by();
		$created_date = created_date();

		return $this->db->query("insert into item_type (name, created_by, created_date) values('$name','$created_by','$created_date')");
	}

	function edit_item($id, $name)
	{
		$modified_by = modified_by();
		$modified_date = modified_date();

		return $this->db->query("update item_type set name='$name', modified_by='$modified_by', modified_date='$modified_date' where id='$id' and deleted_date is null");
	}

	function delete_item($id)
	{
		$deleted_by = deleted_by();
		$deleted_date = deleted_date();

		return $this->db->query("update item_type set deleted_by='$deleted_by', deleted_date='$deleted_date' where id='$id' and deleted_date is null");
	}
}
