<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaksi extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_transaksi', '', true);
		$this->load->model('M_transaksi_cart', '', true);
		$this->load->model('M_outlet', '', true);

		if (!isset($_SESSION)) {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'ADMIN') {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'OWNER') {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'KASIR') {
			redirect('auth/login');
		}
	}

	function index($outlet_id)
	{
		$data['title'] = 'Riwayat Transaksi';
		$data['site_navigation'] = 'Detail';

		$data['outlet'] = $this->M_outlet->get_outlet_by_id($outlet_id);

		$this->load->view('transaksi/riwayat', $data);
	}

	function get_all_transaction($user_id, $outlet_id, $start_date, $end_date)
	{
		$filter = $this->generate_date_filter($start_date, $end_date);

		$result = $this->M_transaksi->get_datatables($user_id, $outlet_id, $filter);

		$response = [
			'success' => true,
			'draw' => $_REQUEST['draw'],
			'recordsTotal' => $this->M_transaksi->count_all($user_id, $outlet_id, $filter),
			'recordsFiltered' => $this->M_transaksi->count_filtered($user_id, $outlet_id, $filter),
			'data' => $result
		];

		return to_json($response);
	}

	function get_transaction_detail($transaction_code)
	{
		$result = $this->M_transaksi->get_transaction_detail($transaction_code);

		if ($result) {

			$response = [
				'success' => true,
				'data' => $result,
				'total' => count($result)
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function print_transaction_detail($transaction_code)
	{
		$this->load->library('Pdf');
		$pdf = new FPDF('P', 'mm', 'A5');

		$result = $this->M_transaksi->get_transaction_detail($transaction_code);
		$summary = $this->M_transaksi->get_transaction_summary($transaction_code);

		$pdf->AddPage();
		$pdf->SetFont('Arial', '', 10);

		$pdf->Cell(25, 5, 'Nota', 0, 0, 'L');
		$pdf->Cell(85, 5, $transaction_code, 0, 0, 'L');
		$pdf->Ln();
		$pdf->Cell(25, 5, 'Tanggal', 0, 0, 'L');
		$pdf->Cell(85, 5, $summary[0]->created_date, 0, 0, 'L');
		$pdf->Ln();
		$pdf->Cell(25, 5, 'Kasir', 0, 0, 'L');
		$pdf->Cell(85, 5, $summary[0]->user_name, 0, 0, 'L');
		$pdf->Ln();
		$pdf->Cell(25, 5, 'Outlet', 0, 0, 'L');
		$pdf->Cell(85, 5, $summary[0]->outlet_name, 0, 0, 'L');
		$pdf->Ln();

		$pdf->Cell(130, 5, '-----------------------------------------------------------------------------------------------------------', 0, 0, 'L');
		$pdf->Ln();

		$pdf->Cell(50, 5, 'Item', 0, 0, 'L');
		$pdf->Cell(25, 5, 'Harga', 0, 0, 'L');
		$pdf->Cell(15, 5, 'Qty', 0, 0, 'L');
		$pdf->Cell(15, 5, 'Disc', 0, 0, 'L');
		$pdf->Cell(25, 5, 'Sub total', 0, 0, 'L');
		$pdf->Ln();

		$pdf->Cell(130, 5, '-----------------------------------------------------------------------------------------------------------', 0, 0, 'L');
		$pdf->Ln();

		$no = 0;
		foreach ($result as $item) {
			$pdf->Cell(50, 5, $item->product, 0, 0, 'L');
			$pdf->Cell(25, 5, $item->cost, 0, 0, 'L');
			$pdf->Cell(15, 5, $item->many);
			$pdf->Cell(15, 5, $item->discount . '%');
			$pdf->Cell(25, 5, $item->sub_total, 0, 0, 'L');
			$pdf->Ln();
			$no++;
		}

		$pdf->Cell(130, 5, '-----------------------------------------------------------------------------------------------------------', 0, 0, 'L');
		$pdf->Ln();

		$pdf->Cell(105, 5, 'Total Bayar', 0, 0, 'R');
		$pdf->Cell(25, 5, $summary[0]->sub_total);
		$pdf->Ln();

		$pdf->Cell(105, 5, 'Cash', 0, 0, 'R');
		$pdf->Cell(25, 5, $summary[0]->payment);
		$pdf->Ln();

		$pdf->Cell(105, 5, 'Kembali', 0, 0, 'R');
		$pdf->Cell(25, 5, $summary[0]->change_payment);
		$pdf->Ln();
		$pdf->Cell(130, 5, '-----------------------------------------------------------------------------------------------------------', 0, 0, 'L');
		$pdf->Ln();
		$pdf->Ln();
		$pdf->Cell(130, 5, "Terimakasih telah berbelanja dengan kami", 0, 0, 'C');
		$pdf->Ln();

		$pdf->Output();
	}

	function generate_date_filter($start_date, $end_date)
	{
		if (empty($start_date) && empty($end_date)) {
			$filter = [
				'start' => date('Y-m-d', strtotime("-1 days")),
				'end' => date('Y-m-d')
			];
		} else if (date('Y-m-d', strtotime($start_date)) == date('Y-m-d', strtotime($end_date))) {
			$filter = [
				'start' => date('Y-m-d', strtotime('-1 day', strtotime($start_date))),
				'end' => date('Y-m-d', strtotime($end_date))
			];
		} else {
			$filter = [
				'start' => date('Y-m-d', strtotime($start_date)),
				'end' => date('Y-m-d', strtotime($end_date))
			];
		}

		return $filter;
	}
}
