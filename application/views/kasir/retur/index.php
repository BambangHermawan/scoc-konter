<?php $this->load->view('_layouts/_template/header') ?>

<style>
	#scanner_qrcode {
		max-width: 100%;
		height: auto;
	}
</style>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-3">
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Filter Transaksi <b><?= $outlet[0]->name ?></b></h3>
				</div>
				<div class="card-body p-2">
					<div class="form-group">
						<div class="col-lg-4"><label for="date_begin">Mulai</label></div>
						<div class="col-lg-12">
							<input type="date" class="form-control form-control-sm"
								   name="date_begin"
								   id="date_begin" required>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-4"><label for="date_end">Sampai</label></div>
						<div class="col-lg-12">
							<input type="date" class="form-control form-control-sm"
								   name="date_end"
								   id="date_end" required>
						</div>
					</div>
				</div>
				<div class="card-footer">
					<div class="float-right">
						<button type="submit" class="btn btn-primary btn-sm" id="btn_search_all">Filter
							Data
						</button>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-9">
			<div class="card card-primary card-outline">
				<div class="card-header">
					<h3 class="card-title">Riwayat Transaksi Retur</b></h3>
					<div class="float-right">
						<button class="btn btn-primary btn-sm" id="btn_add_retur"><i
								class="fa fa-plus"></i> Buat Retur
						</button>
					</div>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table id="table_transaksi_retur_log"
							   class="table table-striped table-hover dt-responsive display nowrap"
							   style="width:100%">
							<thead>
							<tr>
								<th rowspan="2" style="vertical-align:middle" data-orderable="false">No</th>
								<th rowspan="2" style="vertical-align:middle">Nomor Transaksi</th>
								<th rowspan="2" style="vertical-align:middle">Produk</th>
								<th rowspan="2" style="vertical-align:middle">Banyak</th>
								<th colspan="5" style="text-align:center">Status Retur</th>
								<th rowspan="2" style="vertical-align:middle">Tanggal</th>
								<th rowspan="2" style="vertical-align:middle" data-orderable="false">Aksi
								</th>
							</tr>
							<tr>
								<th title="Barang utuh">BU</th>
								<th title="Barang rusak">BR</th>
								<th title="Ganti barang baru">GBB</th>
								<th title="Retur Catatan">C</th>
								<th title="Retur Uang">U</th>
							</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--modal tambah retur-->
<div class="modal fade" id="modal_add_retur">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><h5>Buat Retur</h5></div>
			<div class="modal-body">
				<form id="laporan_pendapatan_form">
					<div class="form-group">
						<div class="col-lg-12">Kode Transaksi (Tertera di struk pembelian)</div>
						<div class="col-lg-12"><input type="text" class="form-control form-control-sm"
													  id="input_laporan_pendapatan_total"
													  name="input_laporan_pendapatan_total" readonly></div>
					</div>

					<div class="form-group">
						<div class="col-lg-6">Barang</div>
						<div class="col-lg-12"><input type="text" class="form-control form-control-sm"
													  id="input_laporan_pendapatan_current_total"
													  name="input_laporan_pendapatan_current_total"
													  onkeypress="return check_int(event)"></div>
					</div>

					<div class="form-group">
						<div class="col-lg-6">Selisih</div>
						<div class="col-lg-12"><input type="text" class="form-control form-control-sm"
													  id="input_laporan_pendapatan_selisih"
													  name="input_laporan_pendapatan_selisih" readonly></div>
					</div>

					<div class="form-group">
						<div class="col-lg-6">Catatan</div>
						<div class="col-lg-12">
							<textarea class="form-control form-control-sm" id="input_laporan_pendapatan_catatan"
									  name="input_laporan_pendapatan_catatan"></textarea>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"
						id="btn_laporan_pendapatan_submit">Simpan
				</button>
			</div>
		</div>
	</div>
</div>

<!--Modal detail transaksi retur-->
<div class="modal fade" id="modal_detail_retur" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	 aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Detail Transaksi retur</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="table_transaksi_detail"
						   class="table table-striped table-hover dt-responsive display nowrap"
						   style="width:100%">
						<thead>
						<tr>
							<th>Tipe</th>
							<th>Barang</th>
							<th>Harga</th>
							<th>Jumlah Beli</th>
							<th>Diskon (%)</th>
							<th>Sub Total</th>
						</tr>
						</thead>
						<tbody>
						</tbody>
						<input id="detail_transaction_code" hidden>
						<tr>
							<td colspan="6" style='text-align:right'>Total Diskon</td>
							<td id="detail_diskon_total"></td>
						</tr>
						<tr>
							<td colspan="6" style='text-align:right'><b>Grand Total</b></td>
							<td id="detail_grand_total"></td>
						</tr>
						<tr>
							<td colspan="6" style='text-align:right'>Bayar</td>
							<td id="detail_payment"></td>
						</tr>
						<tr>
							<td colspan="6" style='text-align:right'>Kembali</td>
							<td id="detail_change_payment"></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning btn-sm" id="btn_print_transaction_detail"
						data-dismiss="modal"><i class="fa fa-print"></i> Cetak
				</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Selesai</button>
			</div>
		</div>
	</div>
</div>

<script>
	//init transaksi retur
	$(document).ready(function () {
		var today = new Date();
		date = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2)

		$("#date_begin").val(date)
		$("#date_end").val(date)

		init_table_transaksi_retur_log()
	})

	//filter transaksi retur
	$('#btn_search_all').click(function () {

		button = "&nbsp;<button class='btn btn-info btn-sm' id='btn_detail_transaction_retur_click'>\
            <i class='fas fa-search'></i>&nbsp;Detail</button>"

		table_transaksi_retur_log = $("#table_transaksi_retur_log").DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			autoWidth: true,
			ajax: "<?= base_url(); ?>kasir/retur/get_all_transaction_retur/" + "<?= $this->uri->segment(4); ?>" + "/" + $('#date_begin').val() + "/" + $('#date_end').val(),
			columns: [
				{
					data: "id",
					width: "0.8%",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: 'transaction_code'
				},
				{
					data: 'product'
				},
				{
					data: 'total_item'
				},
				{
					data: 'retur_br'
				},
				{
					data: 'retur_gbb'
				},
				{
					data: 'retur_bu'
				},
				{
					data: 'retur_c'
				},
				{
					data: 'retur_u'
				},
				{
					data: 'created_date'
				},
				{
					defaultContent: button
				}
			]
		})
	})

	//modal tambah retur
	$('#btn_add_retur').click(function () {
		$('#modal_add_retur').modal({backdrop: 'static', keyboard: false})
	})

	//button modal transfer
	$("#table_transaksi_retur_log").on('click', 'tr #btn_detail_transaction_retur_click', function () {
		$('#modal_detail_retur').modal({backdrop: 'static', keyboard: false})
	})

	//init datatables table retur
	function init_table_transaksi_retur_log() {

		button = "&nbsp;<button class='btn btn-info btn-sm' id='btn_detail_transaction_retur_click'>\
            <i class='fas fa-search'></i>&nbsp;Detail</button>"

		table_transaksi_retur_log = $("#table_transaksi_retur_log").DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			autoWidth: true,
			ajax: "<?= base_url(); ?>kasir/retur/get_all_transaction_retur/" + "<?= $this->uri->segment(4); ?>" + "/" + $('#date_begin').val() + "/" + $('#date_end').val(),
			columns: [
				{
					data: "id",
					width: "0.8%",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: 'transaction_code'
				},
				{
					data: 'product'
				},
				{
					data: 'total_item'
				},
				{
					data: 'retur_br'
				},
				{
					data: 'retur_gbb'
				},
				{
					data: 'retur_bu'
				},
				{
					data: 'retur_c'
				},
				{
					data: 'retur_u'
				},
				{
					data: 'created_date'
				},
				{
					defaultContent: button
				}
			]
		})
	}

	//konversi angka ke format rupiah
	function to_rupiah(angka) {
		var rev = parseInt(angka, 10).toString().split('').reverse().join('');

		var rev2 = '';
		for (var i = 0; i < rev.length; i++) {
			rev2 += rev[i];
			if ((i + 1) % 3 === 0 && i !== (rev.length - 1)) {
				rev2 += ',';
			}
		}

		return 'Rp. ' + rev2.split('').reverse().join('');
	}

</script>

<?php $this->load->view('_layouts/_template/js') ?>
