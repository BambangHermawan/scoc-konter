<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_wewenang extends CI_Model
{
	var $column_order = array('id', 'wewenang');
	var $column_search = array('id', 'wewenang');
	var $order = array('wewenang' => 'asc');

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		$this->db->select('id, wewenang')
			->from('user_roles')
			->where('deleted_date is null')
			->order_by('wewenang', 'ASC');

		$i = 0;
		foreach ($this->column_search as $item) {
			if ($_REQUEST['search']['value']) {

				if ($i === 0) {
					$this->db->group_start();
					$this->db->like($item, $_REQUEST['search']['value']);
				} else {
					$this->db->or_like($item, $_REQUEST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if (isset($_REQUEST['order'])) {
			$this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();

		if ($_REQUEST['length'] != -1)
			$this->db->limit($_REQUEST['length'], $_REQUEST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_all()
	{
		$this->db->select('id, wewenang')
			->from('user_roles')
			->where('deleted_date is null')
			->order_by('wewenang', 'ASC');

		return $this->db->count_all_results();
	}

	function get_wewenang()
	{
		return $this->db->query("select id, wewenang from user_roles where deleted_date is null")->result();
	}

	function add_wewenang($wewenang)
	{
		$created_by = created_by();
		$created_date = created_date();

		$check = $this->db->query("select id from user_roles where wewenang='$wewenang' and deleted_date is null")->result();
		if ($check) return false;
		else {
			return $this->db->query("insert into user_roles (wewenang, created_by, created_date)
            values('$wewenang','$created_by','$created_date')");
		}
	}

	function edit_wewenang($id, $wewenang)
	{
		$modified_by = modified_by();
		$modified_date = modified_date();

		$check = $this->db->query("select id from user_roles where wewenang='$wewenang' and deleted_date is null")->result();
		if ($check) return false;
		else {
			return $this->db->query("update user_roles set wewenang='$wewenang', modified_by='$modified_by', modified_date='$modified_date' where id='$id' and deleted_date is null");
		}
	}

	function delete_wewenang($id)
	{
		$deleted_by = deleted_by();
		$deleted_date = deleted_date();

		return $this->db->query("update user_roles set deleted_by='$deleted_by', deleted_date='$deleted_date' where id='$id' and deleted_date is null");
	}
}
