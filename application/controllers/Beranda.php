<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Beranda extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_laporan', '', true);
		$this->load->model('M_outlet', '', true);

		if (!isset($_SESSION)) {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'ADMIN') {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'OWNER') {
			redirect('auth/login');
		} elseif ($_SESSION['status_user'] == 'KASIR') {
			redirect('auth/login');
		}
	}

	function index()
	{
		$data['title'] = 'Beranda';
		$data['site_navigation'] = 'Beranda';

		$data['start_date'] = date('Y-m-01');
		$data['end_date'] = date('Y-m-d');

		$outlet_id = 0;
		$user_id = 0;

		//transaksi
		$data['transaction'] = $this->M_laporan->get_transaction_by_duration($user_id, $outlet_id, $data['start_date'], $data['end_date'])->count;
		$transaction_unit = $this->M_laporan->get_transaction_unit_by_duration($user_id, $outlet_id, $data['start_date'], $data['end_date'])->count;
		$laba_rugi = $this->M_laporan->get_laba_rugi_by_duration($user_id, $outlet_id, $data['start_date'], $data['end_date']);
		$data['laba_rugi'] = ($laba_rugi->rugi > 0) ? $laba_rugi->rugi : $laba_rugi->untung;
		$data['transaction_unit'] = ($transaction_unit != null) ? $transaction_unit : 0;

		$this->load->view('beranda/index', $data);
	}

	function rekapan_chart($user_id, $outlet_id, $start_date, $end_date)
	{
		if (empty($start_date) && empty($end_date)) {
			$date = [
				'start_date' => date('Y-m-d'),
				'end_date' => date('Y-m-d')
			];
		} else if (date('Y-m-d', strtotime($start_date)) == date('Y-m-d', strtotime($end_date))) {
			$date = [
				'start_date' => date('Y-m-d', strtotime($start_date)) . '-01',
				'end_date' => date('Y-m-d', strtotime($end_date))
			];
		} else {
			$date = [
				'start_date' => date('Y-m-d', strtotime($start_date)),
				'end_date' => date('Y-m-d', strtotime($end_date))
			];
		}

		//default chart
		$data['transaction'] = $this->M_laporan->get_transaction_by_duration($user_id, $outlet_id, $date['start_date'], $date['end_date'])->count;
		$data['transaction_unit'] = $this->M_laporan->get_transaction_unit_by_duration($user_id, $outlet_id, $date['start_date'], $date['end_date'])->count;
		$laba_rugi = $this->M_laporan->get_laba_rugi_by_duration($user_id, $outlet_id, $date['start_date'], $date['end_date']);
		$data['laba_rugi'] = ($laba_rugi->rugi > 0) ? $laba_rugi->rugi : $laba_rugi->untung;
		$data['transaction_by_duration'] = $this->M_laporan->get_transaction_by_duration($user_id, $outlet_id, $date['start_date'], $date['end_date'])->count;
		$data['transaction_unit_by_duration'] = $this->M_laporan->get_transaction_unit_by_duration($user_id, $outlet_id, $date['start_date'], $date['end_date'])->count;
		$laba_rugi_by_duration = $this->M_laporan->get_laba_rugi_by_duration($user_id, $outlet_id, $date['start_date'], $date['end_date']);
		$data['laba_rugi_by_duration'] = ($laba_rugi_by_duration->rugi > 0) ? $laba_rugi_by_duration->rugi : $laba_rugi_by_duration->untung;

		if (count($data) > 0) {

			$response = [
				'success' => true,
				'data' => $data,
				'total' => count($data)
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}
}
