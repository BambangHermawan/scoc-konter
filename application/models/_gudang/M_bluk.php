<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_bluk extends CI_Model
{
	var $column_order = array('item_outlet.id', 'item_outlet.name', 'item_outlet.modal', 'item_outlet.cost', 'item_outlet.discount', 'item_outlet.many', 'item_outlet.unit', 'item_outlet.qrcode', 'item_outlet_category.name');
	var $column_search = array('item_outlet.id', 'item_outlet.name', 'item_outlet.modal', 'item_outlet.cost', 'item_outlet.discount', 'item_outlet.many', 'item_outlet.unit', 'item_outlet.qrcode', 'item_outlet_category.name');
	var $order = array('item_outlet.name' => 'asc');

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query($outlet_id)
	{
		if (isset($outlet_id)) {
			$this->db->select('item_outlet.id, item_outlet.name, format(item_outlet.modal,0) as modal, format(item_outlet.cost,0) as cost, format(item_outlet.discount,0) as discount, item_outlet.many, item_outlet.unit, item_outlet.qrcode, item_outlet_category.name as category_name')
				->from('item_outlet, item_outlet_category')
				->where('item_outlet.deleted_date is null')
				->where('item_outlet_category.deleted_date is null')
				->where('item_outlet.item_outlet_category_id = item_outlet_category.id')
				->where('item_outlet_category.outlet_id', $outlet_id)
				->where('item_outlet.unit !=', 'unit');

		} else {
			return false;
		}

		$i = 0;
		foreach ($this->column_search as $item) {
			if ($_REQUEST['search']['value']) {

				if ($i === 0) {
					$this->db->group_start();
					$this->db->like($item, $_REQUEST['search']['value']);
				} else {
					$this->db->or_like($item, $_REQUEST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if (isset($_REQUEST['order'])) {
			$this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($outlet_id)
	{
		$this->_get_datatables_query($outlet_id);

		if ($_REQUEST['length'] != -1)
			$this->db->limit($_REQUEST['length'], $_REQUEST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($outlet_id)
	{
		$this->_get_datatables_query($outlet_id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_all($outlet_id)
	{
		if (isset($outlet_id)) {
			$this->db->select('item_outlet.id, item_outlet.name, format(item_outlet.modal,0) as modal, format(item_outlet.cost,0) as cost, format(item_outlet.discount,0) as discount, item_outlet.many, item_outlet.unit, item_outlet.qrcode, item_outlet_category.name as category_name')
				->from('item_outlet, item_outlet_category')
				->where('item_outlet.deleted_date is null')
				->where('item_outlet_category.deleted_date is null')
				->where('item_outlet.item_outlet_category_id = item_outlet_category.id')
				->where('item_outlet_category.outlet_id', $outlet_id)
				->where('item_outlet.unit !=', 'unit');

		} else {
			return false;
		}

		return $this->db->count_all_results();
	}

	function set_cost_bluk($product_id, $cost)
	{
		$modified_by = modified_by();
		$modified_date = modified_date();

		$this->db->query("update item_outlet set cost='$cost', modified_by='$modified_by', modified_date='$modified_date' where id='$product_id' and deleted_date is null");

		$CI =& get_instance();
		$CI->load->model('M_log_produk');
		$detail = $CI->M_log_produk->get_detail_product_pulsa_bluk_outlet($product_id);
		$outlet = $CI->M_log_produk->get_outlet($detail->item_outlet_id);

		$activity = "Outlet $outlet->name mengatur harga \"$detail->type_name $detail->category_name $detail->product_name sebesar $cost\"";
		$CI->M_log_produk->insert_log_product($_SESSION['id'], $outlet->id, 'item_outlet', $product_id, NULL, $cost, NULL, NULL, NULL, NULL, $activity, '-', 'cost_product_pulsa_bluk_outlet');

		$this->db->trans_complete();

		return true;
	}

	function set_discount_bluk($product_id, $discount)
	{
		$modified_by = modified_by();
		$modified_date = modified_date();

		$this->db->query("update item_outlet set discount='$discount', modified_by='$modified_by', modified_date='$modified_date' where id='$product_id' and deleted_date is null");

		$CI =& get_instance();
		$CI->load->model('M_log_produk');
		$detail = $CI->M_log_produk->get_detail_product_pulsa_bluk_outlet($product_id);
		$outlet = $CI->M_log_produk->get_outlet($detail->item_outlet_id);

		$activity = "Outlet $outlet->name mengatur diskon \"$detail->type_name $detail->category_name $detail->product_name sebesar $discount\"";
		$CI->M_log_produk->insert_log_product($_SESSION['id'], $outlet->id, 'item_outlet', $product_id, NULL, NULL, NULL, NULL, NULL, $discount, $activity, '-', 'discount_product_pulsa_bluk_outlet');

		$this->db->trans_complete();

		return true;
	}

	function delete_product_bluk($product_id)
	{
		$deleted_by = deleted_by();
		$deleted_date = deleted_date();

		return $this->db->query("update item_outlet set deleted_by='$deleted_by', deleted_date='$deleted_date' where id='$product_id' and deleted_date is null");
	}

	function add_modal_bluk($product_id, $modal)
	{
		$created_by = created_by();
		$created_date = created_date();

		$this->db->trans_begin();

		$item = $this->db->query("
		select item_outlet_category.name as category_name, item_outlet_category.outlet_id as outlet_id, item_outlet_category.modal as modal, outlet.name as outlet_name 
        from outlet, item_outlet_category
        where item_outlet_category.outlet_id=outlet.id and item_outlet_category.id='$product_id' 
        and outlet.deleted_date is null  
        and item_outlet_category.deleted_date is null")->result()[0];

		$akhir = $item->modal + $modal;

		$this->db->query("
		insert into transaction (user_id, outlet_id, item_outlet_id, type, category, product, awl, plus, ak, status, unit, from_outlet, created_by, created_date)
        values('{$_SESSION['id']}','$item->outlet_id','$product_id','Pulsa','$item->category_name','Rp','$item->modal','$modal','$akhir','tambah_saldo','Bluk','$item->outlet_id','$created_by', '$created_date')");

		$transaction_id = $this->db->insert_id();

		$this->db->query("update item_outlet_category set modal=modal+'$modal' where id='$product_id' and deleted_date is null");

		$this->db->trans_complete();

		return $transaction_id;
	}

	function edit_modal_bluk($product_id, $modal)
	{
		$created_by = created_by();
		$created_date = created_date();

		$this->db->trans_begin();

		$item = $this->db->query("
		select item_outlet_category.name as category_name, item_outlet_category.outlet_id as outlet_id, item_outlet_category.modal as modal, outlet.name as outlet_name
        from outlet, item_outlet_category
        where item_outlet_category.outlet_id=outlet.id 
        and item_outlet_category.id='$product_id'
        and outlet.deleted_date is null  
        and item_outlet_category.deleted_date is null")->result()[0];

		$akhir = $item->modal - $modal;

		if ($akhir < 0) {
			return false;
		} else {
			$this->db->query("
			insert into transaction (user_id, outlet_id, item_outlet_id, type, category, product, awl, plus, ak, status, unit, from_outlet, created_by, created_date)
        	values('{$_SESSION['id']}','$item->outlet_id','$product_id','Pulsa','$item->category_name','Rp','$item->modal','$modal','$akhir','tambah_saldo','Bluk', '$item->outlet_id','$created_by','$created_date')");
		}

		$transaction_id = $this->db->insert_id();

		$this->db->query("update item_outlet_category set modal='$akhir' where id='$product_id' and deleted_date is null");

		$this->db->trans_complete();

		return $transaction_id;
	}

	function add_product_bluk($category_id, $name, $modal, $cost, $unit, $product_image)
	{
		$created_by = created_by();
		$created_date = created_date();

		return $this->db->query("
		insert into item_outlet (item_outlet_category_id, name, modal, cost, unit, qrcode, created_by, created_date) 
		values('$category_id','$name','$modal','$cost','$unit','$product_image','$created_by','$created_date')");
	}
}
