<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Karyawan extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_karyawan', '', true);

		if (!isset($_SESSION)) {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'ADMIN') {
			redirect('auth/login');
		} elseif (!$_SESSION['status_user'] == 'OWNER') {
			redirect('auth/login');
		} elseif ($_SESSION['status_user'] == 'KASIR') {
			redirect('auth/login');
		}
	}

	function index()
	{
		$data['title'] = 'Karyawan Outlet';
		$data['site_navigation'] = 'Karyawan Outlet';

		return $this->load->view('karyawan/index', $data);
	}

	function get_all_karyawan()
	{
		$result = $this->M_karyawan->get_datatables();

		$response = [
			'success' => true,
			'draw' => $_REQUEST['draw'],
			'recordsTotal' => $this->M_karyawan->count_all(),
			'recordsFiltered' => $this->M_karyawan->count_filtered(),
			'data' => $result
		];

		return to_json($response);
	}

	function get_karyawan($outlet_id)
	{
		$result = $this->M_karyawan->get_all_karyawan($outlet_id);

		if (count($result) > 0) {
			$response = [
				'success' => 'true',
				'data' => $result,
				'count' => count($result)
			];

			return to_json($response);
		}

		$response = [
			'success' => 'false',
			'data' => [],
			'count' => 0
		];

		return to_json($response);
	}

	function add_karyawan()
	{
		$outlet_id = $this->input->post('outlet_id');
		$user_id = $this->input->post('user_id');

		$result = $this->M_karyawan->add_karyawan($outlet_id, $user_id);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Tambah Karyawan outlet berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Tambah Karyawan outlet gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function edit_karyawan()
	{
		$outlet_employee_id = $this->input->post('outlet_employee_id');
		$outlet_id = $this->input->post('outlet_id');
		$user_id = $this->input->post('user_id');

		$result = $this->M_karyawan->edit_karyawan($outlet_employee_id, $outlet_id, $user_id);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Ubah karyawan berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Ubah karyawan gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function reset_password()
	{
		$id = $this->input->post('id');

		$result = $this->M_karyawan->reset_password_karyawan($id);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Reset password karyawan berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Reset password karyawan gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}

	function delete_karyawan()
	{
		$id = $this->input->post('id');

		$result = $this->M_karyawan->delete_karyawan($id);

		if ($result) {

			$response = [
				'success' => true,
				'type' => 'success',
				'message' => 'Hapus pengguna berhasil',
				'data' => $result,
				'total' => $result
			];

			return to_json($response);
		}

		$response = [
			'success' => false,
			'type' => 'warning',
			'message' => 'Hapus pengguna gagal',
			'data' => [],
			'total' => 0
		];

		return to_json($response);
	}
}
