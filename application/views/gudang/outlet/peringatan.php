<div class="table-responsive">
	<table id="table_peringatan" class="table table-striped table-hover dt-responsive display nowrap"
		   style="width:100%">
		<thead>
		<tr>
			<th class="nomor" data-orderable="false">No</th>
			<th>Tipe</th>
			<th>Kategori</th>
			<th>Nama</th>
			<th>Banyak</th>
		</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>

<script>
	$(document).ready(function () {
		init_table_peringatan()
	})

	//init datatables table peringatan
	function init_table_peringatan() {
		table_peringatan = $("#table_peringatan").DataTable({
			destroy: true,
			autoWidth: true,
			ajax: "<?= base_url(); ?>_gudang/outlet/get_stok_peringatan/<?= $this->uri->segment(4); ?>",
			columns: [
				{
					data: "id",
					width: "0.8%",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					data: "type_name"
				},
				{
					data: "category_name"
				},
				{
					data: "product_name"
				},
				{
					data: "many"
				}
			]
		})
	}
</script>
